import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import javax.swing.text.html.MinimalHTMLWriter;


public class Main {
	private static int width = 0;
	private static int height = 0;
	private static int MIN_SIZE = 512;
	/**
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		
		
		if (!(args.length > 1)) {
			System.out.println("Wrong Parameter - need an Image as Parameter and a project name, e.g. Main image.png myproject");
			System.exit(0);
		}
		System.out.println("Project name: "+args[1]);
		String querySize = "file " + args[0];
		
		XmlWriter xmlWriter = new XmlWriter();
		xmlWriter.setDefaultFloor("1");
		xmlWriter.setDefaultPositionX("297");
		xmlWriter.setDefaultPositionY("198");
		xmlWriter.setDescription("A beatiful map");
		xmlWriter.setName(args[1]);
		xmlWriter.setScale("1");
		xmlWriter.setRealScale("4.8");
		
		
		try {
		
			Process process = Runtime.getRuntime().exec(querySize);
			BufferedReader read = new BufferedReader(new InputStreamReader(
					process.getInputStream()));
			String s;
			StringTokenizer st = null;
			while ((s = read.readLine()) != null) {
				st = new StringTokenizer(s, " ,");
			}
			
			while(st.hasMoreTokens()) {
				String value = st.nextToken(); 
				System.out.println(value);
				
				try {
					width = Integer.parseInt(value);
					st.nextToken();
					height = Integer.parseInt(st.nextToken());
					} catch (NumberFormatException ex) {
//					System.out.println("String ist keine Zahl");
					}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			System.out.println(e1);
		}
		System.out.println("Orignal image size: "+width+" "+height);
		int tmpWidth = width;
		int counter = 0;
		while ( tmpWidth > MIN_SIZE ){
			tmpWidth = tmpWidth/2;
			System.out.println(++counter);
		}
		
		
		ImageWorker iw = new ImageWorker();
		try {
			iw.init(args[0], args[1], xmlWriter, counter);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			iw.cropImage(args[0] ,width, height);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		try {
			xmlWriter.write();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		Process p = Runtime.getRuntime().exec("ls");
//		p.waitFor();
		String formatQuery = "xmllint --format "+args[1]+".xml --output ./"+args[1]+"/mapdef.xml";
		
		Runtime.getRuntime().exec(formatQuery).waitFor();

	}

}
