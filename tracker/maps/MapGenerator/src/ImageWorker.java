import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ImageWorker {
	private int MIN_SIZE = 512;
	private int MAX_LEVELS = 10;
	int counter = 1;
	private String[] imageNames;
	private String projectName;
	XmlWriter writer;
	private int zoom;

	void cropImage(String image, int _width, int _height) throws IOException,
			InterruptedException {

		if (_width > MIN_SIZE) {

			String makeDir = "mkdir " + projectName + "/tiles" + counter;
			String dir = projectName + "/tiles" + counter;
			Process p0 = Runtime.getRuntime().exec(makeDir);
			p0.waitFor();

			String cropQuery = " convert "
					+ image
					+ "  -crop 256x256 -set filename:tile %[fx:page.x/256+1]_%[fx:page.y/256+1] +repage +adjoin "
					+ dir + "/tile_%[filename:tile].png";
			// System.out.println(cropQuery);
			Process p = Runtime.getRuntime().exec(cropQuery);
			p.waitFor();
			zipDir(dir);
			String resizeQuery = "convert " + image + " -resize 50% "
					+ imageNames[counter];
			Process p1 = Runtime.getRuntime().exec(resizeQuery);
			p1.waitFor();

				writer.addLayer("" + counter, "tiles" + counter + ".zip",
						"256", "" + _width, "" + _height, "" + zoom);
				
			zoom= zoom/2;
			
			cropImage(imageNames[counter++], _width / 2, _height / 2);

		}

		// System.out.println(_width);
		// System.out.println(_height);
		// System.out.println(" ");
	}

	void init(String name, String project, XmlWriter writer, int maxScale)
			throws InterruptedException, IOException {
		counter = 0;
		zoom = maxScale + 1;
		imageNames = new String[MAX_LEVELS];
		projectName = project;
		this.writer = writer;
		Process p0 = Runtime.getRuntime().exec("mkdir " + projectName);
		p0.waitFor();
		StringTokenizer stringTokenizer = new StringTokenizer(name, ".");
		String rawName = stringTokenizer.nextToken();
		String extentsion = stringTokenizer.nextToken();

		for (int i = 0; i < MAX_LEVELS; i++) {
			imageNames[i] = rawName + i + "." + extentsion;
		}

	}

	void zipDir(String name) {
		try {
			File inFolder = new File(name);
			File outFolder = new File(name + ".zip");
			ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
					new FileOutputStream(outFolder)));
			BufferedInputStream in = null;
			byte[] data = new byte[1000];
			String files[] = inFolder.list();
			for (int i = 0; i < files.length; i++) {

				in = new BufferedInputStream(new FileInputStream(
						inFolder.getPath() + "/" + files[i]), 1000);
				out.putNextEntry(new ZipEntry(files[i]));
				int count;
				while ((count = in.read(data, 0, 1000)) != -1) {
					out.write(data, 0, count);
				}
				out.closeEntry();
			}
			out.flush();
			out.close();

			Process p0 = Runtime.getRuntime().exec("rm -rf " + name);
			p0.waitFor();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
