import java.awt.List;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.events.XMLEvent;

public class XmlWriter {

	String name;
	String defaultFloor;
	String defaultPositionX, defaultPositionY;
	String scale;
	String realScale;
	String description;
	
	ArrayList<ArrayList<String>> iconSet = new ArrayList<ArrayList<String>>();
	ArrayList<ArrayList<String>> tileSet = new ArrayList<ArrayList<String>>();

	void write() throws IOException, InterruptedException {
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		
		String fileName = name+".xml";
		System.out.println(fileName);
	
		try {

			XMLStreamWriter writer = factory
					.createXMLStreamWriter(new FileWriter(fileName));
			writer.writeStartDocument();

			writer.writeStartElement("Map");
			
			writer.writeStartElement("Info");
			writer.writeAttribute("Name", name);
			writer.writeAttribute("RealScale", realScale);
			writer.writeAttribute("DefaultFloor", defaultFloor);
			writer.writeAttribute("Scale", scale);
			writer.writeAttribute("Description", description);
				writer.writeStartElement("DefaultPosition");
				writer.writeAttribute("x", defaultPositionX);
				writer.writeAttribute("y", defaultPositionY);
				writer.writeEndElement();
			writer.writeEndElement();
			
			writer.writeStartElement("Tiles");
			writer.writeAttribute("floor", defaultFloor);
			for (ArrayList<String> tile : tileSet) {
				writer.writeStartElement("Tile");
//				System.out.println(tile);
				
				writer.writeAttribute("layer", tile.get(0));
				writer.writeAttribute("zip", tile.get(1));
				writer.writeAttribute("tileWidth", tile.get(2));
				writer.writeAttribute("tileHeight", tile.get(2));
				writer.writeAttribute("imageWidth", tile.get(3));
				writer.writeAttribute("imageHeight", tile.get(4));
				writer.writeAttribute("zoomFactor", tile.get(5));
				writer.writeEndElement();
			}
			
			writer.writeEndElement();
//
//			writer.writeStartElement("data");
//			writer.writeAttribute("name", "value");
//
		
			writer.writeEndElement();
			writer.writeEndDocument();

			writer.flush();
			writer.close();

		} catch (XMLStreamException _xml) {
			_xml.printStackTrace();
		} catch (IOException _io) {
			_io.printStackTrace();
		}
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefaultFloor() {
		return defaultFloor;
	}

	public void setDefaultFloor(String defaultFloor) {
		this.defaultFloor = defaultFloor;
	}

	

	public String getDefaultPositionX() {
		return defaultPositionX;
	}

	public void setDefaultPositionX(String defaultPositionX) {
		this.defaultPositionX = defaultPositionX;
	}

	public String getDefaultPositionY() {
		return defaultPositionY;
	}

	public void setDefaultPositionY(String defaultPositionY) {
		this.defaultPositionY = defaultPositionY;
	}

	public String getRealScale() {
		return realScale;
	}

	public void setRealScale(String realScale) {
		this.realScale = realScale;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	void addIcon(String name, String imageFile) {
		ArrayList<String> icon = new ArrayList<String>();
		icon.add(name);
		icon.add(imageFile);
		iconSet.add(icon);
	}
	
	void addLayer(String layer, String zipFile, String tileSize, String width, String height, String zoom ) {
		ArrayList<String> tile = new ArrayList<String>();
		tile.add(layer);
		tile.add(zipFile);
		tile.add(tileSize);
		tile.add(width);
		tile.add(height);
		tile.add(zoom);
		tileSet.add(tile);
	}
}
