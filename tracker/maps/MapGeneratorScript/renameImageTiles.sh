#!bin/bash

# If the --help command was given, print the help message and exit.
if [ "$1" == "--help" ]
then
	echo "$(basename "$0") [--help] -- Tile renaming script"
	echo ""
	echo "Argument 1 - Location of the folder which holds the tiles to be renamed."
	echo "Argument 2 - The name given to the tiles."
	echo "Argument 3 - The number of columns in the image."
	echo ""
	echo "Be sure to have ImageMagick installed (usually installed by default) and run the following command:"
	echo "convert -crop 256x256 <PictureToCrop> <NewImageName>"
	exit
fi

# If no arguments were given, let the user know how to use the help.
if [ $# -ne 3 ]
then
	echo "Missing arguments. Use --help for help."
	exit
fi

# The folder in which all of the image tiles are kept.
FOLDER=$1

# The name given to the tiles by ImageMagick. (Excluding the -). This will
# be the name that you have given when creating the tiles.
TILE_NAMES=$2

# The maximum number of columns before the image needs to start a new row.
MAX_COLUMN=$3

# The current row.
ROW=1

# The current column.
COLUMN=1

# The tile's number given by ImageMagick.
COUNT=0

# The full name of each tile, minus the number (count).
FILE=$FOLDER/$TILE_NAMES-

# While the file exists...
while [ -f $FILE$COUNT ]
do

	# Move the file and change the name to the required tile name in the program.
	mv $FILE$COUNT $FOLDER/tile_$COLUMN\_$ROW.png	
	
	# Add to the count after the file name has been changed.
	let COUNT=$COUNT+1
	
	# Add one to the column.
	let COLUMN=$COLUMN+1
	
	# If the column is greater than the $MAX_COLUMN, then put it back to 1
	# and add one to $ROW.
	if [ $COLUMN -gt $MAX_COLUMN ]
	then
		let COLUMN=1
		let ROW=$ROW+1
	fi
done