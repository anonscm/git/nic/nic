/**
 * @author jens maiero
 *
 * @project plasmona
 *
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.activities;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Point;
import android.util.DisplayMetrics;
import org.indoor.plasmona.R;
import org.indoor.plasmona.activities.preferences.Preferences;
import org.indoor.plasmona.filter.SimpleFilter;
import org.indoor.plasmona.layer.CustomLayerController;
import org.indoor.plasmona.layer.TrackerLayerImpl;
import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.models.MapData;
import org.indoor.plasmona.tracker.Tracker;
import org.indoor.plasmona.tracker.TrackerResultListener;
import org.indoor.plasmona.tracker.TrackerServiceFactory;
import org.indoor.plasmona.tracker.histowifi.HistoConfiguration;
import org.indoor.plasmona.tracker.histowifi.HistoWifi;
import org.indoor.plasmona.tracker.pedometer.Pedometer;
import org.indoor.plasmona.tracker.qrcode.QRCode;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.MessageCodes;
import org.indoor.plasmona.utils.Prefs;
import org.indoor.plasmona.utils.TName;
import org.indoor.plasmona.views.MapView;
import org.indoor.plasmona.views.MapView.OnSelectedItem;
import org.indoor.plasmona.views.map.MapLoader;

import android.annotation.SuppressLint;
import android.app.ActionBar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import org.indoor.plasmona.R;
import org.indoor.plasmona.activities.preferences.Preferences;
import org.indoor.plasmona.filter.SimpleFilter;
import org.indoor.plasmona.layer.CustomLayerController;
import org.indoor.plasmona.layer.TrackerLayerImpl;
import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.models.MapData;
import org.indoor.plasmona.tracker.Tracker;
import org.indoor.plasmona.tracker.TrackerResultListener;
import org.indoor.plasmona.tracker.TrackerServiceFactory;
import org.indoor.plasmona.tracker.histowifi.HistoWifi;
import org.indoor.plasmona.tracker.qrcode.QRCode;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.MessageCodes;
import org.indoor.plasmona.utils.Prefs;
import org.indoor.plasmona.utils.TName;
import org.indoor.plasmona.views.MapView;
import org.indoor.plasmona.views.MapView.OnSelectedItem;
import org.indoor.plasmona.views.map.MapLoader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class PlasmonaActivity. Main Activity this is the Manager class - this
 * class managed all the different tracker implementations, the map and the
 * interaction/gui.
 */
public class PlasmonaActivity extends Activity implements Callback, TrackerResultListener {


    private static final boolean MAPMATCHING = true;

    int[] nodeList = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28};

    int[][] nodeAdjacencyList = {
            {1},                // 0
            {0, 2, 12},         // 1
            {1, 3},             // 2
            {2, 4, 11},         // 3
            {3, 5},             // 4
            {4, 6},             // 5
            {5, 7},             // 6
            {6, 8, 9},          // 7
            {7},                // 8
            {7, 10},            // 9
            {9, 11},            // 10
            {3, 10},            // 11
            {1, 13, 15},        // 12
            {12, 14},           // 13
            {13, 15},           // 14
            {12, 14, 17},       // 15
            {15, 18, 19, 22},   // 16
            {15},               // 17
            {16},               // 18
            {16, 20},           // 19
            {19, 21},           // 20
            {20},               // 21
            {16, 23},           // 22
            {22, 24},           // 23
            {23, 25},           // 24
            {24, 26},           // 25
            {25, 27},           // 26
            {26, 28},           // 27
            {27}                // 28
    };

    int[][] nodePositions = {
            {133, 89},
            {133, 175},
            {133, 243},
            {133, 299},
            {97, 299},
            {97, 375},
            {97, 456},
            {138, 455},
            {137, 490},
            {173, 456},
            {174, 374},
            {174, 300},
            {260, 179},
            {257, 110},
            {376, 110},
            {378, 178},
            {588, 178},
            {370, 239},
            {570, 102},
            {672, 179},
            {730, 181},
            {801, 179},
            {601, 222},
            {612, 270},
            {624, 357},
            {633, 350},
            {642, 389},
            {630, 436},
            {664, 475}
    };


    /**
     * The Constant TAG.
     */
    protected static final String TAG = "PlasmonaActivity";

    /**
     * The project.
     */
    private String project;

    /**
     * The map view.
     */
    private MapView mapView;

    /**
     * The map data.
     */
    private MapData mapData;

    /**
     * The handler.
     */
    private Handler handler;

    /**
     * The track btn.
     */
    private ToggleButton trackBtn;

    /**
     * The mode btn.
     */
    private ToggleButton modeBtn;

    /**
     * The remove box.
     */
    private TextView removeBox;

    /**
     * The item properties.
     */
    private TextView itemProperties;

    /**
     * The information box.
     */
    private TextView informationBox;

    /**
     * The step counter.
     */
    private int stepCounter;

    /**
     * The tracker.
     */
    private static String TRACKER;

    /**
     * The filter.
     */
    private SimpleFilter filter;

    /**
     * The Bundle for store session parameters.
     */
    private Bundle restoreSession = null;

    /**
     * is tracker running?.
     */
    boolean running = false;

    /**
     * The chooser.
     */
    private Spinner chooser;

    /**
     * The list.
     */
    private List<String> activeTracker;

    /**
     * The angle.
     */
    private double angle = 0.0f;

  /*
   * (non-Javadoc)
   * 
   * @see android.app.Activity#onCreate(android.os.Bundle)
   */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




    /*
     * set the bundle later we will check if there are some saved parameters
     */
        restoreSession = savedInstanceState;

        // requestWindowFeature(Window.);

    /*
     * set the application context as a weak reference, hence we can access this
     * application context from other classes
     */
        ApplicationContext.set(getApplicationContext());
        ApplicationContext.setActivity(this);

    /*
     * Look in the extras and get current project name
     */
        project = getIntent().getExtras().getString("project");

    /*
     * set the current project name to the shared preferences
     */
        SharedPreferences prefs = getSharedPreferences(Prefs.PLASMONA, Context.MODE_PRIVATE);
        prefs.edit().putString(Prefs.PROJECT, project).commit();

    /*
     * new handler for handle messages from the trackers
     */
        handler = new Handler(this);

    /*
     * an simple weighted average filter for smoothing data
     */
        filter = new SimpleFilter();

    /*
     * set the project map content
     */
        setContentView(R.layout.mapview);
        mapView = (MapView) findViewById(R.id.mapView);

        activeTracker = new ArrayList<String>();

    /*
     * load the gui stuff disable and enable necessary controls
     */
        loadGUI();

    /*
     * load all shared preferences for tracker configuration
     */
        loadSharedPreferences();

        Prefs.setPathToHistoWifi(Environment.getExternalStorageDirectory() + "/" + ApplicationContext.get().getPackageName() + "/" + project + "/");

    /*
     * load map. in this case from sd card but also possible from a server
     */
        loadMap();

        resetStepCounter();

        Log.d(TAG, "There are " + TrackerServiceFactory.getLocationService().getTrackers().size() + " registered");

    }

    /**
     * On update chooser. This DropDown Menu priors the capture function. The
     * active tracker will set to the capturer event
     */
    private void onUpdateChooser() {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.drawable.plspinner, activeTracker);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        chooser.setAdapter(dataAdapter);

    }

    /**
     * On session start. Check if some trackers are running if so the kepp them
     * alive
     */
    private void onSessionStart() {

        loadSharedPreferences();
        if (restoreSession != null) {
            mapView.setRestored(true);
            trackBtn.setChecked(restoreSession.getBoolean("TrackBtn"));

            for (String s : activeTracker) {
                if (TrackerServiceFactory.getLocationService().findTrackerByName(s) == null) {
                    register(s);
                }
            }

            for (Tracker t : TrackerServiceFactory.getLocationService().getTrackers()) {
                if (!activeTracker.contains(t.getTrackerName())) {
                    TrackerServiceFactory.getLocationService().unregisterTracker(TrackerServiceFactory.getLocationService().findTrackerByName(t.getTrackerName()));
                }
            }

            for (Tracker t : TrackerServiceFactory.getLocationService().getTrackers()) {
                t.setTrackerResultListener(this);
                t.setHandler(handler);
                if (trackBtn.isChecked())
                    t.start();
            }

        } else {

            registerTrackers();

        }

        updateGUI();
    }

    /**
     * Load shared preferences.
     */
    private void loadSharedPreferences() {

        activeTracker.clear();
        SharedPreferences plasmona_prefs = PreferenceManager.getDefaultSharedPreferences(this);

        if (plasmona_prefs.getBoolean("histo", false)) {
            activeTracker.add(TName.HISTOTRACKER);
        }

        if (plasmona_prefs.getBoolean("qr", false)) {

            activeTracker.add(TName.QRCODETRACKER);
        }
        if (plasmona_prefs.getBoolean("pedometer", false)) {

            activeTracker.add(TName.PEDOTRACKER);
        }

        onUpdateChooser();
    }

    /**
     * Register trackers. If aou want to register new trackers you either remove
     * all and add all new ones or you can check which tracker is current
     * available
     */
    private void registerTrackers() {

        onUnRegisterTrackers();
        SharedPreferences plasmona_prefs = PreferenceManager.getDefaultSharedPreferences(this);

        if (plasmona_prefs.getBoolean("histo", false)) {
            register(TName.HISTOTRACKER);
        }

        if (plasmona_prefs.getBoolean("qr", false)) {
            register(TName.QRCODETRACKER);
        }

        if (plasmona_prefs.getBoolean("pedometer", false)) {
            register(TName.PEDOTRACKER);
        }

    }

    /**
     * Register.
     *
     * @param tracker the tracker
     */
    private void register(String tracker) {
        if (TrackerServiceFactory.getLocationService().findTrackerByName(tracker) != null)
            return;
        Tracker t = null;

        try {
            Class clazz = Class.forName("org.indoor.plasmona.tracker." + tracker.toLowerCase() + "." + tracker);
            try {
                t = (Tracker) clazz.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (IllegalAccessException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (t != null) {
            t.setTrackerResultListener(this);
            t.setHandler(handler);
            if (t.getTrackerName().equals(TName.HISTOTRACKER)) {

                if (t.hasConfiguration()) {

                    HistoConfiguration hconfig = (HistoConfiguration) t.getConfiguration();
                    //setzt die Pufferzeit in der online Phase in ms
                    hconfig.setBufferTime(3000);
                    //überprüft, wieviele Access Points im Online Fingerprint wieder gefunden werden, der Fingerprint als "match" Kandidat in Frage kommt in Prozent
                    hconfig.setConfidenceFactor(0.7f);
                    //setzt die Anzahl der Nachbarn für die gewichtete Interpolation, wenn keine Interpolation erwünscht ist einfach auf 1
                    hconfig.setNumberOfNeighbours(8);
                    //normalisiert die diskrete Verteilung auf vierer Bins
                    hconfig.setBinSize(4);
                    //offline: alle 100 ms wird ein Wifi Scan ausgeführt
                    hconfig.setTimeInterval(100);
                    //offline: 50 Scans werden ausgeführt
                    hconfig.setNumberOfScans(100);
                    //überprüft auf Ausreisser
                    hconfig.setOutlierDect(true);
                    //überprüft auf Ausreisser
                    //hconfig.setInterpolation(HistoConfiguration.INTERPOLATE.SQUARE);
                    hconfig.setInterpolation(HistoConfiguration.INTERPOLATE.METRIC);
                    //unterscheide auf welcher Hierarchie Ebene das Matching stattfinden soll der Tracker von Snapshot 0.3.5 wäre MATCH.MEAN_VALUE und ALGORITHM.KULLBACKLEIBLER
                    hconfig.setMatching(HistoConfiguration.MATCH.MEAN_VALUE);
                    //hconfig.setMatching(HistoConfiguration.MATCH.BIN_LEVEL);
                    //wähle ein Matching Algorithmus aus
                    hconfig.setAlgorithm(HistoConfiguration.ALGORITHM.SURVIVEOFFITTEST);
                    //hconfig.setAlgorithm(HistoConfiguration.ALGORITHM.KULLBACKLEIBLER);

                }
            }
        }

    }

    /**
     * Load gui.
     */
    private void loadGUI() {

        modeBtn = (ToggleButton) findViewById(R.id.modeSwitch);
        System.out.println("mode btn " + modeBtn.toString());
        modeBtn.setOnClickListener(onModeBtnListener);

        informationBox = (TextView) findViewById(R.id.info);

        trackBtn = (ToggleButton) findViewById(R.id.trackBtn);
        trackBtn.setOnClickListener(onTrackBtnListener);

        chooser = (Spinner) findViewById(R.id.chooser);
        chooser.setOnItemSelectedListener(onChooserChanged);

        removeBox = (TextView) findViewById(R.id.remove);
        itemProperties = (TextView) findViewById(R.id.iteminfo);

        // if we are in the navigation mode add all elements necessary for
        // navigation
        // same for edit mode
        if (getSharedPreferences(Prefs.PLASMONA, 0).getInt(Prefs.NAV_MODE, 0) == Prefs.NAV_MODE_LOCATE && TRACKER != null) {

            trackBtn.setVisibility(View.VISIBLE);
            trackBtn.setTextOn("Stop");
            trackBtn.setTextOff("Start");

            trackBtn.setText("Start");

        } else if (getSharedPreferences(Prefs.PLASMONA, 0).getInt(Prefs.NAV_MODE, 0) == Prefs.NAV_MODE_EDIT) {
            chooser.setVisibility(View.INVISIBLE);
            trackBtn.setVisibility(View.INVISIBLE);
        }
        addListener();

    }

    /**
     * Update gui.
     */
    private void updateGUI() {

        // loadSharedPreferences();
        if (activeTracker.size() != 0) {

            SharedPreferences prefs = getSharedPreferences("prf", MODE_PRIVATE);
            if (activeTracker.size() == prefs.getInt("spinnerSelection", 0))
                chooser.setSelection(0);
            else
                chooser.setSelection(prefs.getInt("spinnerSelection", 0));
        }

        if (TrackerServiceFactory.getLocationService().getTrackers().size() == 0) {
            trackBtn.setVisibility(View.INVISIBLE);
            modeBtn.setVisibility(View.INVISIBLE);
            informationBox.setText("Info");
            chooser.setVisibility(View.INVISIBLE);
        } else {
            if (getSharedPreferences(Prefs.PLASMONA, 0).getInt(Prefs.NAV_MODE, 0) == Prefs.NAV_MODE_EDIT) {
                trackBtn.setVisibility(View.INVISIBLE);
                chooser.setVisibility(View.INVISIBLE);
                modeBtn.setChecked(true);
            } else {
                trackBtn.setVisibility(View.VISIBLE);
                chooser.setVisibility(View.VISIBLE);
            }
            modeBtn.setVisibility(View.VISIBLE);


        }
    }

    /**
     * Load the a map for a corresponding project.
     */
    private void loadMap() {
        System.out.println("Load Map from " + Environment.getExternalStorageDirectory() + "/" + ApplicationContext.get().getPackageName());
        MapLoader.loadMapXML(handler, new File(Environment.getExternalStorageDirectory() + "/" + ApplicationContext.get().getPackageName() + "/" + project + "/mapdef.xml"));
    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onPause()
     */
    @Override
    protected void onPause() {
        super.onPause();
        onStopTrackers();
    }

    /**
     * On stop trackers.
     */
    private void onStopTrackers() {
        if (TrackerServiceFactory.getLocationService().getTrackers().size() != 0) {
            for (int i = 0; i < TrackerServiceFactory.getLocationService().getTrackers().size(); i++) {
                Log.i(TAG, "Stopping Tracker <" + TrackerServiceFactory.getLocationService().getTrackers().get(i).getTrackerName() + ">");
                TrackerServiceFactory.getLocationService().getTrackers().get(i).stop();
                resetStepCounter();
            }
        }
    }

    /**
     * On un register trackers.
     */
    private void onUnRegisterTrackers() {

        if (TrackerServiceFactory.getLocationService().getTrackers().size() != 0) {
            for (int i = 0; i < TrackerServiceFactory.getLocationService().getTrackers().size(); i++) {
                TrackerServiceFactory.getLocationService().getTrackers().get(i).stop();
                resetStepCounter();
                TrackerServiceFactory.getLocationService().unregisterTracker(TrackerServiceFactory.getLocationService().getTrackers().get(i));
            }
        }
    }

    /**
     * On start trackers.
     */
    private void onStartTrackers() {
        if (TrackerServiceFactory.getLocationService().getTrackers().size() != 0) {
            for (Tracker t : TrackerServiceFactory.getLocationService().getTrackers()) {
                Log.i(TAG, "Starting Tracker <" + t.getTrackerName() + ">");
                t.start();
            }
        }
    }

    /**
     * Reset step counter.
     */
    private void resetStepCounter() {
        stepCounter = 0;
    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onResume()
     */
    @Override
    protected void onResume() {
        super.onResume();
        onSessionStart();
        loadSharedPreferences();
        updateGUI();

    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onRestoreInstanceState(android.os.Bundle)
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onRestoreInstanceState(savedInstanceState);
        restoreSession = savedInstanceState;
    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onStart()
     */
    @Override
    protected void onStart() {
        super.onStart();
        // loadSharedPreferences();
        // updateGUI();
    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onStop()
     */
    @Override
    protected void onStop() {
        super.onStop();

    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onDestroy()
     */
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

    }

    /*
     * (non-Javadoc)
     *
     * @see android.os.Handler.Callback#handleMessage(android.os.Message)
     */
    @Override
    public boolean handleMessage(Message msg) {

        if (msg.what == MessageCodes.MAP_LOADED) {

            mapData = (MapData) msg.obj;
            if (restoreSession != null) {

                mapView.setPreviousLayer((int) (restoreSession.getFloat("Layer")));
                mapData.setLayer((int) (restoreSession.getFloat("Layer")));
                mapView.setLocalScale(restoreSession.getFloat("LocalScale"));
                mapView.getCamera().lookAt(restoreSession.getFloat("PosX"), restoreSession.getFloat("PosY"));
                mapView.setScreenSize(new Point(restoreSession.getInt("ScreenX"), restoreSession.getInt("ScreenY")));
                mapData.setPosition(mapView.getCamPos());

            }
            mapView.setMapData(mapData);
            filter.setPixelMeterRelation(mapData.getRealScale());
            TrackerServiceFactory.getLocationService().setMapData(mapData);
            mapView.init();

        } else if (msg.what == MessageCodes.STEP_DETECTED) {
            informationBox.setText("Steps " + (stepCounter++));

        } else if (msg.what == MessageCodes.COMPASS_EVENT) {
            angle = (Double) msg.obj;
            mapView.setCompassAngle((int) angle);
            informationBox.setText("Degree " + (int) angle + " Steps " + stepCounter);

        } else if (msg.what == MessageCodes.TRACKER_INIT) {
            trackerinit = (Boolean) msg.obj;
        }
        return true;
    }


    private boolean setpdetected = false;

    private boolean trackerinit = false;

    private long trackerupdate = 0;

    GeoPoint puffer = new GeoPoint();

    @Override
    public void onTrackerResult(GeoPoint pos) {
        long timer = System.currentTimeMillis();
        //Log.i(TAG, "Position Changed kk" + pos);
        GeoPoint p = CustomLayerController.getInstance().findLayerByName("User").findTrackerByName("user");

        if (TrackerServiceFactory.getLocationService().getTrackers().size() != 0) {

            Tracker t = TrackerServiceFactory.getLocationService().findTrackerByName(pos.getType());

            if (t != null && pos != null)

                if (t.getTrackerName().equals(TName.HISTOTRACKER)) {
                    GeoPoint point = filter.onFilterMedian(pos);
                    //Log.i(TAG, "Position Changed filtered " + point);
                    if (Math.abs(timer - trackerupdate) > 700) {
                        if (!MAPMATCHING) {
                            trackerupdate = timer;
                            p.setPosition(filter.onSmoothData(point));
                            //Log.i(TAG, "Position Changed smoothed " + p);
                            mapView.centerMapToPoint(p);

                        } else {
                            GeoPoint newPos = filter.onSmoothData(point);

                            p.setPosition(newPos.x(), newPos.y());

                            float minDist = 1000000000.f;

                            float newX = newPos.x();
                            float newY = newPos.y();

                            for (int i : nodeList) {

                                for (int j : nodeAdjacencyList[i]) {
                                    float xDiff1 = newPos.x() - nodePositions[i][0];
                                    float xDiff2 = newPos.x() - nodePositions[j][0];
                                    float yDiff1 = newPos.y() - nodePositions[i][1];
                                    float yDiff2 = newPos.y() - nodePositions[j][1];
                                    float dist1 = (float) Math.sqrt(xDiff1 * xDiff1 + yDiff1 * yDiff1);
                                    float dist2 = (float) Math.sqrt(xDiff2 * xDiff2 + yDiff2 * yDiff2);

                                    float sum = dist1 + dist2;
                                    float newXTemp = ((sum - dist1) * nodePositions[i][0] + (sum - dist2) * nodePositions[j][0]) / sum;
                                    float newYTemp = ((sum - dist1) * nodePositions[i][1] + (sum - dist2) * nodePositions[j][1]) / sum;

                                    float minNewDist = (float)Math.sqrt(Math.pow(newXTemp - newPos.x(), 2) + Math.pow(newYTemp - newPos.y(), 2));
                                    if (minNewDist < minDist) {
                                        minDist = minNewDist;
                                        newX = newXTemp;
                                        newY = newYTemp;

                                    }

                                }

                            }

                            GeoPoint p1 = CustomLayerController.getInstance().findLayerByName("User").findTrackerByName("user_corrected");
                            p1.setPosition(newX, newY);

                        }
                    }

                } else if (t.getTrackerName().equals(TName.QRCODETRACKER)) {
                    mapView.centerMapToPoint(pos);
                    CustomLayerController.getInstance().findLayerByName("User").findTrackerByName("user").setPosition(pos.x(), pos.y());
                    TrackerServiceFactory.getLocationService().findTrackerByName(TName.QRCODETRACKER).stop();

                } else if (t.getTrackerName().equals(TName.PEDOTRACKER)) {

                    // p.setPosition(puffer);
                    // mapView.centerMapToPoint(p);
                }
        }

    }

    /**
     * The on mode btn listener.
     */
    OnClickListener onModeBtnListener = new OnClickListener() {

        @Override
        public void onClick(View arg0) {
            SharedPreferences prefs = getSharedPreferences(Prefs.PLASMONA, Context.MODE_PRIVATE);
            if (!modeBtn.isChecked()) {

                prefs.edit().putInt(Prefs.NAV_MODE, Prefs.NAV_MODE_LOCATE).commit();
                modeBtn.setChecked(false);

            } else {

                prefs.edit().putInt(Prefs.NAV_MODE, Prefs.NAV_MODE_EDIT).commit();
                modeBtn.setChecked(true);
            }
            loadGUI();
        }

    };

    /**
     * The on chooser changed.
     */
    OnItemSelectedListener onChooserChanged = new OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

            TRACKER = chooser.getSelectedItem().toString();
            int selectedPosition = chooser.getSelectedItemPosition();
            SharedPreferences prefs = getSharedPreferences("prf", MODE_PRIVATE);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putInt("spinnerSelection", selectedPosition);
            edit.commit();
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub

        }

    };

    /**
     * The on track btn listener.
     */
    OnClickListener onTrackBtnListener = new OnClickListener() {

        @Override
        public void onClick(View arg0) {

            if (trackBtn.isChecked()) {
                onStartTrackers();
                trackBtn.setChecked(true);

            } else {

                onStopTrackers();
                trackBtn.setChecked(false);
                mapView.history.clear();
            }
        }

    };

    /**
     * Adds the listener.
     */
    private void addListener() {

        mapView.setOnLongpressListener(new MapView.OnLongpressListener() {
            public void onLongpress(final GeoPoint pnt) {
                if (getSharedPreferences(Prefs.PLASMONA, 0).getInt(Prefs.NAV_MODE, 0) == Prefs.NAV_MODE_LOCATE) {

                    CaptureOperation l = new CaptureOperation();
                    pnt.setData(angle);
                    l.setPoint(pnt);
                    l.execute("");
                }
            }
        });

        mapView.setOnSelectionListener(new OnSelectedItem() {

            @Override
            public void onSelection(GeoPoint geo, boolean event) {
                // TODO Auto-generated method stub
                Rect r = new Rect();
                removeBox.getGlobalVisibleRect(r);

                if (r.contains((int) mapView.getScreenPos().x, (int) mapView.getScreenPos().y)) {
                    removeBox.setBackgroundResource(R.drawable.plbutton);
                    removeBox.setPressed(true);
                } else {
                    removeBox.setBackgroundResource(R.drawable.plbutton);
                    removeBox.setPressed(false);
                }

                Rect rect = new Rect();
                itemProperties.getGlobalVisibleRect(rect);

                if (rect.contains((int) mapView.getScreenPos().x, (int) mapView.getScreenPos().y)) {
                    itemProperties.setBackgroundResource(R.drawable.plbutton);
                    itemProperties.setPressed(false);
                } else {
                    itemProperties.setBackgroundResource(R.drawable.plbutton);
                    itemProperties.setPressed(false);
                }

                if (geo.getType().equals("unknown")) {
                    GeoPoint p = CustomLayerController.getInstance().findLayerByName("User").findTrackerByName("user");
                    p.setPosition(geo.x(), geo.y());
                } else if (TrackerServiceFactory.getLocationService().findTrackerByName(geo.getType()).hasCapturer()) {
                    if (event) {
                        if (r.contains((int) mapView.getScreenPos().x, (int) mapView.getScreenPos().y))
                            TrackerServiceFactory.getLocationService().findTrackerByName(geo.getType()).getCapturer().delete(geo);
                        else if (rect.contains((int) mapView.getScreenPos().x, (int) mapView.getScreenPos().y)) {
                            popup(geo.toString());
                            TrackerLayerImpl layer = TrackerServiceFactory.getLocationService().findTrackerByName(geo.getType()).getLayer();
                            geo.setPosition(layer.reloadPosition(geo.getId()));

                        } else {
                            TrackerServiceFactory.getLocationService().findTrackerByName(geo.getType()).getCapturer().update(geo);

                        }
                        removeBox.setVisibility(View.INVISIBLE);
                        itemProperties.setVisibility(View.INVISIBLE);
                        geo.setActive(false);
                    } else if (mapView.isLongPressed()) {
                        geo.setActive(true);
                        removeBox.setVisibility(View.VISIBLE);
                        itemProperties.setVisibility(View.VISIBLE);
                    }

                }
            }
        });
    }

    /**
     * Popup.
     *
     * @param text the text
     */
    private void popup(String text) {
        Toast toast = Toast.makeText(ApplicationContext.get(), text, Toast.LENGTH_SHORT);
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.popupstyle);
        toast.show();

    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onSaveInstanceState(android.os.Bundle)
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Log.i(TAG, "saving project data: layer screen" + mapData.getLayer() + " scale " + mapView.getLocalScale() + " map camera " + mapView.getCamPos().x + "/" + mapView.getCamPos().y);

        outState.putFloat("Layer", mapData.getLayer());
        outState.putFloat("LocalScale", mapView.getLocalScale());
        outState.putFloat("PosX", mapView.getCamPos().x);
        outState.putFloat("PosY", mapView.getCamPos().y);
        outState.putInt("ScreenX", mapView.getScreenSize().x);
        outState.putInt("ScreenY", mapView.getScreenSize().y);
        outState.putBoolean("TrackBtn", trackBtn.isChecked());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.plasmona, menu);
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int id = item.getItemId();
        if (id == R.id.showoptionmenu) {
            Log.d(TAG, "Starting Preferences");
            startActivity(new Intent(this, Preferences.class));
        } else if (id == R.id.showcalibact) {
            Log.d(TAG, "Starting Preferences");
            startActivity(new Intent(this, PedometerCalib.class));
        }

        return true;
    }

    /**
     * The Class CaptureOperation. This class calls the capturer of the current
     * tracker
     */
    class CaptureOperation extends AsyncTask<String, Void, String> {

        /**
         * The pnt.
         */
        GeoPoint pnt = null;

        /**
         * The waiting.
         */
        public ProgressDialog waiting = new ProgressDialog(PlasmonaActivity.this);

        /**
         * Sets the point.
         *
         * @param pnt the new point
         */
        public void setPoint(GeoPoint pnt) {
            this.pnt = pnt;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected String doInBackground(String... params) {
            Tracker t = TrackerServiceFactory.getLocationService().findTrackerByName(TRACKER);

            if (t != null && pnt != null && t.hasCapturer()) {
                Log.i(TAG, "Capture Point at :" + pnt.x() + "/" + pnt.y() + " for " + TRACKER + " " + t.getTrackerName());
                t.getCapturer().captureAt(pnt);
            }

            return null;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            waiting.dismiss();
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {
            waiting.setMessage("Capture Point :" + " " + pnt.toString());
            waiting.show();
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onProgressUpdate(Progress[])
         */
        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

}