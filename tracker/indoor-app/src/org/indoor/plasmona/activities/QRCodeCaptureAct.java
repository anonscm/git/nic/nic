/**
 * @author jens maiero
 *
 * @project plasmona
 *
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.activities;

import org.indoor.plasmona.layer.TrackerLayerImpl;
import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.tracker.TrackerServiceFactory;
import org.indoor.plasmona.tracker.qrcode.QRCode;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.TName;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.zxing.Result;
import com.google.zxing.client.android.CaptureActivity;

//import com.google.zxing.qrcode.encoder.QRCode;

/**
 * The Class QRCodeCaptureAct. This class captures a QR Code and returns based
 * on a Handler the Position of the User. The QR Code information can be
 * extended or reduced. In this implementation the QR Code information does not
 * contain any information about the location. It just contains an ID, and based
 * on the ID and on the QR Code capturer it is possible to stick pre-defined QR
 * Codes to an arbitrary location.
 */
public class QRCodeCaptureAct extends CaptureActivity {

  /** The Constant TAG. */
  public static final String TAG = "CaptureActivity";

  /** The Constant HEADER. */
  public static final String HEADER = "PLASMONA";

  /** The Constant MAPDATA_HEADER. */
  public static final String MAPDATA_HEADER = "MAPDATA";

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.google.zxing.client.android.CaptureActivity#handleDecode(com.google
   * .zxing.Result, android.graphics.Bitmap)
   */
  @Override
  public void handleDecode(Result rawResult, Bitmap barcode) {

    try {
      Log.d(QRCodeCaptureAct.class.getSimpleName(), "Got QR-Code to parse.");
      String text = rawResult.getText();
      /**
       * Check if the Header contains a pre-defined String And check if the
       * Intents Extras contains the capturer flag or not. Capturer: add new
       * Location Tracker: find Location based on ID - Callback onActivityResult
       */
      if (text.startsWith(HEADER)) {
	Log.i(TAG, text);

	String info[] = null;
	info = text.split(" ");
	if (info[1].equals("ID")) {
	  Intent intent = getIntent();

	  if (intent.getExtras().getBoolean("Capturer", false)) {
	    GeoPoint pnt = new GeoPoint();
	    float x = intent.getExtras().getFloat("x", 0);
	    float y = intent.getExtras().getFloat("y", 0);

	    pnt.setPosition(x, y);
	    pnt.setId(Integer.parseInt(info[2]));
	    pnt.setType(TName.QRCODETRACKER);
	    // Log.i("TAG", "write new qrcode reference at " + pnt.toString());
	    // Log.i(TAG, "" +
	    // TrackerServiceFactory.getLocationService().findTrackerByName(TName.QRCODETRACKER).hasCapturer());
	    pnt.setBitmapName("ips_qrcode");
	    int resID = ApplicationContext.get().getResources().getIdentifier(pnt.getBitmapName(), "drawable", ApplicationContext.get().getPackageName());
	    pnt.setIcon(BitmapFactory.decodeResource(ApplicationContext.get().getResources(), resID));
	    pnt.setType(TName.QRCODETRACKER);

	    if (TrackerServiceFactory.getLocationService().findTrackerByName(TName.QRCODETRACKER).hasCapturer()) {
	      TrackerServiceFactory.getLocationService().findTrackerByName(TName.QRCODETRACKER).getCapturer().write(pnt);
	    }

	  } else {

	    TrackerLayerImpl layer = TrackerServiceFactory.getLocationService().findTrackerByName(TName.QRCODETRACKER).getLayer();

	    QRCode tracker = (QRCode) TrackerServiceFactory.getLocationService().findTrackerByName(TName.QRCODETRACKER);
	    if (layer.findPositionById(Integer.parseInt(info[2])) != null) {
	      tracker.onActivityResult(layer.findPositionById(Integer.parseInt(info[2])));

	    }
	    //
	    intent.putExtra("QRCODE", true);
	    setResult(RESULT_OK, intent);
	    finish();
	  }
	  setResult(RESULT_OK, intent);
	  finish();
	}

      }

    } catch (Exception e) {
      Log.d(QRCodeCaptureAct.class.getSimpleName(), "Exception: " + e.toString());
    }
    requestCapture();
  }

  /*
   * (non-Javadoc)
   * 
   * @see android.app.Activity#onBackPressed()
   */
  @Override
  public void onBackPressed() {
    // TODO Auto-generated method stub
    super.onBackPressed();
    Intent intent = getIntent();
    intent.putExtra("QRCODE", true);
    setResult(RESULT_OK, intent);
    finish();
  }
}