/**
 * @author jens maiero
 *
 * @project plasmona
 * 
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.activities;

import org.indoor.plasmona.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * The Class SplashActivity.
 * 
 * @author jens
 * 
 *         Simple Splashscreen Activity which shows the Plasmona Logo and
 *         loads/calls the Project Browser
 */

public class SplashActivity extends Activity {

  /** The Constant SPLASH_LENGTH. */
  private final static int SPLASH_LENGTH = 500;

  /*
   * (non-Javadoc)
   * 
   * @see android.app.Activity#onCreate(android.os.Bundle)
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.splash);

    Thread splashThread = new Thread() {
      @Override
      public void run() {
	try {
	  sleep(SPLASH_LENGTH);

	} catch (InterruptedException e) {
	  // do nothing
	} finally {
	  finish();
	  Intent i = new Intent("org.indoor.plasmona.activities.ProjectsActivity");

	  startActivity(i);
	}
      }
    };
    splashThread.start();
  }
}
