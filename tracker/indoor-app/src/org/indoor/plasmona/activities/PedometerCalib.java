/**
 * @author jens maiero
 *
 * @project plasmona
 *
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphView.LegendAlign;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.GraphViewSeries.GraphViewSeriesStyle;
import com.jjoe64.graphview.LineGraphView;
import org.indoor.plasmona.R;

import java.io.*;
import java.util.Collections;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * The Class PedometerCalib. Based on this class you can calibrate your
 * pedometer. Just walk a view steps and click on calibrate the average of all
 * collected parameters than will commit to the shared preferences (PREFS_NAME =
 * "PedoMeterPrefs") Debug function you also can write accelerometer data to
 * file. And analyse the afterwards This class utilize the graphview lib to
 * visualize accelerometer data.
 */
public class PedometerCalib extends Activity implements SensorEventListener {

    /**
     * The sensor manager.
     */
    protected SensorManager sensorManager;

    /**
     * The Constant PREFS_NAME.
     */
    public static final String PREFS_NAME = "PedoMeterPrefs";

    /**
     * The accelerometer.
     */
    protected Sensor accelerometer;

    /**
     * The start time.
     */
    protected long startTime = 0L;

    /**
     * The last update.
     */
    protected long lastUpdate = 0L;

    /**
     * The update interval.
     */
    protected int updateInterval = 100;

    /**
     * The min.
     */
    private static double MIN = -5;

    /**
     * The max.
     */
    private static double MAX = 20;

    /**
     * The m handler.
     */
    private final Handler mHandler = new Handler();

    /**
     * The m timer2.
     */
    private Runnable mTimer2;

    /**
     * The is running.
     */
    private boolean isRunning = false;

    /**
     * The last_update.
     */
    long last_update = 0;

    /**
     * The start stop calib.
     */
    private ToggleButton startStopCalib;

    /**
     * The start stop test.
     */
    private ToggleButton startStopTest;

    /**
     * The step text.
     */
    private TextView stepText;

    /**
     * The info text.
     */
    private TextView infoText;

    /**
     * The check.
     */
    private Button check;

    /**
     * The graph view.
     */
    private GraphView graphView;

    /**
     * The x value.
     */
    private GraphViewSeries xValue;

    /**
     * The y value.
     */
    private GraphViewSeries yValue;

    /**
     * The z value.
     */
    private GraphViewSeries zValue;

    /**
     * The magnitude.
     */
    private GraphViewSeries magnitude;

    /**
     * The time.
     */
    private double time = 0;

    /**
     * The output data.
     */
    private Vector<String> outputData;

    /**
     * The X test data.
     */
    private Vector<Double> XTestData;

    /**
     * The Y test data.
     */
    private Vector<Double> YTestData;

    /**
     * The Z test data.
     */
    private Vector<Double> ZTestData;

    /**
     * The layout.
     */
    LinearLayout layout;

    /**
     * The T test data.
     */
    private Vector<Double> TTestData;

    /**
     * The is calibrated.
     */
    private boolean isCalibrated;

    /**
     * The timer value.
     */
    private float timerValue;

    /**
     * The peak value.
     */
    private float peakValue;

    /**
     * The number steps.
     */
    public int numberSteps = 0;

    /******************************** start step parameters ************************************/

    /**
     * The local max.
     */
    double localMAX = 1E-9;

    /**
     * The local min.
     */
    double localMIN = 1E9;

    /**
     * The local timer.
     */
    double localTimer = 0;

    /**
     * The window.
     */
    double WINDOW = 200;

    /**
     * The loc index.
     */
    int locIndex = 0;

    /**
     * The old max.
     */
    double oldMax = 1E-9;

    /**
     * The b max.
     */
    boolean bMax = true;

    /**
     * The b min.
     */
    boolean bMin = false;

    /**
     * The b last step.
     */
    boolean bLastStep = false;

    /**
     * The counter.
     */
    int counter = 0;

    /**
     * The last max.
     */
    long lastMax = 0;

    /**
     * The last step.
     */
    long lastStep = -1;

    /**
     * The peak_avg.
     */
    private Vector<Float> peak_avg = new Vector<Float>();

    /**
     * The timer_avg.
     */
    private Vector<Float> timer_avg = new Vector<Float>();

    /**
     * The peak min.
     */
    private float PEAK_MIN = 0.1f;

    /**
     * The peak max.
     */
    private float PEAK_MAX = 0.5f;

    /**
     * The timer min.
     */
    private float TIMER_MIN = 300;

    /**
     * The timer max.
     */
    private float TIMER_MAX = 2000;

    /******************************** end step parameters ************************************/

    /**
     * Called when the activity is first created.
     *
     * @param savedInstanceState the saved instance state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pedometer_calib);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        layout = (LinearLayout) findViewById(R.id.graph1);

        outputData = new Vector<String>();

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        isCalibrated = settings.getBoolean("calibrated", false);
        timerValue = settings.getFloat("timerValue", -1.0f);
        peakValue = settings.getFloat("peakValue", -1.0f);

        Log.i("SHARED PREFERENCES", "Calibrated " + isCalibrated + " timerValue " + timerValue + " peakValue " + peakValue);
        startStopCalib = (ToggleButton) findViewById(R.id.togglecalib);
        startStopTest = (ToggleButton) findViewById(R.id.toggletest);
        stepText = (TextView) findViewById(R.id.step);
        infoText = (TextView) findViewById(R.id.info);

        check = (Button) findViewById(R.id.recorded);
        XTestData = new Vector<Double>();
        YTestData = new Vector<Double>();
        ZTestData = new Vector<Double>();
        TTestData = new Vector<Double>();

        check.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!isRunning) {
                    counter = 0;
                    readTestData();
                } else
                    Log.i("TAG", "Sorry but is already running!");
            }

        });
        startStopCalib.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    counter = 0;
                    stepText.setText("Step " + counter);
                    time = 1d;
                    xValue.resetData(new GraphViewData[]{});
                    yValue.resetData(new GraphViewData[]{});
                    zValue.resetData(new GraphViewData[]{});
                    magnitude.resetData(new GraphViewData[]{});
                    isCalibrated = false;
                    isRunning = true;
                    startScan();
                    startStopTest.setEnabled(false);

                } else {
                    stopScan();
                    mHandler.removeCallbacks(mTimer2);
                    isRunning = false;
                    writeTestData();
                    // File dir = new File( Environment.getExternalStorageDirectory() +
                    // "/" + ApplicationContext.get().getPackageName());
                    // dir.mkdir();
                    // File file = new File(dir, "pedo-test.txt");
                    // try {
                    // save(file, testbuffer);
                    // System.out.println("SAVE the FILE");
                    // } catch (IOException e) {
                    // // TODO Auto-generated catch block
                    // e.printStackTrace();
                    // }
                    //
                    // testbuffer.clear();
                    // try {
                    // read(file, testbuffer);
                    // } catch (StreamCorruptedException e) {
                    // // TODO Auto-generated catch block
                    // e.printStackTrace();
                    // } catch (IOException e) {
                    // // TODO Auto-generated catch block
                    // e.printStackTrace();
                    // } catch (ClassNotFoundException e) {
                    // // TODO Auto-generated catch block
                    // e.printStackTrace();
                    // }

                    startStopTest.setEnabled(true);
                }
            }

        });

        startStopTest.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    counter = 0;
                    stepText.setText("Step " + counter);
                    time = 1d;
                    xValue.resetData(new GraphViewData[]{});
                    yValue.resetData(new GraphViewData[]{});
                    zValue.resetData(new GraphViewData[]{});
                    magnitude.resetData(new GraphViewData[]{});
                    isRunning = true;
                    startScan();
                    startStopCalib.setEnabled(false);

                } else {
                    stopScan();
                    mHandler.removeCallbacks(mTimer2);
                    isRunning = false;
                    // writeTestData();

                    startStopCalib.setEnabled(true);
                }
            }

        });

        xValue = new GraphViewSeries("X-Values", new GraphViewSeriesStyle(Color.RED, 2), new GraphViewData[]{});
        yValue = new GraphViewSeries("Y-Values", new GraphViewSeriesStyle(Color.GREEN, 2), new GraphViewData[]{});
        zValue = new GraphViewSeries("Z-Values", new GraphViewSeriesStyle(Color.BLUE, 2), new GraphViewData[]{});
        magnitude = new GraphViewSeries("Mag-Values", new GraphViewSeriesStyle(Color.YELLOW, 2), new GraphViewData[]{});

        graphView = new LineGraphView(this, "PedometerData");

        ((LineGraphView) graphView).setDrawBackground(false);

        graphView.addSeries(xValue); // data
        graphView.addSeries(yValue); // data
        graphView.addSeries(zValue); // data
        // graphView.addSeries(step); // data
        graphView.addSeries(magnitude); // data

        graphView.setViewPort(2, 200);
        graphView.setScalable(true);
        graphView.setShowLegend(true);
        graphView.setManualYAxisBounds(1.5, 0);
        // graphView.setManualYAxisBounds(20, -5);
        graphView.setLegendAlign(LegendAlign.BOTTOM);
        //

        layout = (LinearLayout) findViewById(R.id.graph1);
        layout.addView(graphView);

    }

    /**
     * Gets the random.
     *
     * @return the random
     */
    private double getRandom() {
        double high = 3;
        double low = 0.5;
        return Math.random() * (high - low) + low;
    }

    /**
     * Write test data.
     */
    private void writeTestData() {
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/pedometerdata/");
        if (!dir.exists()) {
            Log.i("", "Create Dir");
            dir.mkdir();
        }
        File file = new File(dir, "pedo-data.txt");

        try {
            PrintWriter pw = new PrintWriter(new FileWriter(file));
            String tmp = outputData.toString().replaceAll("\\[|,|\\]", "");
            pw.write(tmp);
            pw.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        outputData.clear();

        float peak_calibrated = 0;
        float timer_calibrated = 0;
        for (float f : peak_avg) {
            peak_calibrated += f;
        }

        for (float f : timer_avg) {
            timer_calibrated += f;
        }

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        isCalibrated = true;
        editor.putBoolean("calibrated", true);
        editor.putFloat("timerValue", timer_calibrated / timer_avg.size());
        editor.putFloat("peakValue", peak_calibrated / peak_avg.size());

        timerValue = timer_calibrated / timer_avg.size();
        peakValue = peak_calibrated / peak_avg.size();

        editor.commit();

        infoText.setText(" timer " + timer_calibrated / timer_avg.size() + " peak " + peak_calibrated / peak_avg.size());
    }

    /**
     * Read test data.
     */
    private void readTestData() {

        File[] files = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/pedometerdata/").listFiles();
        search_max = true;
        for (File aFile : files) {
            if (!aFile.isDirectory()) {
                Log.i("TAG", aFile.getName());
                buffer.clear();
                if (aFile.getName().equals("pedo-data.txt")) {
                    try {
                        FileInputStream fIn = new FileInputStream(aFile);
                        InputStreamReader isr = new InputStreamReader(fIn);
                        BufferedReader buffreader = new BufferedReader(isr);

                        XTestData.clear();
                        YTestData.clear();
                        ZTestData.clear();
                        TTestData.clear();

                        String readString = buffreader.readLine();
                        while (readString != null) {
                            ;
                            StringTokenizer st = new StringTokenizer(readString, " ");
                            while (st.hasMoreElements()) {

                                double x = Double.parseDouble(st.nextElement().toString());
                                double y = Double.parseDouble(st.nextElement().toString());
                                double z = Double.parseDouble(st.nextElement().toString());
                                double t = Double.parseDouble(st.nextElement().toString());

                                XTestData.add(x);
                                YTestData.add(y);
                                ZTestData.add(z);
                                TTestData.add(t);
                            }

                            readString = buffreader.readLine();
                        }

                        isr.close();
                        fIn.close();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                    break;
                }
            }

        }

        xValue.resetData(new GraphViewData[]{});
        yValue.resetData(new GraphViewData[]{});
        zValue.resetData(new GraphViewData[]{});
        magnitude.resetData(new GraphViewData[]{});
        graphView.setManualYAxisBounds(1.5, 0);
        time = 1d;

        mTimer2 = new Runnable() {
            int i = 0;

            @Override
            public void run() {
                time += 1d;

                xValue.appendData(new GraphViewData(time, XTestData.elementAt(i)), false);
                yValue.appendData(new GraphViewData(time, YTestData.elementAt(i)), false);
                zValue.appendData(new GraphViewData(time, ZTestData.elementAt(i)), false);

                magnitude.appendData(new GraphViewData(time, XTestData.elementAt(i) + YTestData.elementAt(i) + ZTestData.elementAt(i)), true);
                double mag = XTestData.elementAt(i) + YTestData.elementAt(i) + ZTestData.elementAt(i);

                onDetectStep(ZTestData.elementAt(i), i);
                i++;
                if (i >= XTestData.size()) {
                    isRunning = false;
                    mHandler.removeCallbacks(mTimer2);
                } else {
                    isRunning = true;
                    update();
                    mHandler.postDelayed(this, (long) TTestData.elementAt(i).doubleValue());
                }

            }

        };
        mHandler.postDelayed(mTimer2, 2);

    }

    // /**
    // * On detect step.
    // *
    // * @param mag
    // * the mag
    // * @param index
    // * the index
    // */
    // private void onDetectStep(double mag, int index) {
    //
    // localTimer += TTestData.elementAt(index);
    // locIndex++;
    //
    // if (bMax) {
    // if (mag > localMAX) {
    // locIndex = 0;
    // localMAX = mag;
    // } else if (locIndex > 5) {
    // bMax = false;
    // bMin = true;
    // lastMax = System.currentTimeMillis();
    //
    // }
    // } else if (bMin) {
    // if (mag < localMIN) {
    // locIndex = 0;
    // localMIN = mag;
    // } else if (locIndex > 5) {
    // if (!isCalibrated) {
    // if (Math.abs(localMIN - localMAX) > PEAK_MIN && Math.abs(localMIN -
    // localMAX) < PEAK_MAX && localTimer > TIMER_MIN && localTimer < TIMER_MAX) {
    // peak_avg.add((float) Math.abs(localMIN - localMAX));
    // timer_avg.add((float) localTimer);
    // infoText.setText("P: " + (float) Math.abs(localMIN - localMAX) + " t: " +
    // localTimer + " l: " + (System.currentTimeMillis() - lastMax) + " c: " +
    // isCalibrated);
    //
    // }
    // } else {
    // if (Math.abs(localMIN - localMAX) > (peakValue - (peakValue / 3.0)) &&
    // Math.abs(localMIN - localMAX) < (peakValue + (peakValue / 3.0)) &&
    // localTimer > (timerValue - (timerValue / 3.0)) && localTimer < (timerValue
    // + (timerValue / 3.0))) {
    //
    // // dynamic adapt values
    // timerValue = (float) (timerValue * 5.0f + localTimer) / 6.0f;
    // peakValue = (float) ((peakValue * 5.0f + Math.abs(localMIN - localMAX)) /
    // 6.0f);
    //
    // infoText.setText("Peak: " + peakValue + " " + timerValue + " " +
    // (System.currentTimeMillis() - lastMax) + " " + isCalibrated);
    //
    // SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
    // SharedPreferences.Editor editor = settings.edit();
    //
    // editor.putBoolean("calibrated", true);
    // editor.putFloat("timerValue", timerValue);
    // editor.putFloat("peakValue", peakValue);
    //
    // editor.commit();
    // lastStep = System.currentTimeMillis();
    // }
    // }
    //
    // localMAX = 1E-9;
    // localMIN = 1E9;
    // localTimer = 0;
    // locIndex = 0;
    // bMax = true;
    // bMin = false;
    // }
    // }
    // }

    /**
     * On detect step.
     *
     * @param magnitude
     * of all accelerometer data x+y+z for local peak detection
     * @param _time
     * time of the last call to check if the last step occurs not too
     * fast and not too slow
     * <p/>
     * TODO: some hard coded values have to be analyzed.
     */
    // private void onDetectStep(double mag, double _time) {
    //
    // localTimer += _time;
    // locIndex++;
    //
    // if (bMax) {
    // if (mag > localMAX) {
    // if ((bLastStep && (System.currentTimeMillis() - lastStep) < 280) ||
    // (System.currentTimeMillis() - lastStep) < 200) {
    //
    // locIndex = 0;
    // localMAX = mag;
    // bLastStep = false;
    // } else {
    // locIndex = 0;
    // localMAX = mag;
    // }
    // } else if (locIndex > 5) {
    // infoText.setText("Last Step " + (System.currentTimeMillis() - lastStep));
    // bMax = false;
    // bMin = true;
    // lastMax = System.currentTimeMillis();
    // locIndex = 0;
    // }
    // } else {
    // if (mag < localMIN) {
    // locIndex = 0;
    // localMIN = mag;
    // } else if (locIndex > 5) {
    // if (!isCalibrated) {
    // if (Math.abs(localMIN - localMAX) > PEAK_MIN && Math.abs(localMIN -
    // localMAX) < PEAK_MAX && localTimer > TIMER_MIN && localTimer < TIMER_MAX) {
    //
    // if ((System.currentTimeMillis() - lastMax) > 200) {
    //
    // stepText.setText("Steps: " + ++counter);
    // peak_avg.add((float) Math.abs(localMIN - localMAX));
    // timer_avg.add((float) localTimer);
    //
    // bLastStep = true;
    // lastStep = System.currentTimeMillis();
    // }
    // }
    // } else {
    // if (Math.abs(localMIN - localMAX) > (peakValue - (peakValue / 2.0)) &&
    // Math.abs(localMIN - localMAX) < (peakValue + (peakValue / 2.0)) &&
    // localTimer > (timerValue - (timerValue / 2.0)) && localTimer < (timerValue
    // + (timerValue / 2.0))) {
    //
    // if ((System.currentTimeMillis() - lastMax) > 200) {
    //
    // stepText.setText("Steps: " + ++counter);
    //
    // lastStep = System.currentTimeMillis();
    // bLastStep = true;
    // }
    // }
    // }
    // localMAX = 1E-9;
    // localMIN = 1E9;
    // localTimer = 0;
    // locIndex = 0;
    // bMax = true;
    // bMin = false;
    // }
    // }
    // }

    Vector<Double> buffer = new Vector<Double>(10);
    boolean search_max = true;
    long steptimer = 0l;
    double local_max = 0;
    double local_min = 0;

    /**
     * On detect step.
     *
     * @param _time time of the last call to check if the last step occurs not too
     *              fast and not too slow
     *              <p/>
     */
    private void onDetectStep(double mag, double _time) {

        buffer.insertElementAt(mag, 0);
        steptimer += _time;
        if (buffer.size() > 10)
            buffer.remove(buffer.size() - 1);

        double max = (double) Collections.max(buffer);
        double min = (double) Collections.min(buffer);

        int index_max = buffer.indexOf(max);
        int index_min = buffer.indexOf(min);


//        Log.i("PEDO", "" + buffer);
        // search for the peak the local max should be found between 200 and 400 ms
        // (4*50ms and 8*50ms)
        // index_max-index_min has to be positive otherwise it is a min an the peak
        // has to be between 0.05 and 0.2 in the normalized data

        if (Math.abs(index_max - index_min) >= 4 && Math.abs(index_max - index_min) <= 8 && Math.abs(max - min) > 0.05 && Math.abs(max - min) < 0.2) {

            if (search_max && max > local_min) {
                local_max = max;
                buffer.clear();
                buffer.insertElementAt(max, 0);
                search_max = false;
                steptimer = 0;
            } else if (!search_max && min < local_max) {
                stepText.setText("Max: " + ++counter);
                local_min = min;
                buffer.clear();
                buffer.insertElementAt(min, 0);
                search_max = true;
                steptimer = 0;
            }
        }

        if (!search_max && steptimer > 2200) {
            search_max = true;
            steptimer = 0;

        }
    }

    public void save(File fileName, Vector<Double> v) throws IOException {
        System.out.println(fileName);

        FileOutputStream fout = new FileOutputStream(fileName);
        ObjectOutputStream oos = new ObjectOutputStream(fout);
        oos.writeObject(v);
        fout.close();
    }

    public void read(File fileName, Vector<Double> v) throws StreamCorruptedException, IOException, ClassNotFoundException {
        FileInputStream fin = new FileInputStream(fileName);
        ObjectInputStream ois = new ObjectInputStream(fin);
        v = (Vector<Double>) ois.readObject();
        fin.close();

        System.err.println(v);
    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onPause()
     */
    @Override
    protected void onPause() {
        stopScan();
        mHandler.removeCallbacks(mTimer2);
        super.onPause();
    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onResume()
     */
    @Override
    protected void onResume() {
        super.onResume();

    }

    /**
     * Start scan.
     */
    protected void startScan() {

        sensorManager.registerListener(this, accelerometer, 50000);

    }

    /**
     * Stop scan.
     */
    protected void stopScan() {
        sensorManager.unregisterListener(this);

        update();
    }

    /**
     * Update.
     */
    private void update() {
        lastUpdate = new Date().getTime();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * android.hardware.SensorEventListener#onAccuracyChanged(android.hardware
     * .Sensor, int)
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    /**
     * The old_value.
     */
    private float old_value[] = new float[3];

    /**
     * The a.
     */
    private float a = 0.5f;

    /*
     * (non-Javadoc)
     *
     * @see android.hardware.SensorEventListener#onSensorChanged(android.hardware
     * .SensorEvent)
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        long tim = event.timestamp;
        if (startTime == 0) {
            startTime = event.timestamp;

        }
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            System.out.println("" + event.values[0] + " " + event.values[1] + " " + event.values[2] + " " + (tim - last_update) / 1E6 + " \n");
            time += 1d;
            event.values[0] = (float) ((event.values[0] - MIN) / (MAX - MIN));
            event.values[1] = (float) ((event.values[1] - MIN) / (MAX - MIN));
            event.values[2] = (float) ((event.values[2] - MIN) / (MAX - MIN));

            if (old_value[0] != 0) {
                event.values[0] = lowpassFilter(old_value[0], event.values[0]);
                event.values[1] = lowpassFilter(old_value[1], event.values[1]);
                event.values[2] = lowpassFilter(old_value[2], event.values[2]);
            }

            outputData.add("" + event.values[0] + " " + event.values[1] + " " + event.values[2] + " " + (tim - last_update) / 1E6 + " \n");

            xValue.appendData(new GraphViewData(time, event.values[0]), false);
            yValue.appendData(new GraphViewData(time, event.values[1]), false);
            zValue.appendData(new GraphViewData(time, event.values[2]), false);

            magnitude.appendData(new GraphViewData(time, (event.values[2] + event.values[1] + event.values[0])), true);
            // Log.i("PedoTest", " " + event.values[0] + " " + event.values[1]
            // + " " + event.values[2] + " " + lastUpdate);
            onDetectStep(event.values[2], (tim - last_update) / 1E6);

            update();

            last_update = tim;

            old_value[0] = event.values[0];
            old_value[1] = event.values[1];
            old_value[2] = event.values[2];
        }
    }

    /**
     * Lowpass filter.
     *
     * @param oldValue the old value
     * @param newValue the new value
     * @return the float
     */
    private float lowpassFilter(float oldValue, float newValue) {
        return oldValue + a * (newValue - oldValue);
    }
}
