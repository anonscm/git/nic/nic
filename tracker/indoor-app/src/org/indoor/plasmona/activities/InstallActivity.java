/**
 * @author jens maiero
 *
 * @project plasmona
 * 
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.activities;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.indoor.plasmona.R;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * The Class InstallActivity. Beta implementation which download zip files and
 * extract them to the right directory
 */

public class InstallActivity extends ListActivity {

  /** The new projects. */
  static List<String> NEW_PROJECTS;

  /** The Constant DIALOG_DOWNLOAD_PROGRESS. */
  public static final int DIALOG_DOWNLOAD_PROGRESS = 0;

  /** The m progress dialog. */
  private ProgressDialog mProgressDialog;

  /** The Constant URL. */
  private static final String URL = "http://home.inf.fh-rhein-sieg.de/~jmaier2m/ipservice/";

  /** The local path. */
  private static String LOCAL_PATH = "";

  /** The project name. */
  private static String PROJECT_NAME = "";

  /*
   * (non-Javadoc)
   * 
   * @see android.app.Activity#onCreate(android.os.Bundle)
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    LOCAL_PATH = Environment.getExternalStorageDirectory() + "/" + getApplicationContext().getPackageName() + "/";
    NEW_PROJECTS = new LinkedList<String>();
    // / new projects must manually added to the list NEW_PROJECTS and
    NEW_PROJECTS.add("ivc");

    File f = new File(Environment.getExternalStorageDirectory() + "/" + getApplicationContext().getPackageName());
    File[] files = f.listFiles();
    for (int i = 0; i < files.length; i++) {
      NEW_PROJECTS.remove(files[i].getName());
    }

    ListView listView = getListView();
    listView.setTextFilterEnabled(true);

    LayoutInflater inflater = getLayoutInflater();
    View header = (View) inflater.inflate(R.layout.header, listView, false);
    TextView headerValue = (TextView) header.findViewById(R.id.header);
    headerValue.setText(R.string.installproject);

    listView.addHeaderView(header, null, false);

    setListAdapter(new ArrayAdapter<String>(this, R.layout.mainmenu, NEW_PROJECTS));

    listView.setOnItemClickListener(new OnItemClickListener() {
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	PROJECT_NAME = ((TextView) view).getText().toString();

	new DownloadFileAsync().execute(PROJECT_NAME + ".zip");
      }
    });

  }

  /*
   * (non-Javadoc)
   * 
   * @see android.app.Activity#onCreateDialog(int)
   */
  protected Dialog onCreateDialog(int id) {
    switch (id) {
    case DIALOG_DOWNLOAD_PROGRESS:
      mProgressDialog = new ProgressDialog(this);
      mProgressDialog.setMessage("Downloading and extracting project...");
      mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
      mProgressDialog.setCancelable(false);
      mProgressDialog.show();
      return mProgressDialog;
    default:
      return null;
    }
  }

  /**
   * The Class DownloadFileAsync.
   */
  class DownloadFileAsync extends AsyncTask<String, String, String> {

    /*
     * (non-Javadoc)
     * 
     * @see android.os.AsyncTask#onPreExecute()
     */
    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      showDialog(DIALOG_DOWNLOAD_PROGRESS);

    }

    /*
     * (non-Javadoc)
     * 
     * @see android.os.AsyncTask#doInBackground(Params[])
     */
    @Override
    protected String doInBackground(String... aurl) {
      int count;

      try {

	URL url = new URL(URL + aurl[0]);
	URLConnection conexion = url.openConnection();
	conexion.connect();

	int lenghtOfFile = conexion.getContentLength();

	InputStream input = new BufferedInputStream(url.openStream());
	createFolder(LOCAL_PATH);

	if (!fileExists(PROJECT_NAME)) {
	  OutputStream output = new FileOutputStream(LOCAL_PATH + "/" + aurl[0]);

	  byte data[] = new byte[1024];

	  long total = 0;

	  while ((count = input.read(data)) != -1) {
	    total += count;
	    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
	    output.write(data, 0, count);
	  }

	  output.flush();
	  output.close();
	  unpackZip(aurl[0]);

	}
	input.close();

	new File(LOCAL_PATH + aurl[0]).delete();

	finish();

      } catch (Exception e) {
	publishProgress("error");
	finish();
	e.printStackTrace();

      }
      return null;

    }

    /*
     * (non-Javadoc)
     * 
     * @see android.os.AsyncTask#onProgressUpdate(Progress[])
     */
    protected void onProgressUpdate(String... progress) {
      if (progress[0] == "error")
	Toast.makeText(getApplicationContext(), "Please, check your connection settings", Toast.LENGTH_SHORT).show();
      else
	mProgressDialog.setProgress(Integer.parseInt(progress[0]));
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
     */
    @Override
    protected void onPostExecute(String unused) {
      dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
    }

    /**
     * Creates the folder.
     * 
     * @param filepath
     *          the filepath
     * @return true, if successful
     */
    private boolean createFolder(String filepath) {
      File folder = new File(filepath);

      boolean success = false;
      if (!folder.exists()) {
	success = folder.mkdir();
      } else
	success = true;

      return success;
    }

    /**
     * File exists.
     * 
     * @param filepath
     *          the filepath
     * @return true, if successful
     */
    private boolean fileExists(String filepath) {
      File file = new File(LOCAL_PATH, filepath);

      boolean success = false;
      if (file.exists()) {

	success = true;
      } else {
	success = false;

      }

      return success;
    }

    /**
     * Unpack zip.
     * 
     * @param zipname
     *          the zipname
     * @return true, if successful
     */
    private boolean unpackZip(String zipname) {
      InputStream is;
      ZipInputStream zis;
      try {
	String filename;
	is = new FileInputStream(LOCAL_PATH + zipname);

	createFolder(LOCAL_PATH + PROJECT_NAME);
	zis = new ZipInputStream(new BufferedInputStream(is));
	ZipEntry ze;

	byte[] buffer = new byte[1024];
	int count;

	while ((ze = zis.getNextEntry()) != null) {

	  if (ze.isDirectory()) {
	    createFolder(ze.getName());
	  } else {
	    filename = ze.getName();
	    FileOutputStream fout = new FileOutputStream(LOCAL_PATH + filename);

	    while ((count = zis.read(buffer)) != -1) {
	      fout.write(buffer, 0, count);
	    }

	    fout.close();
	    zis.closeEntry();
	  }
	}

	zis.close();
      } catch (IOException e) {
	e.printStackTrace();
	return false;
      }

      return true;
    }
  }

}