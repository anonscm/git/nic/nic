/**
 * @author jens maiero
 *
 * @project plasmona
 * 
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.activities.preferences;

import org.indoor.plasmona.R;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

/**
 * The Class Preferences.
 */
public class Preferences extends PreferenceActivity implements OnSharedPreferenceChangeListener, OnSeekBarChangeListener {

  /** The Constant TAG. */
  private static final String TAG = "Preferences";

  /*
   * (non-Javadoc)
   * 
   * @see android.preference.PreferenceActivity#onCreate(android.os.Bundle)
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Log.i(TAG, "on preferences create");
    addPreferencesFromResource(R.xml.plasmona_prefs);

  }

  /*
   * (non-Javadoc)
   * 
   * @see android.app.Activity#onResume()
   */
  protected void onResume() {
    super.onResume();
    // Set up a listener whenever a key changes
    getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

  }

  /*
   * (non-Javadoc)
   * 
   * @see android.app.Activity#onPause()
   */
  @Override
  protected void onPause() {
    super.onPause();
    // Unregister the listener whenever a key changes
    getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);

  }

  /*
   * (non-Javadoc)
   * 
   * @see android.content.SharedPreferences.OnSharedPreferenceChangeListener#
   * onSharedPreferenceChanged(android.content.SharedPreferences,
   * java.lang.String)
   */
  @Override
  public void onSharedPreferenceChanged(SharedPreferences arg0, String arg1) {
    Log.i(TAG, "" + arg0);

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * android.widget.SeekBar.OnSeekBarChangeListener#onProgressChanged(android
   * .widget.SeekBar, int, boolean)
   */
  @Override
  public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
    Log.i(TAG, "Seekbar Value " + arg0 + " " + arg1 + " " + arg2);

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * android.widget.SeekBar.OnSeekBarChangeListener#onStartTrackingTouch(android
   * .widget.SeekBar)
   */
  @Override
  public void onStartTrackingTouch(SeekBar arg0) {
    // TODO Auto-generated method stub

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * android.widget.SeekBar.OnSeekBarChangeListener#onStopTrackingTouch(android
   * .widget.SeekBar)
   */
  @Override
  public void onStopTrackingTouch(SeekBar arg0) {
    // TODO Auto-generated method stub

  }
}
