/**
 * @author jens maiero
 *
 * @project plasmona
 * 
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.activities.preferences;

import org.indoor.plasmona.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

/**
 * The Class SeekBarPref.
 */
public class SeekBarPref extends Preference implements OnSeekBarChangeListener {

  /** The tag. */
  private final String TAG = getClass().getName();

  /** The Constant ANDROIDNS. */
  private static final String ANDROIDNS = "http://schemas.android.com/apk/res/android";

  /** The Constant PLASMONANS. */
  private static final String PLASMONANS = "http://plasmona.org";

  /** The Constant DEFAULT_VALUE. */
  private static final int DEFAULT_VALUE = 50;

  /** The m max value. */
  private int mMaxValue = 100;

  /** The m min value. */
  private int mMinValue = 0;

  /** The m interval. */
  private int mInterval = 1;

  /** The m current value. */
  private int mCurrentValue;

  /** The m units left. */
  private String mUnitsLeft = "";

  /** The m units right. */
  private String mUnitsRight = "";

  /** The m seek bar. */
  private SeekBar mSeekBar;

  /** The m status text. */
  private TextView mStatusText;

  /**
   * Instantiates a new seek bar pref.
   * 
   * @param context
   *          the context
   * @param attrs
   *          the attrs
   */
  public SeekBarPref(Context context, AttributeSet attrs) {
    super(context, attrs);
    initPreference(context, attrs);
  }

  /**
   * Instantiates a new seek bar pref.
   * 
   * @param context
   *          the context
   * @param attrs
   *          the attrs
   * @param defStyle
   *          the def style
   */
  public SeekBarPref(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    initPreference(context, attrs);
  }

  /**
   * Inits the preference.
   * 
   * @param context
   *          the context
   * @param attrs
   *          the attrs
   */
  private void initPreference(Context context, AttributeSet attrs) {
    setValuesFromXml(attrs);
    mSeekBar = new SeekBar(context, attrs);
    mSeekBar.setMax(mMaxValue - mMinValue);
    mSeekBar.setOnSeekBarChangeListener(this);
  }

  /**
   * Sets the values from xml.
   * 
   * @param attrs
   *          the new values from xml
   */
  private void setValuesFromXml(AttributeSet attrs) {
    mMaxValue = attrs.getAttributeIntValue(ANDROIDNS, "max", 100);
    mMinValue = attrs.getAttributeIntValue(PLASMONANS, "min", 0);

    mUnitsLeft = getAttributeStringValue(attrs, PLASMONANS, "unitsLeft", "");
    String units = getAttributeStringValue(attrs, PLASMONANS, "units", "");
    mUnitsRight = getAttributeStringValue(attrs, PLASMONANS, "unitsRight", units);

    try {
      String newInterval = attrs.getAttributeValue(PLASMONANS, "interval");
      if (newInterval != null)
	mInterval = Integer.parseInt(newInterval);
    } catch (Exception e) {
      Log.e(TAG, "Invalid interval value", e);
    }

  }

  /**
   * Gets the attribute string value.
   * 
   * @param attrs
   *          the attrs
   * @param namespace
   *          the namespace
   * @param name
   *          the name
   * @param defaultValue
   *          the default value
   * @return the attribute string value
   */
  private String getAttributeStringValue(AttributeSet attrs, String namespace, String name, String defaultValue) {
    String value = attrs.getAttributeValue(namespace, name);
    if (value == null)
      value = defaultValue;

    return value;
  }

  /*
   * (non-Javadoc)
   * 
   * @see android.preference.Preference#onCreateView(android.view.ViewGroup)
   */
  @Override
  protected View onCreateView(ViewGroup parent) {

    RelativeLayout layout = null;

    try {
      LayoutInflater mInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

      layout = (RelativeLayout) mInflater.inflate(R.layout.seekbar_preference, parent, false);
    } catch (Exception e) {
      Log.e(TAG, "Error creating seek bar preference", e);
    }

    return layout;

  }

  /*
   * (non-Javadoc)
   * 
   * @see android.preference.Preference#onBindView(android.view.View)
   */
  @Override
  public void onBindView(View view) {
    super.onBindView(view);

    try {
      // move our seekbar to the new view we've been given
      ViewParent oldContainer = mSeekBar.getParent();
      ViewGroup newContainer = (ViewGroup) view.findViewById(R.id.seekBarPrefBarContainer);

      if (oldContainer != newContainer) {
	// remove the seekbar from the old view
	if (oldContainer != null) {
	  ((ViewGroup) oldContainer).removeView(mSeekBar);
	}
	// remove the existing seekbar (there may not be one) and add ours
	newContainer.removeAllViews();
	newContainer.addView(mSeekBar, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
      }
    } catch (Exception ex) {
      Log.e(TAG, "Error binding view: " + ex.toString());
    }

    updateView(view);
  }

  /**
   * Update a SeekBarPreference view with our current state.
   * 
   * @param view
   *          the view
   */
  protected void updateView(View view) {

    try {
      RelativeLayout layout = (RelativeLayout) view;

      mStatusText = (TextView) layout.findViewById(R.id.seekBarPrefValue);
      mStatusText.setText(String.valueOf(mCurrentValue));
      mStatusText.setMinimumWidth(30);

      mSeekBar.setProgress(mCurrentValue - mMinValue);

      TextView unitsRight = (TextView) layout.findViewById(R.id.seekBarPrefUnitsRight);
      unitsRight.setText(mUnitsRight);

      TextView unitsLeft = (TextView) layout.findViewById(R.id.seekBarPrefUnitsLeft);
      unitsLeft.setText(mUnitsLeft);

    } catch (Exception e) {
      Log.e(TAG, "Error updating seek bar preference", e);
    }

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * android.widget.SeekBar.OnSeekBarChangeListener#onProgressChanged(android
   * .widget.SeekBar, int, boolean)
   */
  @Override
  public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
    int newValue = progress + mMinValue;

    if (newValue > mMaxValue)
      newValue = mMaxValue;
    else if (newValue < mMinValue)
      newValue = mMinValue;
    else if (mInterval != 1 && newValue % mInterval != 0)
      newValue = Math.round(((float) newValue) / mInterval) * mInterval;

    // change rejected, revert to the previous value
    if (!callChangeListener(newValue)) {
      seekBar.setProgress(mCurrentValue - mMinValue);
      return;
    }

    // change accepted, store it
    mCurrentValue = newValue;
    mStatusText.setText(String.valueOf(newValue));
    persistInt(newValue);

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * android.widget.SeekBar.OnSeekBarChangeListener#onStartTrackingTouch(android
   * .widget.SeekBar)
   */
  @Override
  public void onStartTrackingTouch(SeekBar seekBar) {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * android.widget.SeekBar.OnSeekBarChangeListener#onStopTrackingTouch(android
   * .widget.SeekBar)
   */
  @Override
  public void onStopTrackingTouch(SeekBar seekBar) {
    notifyChanged();
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * android.preference.Preference#onGetDefaultValue(android.content.res.TypedArray
   * , int)
   */
  @Override
  protected Object onGetDefaultValue(TypedArray ta, int index) {

    int defaultValue = ta.getInt(index, DEFAULT_VALUE);
    return defaultValue;

  }

  /*
   * (non-Javadoc)
   * 
   * @see android.preference.Preference#onSetInitialValue(boolean,
   * java.lang.Object)
   */
  @Override
  protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {

    if (restoreValue) {
      mCurrentValue = getPersistedInt(mCurrentValue);
    } else {
      int temp = 0;
      try {
	temp = (Integer) defaultValue;
      } catch (Exception ex) {
	Log.e(TAG, "Invalid default value: " + defaultValue.toString());
      }

      persistInt(temp);
      mCurrentValue = temp;
    }

  }

}
