/**
 * @author jens maiero
 *
 * @project plasmona
 * 
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.activities;

import java.io.File;

import org.indoor.plasmona.R;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * The Class ProjectsActivity.
 * 
 * @author jens
 * 
 *         ProjectsAct based on a listview - you can select project which are on
 *         the sdcards folder (org.indoor.plasmona). On the other hand you can
 *         go to the install activity, which can install new project from a
 *         server. (beta) On long click you can also delete projects.
 */

public class ProjectsActivity extends ListActivity {

  /** The projects. */
  private String[] PROJECTS;

  /*
   * (non-Javadoc)
   * 
   * @see android.app.Activity#onCreate(android.os.Bundle)
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);

    ListView listView = getListView();

    listView.setTextFilterEnabled(true);

    LayoutInflater inflater = getLayoutInflater();

    View header = (View) inflater.inflate(R.layout.header, listView, false);

    TextView headerValue = (TextView) header.findViewById(R.id.header);
    headerValue.setText(R.string.projectbrowser);

    listView.addHeaderView(header, null, false);

    updateList();

    listView.setOnItemClickListener(new OnItemClickListener() {
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	if (((TextView) view).getText().toString().equals(PROJECTS[0])) {
	  Intent i = new Intent("org.indoor.plasmona.activities.InstallActivity");
	  startActivity(i);
	} else {

	  open(((TextView) view).getText().toString());
	}
      }
    });

    registerForContextMenu(listView);

  }

  /*
   * (non-Javadoc)
   * 
   * @see android.app.Activity#onCreateContextMenu(android.view.ContextMenu,
   * android.view.View, android.view.ContextMenu.ContextMenuInfo)
   */
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {

    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
    if (info.position > 1) {
      menu.setHeaderTitle(PROJECTS[info.position - 1]);
      String[] menuItems = getResources().getStringArray(R.array.popupmenu);
      for (int i = 0; i < menuItems.length; i++) {
	menu.add(Menu.NONE, i, i, menuItems[i]);
      }
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see android.app.Activity#onContextItemSelected(android.view.MenuItem)
   */
  @Override
  public boolean onContextItemSelected(MenuItem item) {
    AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
    switch (item.getItemId()) {
    case 0:
      open(PROJECTS[info.position - 1]);
      return true;
    case 1:
      delete(new File(Environment.getExternalStorageDirectory() + "/" + getApplicationContext().getPackageName() + "/" + PROJECTS[info.position - 1]));
      return true;
    default:
      return super.onContextItemSelected(item);
    }
  }

  /**
   * Delete.
   * 
   * @param project
   *          the project
   */
  private void delete(File project) {

    if (project.isDirectory())
      for (File child : project.listFiles())
	delete(child);

    project.delete();
    updateList();
  }

  /**
   * Open.
   * 
   * @param project
   *          the project
   */
  private void open(String project) {

    Intent i = new Intent("org.indoor.plasmona.activities.PlasmonaActivity");
    i.putExtra("project", project);
    startActivity(i);
  }

  /**
   * check the sdcard if there are already some projects installed and add the
   * given projects to the list PROJECTS -> listview the first entry in PROJECTS
   * refers to Install...
   */
  private void updateList() {

    String state = Environment.getExternalStorageState();

    if (!Environment.MEDIA_MOUNTED.equals(state)) {
      sdcardAlert();
      Log.e(getClass().getName(), "Storage State: " + state);
    }

    File f = new File(Environment.getExternalStorageDirectory() + "/" + getApplicationContext().getPackageName());
    createFolder(f.toString());
    File[] files = f.listFiles();

    PROJECTS = new String[files.length + 1];
    PROJECTS[0] = "Install...";
    for (int i = 0; i < files.length; i++) {
      // if (files[i].isDirectory())
      PROJECTS[i + 1] = files[i].getName();
    }
    setListAdapter(new ArrayAdapter<String>(this, R.layout.mainmenu, PROJECTS));
  }

  /**
   * Creates the folder.
   * 
   * @param filepath
   *          the filepath
   * @return true on success
   * 
   *         Creates a folder of the given Filepath. Only creates the folder if
   *         the folder does not exists
   */
  private boolean createFolder(String filepath) {

    File folder = new File(filepath);
    boolean success = false;
    if (!folder.exists()) {
      success = folder.mkdir();
    } else
      success = true;

    return success;
  }

  /**
   * Alert Dialog if the sdcards is not mounted and exit.
   */
  public void sdcardAlert() {

    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setCancelable(true);
    builder.setTitle("SD Card not Mounted \n Restart?");
    builder.setInverseBackgroundForced(true);
    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
	dialog.dismiss();
	Intent intent = new Intent(Intent.ACTION_MAIN);
	intent.addCategory(Intent.CATEGORY_HOME);
	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	startActivity(intent);
      }
    });
    AlertDialog alert = builder.create();
    alert.show();
  }

  /*
   * (non-Javadoc)
   * 
   * @see android.app.Activity#onRestart()
   */
  @Override
  protected void onRestart() {
    super.onRestart();
    updateList();
  }

  /*
   * (non-Javadoc)
   * 
   * @see android.app.Activity#onResume()
   */
  @Override
  protected void onResume() {
    super.onResume();
    updateList();
  }
}