package org.indoor.plasmona.views.map;

import org.indoor.plasmona.views.MapView;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

// TODO: Auto-generated Javadoc
/**
 * The Class MapThread.
 */
public class MapThread extends Thread {

	/** The is running. */
	private boolean isRunning = false;

	/** The m holder. */
	private SurfaceHolder mHolder = null;

	/** The m map view. */
	private MapView mMapView = null;

	/**
	 * Instantiates a new map thread.
	 *
	 * @param _holder the _holder
	 * @param _mapView the _map view
	 */
	public MapThread(SurfaceHolder _holder, MapView _mapView) {

		mHolder = _holder;
		mMapView = _mapView;
	}

	/**
	 * Sets the running.
	 *
	 * @param _run the new running
	 */
	public void setRunning(boolean _run) {

		isRunning = _run;
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run() {

		Canvas c;
		while (isRunning) {
			c = null;
			try {
				synchronized (mHolder) {
					if (mHolder != null) {
						c = mHolder.lockCanvas(null);
						if (c != null) {

							mMapView.doDraw(c);
						}
					}
				}
			} finally {
				// do this in a finally so that if an exception is thrown
				// during the above, we don't leave the Surface in an
				// inconsistent state
				if (c != null) {
					mHolder.unlockCanvasAndPost(c);
				}
			}
		}

	}

	/**
	 * Gets the surface holder.
	 *
	 * @return the surface holder
	 */
	public SurfaceHolder getSurfaceHolder() {

		return mHolder;
	}

}