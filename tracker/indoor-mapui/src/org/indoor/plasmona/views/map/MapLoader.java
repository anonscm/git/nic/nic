package org.indoor.plasmona.views.map;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.indoor.plasmona.models.MapData;
import org.indoor.plasmona.utils.MessageCodes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class MapLoader.
 */
public class MapLoader {

	/**
	 * Load map xml.
	 *
	 * @param _messageHandler the _message handler
	 * @param _file the _file
	 */
	public static void loadMapXML(Handler _messageHandler, File _file) {

		MapData mMapData = null;

		if (_file.getName().equals("mapdef.xml")) {
			try {

				SAXParserFactory spf = SAXParserFactory.newInstance();
				SAXParser sp = spf.newSAXParser();
				XMLReader xr = sp.getXMLReader();
				XMLMapParser xmlParser = new XMLMapParser();
				xr.setContentHandler(xmlParser);
				FileInputStream fis = new FileInputStream(_file);
				InputSource inputSource = new InputSource(fis);
				xr.parse(inputSource);
				mMapData = xmlParser.getMapData();
				mMapData.setRootDir(_file.getParentFile());

			} catch (SAXException e) {
				Log.e(MapLoader.class.getSimpleName(), e.toString());
			} catch (ParserConfigurationException e) {
				Log.e(MapLoader.class.getSimpleName(), e.toString());
			} catch (IOException e) {
				Log.e(MapLoader.class.getSimpleName(), e.toString());
			} catch (NumberFormatException e) {
				Log.e(MapLoader.class.getSimpleName(), e.toString());
			}

			Message msg = new Message();
			msg.setTarget(_messageHandler);
			msg.what = MessageCodes.MAP_LOADED;
			msg.obj = mMapData;
			msg.sendToTarget();

		}
	}
}
