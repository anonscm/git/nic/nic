package org.indoor.plasmona.views.map;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.indoor.plasmona.models.MapData;
import org.indoor.plasmona.models.Tile;
import org.indoor.plasmona.models.Tile.meState;
import org.indoor.plasmona.models.TileLayer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class TileController.
 */
public class TileController implements Observer {

	/** The m bounds. */
	private Rect mBounds;

	/** The m data. */
	private MapData mData = null;

	/** The m zip file. */
	private ZipFile mZipFile = null;

	/** The m file. */
	private File mFile = null;

	/** The m all tiles. */
	ArrayList<ArrayList<Tile>> mAllTiles;

	/** The m layer. */
	TileLayer mLayer = null;

	/** The m scale factor. */
	float mScaleFactor = 1.0f;

	/** The m image. */
	private Point mImage = new Point();

	/** The m number tile. */
	private Point mNumberTile = new Point();

	/** x = width and y = height. */
	private Point mTileSize;

	/** The tile size h. */
	int tileSizeH;

	/** The bitmap opts. */
	public static BitmapFactory.Options BITMAP_OPTS = new BitmapFactory.Options();

	static {
		BITMAP_OPTS.inPreferredConfig = Bitmap.Config.RGB_565;
		BITMAP_OPTS.inTempStorage = new byte[16 * 1024];
	}

	/**
	 * Instantiates a new tile controller.
	 */
	public TileController() {
		mTileSize = new Point();
		mImage = new Point();
		mNumberTile = new Point();

	}

	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	public void update(Observable observable, Object data) {
		mLayer = null;
		mFile = null;
		mZipFile = null;
	}

	/**
	 * Calculate.
	 *
	 * @param _canvas the _canvas
	 * @param _data the _data
	 */
	public void calculate(Canvas _canvas, MapData _data) {
		mBounds = _canvas.getClipBounds();
		mData = _data;

		Rect bounds = mBounds;

		if (mLayer == null) {

			mLayer = mData.getLayerToRenderer();
			mAllTiles = mLayer.getAllTiles();

			mImage.x = (int) (mLayer.getImageWidth());
			mImage.y = (int) (mLayer.getImageHeight());

			mTileSize.x = (int) (mLayer.getTileWidth());
			mTileSize.y = (int) (mLayer.getTileHeight());

			mNumberTile.x = (int) Math.ceil((float) mImage.x
					/ (float) mTileSize.x);
			mNumberTile.y = (int) Math.ceil((float) mImage.y
					/ (float) mTileSize.y);

		}

		int searchStartW = ((bounds.left / mTileSize.x) <= 0) ? 1
				: (bounds.left / mTileSize.x);
		int searchStartH = ((bounds.top / mTileSize.y) <= 0) ? 1
				: (bounds.top / mTileSize.y);

		int searchEndW = ((bounds.right / mTileSize.x) >= mNumberTile.x) ? mNumberTile.x - 1
				: (bounds.right / mTileSize.x);
		int searchEndH = ((bounds.bottom / mTileSize.y) >= mNumberTile.y) ? mNumberTile.y - 1
				: (bounds.bottom / mTileSize.y);

		Tile lTile = new Tile();

		mLayer.getLoadedTiles().clear();

		if (mFile == null) {
			mFile = new File(mData.getRootDir(), mLayer.getZip());
		}
		if (mZipFile == null) {
			try {
				mZipFile = new ZipFile(mFile);

			} catch (ZipException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

		for (int i = searchStartW - 1; i < searchEndW + 1; i++) {

			ArrayList<Tile> lList = mAllTiles.get(i);

			for (int j = searchStartH - 1; j < searchEndH + 1; j++) {

				lTile = lList.get(j);
				if (Rect.intersects(lTile.mRect, bounds)) {
					try {

						if (lTile.getTileState() == meState.IN_CACHE) {

							InputStream is = mZipFile.getInputStream(mZipFile
									.getEntry(lTile.mName));
							lTile.mBitmap = BitmapFactory.decodeStream(is,
									null, BITMAP_OPTS);

							lTile.setTileState(meState.IN_MEMORY);

						}
						mLayer.getLoadedTiles().add(lTile);

					} catch (OutOfMemoryError e) {
						Log.e("TileStreamer", e.toString());
					} catch (Exception e) {
						Log.e("TileStreamer", e.toString());

					}

				} else if (!Rect.intersects(lTile.mRect, bounds)) {

					if (lTile.getTileState() == meState.IN_MEMORY) {
						lTile.setTileState(meState.IN_CACHE);
						mLayer.getLoadedTiles().remove(lTile);
						lTile.mBitmap.recycle();
						lTile.mBitmap = null;

					}
				}
			}
		}
	}
}
