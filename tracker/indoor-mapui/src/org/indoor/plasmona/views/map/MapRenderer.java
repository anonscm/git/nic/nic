package org.indoor.plasmona.views.map;

import java.util.List;

import android.util.Log;
import org.indoor.plasmona.models.MapData;
import org.indoor.plasmona.models.Tile;
import org.indoor.plasmona.models.TileLayer;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;



// TODO: Auto-generated Javadoc
/**
 * The Class MapRenderer.
 */
public class MapRenderer {

	/** The m canvas. */
	Canvas mCanvas;

	/** The m map data. */
	MapData mMapData;

	/** The controller. */
	TileController controller;

	/**
	 * Instantiates a new map renderer.
	 *
	 * @param _controller the _controller
	 */
	public MapRenderer(TileController _controller) {
		controller = _controller;
	}

	/**
	 * On draw.
	 *
	 * @param _canvas the _canvas
	 * @param _data the _data
	 */
	public void onDraw(Canvas _canvas, MapData _data) {

		this.mCanvas = _canvas;
		this.mMapData = _data;

		controller.calculate(mCanvas, mMapData);

		Paint paint = new Paint();
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(1.1f);
		paint.setAntiAlias(true);
		// make the entire canvas white
		paint.setColor(Color.YELLOW);
//		 long time = SystemClock.currentThreadTimeMillis();
		TileLayer layer = mMapData.getLayerToRenderer();
//		Log.i("","Rendering layer "+ layer.getLayerId()+ " "+layer.getZip());
		List<Tile> tiles = layer.getLoadedTiles();
		// for (int i = 0; i < tiles.size(); i++) {
//		 Log.i("", "render "+tiles.size());
		for (Tile t : tiles) {
			// Tile t = tiles.get(i);
			mCanvas.drawBitmap(t.mBitmap, null, t.mRect, null);
//			 mCanvas.drawRect(t.mRect, paint);

//			 mCanvas.drawText(t.mName, t.mRect.centerX(), t.mRect.centerY(),paint);
		}


//		mCanvas.drawCircle(mCanvas.getClipBounds().centerX(), mCanvas.getClipBounds().centerY(), 25, paint);
//		 Log.i("PlMapView",
//		 "Time for Draw"
//		 + String.valueOf(SystemClock.currentThreadTimeMillis()
//		 - time) + " ms.");
	}

}
