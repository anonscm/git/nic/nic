package org.indoor.plasmona.views.map;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.indoor.plasmona.models.MapData;
import org.indoor.plasmona.models.TileLayer;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.graphics.PointF;

// TODO: Auto-generated Javadoc
/**
 * The Class XMLMapParser.
 */
public class XMLMapParser extends DefaultHandler {

	/** The tile layers. */
	private Map<Integer, ArrayList<TileLayer>> tileLayers;

	/** The map data. */
	private MapData mapData = null;

	/** The floor. */
	int floor;

	/**
	 * Instantiates a new xML map parser.
	 */
	public XMLMapParser() {

		mapData = new MapData();
		tileLayers = new HashMap<Integer, ArrayList<TileLayer>>();

	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#startDocument()
	 */
	@Override
	public void startDocument() throws SAXException {

		super.startDocument();
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endDocument()
	 */
	@Override
	public void endDocument() throws SAXException {

		super.endDocument();
		mapData.setTileLayers(tileLayers);

	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		if (localName.equals("Info")) {

			mapData.setProjectName(attributes.getValue("Name"));
			mapData.setDescription(attributes.getValue("Description"));
			mapData.setFloor(Integer.parseInt(attributes
					.getValue("DefaultFloor")));
			mapData.setRealScale(Float.parseFloat(attributes
					.getValue("RealScale")));
			mapData.setScale(Float.parseFloat(attributes.getValue("Scale")));
		} else if (localName.equals("DefaultPosition")) {

			int x = Integer.parseInt(attributes.getValue("x"));
			int y = Integer.parseInt(attributes.getValue("y"));
			mapData.setPosition(new PointF((float) x, (float) y));
		} else if (localName.equals("Tiles")) {
			floor = Integer.parseInt(attributes.getValue("floor"));
			ArrayList<TileLayer> layer = new ArrayList<TileLayer>();
			tileLayers.put(floor, layer);

		} else if (localName.equals("Tile")) {

			TileLayer t = new TileLayer();
			t.setLayerId(Integer.parseInt(attributes.getValue("layer")));
			t.setLevelScale(Float.parseFloat(attributes.getValue("zoomFactor")));
			t.setZip(attributes.getValue("zip"));
			t.setImageWidth(Integer.parseInt(attributes.getValue("imageWidth")));
			t.setImageHeight(Integer.parseInt(attributes
					.getValue("imageHeight")));
			t.setTileWidth(Integer.parseInt(attributes.getValue("tileWidth")));
			t.setTileHeight(Integer.parseInt(attributes.getValue("tileHeight")));
			t.genTiles();

			tileLayers.get(floor).add(t);

		}

		super.startElement(uri, localName, qName, attributes);

	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		super.endElement(uri, localName, qName);

		if (localName.equals("Tiles")) {
			Collections.sort(tileLayers.get(floor), new Comparator<TileLayer>() {

				public int compare(TileLayer o1, TileLayer o2) {
					if (o1.getLayerId() < o2.getLayerId()) {
						return -1;
					} else if (o1.getLayerId() == o2.getLayerId()) {
						return 0;
					} else
						return 1;
				}
			});

			mapData.setLayer(tileLayers.get(floor).size() - 1);
		}

	}

	/**
	 * Gets the map data.
	 *
	 * @return the map data
	 */
	public MapData getMapData() {
		return mapData;
	}

	/**
	 * Sets the map data.
	 *
	 * @param mapData the new map data
	 */
	public void setMapData(MapData mapData) {
		this.mapData = mapData;
	}

}
