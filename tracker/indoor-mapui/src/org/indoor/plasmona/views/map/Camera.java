package org.indoor.plasmona.views.map;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;

// TODO: Auto-generated Javadoc
/**
 * The Class Camera.
 */
public class Camera extends Matrix {

	/** The position. */
	PointF position = new PointF();

	/** The scale. */
	PointF scale = new PointF(1.0f, 1.0f);
	// PointF center = new PointF(0, 0);
	/** The axis. */
	PointF axis = new PointF(0, 0);
	// Point screen = new Point(0, 0);
	/** The degrees. */
	float degrees = 0;

	/**
	 * Move.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 */
	public void move(float x, float y) {
		position.x += x;
		position.y += y;
		update();
	}

	/**
	 * Gets the position.
	 * 
	 * @return the position
	 */
	public PointF getPosition() {
		return position;
	}

	/**
	 * Look at.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 */
	public void lookAt(float x, float y) {
		position.x = x;
		position.y = y;
		update();

	}

	// public void setScreenSize(int x, int y) {
	// screen.x = x;
	// screen.y = y;
	// }
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.graphics.Matrix#setScale(float, float)
	 */
	public void setScale(float x, float y) {
		scale.x = x;
		scale.y = y;
		update();
	}

	/**
	 * Gets the scale.
	 * 
	 * @return the scale
	 */
	public PointF getScale() {

		return scale;
	}

	// public void setCenter(int x, int y) {
	//
	// center.x = x;
	// center.y = y;
	// update();
	// }

	/**
	 * Scale and move.
	 * 
	 * @param scaleY
	 *            the scale y
	 * @param scaleX
	 *            the scale x
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 */
	public void scaleAndMove(float scaleY, float scaleX, float x, float y) {

		scale.x = scaleX;
		scale.y = scaleY;

		position.x += x;
		position.y += y;

		update();
	}

	/**
	 * Rotate.
	 * 
	 * @param degrees
	 *            the degrees
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 */
	public void rotate(float degrees, int x, int y) {

		this.degrees = degrees;
		this.axis.x = x;
		this.axis.y = x;
		update();
	}

	/**
	 * Update.
	 * 
	 * @param canvas
	 *            the canvas
	 */
	public void update(Canvas canvas) {

		canvas.setMatrix(this);
	}

	/**
	 * Update.
	 */
	private void update() {

		reset();
		// preTranslate(-center.x, -center.y);
		postScale(scale.x, scale.y);
		postTranslate(position.x, position.y);
	}

}
