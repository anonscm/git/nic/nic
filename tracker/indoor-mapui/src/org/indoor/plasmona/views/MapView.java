package org.indoor.plasmona.views;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.prefs.Preferences;

import org.indoor.plasmona.layer.CustomLayerController;
import org.indoor.plasmona.layer.LayerRenderer;
import org.indoor.plasmona.layer.TrackerLayerImpl;
import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.models.MapData;
import org.indoor.plasmona.models.TileLayer;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.Prefs;
import org.indoor.plasmona.views.map.Camera;
import org.indoor.plasmona.views.map.MapRenderer;
import org.indoor.plasmona.views.map.MapThread;
import org.indoor.plasmona.views.map.TileController;
import org.plasmona.indoor.mapui.R;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

// TODO: Auto-generated Javadoc

/**
 * The Class MapView.
 */
public class MapView extends SurfaceView implements SurfaceHolder.Callback {

    /**
     * The mode.
     */
    private int mode = 0;

    /**
     * The pos.
     */
    private PointF pos = new PointF();

    /**
     * The last_pos.
     */
    private PointF last_pos = new PointF();

    /**
     * The size.
     */
    private Point size = new Point();

    /**
     * The screen pos.
     */
    private PointF screenPos = new PointF();

    /**
     * The longpress timer.
     */
    private Timer longpressTimer = new Timer();

    /**
     * The longpress listener.
     */
    private OnLongpressListener longpressListener;

    /**
     * The longpress threshold.
     */
    private static int LONGPRESS_THRESHOLD = 500;

    /**
     * The longpress movement.
     */
    private static float LONGPRESS_MOVEMENT = 8.0f;

    private Point old_screen_size;

    /**
     * The orientation.
     */
    private int orientation = -1;

    /**
     * The active pointer.
     */
    private int activePointer = -1;

    /**
     * The m previous layer.
     */
    private int mPreviousLayer = -1;

    /**
     * The scale factor.
     */
    private float localScale = 1.0f;

    /**
     * The distance.
     */
    private float distance = 0.0f;

    /**
     * The raw_x.
     */
    private float raw_x = 0;

    /**
     * The raw_y.
     */
    private float raw_y = 0;

    /**
     * The zoom factor.
     */
    private float levelScale = -1;

    /**
     * The cross hair mode.
     */
    private boolean crossHairMode = false;

    /**
     * The is moving.
     */
    private boolean isMoving = false;

    /**
     * The continued.
     */
    private boolean continued;

    /**
     * The orient changed.
     */
    private boolean orientChanged;

    /**
     * The camera.
     */
    private Camera camera = new Camera();

    /**
     * The map data.
     */
    private MapData mapData;

    /**
     * The selection listener.
     */
    private OnSelectedItem selectionListener;

    /**
     * The map thread.
     */
    private MapThread mapThread = null;

    /**
     * The renderer.
     */
    private MapRenderer renderer = null;

    /**
     * The controller.
     */
    private TileController controller;

    /**
     * The scale detector.
     */
    private ScaleGestureDetector scaleDetector;

    /**
     * The history.
     */
    public ArrayList<PointF> history;

    /**
     * The paint.
     */
    Paint paint = new Paint();

    /**
     * The selected point.
     */
    GeoPoint selectedPoint = null;

    /**
     * The geopnt.
     */
    private GeoPoint geopnt;

    private Bitmap bitmapCompass;

    private int compassAngle = 0;

    private GeoPoint userPos;

    TrackerLayerImpl user;


    private boolean restored = false;

    public boolean isRestored() {
        return restored;
    }

    public void setRestored(boolean restored) {
        this.restored = restored;
    }

    /**
     * Instantiates a new map view.
     *
     * @param _context the _context
     */
    public MapView(Context _context) {

        super(_context);
        init(_context);
    }

    /**
     * Instantiates a new map view.
     *
     * @param _context the _context
     * @param _attrs   the _attrs
     */
    public MapView(Context _context, AttributeSet _attrs) {

        super(_context, _attrs);
        init(_context);
    }

    /**
     * Instantiates a new map view.
     *
     * @param _context  the _context
     * @param _attrs    the _attrs
     * @param _defStyle the _def style
     */
    public MapView(Context _context, AttributeSet _attrs, int _defStyle) {

        super(_context, _attrs, _defStyle);
        init(_context);
    }

    /**
     * The Interface OnSelectedItem.
     */
    public interface OnSelectedItem {

        /**
         * On selection.
         *
         * @param geo  the geo
         * @param fire the fire
         */
        public void onSelection(GeoPoint geo, boolean fire);
    }

    /**
     * Sets the on selection listener.
     *
     * @param listener the new on selection listener
     */
    public void setOnSelectionListener(OnSelectedItem listener) {

        selectionListener = listener;
    }

    /**
     * Inits the.
     *
     * @param context the context
     */
    private void init(Context context) {

        old_screen_size = new Point(0, 0);
        camera = new Camera();
        setCamera(camera);

        userPos = new GeoPoint(0, 0, "user");
        int resID2 = ApplicationContext
                .get()
                .getResources()
                .getIdentifier("ips_mee", "drawable",
                        ApplicationContext.get().getPackageName());
        userPos.setIcon(BitmapFactory.decodeResource(ApplicationContext.get()
                .getResources(), resID2));

        user = new TrackerLayerImpl();
        user.setName("User");
        user.add(userPos);

        CustomLayerController.getInstance().updateLayerByName("User", user);



        GeoPoint userPos_corrected = new GeoPoint(0, 0, "user_corrected");
        int resID3 = ApplicationContext
                .get()
                .getResources()
                .getIdentifier("ips_circle", "drawable",
                        ApplicationContext.get().getPackageName());
        userPos_corrected.setIcon(BitmapFactory.decodeResource(ApplicationContext.get()
                .getResources(), resID3));
        CustomLayerController.getInstance().findLayerByName("User").add(userPos_corrected);

        user.add(userPos_corrected);






        controller = new TileController();
        mapThread = new MapThread(getHolder(), this);
        scaleDetector = new ScaleGestureDetector(context, new ScaleListener());
        history = new ArrayList<PointF>();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2.1f);
        paint.setAntiAlias(true);
        // make the entire canvas white
        paint.setColor(Color.RED);
        getHolder().addCallback(this);

    }

    /**
     * Inits the.
     */
    public void init() {
        int resID = ApplicationContext
                .get()
                .getResources()
                .getIdentifier("ips_compass", "drawable",
                        ApplicationContext.get().getPackageName());
        bitmapCompass = BitmapFactory.decodeResource(ApplicationContext.get()
                .getResources(), resID);

        if (renderer == null) {
            renderer = new MapRenderer(controller);
            mapData.addObserver(controller);

            camera.lookAt(mapData.getPosition().x, mapData.getPosition().y);
            pos.x = mapData.getPosition().x;
            pos.y = mapData.getPosition().y;
            camera.setScale(localScale, localScale);

            levelScale = mapData.getLayerToRenderer().getLevelScale();

        }

        findBestLayer();

    }

    // Define the interface we will interact with from our Map

    /**
     * The listener interface for receiving onLongpress events. The class that
     * is interested in processing a onLongpress event implements this
     * interface, and the object created with that class is registered with a
     * component using the component's
     * <code>addOnLongpressListener<code> method. When
     * the onLongpress event occurs, that object's appropriate
     * method is invoked.
     */
    public interface OnLongpressListener {

        /**
         * On longpress.
         *
         * @param pnt the pnt
         */
        public void onLongpress(GeoPoint pnt);
    }

    /**
     * Sets the on longpress listener.
     *
     * @param listener the new on longpress listener
     */
    public void setOnLongpressListener(MapView.OnLongpressListener listener) {

        longpressListener = listener;
    }

    /**
     * Start long click timer.
     */
    private void startLongClickTimer() {

        longpressTimer = new Timer();
        longpressTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                setCrossHairMode(true);

            }

        }, LONGPRESS_THRESHOLD);

    }

    /*
     * (non-Javadoc)
     *
     * @see android.view.View#onTouchEvent(android.view.MotionEvent)
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getPointerCount() > 1)
            longpressTimer.cancel();

        scaleDetector.onTouchEvent(event);
        mode = ApplicationContext.get().getSharedPreferences(Prefs.PLASMONA, 0)
                .getInt(Prefs.NAV_MODE, 0);

        if (!scaleDetector.isInProgress() && event.getPointerCount() == 1) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    startLongClickTimer();

                    //     activePointer = event.getPointerId(0);
                    last_pos.x = event.getX();
                    last_pos.y = event.getY();
                    raw_x = last_pos.x;
                    raw_y = last_pos.y;
                    isMoving = true;
                    break;
                }
                case MotionEvent.ACTION_MOVE: {

                    if (((raw_x - last_pos.x) != 0) || ((raw_y - last_pos.y) != 0)) {
                        raw_x = event.getX();
                        raw_y = event.getY();
                        last_pos.x = raw_x;
                        last_pos.y = raw_y;
                    } else {
                        raw_x = event.getX();
                        raw_y = event.getY();

                        float points[] = new float[3];
                        points[0] = raw_x;
                        points[1] = raw_y;

                        Matrix inverse = new Matrix();
                        camera.invert(inverse);
                        inverse.mapPoints(points);

                        pos.x = (int) (raw_x - last_pos.x);
                        pos.y = (int) (raw_y - last_pos.y);
                        distance += Math.sqrt(Math.pow(raw_x - last_pos.x, 2)
                                + Math.pow(raw_y - last_pos.y, 2));
                        if (distance > LONGPRESS_MOVEMENT) {
                            longpressTimer.cancel();
                            startLongClickTimer();
                            distance = 0.0f;
                        }

                        last_pos.x = raw_x;
                        last_pos.y = raw_y;
                        mapData.setPosition(camera.getPosition());

                        if (!isLongPressed() && mode == 0)
                            camera.move(pos.x, pos.y);
                        else if (mode == 1 && selectedPoint == null)
                            camera.move(pos.x, pos.y);

                        if (selectedPoint != null && mode == 1) {
                            screenPos.x = event.getRawX();
                            screenPos.y = event.getRawY();

                            selectionListener.onSelection(selectedPoint, false);
                        }
                        isMoving = true;
                    }
                    break;

                }
                case MotionEvent.ACTION_UP: {

                    longpressTimer.cancel();

                    if (isLongPressed() && mode == 0) {
                        float points[] = new float[3];
                        points[0] = event.getX();
                        points[1] = event.getY();

                        Matrix inverse = new Matrix();
                        camera.invert(inverse);
                        inverse.mapPoints(points);

                        GeoPoint refPoint = new GeoPoint(points[0] / levelScale,
                                points[1] / levelScale, "null");
                        Log.i("MapView",
                                "LongPressListener event at: " + refPoint.x() + " "
                                        + refPoint.y());
                        longpressListener.onLongpress(refPoint);
                    }

                    setCrossHairMode(false);
                    isMoving = false;

                    if (selectedPoint != null) {
                        selectionListener.onSelection(selectedPoint, true);
                        selectedPoint = null;
                    }
                    break;
                }
            }
        }

        return true;
    }

    /**
     * Do draw.
     *
     * @param canvas the canvas
     */
    @SuppressLint("WrongCall")
    public void doDraw(Canvas canvas) {

        // TODO Auto-generated method stub
        // long time = SystemClock.currentThreadTimeMillis();
        // Log.i("onDraw", "onDraw "+canvas);
        if (renderer != null) {
            canvas.drawColor(Color.LTGRAY);
            canvas.setMatrix(camera);
            renderer.onDraw(canvas, mapData);

            for (int k = 0; k < history.size() - 1; k++) {

                canvas.drawLine(history.get(k).x * (levelScale),
                        history.get(k).y * (levelScale), history.get(k + 1).x
                        * (levelScale), history.get(k + 1).y
                        * (levelScale), paint);

                canvas.drawCircle(history.get(k).x * (levelScale),
                        history.get(k).y * (levelScale), 3, paint);
            }

            if (isLongPressed()) {

                if (mode == 1) {

                    selected(canvas);
                }
            }

            LayerRenderer.getInstance().onDraw(canvas, levelScale);

            if (isLongPressed()) {

                if (mode == 0) {

                    Bitmap crosshair = BitmapFactory.decodeResource(
                            getResources(), R.drawable.ips_crosshair);
                    canvas.drawBitmap(crosshair, raw_x - crosshair.getWidth()
                            / 2, raw_y - crosshair.getHeight() / 2, null);


                }
            }

            canvas.save();

            int compasswidth = bitmapCompass.getWidth();
            int compassheight = bitmapCompass.getHeight();

            canvas.rotate(compassAngle, compassheight / 2, canvas.getHeight()
                    / 2 + compasswidth / 2);
            canvas.drawBitmap(bitmapCompass, 0, canvas.getHeight() / 2, null);
            canvas.restore();


            findBestLayer();
            // Log.d("MapView ", "Rendered Frame in " +
            // String.valueOf(SystemClock.currentThreadTimeMillis() - time)
            // +
            // " ms.");
        }

    }

    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera _camera) {
        camera = _camera;
    }

    /**
     * Selected.
     */
    private void selected(Canvas c) {
        if (isMoving) {

            float points[] = new float[3];
            points[0] = raw_x;
            points[1] = raw_y;

            Matrix inverse = new Matrix();
            camera.invert(inverse);
            inverse.mapPoints(points);

            if (selectedPoint == null)
                selectedPoint = LayerRenderer.getInstance().getSelectedElement(
                        points[0], points[1], localScale);

            if (selectedPoint != null)
                selectedPoint.setPosition(new PointF((points[0] / levelScale),
                        (points[1] / levelScale)));

        } else
            selectedPoint = null;

    }

    /**
     * Find best layer.
     */
    private void findBestLayer() {

        Vector<Float> vec = new Vector<Float>();

        for (TileLayer t : mapData.getTileLayers()) {
            // vec.add(Math.abs(t.getZoomFactor() - realScaleFactor));
            vec.add(Math.abs(t.getLevelScale() - localScale * levelScale));
        }

        int res = vec.indexOf(Collections.min(vec));
        if (res != mPreviousLayer && mPreviousLayer != -1 && res != -1) {
            if ((mPreviousLayer - res) > 0) {
                localScale /= 2.0f;
            } else {
                localScale *= 2.0f;
            }
            setLayer(res);
        }

        mPreviousLayer = res;
    }

    /**
     * Check orientation.
     */
    public void checkOrientation() {


        if (old_screen_size.x != 0 && old_screen_size.y != 0)
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE
                    && isRestored()) {

                camera.move(Math.abs(old_screen_size.x-size.x)/2, -Math.abs(old_screen_size.y-size.y)/2);

            } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT
                    && isRestored()) {

                camera.move(-Math.abs(old_screen_size.x-size.x)/2, Math.abs(old_screen_size.y-size.y)/2);
            }
    }

    public float getLocalScale() {
        return localScale;
    }

    public void setLocalScale(float localScale) {
        this.localScale = localScale;
    }

    /**
     * Sets the layer.
     *
     * @param _layer the new layer
     */
    public void setLayer(int _layer) {

        mapData.setLayer(_layer);
        levelScale = mapData.getLayerToRenderer().getLevelScale();

        Log.i("actual", "FLOOR " + mapData.getFloor() + " levelSCale "
                + levelScale + " layer " + _layer);
        // overlays.setZoomFactor(zoomFactor);
        if (!isLongPressed())
            camera.setScale(localScale, localScale);

    }

    /**
     * Center map to point.
     *
     * @param pnt the pnt
     */
    public void centerMapToPoint(GeoPoint pnt) {

        Log.i("Center Map", "To " + (pnt.x() * (localScale * levelScale)) + " "
                + pnt.y() * (localScale * levelScale));
        getCamera().lookAt((size.x / 2) - pnt.x() * (localScale * levelScale),
                (size.y / 2) - pnt.y() * (localScale * levelScale));
    }

    /*
     * (non-Javadoc)
     *
     * @see android.view.SurfaceView#onMeasure(int, int)
     */
    @Override
    protected void onMeasure(int _widthMeasureSpec, int _heightMeasureSpec) {

        super.onMeasure(_widthMeasureSpec, _heightMeasureSpec);
        Log.i("life", "onmeasure");
        size.x = MeasureSpec.getSize(_widthMeasureSpec);
        size.y = MeasureSpec.getSize(_heightMeasureSpec);

        setMeasuredDimension((int) size.x, (int) size.y);

        checkOrientation();
        // init();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * android.view.SurfaceHolder.Callback#surfaceChanged(android.view.SurfaceHolder
     * , int, int, int)
     */
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * android.view.SurfaceHolder.Callback#surfaceCreated(android.view.SurfaceHolder
     * )
     */
    public void surfaceCreated(SurfaceHolder holder) {

        if (mapThread == null) {
            mapThread = new MapThread(getHolder(), this);
        }

        mapThread.setRunning(true);
        mapThread.start();

    }

    /**
     * Checks if is continued.
     *
     * @return the continued
     */
    public boolean isContinued() {

        return continued;
    }

    /**
     * Sets the continued.
     *
     * @param continued the continued to set
     */
    public void setContinued(boolean continued) {

        this.continued = continued;
    }

    /*
     * (non-Javadoc)
     *
     * @see android.view.SurfaceHolder.Callback#surfaceDestroyed(android.view.
     * SurfaceHolder)
     */
    public void surfaceDestroyed(SurfaceHolder holder) {

        boolean retry = true;
        mapThread.setRunning(false);
        while (retry) {
            try {
                mapThread.join();
                mapThread = null;
                retry = false;
            } catch (InterruptedException e) {
            }
        }

    }

    /**
     * Gets the map data.
     *
     * @return the map data
     */
    public MapData getMapData() {

        return mapData;

    }

    /**
     * Sets the map data.
     *
     * @param _mapData the new map data
     */
    public void setMapData(MapData _mapData) {

        mapData = _mapData;
    }

    /**
     * Gets the map thread.
     *
     * @return the map thread
     */
    public MapThread getMapThread() {
        return mapThread;
    }

    /**
     * Sets the map thread.
     *
     * @param mapThread the new map thread
     */
    public void setMapThread(MapThread mapThread) {
        this.mapThread = mapThread;
    }

    /**
     * Gets the screen pos.
     *
     * @return the screen pos
     */
    public PointF getScreenPos() {
        return screenPos;
    }

    public int getCompassAngle() {
        return compassAngle;
    }

    public void setCompassAngle(int compassAngle) {
        this.compassAngle = compassAngle;
    }

    /**
     * Sets the screen pos.
     *
     * @param screenPos the new screen pos
     */
    public void setScreenPos(PointF screenPos) {
        this.screenPos = screenPos;
    }

    /**
     * The listener interface for receiving scale events. The class that is
     * interested in processing a scale event implements this interface, and the
     * object created with that class is registered with a component using the
     * component's <code>addScaleListener<code> method. When
     * the scale event occurs, that object's appropriate
     * method is invoked.
     */
    private class ScaleListener extends
            ScaleGestureDetector.SimpleOnScaleGestureListener {

        /**
         * The m delta.
         */
        PointF mDelta;

        /**
         * The min scale factor.
         */
        private float MIN_SCALE_FACTOR = 0.5f;

        /**
         * The max scale factor.
         */
        private float MAX_SCALE_FACTOR = 10.0f;

        /**
         * The m scale center.
         */
        private PointF mScaleCenter = new PointF();

        /**
         * Instantiates a new scale listener.
         */
        public ScaleListener() {

            mDelta = new PointF();
        }

        /*
         * (non-Javadoc)
         *
         * @see android.view.ScaleGestureDetector.SimpleOnScaleGestureListener#
         * onScaleBegin(android.view.ScaleGestureDetector)
         */
        public boolean onScaleBegin(ScaleGestureDetector detector) {

            return true;
        }

        /*
         * (non-Javadoc)
         *
         * @see
         * android.view.ScaleGestureDetector.SimpleOnScaleGestureListener#onScaleEnd
         * (android.view.ScaleGestureDetector)
         */
        public void onScaleEnd(ScaleGestureDetector detector) {

            last_pos.x = detector.getFocusX();
            last_pos.y = detector.getFocusY();

        }

        /*
         * (non-Javadoc)
         *
         * @see
         * android.view.ScaleGestureDetector.SimpleOnScaleGestureListener#onScale
         * (android.view.ScaleGestureDetector)
         */
        @Override
        public boolean onScale(ScaleGestureDetector detector) {

            longpressTimer.cancel();
            if (!isLongPressed()) {
                float lScale = detector.getScaleFactor();

                mScaleCenter.x = detector.getFocusX();
                mScaleCenter.y = detector.getFocusY();

                if (localScale * lScale > MIN_SCALE_FACTOR) {
                    if (localScale * lScale < MAX_SCALE_FACTOR) {
                        localScale *= lScale;
                        // realScaleFactor *= lScale;
                    } else {
                        localScale = MAX_SCALE_FACTOR;
                        lScale = 1.0f;
                    }
                } else {
                    localScale = MIN_SCALE_FACTOR;
                    lScale = 1.0f;
                }

                mDelta.x = mScaleCenter.x - camera.getPosition().x;
                mDelta.y = mScaleCenter.y - camera.getPosition().y;

                mDelta.x = mDelta.x * lScale - mDelta.x;
                mDelta.y = mDelta.y * lScale - mDelta.y;

                // pos.x -= mDelta.x;
                // pos.y -= mDelta.y;
                camera.scaleAndMove(localScale, localScale, -mDelta.x,
                        -mDelta.y);
                // camera.move(-mDelta.x, -mDelta.y);
                // camera.setScale(scaleFactor, scaleFactor);
                mapData.setPosition(camera.getPosition());
            }
            return true;
        }
    }

    /**
     * Checks if is long pressed.
     *
     * @return the crossHairMode
     */
    public boolean isLongPressed() {

        return crossHairMode;
    }

    /**
     * Sets the cross hair mode.
     *
     * @param crossHairMode the crossHairMode to set
     */
    public void setCrossHairMode(boolean crossHairMode) {

        this.crossHairMode = crossHairMode;
    }

    /**
     * Sets the position.
     *
     * @param _pos the new position
     */
    public void setPosition(GeoPoint _pos) {

        geopnt = _pos;
    }

    /**
     * Gets the position.
     *
     * @return the position
     */
    public GeoPoint getPosition() {

        return geopnt;
    }

    /**
     * Gets the cam pos.
     *
     * @return the cam pos
     */
    public PointF getCamPos() {

        return camera.getPosition();
    }

    /**
     * Sets the cam pos.
     *
     * @param _pos the new cam pos
     */
    public void setCamPos(PointF _pos) {

        camera.lookAt(_pos.x, _pos.y);
    }

    /**
     * Gets the cam scale.
     *
     * @return the cam scale
     */
    public PointF getCamScale() {

        return camera.getScale();
    }

    /**
     * Sets the cam scale.
     *
     * @param _scale the new cam scale
     */
    public void setCamScale(PointF _scale) {

        camera.setScale(_scale.x, _scale.y);
    }

    /**
     * Gets the zoom factor.
     *
     * @return the zoom factor
     */
    public float getLevelScale() {

        return levelScale;
    }

    /**
     * Sets the zoom factor.
     *
     * @param _zoomFactor the new zoom factor
     */
    public void setLevelScale(float _zoomFactor) {
        mapData.setLayer((int) _zoomFactor);
        levelScale = _zoomFactor;
    }

    /**
     * Gets the previous layer.
     *
     * @return the previous layer
     */
    public int getPreviousLayer() {

        return mPreviousLayer;
    }

    /**
     * Sets the previous layer.
     *
     * @param _layer the new previous layer
     */
    public void setPreviousLayer(int _layer) {

        mPreviousLayer = _layer;
    }

    /**
     * Gets the orientation.
     *
     * @return the orientation
     */
    public int getOrientation() {

        return orientation;
    }

    /**
     * Sets the orientation.
     *
     * @param orientation the orientation to set
     */
    public void setOrientation(int orientation) {

        this.orientation = orientation;
    }

    public Point getScreenSize() {
        return size;
    }

    public void setScreenSize(Point old_screen_size) {
        this.old_screen_size = old_screen_size;
    }
}
