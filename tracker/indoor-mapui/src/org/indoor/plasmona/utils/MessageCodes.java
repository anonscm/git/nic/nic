package org.indoor.plasmona.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class MessageCodes.
 */
public class MessageCodes {
	
	/** The Constant MESSAGE_BITMAP_LOADING_SUCCESS. */
	public static final int MESSAGE_BITMAP_LOADING_SUCCESS = 0;
	
	/** The Constant MESSAGE_BITMAP_LOADING_FAILED. */
	public static final int MESSAGE_BITMAP_LOADING_FAILED = 1;
	
	/** The Constant PATHFINDING_MESSAGE_ID. */
	public static final int PATHFINDING_MESSAGE_ID = 2;
	
	/** The Constant TILE_LOADING_FINISHED. */
	public static final int TILE_LOADING_FINISHED = 3;
	
	/** The Constant MAP_LOADED. */
	public static final int MAP_LOADED = 4;
	
	/** The Constant ZIP_STREAMING_FINISHED. */
	public static final int ZIP_STREAMING_FINISHED = 5;
	
	/** The Constant ZIP_STREAMING_ERROR. */
	public static final int ZIP_STREAMING_ERROR = 6;
	
	/** The Constant TILE_STREAMING_ERROR. */
	public static final int TILE_STREAMING_ERROR = 7;
	
	/** The Constant POSITION_UPDATED. */
	public static final int POSITION_UPDATED = 8;
	
	/** The Constant PROJECT. */
	public static final String PROJECT = "PROJECT";
	
	/** The Constant LAYER_LOADED. */
	public static final int LAYER_LOADED = 10;
	
	/** The Constant WIFI_SCAN_FINISHED. */
	public static final int WIFI_SCAN_FINISHED = 11;
	
	/** The Constant TRACKER_UPDATE. */
	public static final int TRACKER_UPDATE = 12;
	
	/** The Constant STEP_DETECTED. */
	public static final int STEP_DETECTED = 13;

	public static final int COMPASS_EVENT = 14;

    /** The Constant TRACKER_UPDATE. */
    public static final int TRACKER_INIT = 15;
}