/**
 * @author jens maiero
 *
 * @project plasmona
 * 
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.exceptions;

/**
 * The Class TrackerNotFoundException.
 */
public class TrackerNotFoundException extends Exception {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -4336110951023533645L;

  /**
   * Instantiates a new tracker not found exception.
   */
  public TrackerNotFoundException() {
  }

  /**
   * Instantiates a new tracker not found exception.
   * 
   * @param detailMessage
   *          the detail message
   */
  public TrackerNotFoundException(String detailMessage) {
    super(detailMessage);

  }

  /**
   * Instantiates a new tracker not found exception.
   * 
   * @param throwable
   *          the throwable
   */
  public TrackerNotFoundException(Throwable throwable) {
    super(throwable);

  }

  /**
   * Instantiates a new tracker not found exception.
   * 
   * @param detailMessage
   *          the detail message
   * @param throwable
   *          the throwable
   */
  public TrackerNotFoundException(String detailMessage, Throwable throwable) {
    super(detailMessage, throwable);

  }
}
