/**
 * @author jens maiero
 *
 * @project plasmona
 * 
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.layer;

/**
 * The Interface TrackerLayer.
 */
public interface TrackerLayer {

  /**
   * Load. Interface for loading layer. Example implementation see trackers. It
   * is possible to save your Layer where you want - www, database, xml or raw.
   * You have to take care if you layer is correct implemented see already
   * implemented trackers
   */
  void load();

  /**
   * Reload. See also the method {@link #load}
   */
  void reload();

  /**
   * Update. See also the method {@link #load}
   */
  void update();
}
