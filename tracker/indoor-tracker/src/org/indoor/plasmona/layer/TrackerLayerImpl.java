/**
 * @author jens maiero
 *
 * @project plasmona
 * 
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.layer;

import java.util.ArrayList;

import org.indoor.plasmona.models.GeoPoint;

import android.graphics.PointF;

/**
 * The Class TrackerLayerImpl.
 */
public class TrackerLayerImpl extends ArrayList<GeoPoint> {


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The name. */
	private String name = null;
	
	/** The active. */
	private boolean active = true;

	/**
	 * Instantiates a new tracker layer impl.
	 */
	public TrackerLayerImpl() {

	}
	
	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param active the new active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Add a new Element to the Layer
	 *
	 * @param x coordinate
	 * @param y coordinate
	 * @param name the name of the Point
	 * @return the new Position
	 */
	public GeoPoint newPosition(int x, int y, String name) {
		GeoPoint geo = new GeoPoint(x, y, name);
		add(geo);
		return geo;
	}

	/**
	 * Find position by id.
	 *
	 * @param _i the _i
	 * @return the position
	 */
	public GeoPoint findPositionById(int _i) {
		for (int i = 0; i < size(); i++) {
			if (get(i).getId() == _i)
				return get(i);
		}
		return null;
	}

	
	public GeoPoint findTrackerByName(String name) {

		for ( GeoPoint p : this ) {
            // This doesn't seem quite right... we return null if ANY point lacks a name, before we find the point with the
            // requested name?
			if (p.getName() == null)
				return null;
			else if (p.getName().equals(name))
				return p;
		}
		
		return null;
		
	}

	/**
	 * Reload position.
	 *
	 * @param id the id
	 * @return the point f
	 */
	public PointF reloadPosition(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
