/**
 * @author jens maiero
 *
 * @project plasmona
 *
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.layer;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.FloatMath;
import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.tracker.Tracker;
import org.indoor.plasmona.tracker.TrackerServiceFactory;

import java.util.List;

/**
 * The Class LayerRenderer. Is a Singleton the only instance can access through
 * getInstance()
 */
public class LayerRenderer {

  /** The tag. */
  private static String TAG = "LayerRenderer";

  /** The instance. */
  private static LayerRenderer instance = null;

  /** The zoom. */
  private float zoom = 0;

  /** The paint. */
  protected Paint paint;

  /**
   * Instantiates a new layer renderer.
   */
  private LayerRenderer() {
    init();
  }

  /**
   * Inits the. Init is called int the Singleton contructor and is, in this
   * cased, only used for initialization of a paint context.
   */

  private void init() {
    paint = new Paint();
    paint.setStyle(Paint.Style.STROKE);
    paint.setStrokeWidth(5);
    paint.setAntiAlias(true);
    paint.setColor(Color.RED);
    paint.setAlpha(100);

  }

  /**
   * Gets the single instance of LayerRenderer.
   *
   * @return single instance of LayerRenderer
   */
  public static LayerRenderer getInstance() {
    if (instance == null) {
      instance = new LayerRenderer();
    }
    return instance;
  }

  /**
   * On draw.
   *
   * @param canvas
   *          the canvas
   * @param depth
   *          the depth
   *
   *          renders all register Layers and the custom layers
   */
  public void onDraw(Canvas canvas, float depth) {
      zoom = depth;

      float[] f = new float[9];
      canvas.getMatrix().getValues(f);
      float scaleX = f[Matrix.MSCALE_X];
      float scaleY = f[Matrix.MSCALE_Y];

      // depth /2.0 is the area of scaling, i.e. if depth = 2 you can scale from 2 to 2, if it is 4 you can scale from 2 to 4
      // scaleX+0.25 sets  scale to nearly 1 and 1.333 allows to scale between 1 to 2 as the area scaleX works is round about from 0.75 to 1.5
      // this scale is not perfect but works nearly smooth throughout the whole scale able distance
      final float bitmapScaleByDepth = FloatMath.sqrt(depth) / 3.0f * (scaleX + 0.25f) * (1.333f);
      final float bitmapScaleByDepthWiFiIcons = depth / 4f * (scaleX + 0.25f) * (1.333f);

      List<Tracker> trackers = TrackerServiceFactory.getLocationService().getTrackers();
      float centerBitmap = -1;

      for (Tracker t : trackers) {
          if (t.hasLayer()) {
              TrackerLayerImpl layer = t.getLayer();
              synchronized (layer) {
                  for (GeoPoint p : layer) {
                      if (centerBitmap == -1)
                          if (p.getIcon() == null) {
                              continue;
                          }


                      if (p.isActive()) {
                          final Bitmap scaledBitmap = Bitmap.createScaledBitmap(p.getIcon(), (int) ((p.getIcon().getWidth()) / scaleX * bitmapScaleByDepthWiFiIcons / 0.6f),
                                  (int) ((p.getIcon().getHeight()) / scaleY * bitmapScaleByDepthWiFiIcons / 0.6f), false);
                          centerBitmap = scaledBitmap.getWidth() / 2.0f;

                          canvas.drawBitmap(scaledBitmap, p.x() * depth - scaledBitmap.getWidth() / 2.0f, p.y() * depth - scaledBitmap.getWidth() / 2.0f, null);
                          canvas.drawCircle(p.x() * depth, p.y() * depth, scaledBitmap.getWidth() / 2.0f, paint);
                      } else {
                          final Bitmap scaledBitmap = Bitmap.createScaledBitmap(p.getIcon(), (int) (p.getIcon().getWidth() / scaleX * bitmapScaleByDepthWiFiIcons),
                                  (int) (p.getIcon().getHeight() / scaleY * bitmapScaleByDepthWiFiIcons), false);
                          centerBitmap = scaledBitmap.getWidth() / 2.0f;

                          canvas.drawBitmap(scaledBitmap, p.x() * depth - centerBitmap, p.y() * depth - centerBitmap, null);

                      }
                  }
              }
          }
      }

      CustomLayerController clc = CustomLayerController.getInstance();

      for (TrackerLayerImpl t : clc.values()) {
          if (t.getName() != null)
              for (GeoPoint p : t) {


                  final Bitmap scaledBitmap = Bitmap.createScaledBitmap(p.getIcon(), (int) (p.getIcon().getWidth() / scaleX * bitmapScaleByDepth), (int) (p.getIcon().getHeight() / scaleY * bitmapScaleByDepth), false);
                  centerBitmap = scaledBitmap.getWidth() / 2.0f;
                  canvas.drawBitmap(scaledBitmap, p.x() * depth - centerBitmap, p.y() * depth - centerBitmap, null);
              }
      }
  }
  /**
   * Gets the selected element.
   *
   * @param x
   *          the x of the current pointer
   * @param y
   *          the y of the current pointer
   * @param scale
   *          the current scale of the map
   * @return the ID of the selected Item/Element
   *
   *
   */
  public GeoPoint getSelectedElement(float x, float y, float scale) {

    List<Tracker> trackers = TrackerServiceFactory.getLocationService().getTrackers();

    for (Tracker t : trackers) {
      if (t.hasLayer()) {
	// if (l.isActive())
	TrackerLayerImpl l = t.getLayer();
	for (GeoPoint p : l) {
	  if ((p.x() * zoom - p.getIcon().getWidth() / (2 * scale)) < x && x < (p.x() * zoom + p.getIcon().getWidth() / (2 * scale))) {
	    if ((p.y() * zoom - p.getIcon().getHeight() / (2 * scale)) < y && y < (p.y() * zoom + p.getIcon().getHeight() / (2 * scale))) {
	      // Log.i("Found ", "" + p.getID());
	      p.setActive(true);
	      return p;
	    }
	  }
	}

      }
    }

    CustomLayerController clc = CustomLayerController.getInstance();
    // if (wifi)
    for (TrackerLayerImpl t : clc.values()) {
      for (GeoPoint p : t) {
	if ((p.x() * zoom - p.getIcon().getWidth() / (2 * scale)) < x && x < (p.x() * zoom + p.getIcon().getWidth() / (2 * scale))) {
	  if ((p.y() * zoom - p.getIcon().getHeight() / (2 * scale)) < y && y < (p.y() * zoom + p.getIcon().getHeight() / (2 * scale))) {
	    // Log.i("Found ", "" + p.getID());
	    p.setActive(true);
	    return p;
	  }
	}
      }
    }

    return null;

  }
}
