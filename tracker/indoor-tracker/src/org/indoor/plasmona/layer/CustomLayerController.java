package org.indoor.plasmona.layer;

import android.util.Log;

import java.util.concurrent.ConcurrentHashMap;

/**
 * The Class CustomLayerController. This class allows to generate custom layers
 * on the map for displaying Debug Stuff or other User defined overlays
 * 
 * You only have to register a registerOrCreateLayer a new Instance of @TrackerLayerImpl
 * You can access the Layer by findLayerByName(...). If you want a special
 * behavior add a case in the @LayerRenderer
 * 
 */

public class CustomLayerController extends ConcurrentHashMap<String, TrackerLayerImpl> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The Constant TAG. */
  private static final String TAG = "CustomLayerController";

  /** The instance. */
  private static CustomLayerController instance = null;

  /**
   * Instantiates a new custom layer controller.
   */
  private CustomLayerController() {

  }

  /**
   * Gets the single instance of CustomLayerController.
   * 
   * @return single instance of CustomLayerController
   */
  public static CustomLayerController getInstance() {
    if (instance == null) {
      instance = new CustomLayerController();
    }
    return instance;

  }

  /**
   * Register or create layer.
   * 
   * @param name
   *          the name
   * @param layer
   *          the layer
   * @return the tracker layer impl
   */
  public TrackerLayerImpl registerOrCreateLayer(String name, TrackerLayerImpl layer) {

      if (containsKey(name))
      return get(name);

    if (layer == null)
      return null;

    layer.setName(name);
    put(name, layer);
    return layer;

  }

  /**
   * Update layer by name.
   * 
   * @param name
   *          the name
   * @param layer
   *          the layer
   */
  public void updateLayerByName(String name, TrackerLayerImpl layer) {
    if (containsKey(name)) {
        put(name, layer);
    } else
      registerOrCreateLayer(name, layer);
  }

  /**
   * Find layer by name.
   * 
   * @param name
   *          the name
   * @return the tracker layer impl
   */
  public TrackerLayerImpl findLayerByName(String name) {
    return registerOrCreateLayer(name, null);

  }

  /**
   * Unregister.
   * 
   * @param name
   *          the name
   */
  public void unregister(String name) {
    remove(name);
  }
}
