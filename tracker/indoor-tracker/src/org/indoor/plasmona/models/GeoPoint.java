package org.indoor.plasmona.models;

import android.graphics.Bitmap;

/**
 * The Class Position.
 */
public class GeoPoint extends Positionable {


    private String name = null;

    private String type = "unknown";

    private Bitmap icon = null;

    private String bitmapName = null;

    private Object data;

    private int id;

    /**
     * The z axis
     */
    private Float z;

    private boolean active = false;

    /**
     * Default constructor
     */
    public GeoPoint() {

    }

    /**
     * Instantiates a new position.
     *
     * @param x    The x axis.
     * @param y    The y axis.
     * @param name The name of the point.
     */
    public GeoPoint(float x, float y, String name) {
        super(x, y);
        this.name = name;
    }


    public GeoPoint(float x, float y, int id) {
        super(x, y);
        this.id = id;
    }

    /**
     * Instantiates a new position with x,y and z axes
     *
     * @param x    The x axis.
     * @param y    The y axis.
     * @param z    The z axis.
     * @param name The name of the point.
     */
    public GeoPoint(float x, float y, float z, String name) {
        super(x, y);
        this.z = z;
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Bitmap getIcon() {
        return icon;
    }

    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }

    public String getBitmapName() {
        return bitmapName;
    }

    public void setBitmapName(String bitmapName) {
        this.bitmapName = bitmapName;
    }


    public Float getZ() {
        return z;
    }

    public void setZ(Float z) {
        this.z = z;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double scalarProduct( GeoPoint q) {
        return (this.x()*q.x()+ this.y()*q.y());
    }

    public double norm () {
        return Math.sqrt(Math.pow(this.x(),2)+Math.pow(this.y(),2));
    }
    /**
     * Get info output
     *
     * @return Info message.
     */
    public String toString() {
        return ("X: " + x() + " Y: " + y() + " id " + getId());
    }

    public void setPosition(GeoPoint geoPoint) {
        setPosition(geoPoint.x(), geoPoint.y());
    }
}

