/**
 * @author jens maiero
 *
 * @project plasmona
 *
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.models;

import java.util.ArrayList;
import java.util.Vector;

import android.graphics.Rect;
import android.os.SystemClock;
import android.util.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class TileLayer.
 */
public class TileLayer {

  /** The tile prefix. */
  public static String TILE_PREFIX = "tile_";

  /** The tile suffix. */
  public static String TILE_SUFFIX = ".png";

  /** The m zip. */
  private String mZip;

  /** The m layer id. */
  private int mLayerId;

  /** The m tile width. */
  private int mTileWidth;

  /** The m tile height. */
  private int mTileHeight;

  /** The m image height. */
  private int mImageHeight;

  /** The m image width. */
  private int mImageWidth;

  /** The m zoom factor. */
  private float mLevelScale;

  /** The m loaded tiles. */
  private Vector<Tile> mLoadedTiles = new Vector<Tile>();

  /** The m all tiles. */
  private ArrayList<ArrayList<Tile>> mAllTiles = new ArrayList<ArrayList<Tile>>();

  /**
   * Sets the tile width.
   * 
   * @param tileWidth
   *          the new tile width
   */
  public void setTileWidth(int tileWidth) {
    this.mTileWidth = tileWidth;
  }

  /**
   * Gets the tile width.
   * 
   * @return the tile width
   */
  public int getTileWidth() {
    return mTileWidth;
  }

  /**
   * Sets the tile height.
   * 
   * @param tileHeight
   *          the new tile height
   */
  public void setTileHeight(int tileHeight) {
    this.mTileHeight = tileHeight;
  }

  /**
   * Gets the tile height.
   * 
   * @return the tile height
   */
  public int getTileHeight() {
    return mTileHeight;
  }

  /**
   * Sets the image height.
   * 
   * @param imageHeight
   *          the new image height
   */
  public void setImageHeight(int imageHeight) {
    this.mImageHeight = imageHeight;
  }

  /**
   * Gets the image height.
   * 
   * @return the image height
   */
  public int getImageHeight() {
    return mImageHeight;
  }

  /**
   * Sets the image width.
   * 
   * @param imageWidth
   *          the new image width
   */
  public void setImageWidth(int imageWidth) {
    this.mImageWidth = imageWidth;
  }

  /**
   * Gets the image width.
   * 
   * @return the image width
   */
  public int getImageWidth() {
    return mImageWidth;
  }

  /**
   * Sets the layer id.
   * 
   * @param layerId
   *          the new layer id
   */
  public void setLayerId(int layerId) {
    this.mLayerId = layerId;
  }

  /**
   * Gets the layer id.
   * 
   * @return the layer id
   */
  public int getLayerId() {
    return mLayerId;
  }

  /**
   * Sets the all tiles.
   * 
   * @param allTiles
   *          the new all tiles
   */
  public void setAllTiles(ArrayList<ArrayList<Tile>> allTiles) {
    this.mAllTiles = allTiles;
  }

  /**
   * Gets the all tiles.
   * 
   * @return the all tiles
   */
  public ArrayList<ArrayList<Tile>> getAllTiles() {
    return mAllTiles;
  }

  /**
   * Sets the loaded tiles.
   * 
   * @param loadedTiles
   *          the new loaded tiles
   */
  public void setLoadedTiles(Vector<Tile> loadedTiles) {
    this.mLoadedTiles = loadedTiles;
  }

  /**
   * Gets the loaded tiles.
   * 
   * @return the loaded tiles
   */
  public Vector<Tile> getLoadedTiles() {
    return mLoadedTiles;
  }

  /**
   * Gen tiles.
   */
  public void genTiles() {
    long time = SystemClock.currentThreadTimeMillis();

    for (int x = 0; x < mImageWidth; x += mTileWidth) {
      ArrayList<Tile> arraylist = new ArrayList<Tile>();
      for (int y = 0; y < mImageHeight; y += mTileHeight) {
	Tile t = new Tile();
	t.mName = TILE_PREFIX + String.valueOf(x / mTileWidth + 1) + "_" + String.valueOf(y / mTileHeight + 1) + TILE_SUFFIX;

	t.mRect = new Rect(x, y, Math.min(mImageWidth, x + mTileWidth), Math.min(mImageHeight, y + mTileHeight));
	t.mLayer = this;
	arraylist.add(t);
      }
      mAllTiles.add(arraylist);
    }

    Log.d("TileLayer", "Generated tiles in " + String.valueOf(SystemClock.currentThreadTimeMillis() - time) + " ms.");
  }

  /**
   * Sets the zip.
   * 
   * @param zip
   *          the new zip
   */
  public void setZip(String zip) {
    this.mZip = zip;
  }

  /**
   * Gets the zip.
   * 
   * @return the zip
   */
  public String getZip() {
    return mZip;
  }

  /**
   * Sets the zoom factor.
   * 
   * @param zoomFactor
   *          the new zoom factor
   */
  public void setLevelScale(float zoomFactor) {
    // Log.i("Plasmona", "setZoomFactor");
    this.mLevelScale = zoomFactor;
  }

  /**
   * Gets the zoom factor.
   * 
   * @return the zoom factor
   */
  public float getLevelScale() {
    return mLevelScale;
  }
}