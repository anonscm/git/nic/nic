/**
 * @author jens maiero
 *
 * @project plasmona
 * 
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.models;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import java.util.Observable;

import android.graphics.PointF;

// TODO: Auto-generated Javadoc
/**
 * The Class MapData. This class represent all relevant parameters of a Map.
 */
public class MapData extends Observable {


	/** The tile layers. */
	Map<Integer, ArrayList<TileLayer>> tileLayers = null;

	/** The root dir. */
	File rootDir = null;

	/** The layer. */
	private int layer = -1;

	/** The current position. */
	private PointF currentPosition;

	/** The real scale. */
	private float realScale = 0;

	/** The floor. */
	private int floor = 0;

	/** The scale. */
	private float scale = 1.0f;

	/** The description. */
	private String description;

	/** The project name. */
	private String projectName;
	
	private String uuid;
	

	/**
	 * Instantiates a new map data.
	 */
	public MapData() {

		super();
		currentPosition = new PointF(0.f, 0.f);
	}

	/**
	 * Gets the real scale.
	 * 
	 * @return the real scale
	 */
	public float getRealScale() {
		return realScale;
	}

	/**
	 * Sets the real scale.
	 * 
	 * @param realScale
	 *            the new real scale
	 */
	public void setRealScale(float realScale) {
		this.realScale = realScale;
	}

	/**
	 * Gets the floor.
	 * 
	 * @return the floor
	 */
	public int getFloor() {

		return floor;
	}

	/**
	 * Sets the floor.
	 * 
	 * @param floor
	 *            the new floor
	 */
	public void setFloor(int floor) {

		this.floor = floor;
		super.setChanged();
		super.notifyObservers();
	}

	/**
	 * Gets the scale.
	 * 
	 * @return the scale
	 */
	public float getScale() {
		return scale;
	}

	/**
	 * Sets the scale.
	 * 
	 * @param scale
	 *            the new scale
	 */
	public void setScale(float scale) {

		this.scale = scale;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {

		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {

		this.description = description;
	}

	/**
	 * Gets the project name.
	 * 
	 * @return the project name
	 */
	public String getProjectName() {

		return projectName;
	}

	/**
	 * Sets the project name.
	 * 
	 * @param projectName
	 *            the new project name
	 */
	public void setProjectName(String projectName) {

		this.projectName = projectName;
	}

	/**
	 * Gets the position.
	 * 
	 * @return the position
	 */
	public PointF getPosition() {

		return currentPosition;
	}

	/**
	 * Sets the position.
	 * 
	 * @param _StartPosition
	 *            the new position
	 */
	public void setPosition(PointF _StartPosition) {

		this.currentPosition = _StartPosition;
	}

	/**
	 * Gets the root dir.
	 * 
	 * @return the root dir
	 */
	public File getRootDir() {

		return rootDir;
	}

	/**
	 * Sets the root dir.
	 * 
	 * @param _file
	 *            the new root dir
	 */
	public void setRootDir(File _file) {

		this.rootDir = _file;
	}

	/**
	 * Gets the tile layers.
	 * 
	 * @return the tile layers
	 */
	public ArrayList<TileLayer> getTileLayers() {

		return tileLayers.get(floor);
	}

	/**
	 * Sets the tile layers.
	 * 
	 * @param _tileLayers
	 *            the _tile layers
	 */
	public void setTileLayers(Map<Integer, ArrayList<TileLayer>> _tileLayers) {

		tileLayers = _tileLayers;
	}

	/**
	 * Gets the layer to renderer.
	 * 
	 * @return the layer to renderer
	 */
	public TileLayer getLayerToRenderer() {

		return tileLayers.get(floor).get(layer);
	}

	/**
	 * Sets the layer.
	 * 
	 * @param _layer
	 *            the new layer
	 */
	public void setLayer(int _layer) {

		layer = _layer;
		super.setChanged();
		super.notifyObservers();
	}

	/**
	 * Gets the layer.
	 * 
	 * @return the layer
	 */
	public int getLayer() {

		return layer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "Map data: layer " + layer + " floor " + floor + " scale "
				+ scale + " realscale " + realScale;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
