package org.indoor.plasmona.models;

import android.graphics.PointF;
import android.util.FloatMath;

// TODO: Auto-generated Javadoc
/**
 * The Class Positionable.
 */
public class Positionable {

  /** The position. */
  private PointF position;

  /** The floor id. */
  private int floorId = 0;

  /** The scale. */
  private float scale = 0;

  /**
   * Instantiates a new positionable.
   */
  public Positionable() {
    position = new PointF();
  }

  /**
   * Instantiates a new positionable.
   * 
   * @param x
   *          the x
   * @param y
   *          the y
   */
  public Positionable(float x, float y) {

    position = new PointF(x, y);
  }

  /**
   * Sets the position.
   * 
   * @param x
   *          the x
   * @param y
   *          the y
   */
  public void setPosition(float x, float y) {

    position.x = x;
    position.y = y;

  }

  /**
   * Sets the scale.
   * 
   * @param _scale
   *          the new scale
   */
  public void setScale(float _scale) {
    scale = _scale;
  }

  /**
   * Gets the scale.
   * 
   * @return the scale
   */
  public float getScale() {
    return scale;
  }

  /**
   * X.
   * 
   * @return the float
   */
  public float x() {

    return position.x;
  }

  /**
   * Y.
   * 
   * @return the float
   */
  public float y() {

    return position.y;

  }

  /**
   * Sets the x.
   * 
   * @param x
   *          the new x
   */
  public void setX(float x) {

    position.x = x;
  }

  /**
   * Sets the y.
   * 
   * @param y
   *          the new y
   */
  public void setY(float y) {

    position.y = y;

  }

  /**
   * Sets the position.
   * 
   * @param pnt
   *          the new position
   */
  public void setPosition(PointF pnt) {

    position.set(pnt.x, pnt.y);

  }

  /**
   * Gets the distance to.
   * 
   * @param o
   *          the o
   * @return the distance to
   */
  public double getDistanceTo(Positionable o) {

    float dx = Math.abs(o.position.x - position.x);
    float dy = Math.abs(o.position.y - position.y);

    return Math.sqrt((dx * dx) + (dy * dy));

  }

  /**
   * Sets the floor id.
   * 
   * @param floorId
   *          the new floor id
   */
  public void setFloorId(int floorId) {
    this.floorId = floorId;
  }

  /**
   * Gets the floor id.
   * 
   * @return the floor id
   */
  public int getFloorId() {

    return floorId;

  }
}