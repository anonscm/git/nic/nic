/**
 * @author jens maiero
 *
 * @project plasmona
 *
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.models;

import android.graphics.Bitmap;
import android.graphics.Rect;

/**
 * The Class Tile.
 */
public class Tile {

  /**
   * The Enum meState.
   */
  public enum meState {

    /** The in cache. */
    IN_CACHE,
    /** The in memory. */
    IN_MEMORY
  }

  /** The m name. */
  public String mName = null;

  /** The m state. */
  private meState mState = meState.IN_CACHE;

  /** The m layer. */
  public TileLayer mLayer = null;

  /** The m rect. */
  public Rect mRect;

  /** The m bitmap. */
  public Bitmap mBitmap;

  /**
   * Sets the tile state.
   * 
   * @param _tileState
   *          the new tile state
   */
  public void setTileState(meState _tileState) {

    synchronized (this) {
      this.mState = _tileState;
    }

  }

  /**
   * Gets the tile state.
   * 
   * @return the tile state
   */
  public meState getTileState() {

    synchronized (this) {
      return mState;
    }

  }

}
