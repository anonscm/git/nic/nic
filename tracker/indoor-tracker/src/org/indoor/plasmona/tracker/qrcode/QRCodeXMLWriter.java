package org.indoor.plasmona.tracker.qrcode;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.Prefs;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.graphics.PointF;
import android.os.Environment;
import android.util.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class QRCodeXMLWriter.
 */
public class QRCodeXMLWriter {


	/** The qrcode ref file. */
	private static String QRCODE_REF_FILE = "qr-references.xml";

	/** The project. */
	private static String project;

	/**
	 * Write point.
	 *
	 * @param geo the geo
	 */
	public static void writePoint(GeoPoint geo) {
		project = ApplicationContext.get().getSharedPreferences(Prefs.PLASMONA, 0).getString(Prefs.PROJECT, "NULL");
		String filename = Environment.getExternalStorageDirectory() + "/" + ApplicationContext.get().getPackageName() + "/" + project + "/" + QRCODE_REF_FILE;

		boolean exists = (new File(filename)).exists();

		if (!exists) {

			try {
				createReferenceFile(filename);

			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			FileInputStream filestream = new FileInputStream(filename);

			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filestream);

			// get root node
			Node root = doc.getFirstChild();
			Element qrcodeID = doc.createElement("QRCodeLocation");

			qrcodeID.setAttribute("id", "" + geo.getId());
			qrcodeID.setAttribute("bitmapName", "ips_qrcode");

			qrcodeID.setAttribute("x", "" + geo.x());
			qrcodeID.setAttribute("y", "" + geo.y());

			root.appendChild(qrcodeID);

			TransformerFactory transformerFactory = TransformerFactory.newInstance();

			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filename));
			transformer.transform(source, result);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ioe) {
			Log.e("ERROR", "IO Error");
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Update point.
	 *
	 * @param geo the geo
	 */
	public static void updatePoint(GeoPoint geo) {
		project = ApplicationContext.get().getSharedPreferences(Prefs.PLASMONA, 0).getString(Prefs.PROJECT, "NULL");
		String filename = Environment.getExternalStorageDirectory() + "/" + ApplicationContext.get().getPackageName() + "/" + project + "/" + QRCODE_REF_FILE;
		boolean update = false;
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			FileInputStream filestream = new FileInputStream(filename);

			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filestream);

			Node root = doc.getFirstChild();
			NodeList nList = root.getChildNodes();

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);

				if ("QRCodeLocation".equals(nNode.getNodeName())) {
					Element eElement = (Element) nNode;
					if (eElement.hasAttribute("id") && eElement.getAttribute("id").equals(String.valueOf(geo.getId()))) {
						eElement.setAttribute("x", ""+geo.x());
						eElement.setAttribute("y", ""+geo.y());
						update = true;
						break;
					}
				}
			}

			if (update) {
				TransformerFactory transformerFactory = TransformerFactory.newInstance();

				Transformer transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
				transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(filename));
				transformer.transform(source, result);
				update = false;
			}

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ioe) {
			Log.e("ERROR", "IO Error");
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the position.
	 *
	 * @param id the id
	 * @return the position
	 */
	public static PointF getPosition(int id) {
		project = ApplicationContext.get().getSharedPreferences(Prefs.PLASMONA, 0).getString(Prefs.PROJECT, "NULL");
		String filename = Environment.getExternalStorageDirectory() + "/" + ApplicationContext.get().getPackageName() + "/" + project + "/" + QRCODE_REF_FILE;

		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			FileInputStream filestream = new FileInputStream(filename);

			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filestream);

			Node root = doc.getFirstChild();
			NodeList nList = root.getChildNodes();
			PointF pnt = new PointF();
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);

				if ("QRCodeLocation".equals(nNode.getNodeName())) {
					Element eElement = (Element) nNode;
					if (eElement.hasAttribute("id") && eElement.getAttribute("id").equals(String.valueOf(id))) {
						pnt.x  = Float.parseFloat(eElement.getAttribute("x"));
						pnt.y  = Float.parseFloat(eElement.getAttribute("y"));

						return pnt;

					}
				}
			}

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ioe) {
			Log.e("ERROR", "IO Error");
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
		return null;
	}

	/**
	 * Delete point.
	 *
	 * @param geo the geo
	 */
	public static void deletePoint(GeoPoint geo) {
		project = ApplicationContext.get().getSharedPreferences(Prefs.PLASMONA, 0).getString(Prefs.PROJECT, "NULL");
		String filename = Environment.getExternalStorageDirectory() + "/" + ApplicationContext.get().getPackageName() + "/" + project + "/" + QRCODE_REF_FILE;
		boolean update = false;
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			FileInputStream filestream = new FileInputStream(filename);

			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filestream);

			Node root = doc.getFirstChild();
			NodeList nList = root.getChildNodes();

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);

				if ("QRCodeLocation".equals(nNode.getNodeName())) {
					Element eElement = (Element) nNode;
					if (eElement.hasAttribute("id") && eElement.getAttribute("id").equals(String.valueOf(geo.getId()))) {

						root.removeChild(eElement);
						update = true;
						break;
					}
				}
			}

			if (update) {
				TransformerFactory transformerFactory = TransformerFactory.newInstance();

				Transformer transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
				transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(filename));
				transformer.transform(source, result);
				update = false;
			}

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ioe) {
			Log.e("ERROR", "IO Error");
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	
  /**
   * Creates the reference file.
   * 
   * @param filename
   *          the filename
   * @throws ParserConfigurationException
   *           the parser configuration exception
   * @throws TransformerException
   *           the transformer exception
   */
  public static void createReferenceFile(String filename) throws ParserConfigurationException, TransformerException {
    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
    Document doc = docBuilder.newDocument();
    Element rootElement = doc.createElement(project);
    doc.appendChild(rootElement);
    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    Transformer transformer = transformerFactory.newTransformer();
    DOMSource source = new DOMSource(doc);
    StreamResult result = new StreamResult(new File(filename));

    transformer.transform(source, result);

  }
}
