/**
 * @author jens maiero
 *
 * @project plasmona
 *
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.tracker.qrcode;

import java.io.File;

import org.indoor.plasmona.layer.TrackerLayer;
import org.indoor.plasmona.layer.TrackerLayerImpl;
import org.indoor.plasmona.tracker.TrackerServiceFactory;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.Prefs;
import org.indoor.plasmona.utils.TName;

import android.graphics.PointF;
import android.os.Environment;
import android.util.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class QRCodeLayer.
 */
public class QRCodeLayer extends TrackerLayerImpl implements TrackerLayer {

  /** The tag. */
  private static String TAG = "QRCodeLayer";

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -457092788508004893L;

  /** The project. */
  private String project;

  /*
   * (non-Javadoc)
   * 
   * @see org.indoor.plasmona.layer.TrackerLayer#load()
   */
  @Override
  public void load() {
    Log.i(TAG, "load()");
    project = ApplicationContext.get().getSharedPreferences(Prefs.PLASMONA, 0).getString(Prefs.PROJECT, "NULL");

    File f = new File(Environment.getExternalStorageDirectory() + "/" + ApplicationContext.get().getPackageName() + "/" + project + "/qr-references.xml");
    if (f.exists())
      TrackerServiceFactory.getLocationService().findTrackerByName(TName.QRCODETRACKER).registerLayer(QRCodeXMLReader.loadLayer(f));
    else {
      TrackerServiceFactory.getLocationService().findTrackerByName(TName.QRCODETRACKER).registerLayer(QRCodeXMLReader.loadLayer(f));
    }

  }

  /*
   * (non-Javadoc)
   * 
   * @see org.indoor.plasmona.layer.TrackerLayer#reload()
   */
  @Override
  public void reload() {
    Log.i(TAG, "reload()");

  }

  /*
   * (non-Javadoc)
   * 
   * @see org.indoor.plasmona.layer.TrackerLayer#reloadPosition(int)
   */
  public PointF reloadPosition(int id) {
    Log.i(TAG, "reloadPosition (" + id + " )");
    return QRCodeXMLWriter.getPosition(id);

  }

  /*
   * (non-Javadoc)
   * 
   * @see org.indoor.plasmona.layer.TrackerLayer#update()
   */
  @Override
  public void update() {
    // TODO Auto-generated method stub

  }
}
