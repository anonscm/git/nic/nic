package org.indoor.plasmona.tracker.qrcode;

import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.tracker.Capturer;
import org.indoor.plasmona.tracker.TrackerServiceFactory;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.TName;

import android.content.Intent;
import android.graphics.PointF;
import android.util.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class QRCodeCapturer.
 */

public class QRCodeCapturer  implements Capturer {

	/** The Constant TAG. */
	protected static final String TAG = "QRCodeCapturer";

	/**
	 * Instantiates a new qR code capturer.
	 */
	public QRCodeCapturer() {
		TrackerServiceFactory.getLocationService().findTrackerByName(TName.QRCODETRACKER).registerCapturer(this);

	}

	/* (non-Javadoc)
	 * @see org.indoor.plasmona.tracker.Capturer#captureAt(org.indoor.plasmona.models.Position)
	 */
	@Override
	public void captureAt(GeoPoint pnt) {
		Log.i(TAG, "captureAt(Position pnt)");
		Intent i = new Intent("org.indoor.plasmona.activities.QRCodeCaptureAct");
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		i.putExtra("Capturer", true);
		i.putExtra("x", pnt.x());
		i.putExtra("y", pnt.y());

		ApplicationContext.get().startActivity(i);

	}

	/* (non-Javadoc)
	 * @see org.indoor.plasmona.tracker.Capturer#write(org.indoor.plasmona.models.Position)
	 */
	@Override
	public void write(GeoPoint pnt) {
		QRCodeXMLWriter.writePoint(pnt);
		Log.i(TAG, "captureAt(Position pnt)");

		QRCodeLayer qr = (QRCodeLayer) TrackerServiceFactory.getLocationService().findTrackerByName(TName.QRCODETRACKER).getLayer();
		Log.i(TAG, "QRCodeLayer "+qr);
		synchronized (qr) {
			qr.add(pnt);
		}

	}

	/* (non-Javadoc)
	 * @see org.indoor.plasmona.tracker.Capturer#update(org.indoor.plasmona.models.Position)
	 */
	@Override
	public void update(GeoPoint pnt) {
		QRCodeXMLWriter.updatePoint(pnt);

		QRCodeLayer qr = (QRCodeLayer) TrackerServiceFactory.getLocationService().findTrackerByName(TName.QRCODETRACKER).getLayer();
		synchronized (qr) {
			qr.findPositionById(pnt.getId()).setPosition(new PointF(pnt.x(), pnt.y()));
		}
	}

	/* (non-Javadoc)
	 * @see org.indoor.plasmona.tracker.Capturer#delete(org.indoor.plasmona.models.Position)
	 */
	@Override
	public void delete(GeoPoint pnt) {
		QRCodeXMLWriter.deletePoint(pnt);

		QRCodeLayer qr = (QRCodeLayer) TrackerServiceFactory.getLocationService().findTrackerByName(TName.QRCODETRACKER).getLayer();
		synchronized (qr) {
			qr.remove(qr.findPositionById(pnt.getId()));
		}
	}
}
