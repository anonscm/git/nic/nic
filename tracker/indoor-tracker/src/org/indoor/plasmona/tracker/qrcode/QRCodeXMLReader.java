package org.indoor.plasmona.tracker.qrcode;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.indoor.plasmona.layer.TrackerLayerImpl;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import android.util.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class QRCodeXMLReader.
 */
public class QRCodeXMLReader {

  /** The tag. */
  private static String TAG = "QRCodeXMLReader";

  /** The qr layer. */
  private static QRCodeLayer qrLayer = null;

  /**
   * Load layer.
   * 
   * @param _file
   *          the _file
   * @return the tracker layer impl
   */
  public static TrackerLayerImpl loadLayer(File _file) {
    Log.i(TAG, "loadLayer( " + _file.getAbsolutePath() + " )");
    if (_file.getName().equals("qr-references.xml") && _file.exists()) {
      try {

	SAXParserFactory spf = SAXParserFactory.newInstance();
	SAXParser sp = spf.newSAXParser();
	XMLReader xr = sp.getXMLReader();
	QRCodeXMLParser xmlParser = new QRCodeXMLParser();
	xr.setContentHandler(xmlParser);
	FileInputStream fis = new FileInputStream(_file);
	InputSource inputSource = new InputSource(fis);
	xr.parse(inputSource);

	qrLayer = xmlParser.getLayer();

	return qrLayer;

      } catch (SAXException e) {
	Log.e(QRCodeXMLReader.class.getSimpleName(), e.toString());
      } catch (ParserConfigurationException e) {
	Log.e(QRCodeXMLReader.class.getSimpleName(), e.toString());
      } catch (IOException e) {
	Log.e(QRCodeXMLReader.class.getSimpleName(), e.toString());
      } catch (NumberFormatException e) {
	Log.e(QRCodeXMLReader.class.getSimpleName(), e.toString());
      }
    }
    return new QRCodeLayer();
  }
}
