package org.indoor.plasmona.tracker.qrcode;

import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.tracker.TrackerImpl;
import org.indoor.plasmona.tracker.TrackerService;
import org.indoor.plasmona.tracker.TrackerServiceFactory;
import org.indoor.plasmona.utils.ApplicationContext;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class QRCodeTracker.
 */
public class QRCode extends TrackerImpl {

  /** The Constant TAG. */
  protected static final String TAG = "QRCodeTracker";

  /** The initialized. */
  private boolean initialized = false;

  /** The layer. */
  private QRCodeLayer layer;

  /** The capture. */
  private QRCodeCapturer capture;

  /** The handler. */
  private Handler handler;

  /**
   * Instantiates a new qR code tracker.
   * 
   * @param handler
   *          the handler
   */
  public QRCode() {
    this(TrackerServiceFactory.getLocationService());
  }

  /**
   * Instantiates a new qR code tracker.
   * 
   * @param handler
   *          the handler
   * @param locationService
   *          the location service
   */
  public QRCode(TrackerService locationService) {
    super(locationService);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.indoor.plasmona.tracker.TrackerImpl#init()
   */
  public void init() {
    Log.i(TAG, "init()");
    layer = new QRCodeLayer();

    layer.load();

    capture = new QRCodeCapturer();

    initialized = true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.indoor.plasmona.tracker.TrackerImpl#start()
   */
  @Override
  public void start() {
    Log.i(TAG, "start()");
    running = true;

    Intent i = new Intent("org.indoor.plasmona.activities.QRCodeCaptureAct");
    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    i.putExtra("Capturer", false);
    ApplicationContext.get().startActivity(i);

  }

  /*
   * (non-Javadoc)
   * 
   * @see org.indoor.plasmona.tracker.TrackerImpl#stop()
   */
  @Override
  public void stop() {
    Log.i(TAG, "stop()");
    running = false;

  }

  /**
   * On activity result.
   * 
   * @param pnt
   *          the pnt
   */
  public void onActivityResult(GeoPoint pnt) {
    if (listener != null) {
      Log.i(TAG, "on tracker result");
      listener.onTrackerResult(pnt);
    }
  }

  /**
   * Checks if is initialized.
   * 
   * @return true, if is initialized
   */
  public boolean isInitialized() {
    return initialized;
  }

  /**
   * Sets the initialized.
   * 
   * @param initialized
   *          the new initialized
   */
  public void setInitialized(boolean initialized) {
    this.initialized = initialized;
  }
}
