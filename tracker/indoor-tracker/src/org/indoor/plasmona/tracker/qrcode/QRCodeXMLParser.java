package org.indoor.plasmona.tracker.qrcode;

import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.TName;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.graphics.BitmapFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class QRCodeXMLParser.
 */
public class QRCodeXMLParser extends DefaultHandler {


	/** The qr layer. */
	private QRCodeLayer qrLayer = null;

	/** The geo. */
	private GeoPoint geo = null;

	/**
	 * Instantiates a new qR code xml parser.
	 */
	public QRCodeXMLParser() {

	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#startDocument()
	 */
	@Override
	public void startDocument() throws SAXException {
		// Log.i("!", "Start Doc");
		qrLayer = new QRCodeLayer();
		super.startDocument();
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		if (geo == null)
			geo = new GeoPoint();
		if (localName.equals("QRCodeLocation")) {
			geo.setId(Integer.parseInt(attributes.getValue("id")));
			String bitmapName = attributes.getValue("bitmapName");
			geo.setName(attributes.getValue("id"));
			int resID = ApplicationContext.get().getResources().getIdentifier(bitmapName, "drawable", ApplicationContext.get().getPackageName());
			geo.setIcon(BitmapFactory.decodeResource(ApplicationContext.get().getResources(), resID));
			geo.setType(TName.QRCODETRACKER);
			geo.setPosition(Float.parseFloat(attributes.getValue("x")), Float.parseFloat(attributes.getValue("y")));
			geo.setBitmapName(bitmapName);

		}

		super.startElement(uri, localName, qName, attributes);

	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {

		if (localName.equals("QRCodeLocation")) {
			qrLayer.add(geo);
			geo = null;
		}

		super.endElement(uri, localName, qName);
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endDocument()
	 */
	@Override
	public void endDocument() throws SAXException {
		// Log.i("EndDoc", "EndDoc");
		super.endDocument();
	}

	/**
	 * Gets the layer.
	 *
	 * @return the overlayLayer
	 */
	public QRCodeLayer getLayer() {
		return qrLayer;
	}

	/**
	 * Sets the layer.
	 *
	 * @param qrLayer the new layer
	 */
	public void setLayer(QRCodeLayer qrLayer) {
		this.qrLayer = qrLayer;
	}
}
