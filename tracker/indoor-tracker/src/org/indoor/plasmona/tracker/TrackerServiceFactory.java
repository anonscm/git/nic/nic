package org.indoor.plasmona.tracker;

/**
 * A factory for creating TrackerService objects.
 */
public class TrackerServiceFactory {

  /** The ts. */
  protected static TrackerService ts = null;

  /**
   * Sets the location service.
   * 
   * @param ts
   *          the new location service
   */
  static void setLocationService(TrackerService ts) {
    TrackerServiceFactory.ts = ts;
  }

  /**
   * Gets the location service.
   * 
   * @return the location service
   */
  public static TrackerService getLocationService() {
    if (ts == null) {
      // throw new
      // LocationServiceException("no location service defined!");
      ts = new TrackerServiceImpl();
    }
    return ts;
  }

}
