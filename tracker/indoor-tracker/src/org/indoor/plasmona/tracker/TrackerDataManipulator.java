package org.indoor.plasmona.tracker;

import org.indoor.plasmona.models.GeoPoint;

/**
 * Interface for writing, updating and deleting tracker data.
 *
 * @author Desiree Amling <d.amling@tarent.de>
 * @author Dino Omanovic <d.omanovic@tarent.de>
 */
public interface TrackerDataManipulator {

    /**
     * Writes a {@link GeoPoint} and all data associated with it. Contains bin size for trackers in need of it.
     *
     * @param geoPoint the {@link GeoPoint} to write
     * @param nodeData the node data associated with a {@link GeoPoint} to write (can be any {@link Object})
     * @param binSize  the bin size
     */
    public abstract void writePoint(final GeoPoint geoPoint, final Object nodeData, final int binSize);

    /**
     * Writes a {@link GeoPoint} and all data associated with it.
     *
     * @param geoPoint the {@link GeoPoint} to write
     * @param nodeData the node data associated with a {@link GeoPoint} to write (can be any {@link Object})
     */
    public abstract void writePoint(final GeoPoint geoPoint, final Object nodeData);

    /**
     * Updates an existing {@link GeoPoint}.
     *
     * @param geoPoint the {@link GeoPoint} to update
     */
    public abstract void updatePoint(final GeoPoint geoPoint);

    /**
     * Deletes an existing {@link GeoPoint}.
     *
     * @param geoPoint the {@link GeoPoint} to delete
     */
    public abstract void deletePoint(final GeoPoint geoPoint);


}
