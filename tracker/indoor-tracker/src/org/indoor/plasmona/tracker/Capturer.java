/**
 * @author jens maiero
 *
 * @project plasmona
 * 
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.tracker;

import org.indoor.plasmona.models.GeoPoint;

/**
 * The Interface Capturer. This interface is used for capturing arbitrary events
 * for tracker-based events. Implement the methods captureAt, ..., update
 */
public interface Capturer {

  /**
   * Capture at.
   * 
   * @param pnt
   *          the pnt if your tracker is registered and in the priority list
   *          first the method will be call from the main activity based on the
   *          callback longpressed listener
   */
  public void captureAt(GeoPoint pnt);

  /**
   * Write.
   * 
   * @param pnt
   *          the pnt write the point to file /database or somewhere
   */
  public void write(GeoPoint pnt);

  /**
   * Update.
   * 
   * @param pnt
   *          the pnt
   * 
   */
  public void update(GeoPoint pnt);

  /**
   * Delete.
   * 
   * @param pnt
   *          the pnt
   */
  public void delete(GeoPoint pnt);

}
