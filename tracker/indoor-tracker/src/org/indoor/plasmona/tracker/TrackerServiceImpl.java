package org.indoor.plasmona.tracker;

import java.util.List;
import java.util.Vector;

import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.models.MapData;

import android.util.Log;

/**
 * The Class TrackerServiceImpl.
 */
public class TrackerServiceImpl implements TrackerService {

    /**
     * The Constant TAG.
     */
    protected static final String TAG = "TrackerServiceImpl";

    /**
     * The pos.
     */
    protected GeoPoint pos;

    /**
     * The trackers.
     */
    protected Vector<Tracker> trackers;

    /**
     * The map data.
     */
    protected MapData mapData;

    static {
        TrackerServiceFactory.setLocationService(new TrackerServiceImpl());
    }

    /**
     * Instantiates a new tracker service impl.
     */
    public TrackerServiceImpl() {
        trackers = new Vector<Tracker>();
        pos = new GeoPoint();
        TrackerServiceFactory.setLocationService(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.TrackerService#getPosition()
     */
    @Override
    public GeoPoint getPosition() {
        // TODO Auto-generated method stub
        return pos;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.indoor.plasmona.tracker.TrackerService#updatePosition(org.indoor.plasmona
     * .models.Position)
     */
    @Override
    public GeoPoint updatePosition(GeoPoint pos) {
        Log.i(TAG, "UpdatePosition");
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.indoor.plasmona.tracker.TrackerService#registerTracker(org.indoor.plasmona
     * .tracker.Tracker)
     */
    @Override
    public void registerTracker(Tracker tracker) {
        Log.i(TAG, "Register Tracker " + tracker.getTrackerName());
        trackers.add(tracker);
        tracker.setTrackerService(this);

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.indoor.plasmona.tracker.TrackerService#unregisterTracker(org.indoor
     * .plasmona.tracker.Tracker)
     */
    @Override
    public void unregisterTracker(Tracker tracker) {
        if (trackers.contains(tracker)) {
            trackers.remove(tracker);
        }

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.indoor.plasmona.tracker.TrackerService#registerTracker(java.lang.String
     * )
     */
    @Override
    public void registerTracker(String name) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        Object o = Class.forName(name).newInstance();
        if (!(o instanceof Tracker)) {
            throw new InstantiationException("Tracker " + name + " is not a Tracker");
        }
        this.registerTracker((Tracker) o);

    }

    /**
     * Unregister provider.
     *
     * @param name the name
     * @throws InstantiationException the instantiation exception
     * @throws IllegalAccessException the illegal access exception
     * @throws ClassNotFoundException the class not found exception
     */
    public void unregisterProvider(String name) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        Object o = Class.forName(name).newInstance();
        if (!(o instanceof Tracker)) {
            throw new InstantiationException("Provider " + name + " is not a Provider");
        }
        this.unregisterTracker((Tracker) o);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.TrackerService#getTrackers()
     */
    @Override
    public List<Tracker> getTrackers() {
        return trackers;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.indoor.plasmona.tracker.TrackerService#setMapData(org.indoor.plasmona
     * .models.MapData)
     */
    @Override
    public void setMapData(MapData data) {
        mapData = data;

    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.TrackerService#getMapData()
     */
    @Override
    public MapData getMapData() {
        return mapData;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.indoor.plasmona.tracker.TrackerService#findTrackerByName(java.lang.
     * String)
     */
    @Override
    public Tracker findTrackerByName(String name) {

        for (Tracker t : trackers)
            if (t.getTrackerName().equals(name))
                return t;

        return null;

    }

}
