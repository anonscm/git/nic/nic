package org.indoor.plasmona.tracker;

import org.indoor.plasmona.layer.TrackerLayerImpl;
import org.indoor.plasmona.models.GeoPoint;

import android.os.Handler;

// TODO: Auto-generated Javadoc

/**
 * The Interface Tracker.
 */
public interface Tracker {

    /**
     * Sets the tracker service.
     *
     * @param service the new tracker service
     */
    public void setTrackerService(TrackerService service);

    /**
     * Unset tracker service.
     *
     * @param service the service
     */
    public void unsetTrackerService(TrackerService service);

    /**
     * Gets the tracker name.
     *
     * @return the tracker name
     */
    public String getTrackerName();

    /**
     * Gets the x.
     *
     * @return the x
     */
    public float getX();

    /**
     * Gets the y.
     *
     * @return the y
     */
    public float getY();

    /**
     * Gets the position.
     *
     * @return the position
     */
    public GeoPoint getPosition();

    /**
     * Inits the.
     */
    public void init();

    /**
     * Start.
     */
    public void start();

    /**
     * Stop.
     */
    public void stop();

    /**
     * Unregister tracker.
     */
    public void unregisterTracker();

    /**
     * Sets the tracker result listener.
     *
     * @param listener the new tracker result listener
     */
    public void setTrackerResultListener(TrackerResultListener listener);

    public void setHandler(Handler handler);

    /**
     * Register capturer.
     *
     * @param capturer the capturer
     */
    public void registerCapturer(Capturer capturer);

    /**
     * Checks for capturer.
     *
     * @return true, if successful
     */
    public boolean hasCapturer();

    /**
     * Gets the capturer.
     *
     * @return the capturer
     */
    public Capturer getCapturer();

    /**
     * Register loader.
     *
     * @param loader the loader
     */
    public void registerLoader(Loader loader);


    public boolean hasConfiguration();


    public Configuration getConfiguration();


    public void registerConfiguration(Configuration config);

    /**
     * Checks for loader.
     *
     * @return true, if successful
     */
    public boolean hasLoader();

    /**
     * Gets the loader.
     *
     * @return the loader
     */
    public Loader getLoader();

    /**
     * Register layer.
     *
     * @param layer the layer
     */
    public void registerLayer(TrackerLayerImpl layer);

    /**
     * Checks for layer.
     *
     * @return true, if successful
     */
    public boolean hasLayer();

    /**
     * Gets the layer.
     *
     * @return the layer
     */
    public TrackerLayerImpl getLayer();

    /**
     * Unregister layer.
     *
     * @param string the string
     */
    public void unregisterLayer(String string);


}
