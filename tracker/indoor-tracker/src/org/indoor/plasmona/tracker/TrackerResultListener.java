package org.indoor.plasmona.tracker;

import org.indoor.plasmona.models.GeoPoint;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving trackerResult events. The class that is
 * interested in processing a trackerResult event implements this interface, and
 * the object created with that class is registered with a component using the
 * component's <code>addTrackerResultListener<code> method. When
 * the trackerResult event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see TrackerResultEvent
 */
public interface TrackerResultListener {

  /**
   * On tracker result.
   * 
   * @param pos
   *          the pos
   */
  public void onTrackerResult(GeoPoint pos);
}
