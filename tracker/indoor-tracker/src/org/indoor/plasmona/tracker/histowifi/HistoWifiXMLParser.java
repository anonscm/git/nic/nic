package org.indoor.plasmona.tracker.histowifi;

import android.graphics.BitmapFactory;
import android.util.Log;
import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.Prefs;
import org.indoor.plasmona.utils.TName;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;

// TODO: Auto-generated Javadoc
/**
 * The Class HistoWifiXMLParser.
 */
public class HistoWifiXMLParser extends DefaultHandler {

  /** The histo layer. */
  private HistoWifiLayer histoLayer;

  /** The geo. */
  private GeoPoint geo = null;

  /** The histograms. */
  private HashMap<String, HashMap<Integer, Float>> histograms;

  /** The histogram. */
  HashMap<Integer, Float> histogram;

  /** The current key. */
  private String currentKey;

  /** The min. */
  private int MIN = 0;

  /**
   * Instantiates a new histo wifi xml parser.
   */
  public HistoWifiXMLParser() {

	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#startDocument()
	 */
	@Override
	public void startDocument() throws SAXException {

		histoLayer = new HistoWifiLayer();
		super.startDocument();
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		super.startElement(uri, localName, qName, attributes);

		if (localName.equals("Location")) {
			geo = new GeoPoint();

			Log.i("XML Wifi", attributes.getValue("x") + " " + attributes.getValue("y") + " " + attributes.getValue("bitmapName")+ " "+ attributes.getValue("id"));
			geo.setPosition(Float.parseFloat(attributes.getValue("x")), Float.parseFloat(attributes.getValue("y")));
			String bitmapName = attributes.getValue("bitmapName");
			int resID = ApplicationContext.get().getResources().getIdentifier(bitmapName, "drawable", ApplicationContext.get().getPackageName());
            if(Prefs.isFingerprintVisible()) {
                geo.setIcon(BitmapFactory.decodeResource(ApplicationContext.get().getResources(), resID));
			    geo.setBitmapName(bitmapName);
            }
			if (Integer.parseInt(attributes.getValue("id")) > MIN )
				MIN = Integer.parseInt(attributes.getValue("id"));

			geo.setId( Integer.parseInt(attributes.getValue("id") ));
			geo.setType( TName.HISTOTRACKER );
			histograms = new HashMap<String, HashMap<Integer, Float>>();
		}

		if (localName.equals("Node")) {

			histogram = new HashMap<Integer, Float>();
			currentKey = attributes.getValue("BSSID");

		}

		if (localName.equals("Bin")) {

			histogram.put(Integer.parseInt(attributes.getValue("level")), Float.parseFloat(attributes.getValue("probability")));
			histograms.put(currentKey, histogram);
		}
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {

		if (localName.equals("Location")) {

			geo.setData(histograms);
			histoLayer.add(geo);
		}

		super.endElement(uri, localName, qName);
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endDocument()
	 */
	@Override
	public void endDocument() throws SAXException {

		super.endDocument();

	}

	/**
	 * Gets the layer.
	 *
	 * @return the overlayLayer
	 */
	public HistoWifiLayer getLayer() {

		return histoLayer;
	}

	/**
	 * Sets the layer.
	 *
	 * @param histoLayer the new layer
	 */
	public void setLayer(HistoWifiLayer histoLayer) {

		this.histoLayer = histoLayer;
	}

	/**
	 * Gets the new id.
	 *
	 * @return the new id
	 */
	public int getNewID() {
		return MIN;
	}

}
