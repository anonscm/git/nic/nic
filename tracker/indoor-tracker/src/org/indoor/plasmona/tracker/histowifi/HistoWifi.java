package org.indoor.plasmona.tracker.histowifi;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Message;
import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.tracker.TrackerImpl;
import org.indoor.plasmona.tracker.TrackerService;
import org.indoor.plasmona.tracker.TrackerServiceFactory;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.TName;

import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * The Class HistoWifiTracker.
 */
public class HistoWifi extends TrackerImpl {

    /**
     * The Constant TAG.
     */
    private static final String TAG = "HistoWifiTracker";

    /**
     * The initialized.
     */
    private boolean initialized = false;

    /**
     * The capture.
     */
    private HistoWifiCapturer capture;

    /**
     * The layer.
     */
    private HistoWifiLayer layer;

    /**
     * The matcher.
     */
    private HistoWifiMatcher matcher;


    private HistoConfiguration hconfig;

    /**
     * The interval ms.
     */
    public static long INTERVAL_MS = HistoConfiguration.getInstance().getTimeInterval();

    /**
     * The timer.
     */
    protected Timer timer;


    /**
     * Instantiates a new histo wifi tracker.
     */
    public HistoWifi() {

    }

    /**
     * Instantiates a new histo wifi tracker.
     *
     * @param locationService the location service
     */
    public HistoWifi(TrackerService locationService) {
        super(locationService);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.TrackerImpl#init()
     */
    public void init() {

        layer = new HistoWifiLayer();
        layer.load();

        hconfig = HistoConfiguration.getInstance();
        TrackerServiceFactory.getLocationService().findTrackerByName(TName.HISTOTRACKER).registerConfiguration(hconfig);

        matcher = new HistoWifiMatcher();

        capture = new HistoWifiCapturer();
        TrackerServiceFactory.getLocationService().findTrackerByName(TName.HISTOTRACKER).registerCapturer(capture);

        initialized = true;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.TrackerImpl#start()
     */
    @Override
    public void start() {
        SharedPreferences plasmona_prefs = PreferenceManager.getDefaultSharedPreferences(ApplicationContext.get());
        matcher.init();
        // INTERVAL_MS = plasmona_prefs.getInt("bfwifi_interval", 0);

        running = true;

        timer = new Timer("Update", false);
        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                update();
            }
        };
        timer.schedule(task, 0, HistoConfiguration.getInstance().getTimeInterval());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.TrackerImpl#stop()
     */
    @Override
    public void stop() {


        running = false;

        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }

        matcher.setInitialized(false);

    }

    /**
     * Update.
     */
    private void update() {
        long time = SystemClock.currentThreadTimeMillis();
        if (running) {
            matcher.queue();

            GeoPoint l_pos = null;
            if (HistoConfiguration.getInstance().getMatching() == HistoConfiguration.MATCH.MEAN_VALUE)
                l_pos = (GeoPoint) matcher.doMatchAtRouterLevel();
            else if (HistoConfiguration.getInstance().getMatching() == HistoConfiguration.MATCH.BIN_LEVEL)
                l_pos = (GeoPoint) matcher.doMatchAtBinLevel();
            if (l_pos != null) {

                if (matcher.isInitialized()) {
                    Message msg = new Message();
                    msg.setTarget(handler);
                    msg.what = 15;
                    msg.obj = true;
                    msg.sendToTarget();
                }

               //l_pos.setData(System.currentTimeMillis());
                l_pos.setType(TName.HISTOTRACKER);
                Log.i(TAG, "update histo-based hwifi tracker " + l_pos + " listener " + listener);

                if (listener != null) {
                    Log.i(TAG, "on tracker result");
                    listener.onTrackerResult(l_pos);
                }
            }
            Log.d(TAG, "Tracked in " + String.valueOf(SystemClock.currentThreadTimeMillis() - time) + " ms.");
        }
    }

    /**
     * On activity result.
     *
     * @param pnt the pnt
     */
    public void onActivityResult(GeoPoint pnt) {
        if (listener != null) {
            Log.i(TAG, "on tracker result");
            listener.onTrackerResult(pnt);
        }
    }

    /**
     * Checks if is initialized.
     *
     * @return true, if is initialized
     */
    public boolean isInitialized() {
        return initialized;
    }

    /**
     * Sets the initialized.
     *
     * @param initialized the new initialized
     */
    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

}
