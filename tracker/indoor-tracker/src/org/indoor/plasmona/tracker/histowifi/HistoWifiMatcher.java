/**
 * @author jens maiero
 *
 * @project plasmona
 *
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.tracker.histowifi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.Vector;

import org.indoor.plasmona.layer.TrackerLayerImpl;
import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.models.Positionable;
import org.indoor.plasmona.tracker.TrackerServiceFactory;
import org.indoor.plasmona.tracker.histowifi.HistoConfiguration.ALGORITHM;
import org.indoor.plasmona.tracker.histowifi.HistoConfiguration.INTERPOLATE;
import org.indoor.plasmona.tracker.histowifi.distance.DistanceAlgorithm;
import org.indoor.plasmona.tracker.histowifi.interpolation.Interpolation;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.Bin;
import org.indoor.plasmona.utils.MapSort;
import org.indoor.plasmona.utils.OutlierDetection;
import org.indoor.plasmona.utils.TName;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.util.Log;

/**
 * The Class HistoWifiMatcher.
 */
public class HistoWifiMatcher {

    /**
     * The logging-prefix for this class.
     */
    private static final String TAG = "HistoWifiMatcher";

    /**
     * The android-WifiManager which does the scanning.
     */
    private WifiManager wifiManager;

    /**
     * In the buffer we collect histograms with a timestamp. Old histograms will be deleted and new ones will be added.
     * That way the buffer contains an approximation of a multi-scan-histogram of the current position. In contrast to
     * the admin, who can be supposed to stand sill until the required number of scans is complete, the enduser will
     * walk around and will not be aware of the background-scanning that is taking place. A bigger buffer will create
     * nicer curves but will include scans from the past, when the user might have been at some other position.
     */
    private Vector<HistoWifiModel> buffer;

    // TODO
    private HashMap<Integer, HashMap<String, Float>> fingerprintStorage = new HashMap<Integer, HashMap<String, Float>>();

    // TODO
    private HashMap<String, Float> histogramBuffer;

    /**
     * The result.
     * TODO
     */
    private HistoWifiModel fingerprintBuffer;

    // The layer for the tracker with all saved fingerprints.
    // Be careful not to confuse the GeoPoints, that the TrackerLayerImpl holds, with the OSM-GeoPoints.
    // The org.indoor.plasmona.models.GeoPoint is more like our OverlayItem in that it also contains display-stuff.
    // More importantly it also contains a generic "data"-field in which the HistoWifiCapturer puts the histogram.
    // (the Pedometer also uses this data-field to store stuff)
    private TrackerLayerImpl layerData;

    /**
     * This map counts how often each accesspoint was visible over the scans in our buffer. This is used to calculate
     * the observed probabilities of the signal strength levels. (see HistoCaptureThread or HistogramBuilder).
     */
    private HashMap<String, Integer> occurrence;

    /**
     * Indicates that init() has been executed. Only then will the HistoWifi.update send messages. (What messages? To whom?)
     */
    private boolean initialized = false;

    // TODO
    private Vector<Integer> binarray;

    /**
     * Inits the HistoWifiMatcher. Turn on wifi, init collections, build binarray, load fingerprints.
     */
    public void init() {
        ApplicationContext.get(); // This method doesn't seem to have any sideeffects. Is this call useless?

        wifiManager = (WifiManager) ApplicationContext.get().getSystemService(Context.WIFI_SERVICE);

        boolean wifiEnabled = wifiManager.isWifiEnabled();
        if (!wifiEnabled) {
            wifiManager.setWifiEnabled(true);

        }

        binarray = new Vector<Integer>();
        fingerprintBuffer = new HistoWifiModel();
        histogramBuffer = new HashMap<String, Float>();

        // Could the binarray-concept be replaced by direct calculations? See class Bin.
        for (int i = Bin.BIN_MIN_DB; i <= Bin.BIN_MAX_DB; i += getBinSize()) {
            binarray.add(i + (getBinSize() / 2));
        }

        buffer = new Vector<HistoWifiModel>();
        occurrence = new HashMap<String, Integer>();

        //initial load the stored fingerpints
        loadFingerprints();

        setInitialized(true);
    }

    private void loadFingerprints() {
        //get the layer for the tracker with all saved fingerprints
        layerData = TrackerServiceFactory.getLocationService().findTrackerByName(TName.HISTOTRACKER).getLayer();
        HashMap<String, Float> fingerprint_distribution;

        for (GeoPoint location : layerData) {
            HashMap<String, HashMap<Integer, Float>> fingerprint = (HashMap<String, HashMap<Integer, Float>>) location.getData();
            // Log.i(TAG, "HISTOGRAMM BUFFER " + fingerprint);
            fingerprint_distribution = new HashMap<String, Float>();

            for (Entry<String, HashMap<Integer, Float>> data : fingerprint.entrySet()) {
                fingerprint_distribution.put(data.getKey(), -mean((HashMap<Integer, Float>) data.getValue().clone()) / 100.f);

            }
            fingerprintStorage.put(location.getId(), fingerprint_distribution);
        }

    }

    /**
     * Queue: add the raw scan data to the buffer all data are buffered from now
     * to BUFFER_TIME.
     */
    public void queue() {

        HistoWifiModel wifiscan = new HistoWifiModel();

        wifiManager.startScan();
        List<ScanResult> scans = wifiManager.getScanResults();

        long now = System.currentTimeMillis();

        wifiscan.setTimestamp(System.currentTimeMillis());

        if (scans != null) {

            for (ScanResult scan : scans) {
                if (Bin.getBin(scan.level) != -1) {
                    if (!wifiscan.containsKey(scan.BSSID)) {
                        HashMap<Integer, Float> bar = new HashMap<Integer, Float>();

                        bar.put(Bin.getBin(scan.level), 1.0f);
                        wifiscan.put(scan.BSSID, bar);


                    } else {

                        HashMap<Integer, Float> bar = wifiscan.get(scan.BSSID);
                        if (bar.containsKey(Bin.getBin(scan.level))) {
                            bar.put(Bin.getBin(scan.level), (float) bar.get(Bin.getBin(scan.level)) + 1.0f);

                        } else {
                            bar.put(Bin.getBin(scan.level), 1.0f);
                        }
                    }
                }
            }
            buffer.add(wifiscan);
        }

        for (int i = buffer.size() - 1; i >= 0; i--) {
            if (buffer.get(i).getTimestamp() < (now - getBufferTime())) {
                buffer.remove(i);
            }
        }

        doOccurence();

        doHistogram();

        doDataStructure();
        // Log.i(TAG, "histo data: "+buffer);
    }

    private void doDataStructure() {

        for (Entry<String, HashMap<Integer, Float>> data : fingerprintBuffer.entrySet()) {
            // mean rssi for current location with scan acces points
            histogramBuffer.put(data.getKey(), -mean((HashMap<Integer, Float>) data.getValue().clone()) / 100.f);
        }
    }

    /**
     * doOccurence count the occurrence of the different mac adresses.
     */
    private void doOccurence() {
        occurrence.clear();

        for (HistoWifiModel data : buffer) {
            for (Entry<String, HashMap<Integer, Float>> entry : data.entrySet()) {
                if (!occurrence.containsKey(entry.getKey()))
                    occurrence.put(entry.getKey(), 1);
                else
                    occurrence.put(entry.getKey(), Integer.valueOf(((Integer) occurrence.get(entry.getKey()).intValue() + 1)));
            }
        }
    }

    /**
     * calculates the histogram based on the pre-calculated data (see doOccurence
     * and queue).
     */
    private void doHistogram() {
        fingerprintBuffer.clear();

        for (HistoWifiModel data : buffer) {
            HashMap<Integer, Float> histogram;
            for (Entry<String, HashMap<Integer, Float>> entry : data.entrySet()) {

                String key = entry.getKey();
                HashMap<Integer, Float> value = entry.getValue();

                for (Entry<Integer, Float> subentry : value.entrySet()) {
                    int level = subentry.getKey();

                    if (!fingerprintBuffer.containsKey(key)) {
                        histogram = new HashMap<Integer, Float>();
                        histogram.put(level, (1.0f / occurrence.get(key).intValue()));
                        fingerprintBuffer.put(key, histogram);
                    } else {
                        if (!fingerprintBuffer.get(key).containsKey(level))
                            fingerprintBuffer.get(key).put(level, (1.0f / occurrence.get(key).intValue()));
                        else {
                            float tmp = fingerprintBuffer.get(key).get(level) + (1.0f / occurrence.get(key).intValue());
                            fingerprintBuffer.get(key).put(level, tmp);
                        }
                    }
                }
            }
        }
    }
    /**
     * Do match.
     *
     * @return the positionable
     */
    //
    public Positionable doMatchAtRouterLevel() {

        double partDistance = 0.0;
        int id = -1;
        TreeMap<Integer, Double> id_distance_map = new TreeMap<Integer, Double>();
        // for each saved fingerprint
        for (Entry<Integer, HashMap<String, Float>> entry : fingerprintStorage.entrySet()) {
            HashMap<String, Float> histogram = entry.getValue();
            partDistance = distanceMean(histogram, histogramBuffer);

            if (partDistance != Double.MAX_VALUE)
                id_distance_map.put(entry.getKey(), partDistance);
        }

        Map<Integer, Double> distance_map = MapSort.sortByComparator(id_distance_map);
        if (distance_map.size() > 0)
            id = (Integer) distance_map.keySet().toArray()[0];
        Log.i(TAG, "dist " + distance_map);

        if (getNumberOfNeighbours() == 1 || distance_map.size() == 1)
            return layerData.findPositionById(id);

        return interpolate(distance_map);

    }


    public Positionable doMatchAtBinLevel() {

        HashMap<String, HashMap<Integer, Float>> fp = fingerprintBuffer;
        double distance = 0.0;
        int id = -1;
        TreeMap<Integer, Double> id_distance_map = new TreeMap<Integer, Double>();
        // Log.i(TAG, "resulti");
        for (GeoPoint location : layerData) {

            //The location data is sometime set as a Long, see HistoWifi#update
            //okay by j. maiero: by T. Kudla 22.08.13: fix this
            if (location.getData() instanceof HashMap) {
                HashMap<String, HashMap<Integer, Float>> fingerprint = (HashMap<String, HashMap<Integer, Float>>) location.getData();


                distance = distance(fingerprint, fp);
                if (distance != Double.MAX_VALUE)
                    id_distance_map.put(location.getId(), distance);
            }
        }
        Map<Integer, Double> distance_map = MapSort.sortByComparator(id_distance_map);

        Log.d(TAG, "distance map: " + distance_map);

        if (distance_map.size() > 0)
            id = (Integer) distance_map.keySet().toArray()[0];

        if (getNumberOfNeighbours() == 1 || distance_map.size() == 1)
            return layerData.findPositionById(id);

        return interpolate(distance_map);
    }

    private GeoPoint interpolate(Map<Integer, Double> distanceMap) {
        ArrayList<GeoPoint> pointList;

        if (hasOutlierDect() && distanceMap.size() > 1)
            // Outlier-detection is active and we have enough neighbours to spare, see which fingerprint-positions remain:
            pointList = outlier(distanceMap);
        else {
            // Outlier-detection is off or we have only a single neighbour anyway:
            pointList = new ArrayList<GeoPoint>();
            for (Integer myid : distanceMap.keySet()) {
                // Get the fingerprint-position for the fingerprint with this id:
                pointList.add(layerData.findPositionById(myid));
            }
        }

        Interpolation interpolation = getInterpolation().getImpl();
        final GeoPoint result = interpolation.interpolate(distanceMap, pointList);

        return result;
    }

    private ArrayList<GeoPoint> outlier(Map<Integer, Double> distance_map) {
        int neighbours = getNumberOfNeighbours()+1;

        if (distance_map.size() < neighbours)
            neighbours = distance_map.size();

        int i = 0;
        ArrayList<GeoPoint> list = new ArrayList<GeoPoint>();
        for (Map.Entry<Integer, Double> entry : distance_map.entrySet()) {
            if (i >= neighbours) {
                break;
            }
            i++;
            list.add(layerData.findPositionById(entry.getKey()));
        }

        OutlierDetection outlierDetection = new OutlierDetection();
        outlierDetection.setCandidates(list);

        return outlierDetection.removeOutlier();

    }

    private double distance(HashMap<String, HashMap<Integer, Float>> pRefFingerprint, HashMap<String, HashMap<Integer, Float>> pBuffer) {

        double mean_buf, mean_ref, var_buf, var_ref;
        int matches = 0;
        double distance = 0.0;
        double meanDistance = 0.0;

        final ALGORITHM algo = getDistanceAlgoritm();

        for (String macRef : pRefFingerprint.keySet()) {
            if (pBuffer.containsKey(macRef)) {
                matches++;
                mean_buf = -mean(pBuffer.get(macRef));
                mean_ref = -mean(pRefFingerprint.get(macRef));
                var_ref = var(pRefFingerprint.get(macRef));

                meanDistance += Math.abs(mean_buf - mean_ref);

                switch (algo) {
                    case GAUSS:
                        double tmp = gauss(mean_buf / 100, mean_ref / 100, var_ref);
                        distance += tmp;
                        if (tmp == 0) matches = matches - 1;

                        break;
                    default:
                        DistanceAlgorithm algorithm = algo.getImpl();

                        distance += algorithm.calcBin(pBuffer.get(macRef), pRefFingerprint.get(macRef), binarray);
                        break;
                }
            }
        }

        float confidence = (float) matches / (float) pRefFingerprint.size();
        //Log.i(TAG, "resulti confidence:" + confidence + " matches: " + matches + " ref size: " + pRefFingerprint.size() + " distance: " + distance / matches + " distance with conf" + distance / (confidence * matches) + " m: " + meandistance / matches); // + " v:" + sderivationdistance / matches);
        //Log.i(TAG, "resulti c " + distance / (confidence * matches) * (meandistance / matches) + " " + distance);
        if (confidence > getConfidenceFactor() && (meanDistance / matches) < 10)
            return distance / (confidence * matches) * (meanDistance / matches);
        else
            return Double.MAX_VALUE;
    }


    private double gauss(double z, double m, double v) {

        if (v != 0)
            return (1.0f / Math.sqrt(2 * Math.PI * Math.pow(v, 2))) * Math.exp(-((Math.pow(z - m, 2) / (2 * Math.pow(v, 2)))));
        else return 0;

    }

    /**
     * Liefert die Distnaz zwischen Fingerprint unt aktueller Position (Histogram). Dazu wird der
     * Algorithmus verwendet, der in {@link HistoConfiguration#getAlgorithm()} gesetzt ist.
     *
     * @param pHistogram
     * @param pHistogramBuffer
     * @return
     */
    private double distanceMean(HashMap<String, Float> pHistogram, HashMap<String, Float> pHistogramBuffer) {
        DistanceAlgorithm algorithm = getDistanceAlgoritm().getImpl();

        if(algorithm != null){
            return algorithm.calc(pHistogram, pHistogramBuffer);
        }

        return 0.0;
    }


    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    private float mean(HashMap<Integer, Float> value) {
        float mean = 0.0f;

        for (Integer bins : value.keySet()) {
            mean += value.get(bins) * bins;
        }
        return mean;
    }


    private float var(HashMap<Integer, Float> value) {
        float mean = 0.0f;
        float mean_pow = 0.0f;

        for (Integer bins : value.keySet()) {
            mean += value.get(bins) * bins;
            mean_pow += value.get(bins) * Math.pow(bins, 2);
        }

        float var = (float) (mean_pow - Math.pow(mean(value), 2));
        if (var < 0)
            return 0.f;
        return var;
    }

    protected float getConfidenceFactor() {
        return HistoConfiguration.getInstance().getConfidenceFactor();
    }
    protected INTERPOLATE getInterpolation() {
        return HistoConfiguration.getInstance().getInterpolation();
    }
    protected boolean hasOutlierDect() {
        return HistoConfiguration.getInstance().hasOutlierDect();
    }
    protected int getNumberOfNeighbours() {
        return HistoConfiguration.getInstance().getNumberOfNeighbours();
    }
    protected long getBufferTime() {
        return HistoConfiguration.getInstance().getBufferTime();
    }
    protected int getBinSize() {
        return HistoConfiguration.getInstance().getBinSize();
    }
    protected ALGORITHM getDistanceAlgoritm() {
        return HistoConfiguration.getInstance().getAlgorithm();
    }
}
