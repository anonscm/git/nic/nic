package org.indoor.plasmona.tracker.histowifi.distance;

import java.util.List;
import java.util.Map;

public class BhattcharyyaAlgorithm implements DistanceAlgorithm {

    @Override
    public double calcBin(Map<Integer, Float> buffer,
            Map<Integer, Float> reference,
            List<Integer> binarray) {

        float p, q;

        float result = 0.0f;
        float s1 = 0, s2 = 0;


        for (Integer bin : binarray) {

            if (buffer.get(bin) != null && reference.get(bin) != null) {
                p = buffer.get(bin);
                q = reference.get(bin);

                result += Math.sqrt(p * q);
                s1 += p;
                s2 += q;
            } else {

                p = 1.2f;
                q = 0.2f;

                result += Math.sqrt(p * q);
                s1 += p;
                s2 += q;
            }
        }

        s1 *= s2;
        s1 = (float) (Math.abs(s1) > 0.0f ? 1. / Math.sqrt(s1) : 1.);
        result = (float) Math.sqrt(Math.max(1. - result * s1, 0.));

        return result;
    }

    @Override
    public double calc(Map<String, Float> fingerprint,
            Map<String, Float> histogram) {

        return Double.MAX_VALUE;
    }

}
