package org.indoor.plasmona.tracker.histowifi.metric;

import java.util.HashMap;

public class BhattcharyyaMetric extends AbstractMetric {

    @Override
    public float calc(HashMap<Integer, Float> value,
            HashMap<Integer, Float> value2) {

        normalize(value, value2);
        float result = 0.0f;
        float s1 = 0, s2 = 0;

        for (Integer bins : value.keySet()) {

            float a = value.get(bins);
            float b = value2.get(bins);

            result += Math.sqrt(a * b);
            s1 += a;
            s2 += b;
        }
        s1 *= s2;
        s1 = (float) (Math.abs(s1) > 0.0f ? 1. / Math.sqrt(s1) : 1.);
        result = (float) Math.sqrt(Math.max(1. - result * s1, 0.));

        return result;
    }

}
