package org.indoor.plasmona.tracker.histowifi;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.Bin;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.SystemClock;
import android.util.Log;

/**
 * The Class HistoCaptureThread. It does the capturing of the wifi-histograms. This capturing-process does not depend
 * on points/coordinates or some such. It just looks at the current wifi-situation.
 * This class might be usable as a standalone-thread in a java-desktop-application. But in the android-app it is never
 * started as a thread (for which Thread.start would be used). Instead, its run-method is called directly inside an
 * AsyncTask.
 */
public class HistoCaptureThread extends Thread {

    /**
     * The logging-prefix for this class.
     */
    private static final String TAG = "HistoCaptureThread";

    /**
     * The capturer. It is never used here. It's actually the other way round: the HistoWifiCapturer creates and uses
     * this HistoCaptureThread.
     */
    private HistoWifiCapturer capturer;

    /**
     * The wifi manager is the android-way of talking to the wifi-hardware.
     */
    private WifiManager wifiManager;

    /**
     * A histogram is a mapping of BSSID (MAC-address of an accesspoint) to a list of (signalstrength,
     * probability)-pairs.
     */
    public HashMap<String, HashMap<Integer, Float>> histogramm;

    /**
     * The occurence-map counts how often a BSSID occurs in a histogram. The HistoCaptureThread does multiple scans.
     * See HistoConfiguration.NUMBER_OF_SCANS (currently 50). It is possible that the WifiManager doesn't return the
     * exact same list of accesspoints in each of these scans. This information is required to calculate the
     * probability of each measured signal-strength-level.
     */
    public HashMap<String, Integer> occurence;

    /**
     * Instantiates a new histo capture thread.
     *
     * @param rec the capturer. An unused parameter.
     */
    public HistoCaptureThread(HistoWifiCapturer rec) {
        capturer = rec;

        histogramm = new HashMap<String, HashMap<Integer, Float>>();

        occurence = new HashMap<String, Integer>();

        wifiManager = (WifiManager) ApplicationContext.get().getSystemService(ApplicationContext.get().WIFI_SERVICE);

        boolean wifiEnabled = wifiManager.isWifiEnabled();
        if (!wifiEnabled) {
            wifiManager.setWifiEnabled(true);
        }
    }

    /*
     * Normally this method would be called by the base-class when Thread.start is called. Here it is called directly
     * by HistoWifiCapturer, i.e. in the same thread (but see the AsyncTask in PlasmonaActivity).
     *
     * @see java.lang.Thread#run()
     */
    public void run() {
        // The time is only used for the log. It is not required for the capturing per se.
        long time = SystemClock.currentThreadTimeMillis();

        doCapture();

        // A bit redundant as the same statement appears in the calling method...
        Log.i(TAG, "Time for Data" + String.valueOf(SystemClock.currentThreadTimeMillis() - time) + " ms.");
    }

    /**
     * Do capture. Does a series of scans which will usually, for any given accesspoint, not measure the exact same
     * signal strength each time. Therefore the observed probability of each measured strength is calculated and
     * recorded in the histogram.
     */
    public void doCapture() {

        for (int i = 0; i < HistoConfiguration.getInstance().getNumberOfScans(); i++) {
            // Starting a scan and then immediately asking for results doesn't seem to be the recommended way because
            // the scanning happens asynchronously (i.e. startScan returns immediately).
            // See:
            // http://stackoverflow.com/a/7625152
            // http://www.androidsnippets.com/scan-for-wireless-networks
            wifiManager.startScan();
            List<ScanResult> scans = wifiManager.getScanResults();

            if (scans != null)
                for (ScanResult scan : scans) {
                    // The bin is used to group signal-levels,
                    // e.g. -37, -38, -39 and -40 will all be considered equal to -38.
                    int levelBin = Bin.getBin(scan.level);
                    if (levelBin != -1) { // Signals in the bin "-1" are either too strong or too weak. Why?
                        if (!histogramm.containsKey(scan.BSSID)) {

                            HashMap<Integer, Float> bar = new HashMap<Integer, Float>();

                            bar.put(levelBin, 1.0f);
                            histogramm.put(scan.BSSID, bar);
                            occurence.put(scan.BSSID, 1);

                        } else {
                            HashMap<Integer, Float> bar = histogramm.get(scan.BSSID);
                            occurence.put(scan.BSSID, occurence.get(scan.BSSID) + 1);
                            if (bar.containsKey(levelBin)) {
                                bar.put(levelBin, (float) bar.get(levelBin) + 1.0f);

                            } else {
                                bar.put(levelBin, 1.0f);

                            }
                        }
                    }
                }


            try {
                // Wait for (currently) 100 milliseconds. => A full scan takes at least 5 seconds.
                sleep(HistoConfiguration.getInstance().getTimeInterval());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        // For each accesspoint...
        for (Entry<String, HashMap<Integer, Float>> d : histogramm.entrySet()) {
            System.out.println(d.getKey());
            HashMap<Integer, Float> value = d.getValue();
            // For each signal-strength-level that occurred... calculate its observed probability:
            for (Integer bin : value.keySet()) {
                value.put(bin, value.get(bin) / occurence.get(d.getKey()));
            }
        }
    }
}