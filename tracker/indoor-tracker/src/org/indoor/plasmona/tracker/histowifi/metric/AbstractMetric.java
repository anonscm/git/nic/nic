package org.indoor.plasmona.tracker.histowifi.metric;

import java.util.Collections;
import java.util.HashMap;

public abstract class AbstractMetric implements Metric {

    @Override
    public abstract float calc(HashMap<Integer, Float> value, HashMap<Integer, Float> value2);

    /**
     * add all missing bins after normalize both have the same size with identical
     * bin
     *
     * @param value  the value
     * @param value2 the value2
     */
    protected void normalize(HashMap<Integer, Float> value, HashMap<Integer, Float> value2) {

        int minref = Collections.max(value.keySet());
        int maxref = Collections.min(value.keySet());
        int mincurr = Collections.max(value2.keySet());
        int maxcurr = Collections.min(value2.keySet());

        for (int i = ((maxcurr < maxref) ? maxcurr : maxref); i <= ((mincurr > minref) ? mincurr : minref); i += 2) {
            if ((!value.keySet().contains(i)) && value2.keySet().contains(i))
                value.put(i, 0.0f);

            if ((!value2.keySet().contains(i)) && value.keySet().contains(i))
                value2.put(i, 0.0f);
        }
    }

    /**
     * Distance point2 point.
     *
     * @param x1 the x1
     * @param y1 the y1
     * @param x2 the x2
     * @param y2 the y2
     * @return the float
     */
    protected float distancePoint2Point(float x1, float y1, float x2, float y2) {
        float dx = Math.abs(x1 - y1);
        float dy = Math.abs(x2 - y2);

        if (dx <= 2.0)
            return dy * (dx + 1.0f);
        else
            return 0.0f;
    }
}
