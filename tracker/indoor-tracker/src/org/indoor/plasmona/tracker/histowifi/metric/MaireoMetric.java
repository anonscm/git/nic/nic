package org.indoor.plasmona.tracker.histowifi.metric;

import java.util.HashMap;
import java.util.Map.Entry;

public class MaireoMetric extends AbstractMetric {

    @Override
    public float calc(HashMap<Integer, Float> value,
            HashMap<Integer, Float> value2) {

        normalize(value, value2);

        float dis = 0.0f;
        int cnt = 0, negative = 0;
        for (Entry<Integer, Float> reference : value.entrySet()) {

            for (Entry<Integer, Float> current : value2.entrySet()) {

                float tmp_distance = distancePoint2Point(
                        Float.valueOf(reference.getKey()),
                        Float.valueOf(current.getKey()),

                        Float.valueOf(reference.getValue()),
                        Float.valueOf(current.getValue()));

                if ((tmp_distance > 0.0f)) {
                    dis += tmp_distance;
                    cnt++;
                } else
                    negative++;
            }
        }

        if (cnt >= 1.0) {

            return (dis / cnt) * (float) (negative / cnt);
        }

        return 0.0f;
    }

}
