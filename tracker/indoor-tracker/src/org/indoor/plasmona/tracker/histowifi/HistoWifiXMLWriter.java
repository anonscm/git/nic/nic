package org.indoor.plasmona.tracker.histowifi;

import android.graphics.PointF;
import android.util.Log;
import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.tracker.TrackerDataManipulator;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.Prefs;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Class HistoWifiXMLWriter.
 */

public class HistoWifiXMLWriter implements TrackerDataManipulator {

    /**
     * The Constant TAG.
     */
    private static final String TAG = "HistoWifiXMLWriter";

    /**
     * The wifi ref file.
     */
    private static String WIFI_REF_FILE = "histo-wifi-references.xml";

    /**
     * The project.
     */
    private static String project;


    /**
     * Write point.
     *
     * @param geoPoint     the geoPoint
     * @param nodeData    the nodeData
     * @param binSize the binSize
     */
    @Override
    public void writePoint(final GeoPoint geoPoint, final Object nodeData, final int binSize) {
        project = ApplicationContext.get().getSharedPreferences(Prefs.PLASMONA, 0).getString(Prefs.PROJECT, "NULL");
        String filename = Prefs.getPathToHistoWifi() + WIFI_REF_FILE;
        Log.i(TAG, "write point " + filename);
        boolean exists = (new File(filename)).exists();
        if (!exists) {
            System.out.println("create");
            try {
                createReferenceFile(filename);

            } catch (ParserConfigurationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (TransformerException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            FileInputStream filestream = new FileInputStream(filename);

            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(filestream);

            // get root node
            Node root = doc.getFirstChild();
            Element location = doc.createElement("Location");

            location.setAttribute("bitmapName", "ips_histo");
            location.setAttribute("binsize", "" + binSize);
            location.setAttribute("x", "" + geoPoint.x());
            location.setAttribute("y", "" + geoPoint.y());
            location.setAttribute("z", "" + geoPoint.getZ());
            location.setAttribute("id", "" + geoPoint.getId());

            HashMap<String, HashMap<Integer, Float>> castedNodeData = (HashMap<String, HashMap<Integer, Float>>) nodeData;


            for (Map.Entry<String, HashMap<Integer, Float>> entry : castedNodeData.entrySet()) {

                String key = entry.getKey();
                HashMap<Integer, Float> value = entry.getValue();

                List<Integer> sorted = new ArrayList<Integer>((value.keySet()));
                Collections.sort(sorted);

                Element node = doc.createElement("Node");
                node.setAttribute("BSSID", "" + key);

                Log.i("Node", "" + key);
                for (Integer aSorted : sorted) {
                    Element bin = doc.createElement("Bin");
                    int subKey = aSorted;
                    float subValue = value.get(subKey);
                    bin.setAttribute("level", "" + subKey);
                    bin.setAttribute("probability", "" + subValue);
                    Log.i("Bin", " " + subKey + " " + subValue);
                    node.appendChild(bin);
                }

                location.appendChild(node);
            }

            root.appendChild(location);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();

            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filename));
            transformer.transform(source, result);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (IOException ioe) {
            Log.e("ERROR", "IO Error");
            ioe.printStackTrace();
        } catch (SAXException sae) {
            sae.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void writePoint(GeoPoint geoPoint, Object nodeData) {

    }

    /**
     * Update point.
     *
     * @param geoPoint the geo
     */
    @Override
    public void updatePoint(final GeoPoint geoPoint) {
        project = ApplicationContext.get().getSharedPreferences(Prefs.PLASMONA, 0).getString(Prefs.PROJECT, "NULL");
        String filename = Prefs.getPathToHistoWifi() + WIFI_REF_FILE;
        boolean update = false;
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            FileInputStream filestream = new FileInputStream(filename);

            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(filestream);

            Node root = doc.getFirstChild();
            NodeList nList = root.getChildNodes();
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);

                if ("Location".equals(nNode.getNodeName())) {
                    Element eElement = (Element) nNode;
                    if (eElement.hasAttribute("id") && eElement.getAttribute("id").equals(String.valueOf(geoPoint.getId()))) {
                        Log.i(TAG, "Find element and update it!");
                        eElement.setAttribute("x", "" + geoPoint.x());
                        eElement.setAttribute("y", "" + geoPoint.y());
                        update = true;
                        break;
                    }
                }
            }

            if (update) {
                TransformerFactory transformerFactory = TransformerFactory.newInstance();

                Transformer transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                DOMSource source = new DOMSource(doc);
                StreamResult result = new StreamResult(new File(filename));
                transformer.transform(source, result);
                update = false;
            }

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (IOException ioe) {
            Log.e("ERROR", "IO Error");
            ioe.printStackTrace();
        } catch (SAXException sae) {
            sae.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the position.
     *
     * @param id the id
     * @return the position
     */
    public static PointF getPosition(final int id) {
        project = ApplicationContext.get().getSharedPreferences(Prefs.PLASMONA, 0).getString(Prefs.PROJECT, "NULL");
        //		String filename = Environment.getExternalStorageDirectory() + "/" + ApplicationContext.get().getPackageName() + "/" + project + "/" + WIFI_REF_FILE;
        String filename = Prefs.getPathToHistoWifi() + WIFI_REF_FILE;

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            FileInputStream filestream = new FileInputStream(filename);

            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(filestream);

            Node root = doc.getFirstChild();
            NodeList nList = root.getChildNodes();
            PointF pnt = new PointF();
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);

                if ("Location".equals(nNode.getNodeName())) {
                    Element eElement = (Element) nNode;
                    if (eElement.hasAttribute("id") && eElement.getAttribute("id").equals(String.valueOf(id))) {
                        pnt.x = Float.parseFloat(eElement.getAttribute("x"));
                        pnt.y = Float.parseFloat(eElement.getAttribute("y"));

                        return pnt;

                    }
                }
            }

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (IOException ioe) {
            Log.e("ERROR", "IO Error");
            ioe.printStackTrace();
        } catch (SAXException sae) {
            sae.printStackTrace();
        }
        return null;
    }

    /**
     * Delete point.
     *
     * @param geoPoint the geo
     */
    @Override
    public void deletePoint(final GeoPoint geoPoint) {
        project = ApplicationContext.get().getSharedPreferences(Prefs.PLASMONA, 0).getString(Prefs.PROJECT, "NULL");
        String filename = Prefs.getPathToHistoWifi() + WIFI_REF_FILE;
        boolean update = false;
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            FileInputStream filestream = new FileInputStream(filename);

            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(filestream);

            Node root = doc.getFirstChild();
            NodeList nList = root.getChildNodes();

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);

                if ("Location".equals(nNode.getNodeName())) {
                    Element eElement = (Element) nNode;
                    if (eElement.hasAttribute("id") && eElement.getAttribute("id").equals(String.valueOf(geoPoint.getId()))) {
                        System.out.println(eElement.getAttribute("id"));
                        root.removeChild(eElement);
                        update = true;
                        break;
                    }
                }
            }

            if (update) {
                TransformerFactory transformerFactory = TransformerFactory.newInstance();

                Transformer transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                DOMSource source = new DOMSource(doc);
                StreamResult result = new StreamResult(new File(filename));
                transformer.transform(source, result);
                update = false;
            }

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (IOException ioe) {
            Log.e("ERROR", "IO Error");
            ioe.printStackTrace();
        } catch (SAXException sae) {
            sae.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates the reference file.
     *
     * @param filename the filename
     * @throws ParserConfigurationException the parser configuration exception
     * @throws TransformerException         the transformer exception
     */
    public static void createReferenceFile(final String filename) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement(project);
        doc.appendChild(rootElement);
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File(filename));

        transformer.transform(source, result);

    }
}
