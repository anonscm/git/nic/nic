package org.indoor.plasmona.tracker.histowifi.distance;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.indoor.plasmona.tracker.histowifi.HistoConfiguration;

public class SurviveOfFittestAlgorithm implements DistanceAlgorithm {

    @Override
    public double calcBin(Map<Integer, Float> buffer,
            Map<Integer, Float> reference, List<Integer> binarray) {

        return Double.MAX_VALUE;
    }

    @Override
    public double calc(Map<String, Float> fingerprint,
            Map<String, Float> histogram) {

        float p, q;
        int matched_ap = 0;
        double d = Double.MAX_VALUE;

        double kl1 = 0.0;
        double kl2 = 0.0;
        // for each saved reference
        for (Entry<String, Float> value : fingerprint.entrySet()) {

            if (histogram.containsKey(value.getKey())) {
                p = 1 - histogram.get(value.getKey());
                q = 1 - value.getValue();
               // Log.i(TAG, "survive p und q "+p+" "+q );
                float differenz = p-q;
                float quadrat = (float)Math.pow(differenz,2);
                float minimum = quadrat - (1-Math.min(p,q));
                float mittelwert = quadrat - (1- Math.abs(p-q)/2);
                float weightmin = 1- minimum;
                float weightmittel = 1- mittelwert;
                float modifywmin = weightmin * Math.min(p,q);
                float modifywmittel =  weightmittel* Math.min(p,q);

                matched_ap++;
              //  d +=(1-modifywmin);
                //Log.i(TAG, "survive distance  = "+(1-modifywmittel) +" "+ (1-modifywmin));
                kl1 += p * Math.log(p / q)* modifywmin;
                kl2 += q * Math.log(q / p)* modifywmin;

            }
        }
        float confidence = (float) matched_ap / (float) fingerprint.size();
        if (confidence > HistoConfiguration.getInstance().getConfidenceFactor()) {
            return kl1 + kl2;
        }
        return d;
    }

}
