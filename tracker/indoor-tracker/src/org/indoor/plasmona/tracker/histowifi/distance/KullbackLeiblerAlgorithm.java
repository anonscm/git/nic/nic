package org.indoor.plasmona.tracker.histowifi.distance;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.indoor.plasmona.tracker.histowifi.HistoConfiguration;

public class KullbackLeiblerAlgorithm implements DistanceAlgorithm {

    @Override
    public double calcBin(Map<Integer, Float> buffer,
            Map<Integer, Float> reference,
            List<Integer> binarray) {

        float p, q, a;
        double kl1 = 0.0;
        double kl2 = 0.0;
        int matches = 0;
        double d = Double.MAX_VALUE;
        for (Integer bin : binarray) {

            if (buffer.get(bin) != null && reference.get(bin) != null) {

                p = buffer.get(bin);
                q = reference.get(bin);

                kl1 += p * Math.log(p / q);
                kl2 += q * Math.log(q / p);

                matches++;
            } else {

                p = 1.2f;
                q = 0.2f;

                kl1 += p * Math.log(p / q);
                kl2 += q * Math.log(q / p);

            }
        }

        d = (kl1 + kl2) / binarray.size();

        return d;
    }

    @Override
    public double calc(Map<String, Float> fingerprint,
            Map<String, Float> histogram) {

        double kl1 = 0.0;
        double kl2 = 0.0;

        double d = Double.MAX_VALUE;

        int matched_ap = 0;

        // for each saved reference
        //TODO error metric for number of access points or miss-matching
        for (Entry<String, Float> value : fingerprint.entrySet()) {
            float p, q, a, wp, wq;
            if (histogram.containsKey(value.getKey())) {

                p = histogram.get(value.getKey());
                q = value.getValue();

                kl1 += p * Math.log(p / q);// * wp;
                kl2 += q * Math.log(q / p);// * wq;
                //Log.i(TAG, "Kull kl1 = "+ p * Math.log(p / q) * wp+" kl2 = "+ q * Math.log(q / p) * wq+" wq = "+wq+" wp "+wp +" p = " +p+" q = "+q);
                matched_ap++;
            }

        }
        float confidence = (float) matched_ap / (float) fingerprint.size();

        if (confidence > HistoConfiguration.getInstance().getConfidenceFactor()) {
            d = kl1 + kl2;
        }

        return d;
    }

}
