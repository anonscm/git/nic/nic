package org.indoor.plasmona.tracker.histowifi.metric;

import java.util.HashMap;

import org.indoor.plasmona.tracker.histowifi.HistoWifiMatcher;

/**
 * TODO:Klärung: ich war mir nicht sicher, ob die Metriken noch benötigt werden.
 * Sie waren vorher als Methoden in der  {@link HistoWifiMatcher}-Klasse definiert.
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 *
 */
public interface Metric {

    float calc(HashMap<Integer, Float> value, HashMap<Integer, Float> value2);
}
