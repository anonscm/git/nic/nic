package org.indoor.plasmona.tracker.histowifi;

import java.util.Comparator;

// TODO: Auto-generated Javadoc
/**
 * The Class HistoWifiScanData.
 */
public class HistoWifiScanData {

  /** The ssid. */
  private String ssid;

  /** The bssid. */
  private String bssid;

  /** The level. */
  private int level;

  /** The frequency. */
  private int frequency;

  /** The timestamp. */
  private long timestamp;

  /**
   * Instantiates a new histo wifi scan data.
   * 
   * @param _ssid
   *          the _ssid
   * @param _bssid
   *          the _bssid
   * @param _level
   *          the _level
   * @param _freq
   *          the _freq
   * @param _timestamp
   *          the _timestamp
   */
  public HistoWifiScanData(String _ssid, String _bssid, int _level, int _freq, long _timestamp) {
    this.ssid = _ssid;
    this.bssid = _bssid;
    this.level = _level;
    this.frequency = _freq;
    this.timestamp = _timestamp;

  }

  /**
   * Instantiates a new histo wifi scan data.
   * 
   * @param _ssid
   *          the _ssid
   * @param _bssid
   *          the _bssid
   * @param _level
   *          the _level
   * @param _freq
   *          the _freq
   * @param _timestamp
   *          the _timestamp
   * @param occurrency
   *          the occurrency
   */
  public HistoWifiScanData(String _ssid, String _bssid, int _level, int _freq, long _timestamp, float occurrency) {
    this.ssid = _ssid;
    this.bssid = _bssid;
    this.level = _level;
    this.frequency = _freq;
    this.timestamp = _timestamp;

  }

  /**
   * Gets the timestamp.
   * 
   * @return the timestamp
   */
  public long getTimestamp() {
    return timestamp;
  }

  /**
   * Sets the timestamp.
   * 
   * @param timestamp
   *          the new timestamp
   */
  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  /**
   * Gets the frequency.
   * 
   * @return the frequency
   */
  public int getFrequency() {
    return frequency;
  }

  /**
   * Sets the frequency.
   * 
   * @param frequency
   *          the new frequency
   */
  public void setFrequency(int frequency) {
    this.frequency = frequency;
  }

  /**
   * Gets the ssid.
   * 
   * @return the ssid
   */
  public String getSSID() {
    return ssid;
  }

  /**
   * Sets the ssid.
   * 
   * @param name
   *          the new ssid
   */
  public void setSSID(String name) {
    this.ssid = name;
  }

  /**
   * Gets the bssid.
   * 
   * @return the bssid
   */
  public String getBSSID() {
    return bssid;
  }

  /**
   * Sets the bssid.
   * 
   * @param bssid
   *          the new bssid
   */
  public void setBSSID(String bssid) {
    this.bssid = bssid;
  }

  /**
   * Gets the level.
   * 
   * @return the level
   */
  public int getLevel() {
    return level;
  }

  /**
   * Sets the level.
   * 
   * @param _level
   *          the new level
   */
  public void setLevel(int _level) {
    this.level = _level;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return "" + this.ssid + " " + this.bssid + " " + this.level + " " + this.frequency + " " + timestamp;
  }

}

class CustomComparator implements Comparator<HistoWifiScanData> {
  @Override
  public int compare(HistoWifiScanData o1, HistoWifiScanData o2) {
    return o1.getBSSID().compareTo(o2.getBSSID());
  }
}
