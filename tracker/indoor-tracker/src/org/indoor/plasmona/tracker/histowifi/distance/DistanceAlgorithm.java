package org.indoor.plasmona.tracker.histowifi.distance;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 *
 */
public interface DistanceAlgorithm {

    double calcBin(Map<Integer, Float> buffer, Map<Integer, Float> reference, List<Integer> binarray);

    double calc(Map<String, Float> fingerprint, Map<String, Float> histogram);
}
