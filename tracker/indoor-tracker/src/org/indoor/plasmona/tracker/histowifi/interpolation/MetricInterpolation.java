package org.indoor.plasmona.tracker.histowifi.interpolation;

import java.util.List;
import java.util.Map;

import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.utils.TName;

/**
 * The MetricInterpolation takes the distances, as calculated by the chosen distance-algorithm, into account.
 * The smaller the distance to a fingerprint, the larger the interpolation-weight of its coordinates.
 * It works like this:
 * - given three distances:
 *   - d1 = 1
 *   - d2 = 1
 *   - d3 = 4
 * - calculate the distanceSum = 1 + 1 + 4 = 6
 * - calculate the totalDifferenceSum of the differences that each distance has to the distanceSum:
 *   - (6-1) + (6-1) + (6-4) = 5 + 5 + 2 = 12
 * - the weight for each point is: the difference between its own distance and the distanceSum, divided by the totalDifferenceSum:
 *   - d1 = (6-1) / 12 = 5/12
 *   - d2 = (6-1) / 12 = 5/12
 *   - d3 = (6-4) / 12 = 2/12
 * - since these weights add up to 1, no normalization is required after we have multiplied all coordinates with their weights.
 */
public class MetricInterpolation implements Interpolation {

    @Override
    public GeoPoint interpolate(Map<Integer, Double> distanceMap,
            List<GeoPoint> pointList) {

        // This is the sum over the distances of all the neighbours.
        float distanceSum = 0;

        // This is the sum over the differences between each distance and the totalDistanceSum.
        float totalDifferenceSum = 0;

        if (pointList.size() == 0)
            return null;

        for (int i = 0; i < pointList.size(); i++) {
            distanceSum += distanceMap.get(pointList.get(i).getId());

        }
        for (int i = 0; i < pointList.size(); i++) {
            totalDifferenceSum += distanceSum - distanceMap.get(pointList.get(i).getId());
        }

        float x = 0;
        float y = 0;

        for (int i = 0; i < pointList.size(); i++) {
            double factor = (distanceSum - distanceMap.get(pointList.get(i).getId())) / totalDifferenceSum;

            x += pointList.get(i).x() * factor;
            y += pointList.get(i).y() * factor;
        }

        GeoPoint geo = new GeoPoint(x, y, TName.HISTOTRACKER);

        return geo;
    }

}
