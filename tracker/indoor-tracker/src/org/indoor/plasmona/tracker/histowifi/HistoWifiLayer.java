package org.indoor.plasmona.tracker.histowifi;

import android.graphics.PointF;
import android.util.Log;
import org.indoor.plasmona.layer.TrackerLayer;
import org.indoor.plasmona.layer.TrackerLayerImpl;
import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.tracker.TrackerServiceFactory;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.Prefs;
import org.indoor.plasmona.utils.TName;

import java.io.File;

// TODO: Auto-generated Javadoc

/**
 * The Class HistoWifiLayer.
 */
public class HistoWifiLayer extends TrackerLayerImpl implements TrackerLayer {


    private static final long serialVersionUID = 1L;

    /**
     * The Constant TAG.
     */
    private static final String TAG = "HistoWifiLayer";

    /**
     * The project.
     */
    private String project;

    /**
     * The wifi ref file.
     */
    private static String WIFI_REF_FILE = "histo-wifi-references.xml";

    /**
     * The min.
     */
    private int MIN = 0;

    /* (non-Javadoc)
     * @see org.indoor.plasmona.layer.TrackerLayerImpl#load()
     */
    @Override
    public void load() {

        project = ApplicationContext.get().getSharedPreferences(Prefs.PLASMONA, 0).getString(Prefs.PROJECT, "NULL");

        File f = new File(Prefs.getPathToHistoWifi() + WIFI_REF_FILE);
        if (f.exists() && TrackerServiceFactory.getLocationService().findTrackerByName(TName.HISTOTRACKER).getLayer() == null)
            TrackerServiceFactory.getLocationService().findTrackerByName(TName.HISTOTRACKER).registerLayer(HistoWifiXMLReader.loadLayer(f));
        else
        TrackerServiceFactory.getLocationService().findTrackerByName(TName.HISTOTRACKER).registerLayer(HistoWifiXMLReader.loadLayer(f));

        generateID();
    }

    /* (non-Javadoc)
     * @see org.indoor.plasmona.layer.TrackerLayerImpl#reload()
     */
    @Override
    public void reload() {
        Log.i(TAG, "reload()");

    }


    /* (non-Javadoc)
     * @see org.indoor.plasmona.layer.TrackerLayerImpl#reloadPosition(int)
     */
    public PointF reloadPosition(int id) {
        Log.i(TAG, "reloadPosition (" + id + " )");
        return HistoWifiXMLWriter.getPosition(id);

    }


    /**
     * Generate id.
     *
     * @return the int
     */
    public int generateID() {
        int res;
        for (GeoPoint p : this) {
            if (p.getId() > MIN)
                MIN = p.getId();
        }


        res = MIN + 1;

        return res;
    }


    @Override
    public void update() {
        // TODO Auto-generated method stub
    }

}
