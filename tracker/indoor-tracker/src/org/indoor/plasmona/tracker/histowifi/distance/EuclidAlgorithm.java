package org.indoor.plasmona.tracker.histowifi.distance;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.indoor.plasmona.tracker.histowifi.HistoConfiguration;

public class EuclidAlgorithm implements DistanceAlgorithm {

    /**
     * A strange and wondrous method which picks indices from out of buffer and reference, according to a list of
     * bin-names, and sums up the differences.
     * TODO: if buffer and reference contain only entries that fit the bins, why do we need the binarray? If they
     *       contain additional entries, why do we ignore those?
     *
     * @param buffer
     * @param reference
     * @param binarray
     * @return
     */
    @Override
    public double calcBin(Map<Integer, Float> buffer,
            Map<Integer, Float> reference,
            List<Integer> binarray) {

        // Should we call this "difference"? Or even "distance"? It will hold the sum of the differences between all
        // the corresponding bins in buffer and reference.
        double d = 0.0;

        for (Integer bin : binarray) {
            // If buffer and reference both have a probability entry in this bin we calculate the differecence
            // and add it to our sum. If either of them doesn't then we add the maximal distance of 1.0.
            // TODO: why not treat the probability as 0 in this case?
            // TODO: why add 1.0 when BOTH of them have zero probability for this bin?
            if (buffer.get(bin) != null && reference.get(bin) != null) {
                float p, q;
                p = buffer.get(bin);
                q = reference.get(bin);

                d += Math.abs(p - q);
            } else {
                d += 1.0;
            }
        }

        return d / binarray.size();
    }


    /**
     * Berechnet den <a href="http://de.wikipedia.org/wiki/Euklidischer_Abstand"><b>Euklidischer Abstand</b></a>
     * zwischen einem Fingerprint und aktueller Position (bzw. dem dort gemessenen Histogram).
     *
     * @param fingerprint a fingerprint as a mapping of BSSID to signal-strength (as a single float, not a distribution?!)
     * @param histogram what was measured now, also as a mapping of BSSID to signal-strength.
     * @return the distance, or Double.MAX_VALUE if the required confidence-level is not reached.
     */
    @Override
    public double calc(Map<String, Float> fingerprint,
            Map<String, Float> histogram) {

        // Here we accumulate the distance over all the dimensions (the dimensions, in our case, are the different
        // accesspoints).
        double accumulator = 0.0;

        // If the distance cannot be calculated with some confidence we will return maximum distance.
        double distance = Double.MAX_VALUE;

        // In this variable we will count the number of accesspoints that are present in both the fingerprint
        // and the histogram. If they share only a small part of the accesspoints then it doesn't help much that these
        // seem close. The default requirement seems to be 50% (see CONFIDENCE_FACTOR) of the accesspoints that were
        // recorded in the fingerprint.
        int matchedAccessPoint = 0;

        // For each saved accesspoint in the fingerprint:
        // TODO error metric for number of access points or miss-matching
        for (Entry<String, Float> accesspoint : fingerprint.entrySet()) {

            String bssid = accesspoint.getKey();
            // An accesspoint, which is present in the fingerprint but was not measured right now, is ignored (instead
            // of being treated as having a signal strength of 0, for example).
            if (histogram.containsKey(bssid)) {
                float p, q;
                p = histogram.get(bssid);
                q = accesspoint.getValue();

                accumulator += Math.pow((p - q), 2.0);
                matchedAccessPoint++;
            }

        }

        // For a real euclidean distance we would now calculate the square root of the accumulated distance.
        // But that is not done here. Maybe we are only interested in comparing/ordering the distances, from multiple
        // calls to this method (with different fingerprints). The ordering would be the same with or without the
        // square root.

        // Confidence goes from 0 (no matching accesspoints at all) to 1 (all recorded accesspoints were found).
        float confidence = (float) matchedAccessPoint / (float) fingerprint.size();

        if (confidence > HistoConfiguration.getInstance().getConfidenceFactor()) {
            distance = accumulator;
        }

        return distance;
    }

}
