package org.indoor.plasmona.tracker.histowifi.interpolation;

import java.util.List;
import java.util.Map;

import org.indoor.plasmona.models.GeoPoint;

public interface Interpolation {

    GeoPoint interpolate(Map<Integer, Double> distanceMap, List<GeoPoint> pointList);
}
