package org.indoor.plasmona.tracker.histowifi.distance;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.indoor.plasmona.tracker.histowifi.HistoConfiguration;

public class ChiSquareAlgorithm implements DistanceAlgorithm {

    @Override
    public double calcBin(Map<Integer, Float> buffer,
            Map<Integer, Float> reference,
            List<Integer> binarray) {

        int matches = 0;
        double d = 0.0;
        float p, q, a;


        for (Integer bin : binarray) {

            if (buffer.get(bin) != null && reference.get(bin) != null) {
                p = buffer.get(bin);
                q = reference.get(bin);

                a = Math.abs(p - q);

                d += (a * a / p);
                d += (a * a / q);

            } else {

                p = 1.2f;
                q = 0.2f;

                a = Math.abs(p - q);

                d += (a * a / p);
                d += (a * a / q);
            }
        }

        return d / binarray.size();
    }

    @Override
    public double calc(Map<String, Float> fingerprint,
            Map<String, Float> histogram) {

        double d = Double.MAX_VALUE;

        int matched_ap = 0;
        int ap = 0;
        // for each saved reference
        for (Entry<String, Float> value : fingerprint.entrySet()) {
            float p, q, a;
            if (histogram.containsKey(value.getKey())) {

                p = histogram.get(value.getKey());
                q = value.getValue();
                a = Math.abs(p - q);

                d += (a * a / p);
                d += (a * a / q);

                matched_ap++;
            }

        }
        float confidence = (float) matched_ap / (float) fingerprint.size();

        if (confidence > HistoConfiguration.getInstance().getConfidenceFactor()) {
            return d;
        }
        return d;
    }

}
