package org.indoor.plasmona.tracker.histowifi.interpolation;

import java.util.List;
import java.util.Map;

import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.utils.TName;

/**
 * The LinearInterpolation takes all the neighbours, which are ordered by ascending distance, and assigns to each of
 * them a weight, that is the number of neighbours that are farther away. With these weights the target position is
 * interpolated. The individual distances are ignored, i.e. it doesn't matter how much farther off the second neighbour
 * is, compared to the first one. Only the order is important.
 * Example: if we have 4 neighbours, the nearest one will have the weight 4, the second  3, the third 2 and the
 *          last will have weight 1.
 */
public class LinearInterpolation implements Interpolation {

    @Override
    public GeoPoint interpolate(Map<Integer, Double> unusedDistanceMap,
            List<GeoPoint> pointList) {

        float x = 0;
        float y = 0;
        int n = pointList.size();
        for (int i = 0; i < n; i++) {
            float linearweight = (n - i);

            x += pointList.get(i).x() * linearweight;
            y += pointList.get(i).y() * linearweight;
        }

        int normalizefactor = (n * (n + 1)) / 2;

        GeoPoint geo = new GeoPoint(x / normalizefactor, y / normalizefactor, TName.HISTOTRACKER);

        return geo;
    }

}
