package org.indoor.plasmona.tracker.histowifi.interpolation;

import java.util.List;
import java.util.Map;

import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.utils.TName;

public class SquareInterpolation implements Interpolation {

    @Override
    public GeoPoint interpolate(Map<Integer, Double> unusedDistanceMap,
            List<GeoPoint> pointList) {

        float x = 0;
        float y = 0;
        int n = pointList.size();
        for (int i = 0; i < n; i++) {

            double squareweight = Math.pow((pointList.size() - i), 2);
            x += pointList.get(i).x() * squareweight;
            y += pointList.get(i).y() * squareweight;
        }

        int normalizefactor = (n * (n + 1) * (2 * n + 1)) / 6;

        GeoPoint geo = new GeoPoint(x / normalizefactor, y / normalizefactor, TName.HISTOTRACKER);

        return geo;
    }

}
