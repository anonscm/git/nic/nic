package org.indoor.plasmona.tracker.histowifi.metric;

import java.util.HashMap;

public class IntersectionMetric extends AbstractMetric {

    @Override
    public float calc(HashMap<Integer, Float> value,
            HashMap<Integer, Float> value2) {

        normalize(value, value2);

        float result = 0.0f;
        for (Integer bins : value.keySet())
            result += Math.min(value.get(bins), value2.get(bins));
        return result;
    }

}
