package org.indoor.plasmona.tracker.histowifi.metric;

import java.util.HashMap;

public class CorrelationMetric extends AbstractMetric {

    @Override
    public float calc(HashMap<Integer, Float> value,
            HashMap<Integer, Float> value2) {

        normalize(value, value2);

        float result = 0.0f;
        float s1 = 0, s2 = 0, s11 = 0, s12 = 0, s22 = 0;
        for (Integer bins : value.keySet()) {

            float a = value.get(bins);
            float b = value2.get(bins);

            s12 += a * b;
            s1 += a;
            s11 += a * a;
            s2 += b;
            s22 += b * b;
        }

        int total = value.size();
        double scale = 1. / total;
        double num = s12 - s1 * s2 * scale;
        double denom2 = (s11 - s1 * s1 * scale) * (s22 - s2 * s2 * scale);
        result = (float) (Math.abs(denom2) > 0.0f ? num / Math.sqrt(denom2) : 1.);

        return Math.abs(result);
    }

}
