package org.indoor.plasmona.tracker.histowifi;

import java.util.HashMap;

// TODO: Auto-generated Javadoc
/**
 * The Class HistoWifiModel.
 */
public class HistoWifiModel extends HashMap<String, HashMap<Integer, Float>> {

  /** The timestamp. */
  private long timestamp;

  /**
   * Gets the timestamp.
   * 
   * @return the timestamp
   */
  public long getTimestamp() {
    return timestamp;
  }

  /**
   * Sets the timestamp.
   * 
   * @param timestamp
   *          the new timestamp
   */
  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

}
