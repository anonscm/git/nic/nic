package org.indoor.plasmona.tracker.histowifi;

import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.os.SystemClock;
import android.util.Log;
import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.tracker.Capturer;
import org.indoor.plasmona.tracker.TrackerDataManipulator;
import org.indoor.plasmona.tracker.TrackerServiceFactory;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.Bin;
import org.indoor.plasmona.utils.TName;

/**
 * The Class HistoWifiCapturer is used to capture Wifi-fingerprints.
 */
public class HistoWifiCapturer implements Capturer {

    /**
     * The logging-prefix for this class.
     */
    private static final String TAG = "HistoWifiCapturer";


    /**
     * The capture-thread which is used to measure the wifi-stuff.
     * But strangely it seems that this is not at all used to collect the data in the background!
     * This class here just calls the run()-method, instead of really starting a new thread (with start()).
     * This will cause this HistoWifiCapturer to simply block until the capThread is done.
     * See: http://stackoverflow.com/questions/262816/when-would-you-call-javas-thread-run-instead-of-thread-start
     * Maybe the backgrounding is done purely by Android-mechanics (see inner class CaptureOperation in
     * PlasmonaActivity). But why should it be a Thread then, or why would we directly call its run() here?
     * Perhaps so that it can be used outside of Android, in a normal java-application?
     */
    private HistoCaptureThread capThread;

    /**
     * Instantiation of the {@link HistoWifiXMLWriter} based on the {@link TrackerDataManipulator}.
     * It is used to write/delete/update single points together with their histogram.
     * Looks complicated. We might not need it if we keep all the measured fingerprints in memory and just serialize
     * them to xml/json/whatever when we save/upload/exit.
     */
    private final TrackerDataManipulator histoWifiXMLWriter = new HistoWifiXMLWriter();


    /**
     * Constructor. Instantiates a new HistoWifiCapturer and registers it with the HistoWifi-tracker.
     * This seems to only serve the purpose of making it available to the PlasmonaActivity, from where fingerprints are
     * deleted, updated and captured.
     */
    public HistoWifiCapturer() {
        TrackerServiceFactory.getLocationService().findTrackerByName(TName.HISTOTRACKER).registerCapturer(this);
    }

    /*
     * Capture a histogram/fingerprint at some point. Note that the GeoPoint is a plasmona-class and it is much more
     * than a simple lat/long-pair. It does, however, seem to inherit the simple cartesian distance-function from
     * its Positionable-baseclass (sqrt(dx^2 + dy^2)). But it also includes details relating to its GUI-representation.
     * @see org.indoor.plasmona.tracker.Capturer#captureAt(org.indoor.plasmona.models.Position)
     */
    @Override
    public void captureAt(GeoPoint pnt) {

        // The time is only used for the log. It is not required for the capturing per se.
        long time = SystemClock.currentThreadTimeMillis();
        capThread = new HistoCaptureThread(this);
        // This is a normal, blocking method-call and has nothing to do with multithreading!
        // Also note how the parameter pnt is not passed on to the capturer. I.e. the capturing is independent of
        // any concept of location - as was to be expected.
        capThread.run();

        // Only now are the point and the capture-result (the histogram) combined, when they are written out together:
        write(pnt);

        Log.i(TAG, "Time for Data" + String.valueOf(SystemClock.currentThreadTimeMillis() - time) + " ms.");
    }

    /*
     * Write the point to XML and add it to the layer (for display).
     * @see org.indoor.plasmona.tracker.Capturer#write(org.indoor.plasmona.models.Position)
     */
    @Override
    public void write(GeoPoint pnt) {

        // This seems rather boring, because the bitmap will be the same for all points that this capturer creates.
        // Why would we want to do this again for each point?
        int resID = ApplicationContext.get().getResources().getIdentifier("ips_histo", "drawable", ApplicationContext.get().getPackageName());
        pnt.setIcon(BitmapFactory.decodeResource(ApplicationContext.get().getResources(), resID));

        // The HistoWifiLayer is only necessary to find the next free ID which we can assign to our new point.
        HistoWifiLayer layer = (HistoWifiLayer) TrackerServiceFactory.getLocationService().findTrackerByName(TName.HISTOTRACKER).getLayer();

        // Maybe this would be nicer if pnt were of a specialized class, encapsulating the boring stuff?
        pnt.setId(layer.generateID());
        pnt.setType(TName.HISTOTRACKER);
        pnt.setData(capThread.histogramm);
        pnt.setZ(new Float(1));

        Log.i(TAG, "Capturing "+capThread.histogramm);
        // The pnt already contains the capThread.histogramm, so this call could probably be refactored.
        histoWifiXMLWriter.writePoint(pnt, capThread.histogramm, HistoConfiguration.getInstance().getBinSize());

        // This is the same layer from which we got the ID a few lines earlier. What about reusing it?
        HistoWifiLayer histo = (HistoWifiLayer) TrackerServiceFactory.getLocationService().findTrackerByName(TName.HISTOTRACKER).getLayer();

        // The need for synchronization might arise from this code here being called in an AsyncTask while the
        // GUI-thread wants to execute LayerRenderer.onDraw, which is the only other class that synchronizes on this
        // layer. Might result in a ConcurrentModificationException.
        synchronized (histo) {
            // This is actually just ArrayList.add(Object).
            histo.add(pnt);
        }
    }

    /*
     * Update the position of a point without capturing a new histogram. Will update the XML and the display-layer.
     * @see org.indoor.plasmona.tracker.Capturer#update(org.indoor.plasmona.models.Position)
     */
    @Override
    public void update(GeoPoint pnt) {

        histoWifiXMLWriter.updatePoint(pnt);

        HistoWifiLayer histo = (HistoWifiLayer) TrackerServiceFactory.getLocationService().findTrackerByName(TName.HISTOTRACKER).getLayer();
        synchronized (histo) {
            histo.findPositionById(pnt.getId()).setPosition(new PointF(pnt.x(), pnt.y()));
        }
    }

    /*
     * Remove a point from the XML and from the display-layer.
     * @see org.indoor.plasmona.tracker.Capturer#delete(org.indoor.plasmona.models.Position)
     */
    @Override
    public void delete(GeoPoint pnt) {

        histoWifiXMLWriter.deletePoint(pnt);
        HistoWifiLayer histo = (HistoWifiLayer) TrackerServiceFactory.getLocationService().findTrackerByName(TName.HISTOTRACKER).getLayer();
        synchronized (histo) {
            histo.remove(histo.findPositionById(pnt.getId()));
        }
    }

}
