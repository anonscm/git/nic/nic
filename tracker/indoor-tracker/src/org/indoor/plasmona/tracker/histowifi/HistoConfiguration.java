package org.indoor.plasmona.tracker.histowifi;

import org.indoor.plasmona.tracker.Configuration;
import org.indoor.plasmona.tracker.histowifi.distance.BhattcharyyaAlgorithm;
import org.indoor.plasmona.tracker.histowifi.distance.ChiSquareAlgorithm;
import org.indoor.plasmona.tracker.histowifi.distance.DistanceAlgorithm;
import org.indoor.plasmona.tracker.histowifi.distance.EuclidAlgorithm;
import org.indoor.plasmona.tracker.histowifi.distance.KullbackLeiblerAlgorithm;
import org.indoor.plasmona.tracker.histowifi.distance.SurviveOfFittestAlgorithm;
import org.indoor.plasmona.tracker.histowifi.interpolation.Interpolation;
import org.indoor.plasmona.tracker.histowifi.interpolation.LinearInterpolation;
import org.indoor.plasmona.tracker.histowifi.interpolation.MetricInterpolation;
import org.indoor.plasmona.tracker.histowifi.interpolation.SquareInterpolation;

/**
 * Created with IntelliJ IDEA.
 * User: jens
 * Date: 8/19/13
 * Time: 12:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class HistoConfiguration implements Configuration {

    private static HistoConfiguration instance = null;

    private long TIME_INTERVAL = 100;

    private long NUMBER_OF_SCANS = 50;

    private float CONFIDENCE_FACTOR = 0.5f;

    private int NUMBER_OF_NEIGHBOURS = 4;

    private int BIN_SIZE = 4;

    private boolean OUTLIER = true;

    private long BUFFER_TIME = NUMBER_OF_SCANS * TIME_INTERVAL;

    public static enum MATCH {
        BIN_LEVEL, MEAN_VALUE;
    }

    public static enum ALGORITHM {
        KULLBACKLEIBLER(new KullbackLeiblerAlgorithm()),
        CHISQUARE(new ChiSquareAlgorithm()),
        EUCLID(new EuclidAlgorithm()),
        GAUSS(null),
        BHATTCHARYYA(new BhattcharyyaAlgorithm()),
        SURVIVEOFFITTEST(new SurviveOfFittestAlgorithm());

        private DistanceAlgorithm impl;

        private ALGORITHM(DistanceAlgorithm impl){
            this.impl = impl;
        }

        public DistanceAlgorithm getImpl(){
            return impl;
        }
    }

    public static enum INTERPOLATE {
        METRIC(new MetricInterpolation()),
        LINEAR(new LinearInterpolation()),
        SQUARE(new SquareInterpolation());

        private Interpolation impl;

        private INTERPOLATE(Interpolation impl){
            this.impl = impl;
        }

        public Interpolation getImpl(){
            return impl;
        }
    }

    private INTERPOLATE IPOLATE = INTERPOLATE.METRIC;

    private MATCH MATCHING = MATCH.BIN_LEVEL;

    private ALGORITHM METHOD = ALGORITHM.KULLBACKLEIBLER;



    protected HistoConfiguration() {
        // Exists only to defeat instantiation.
    }
    public static HistoConfiguration getInstance() {
        if(instance == null) {
            instance = new HistoConfiguration();
        }
        return instance;
    }

    public long getTimeInterval() {
        return TIME_INTERVAL;
    }

    public void setTimeInterval(long TIME_INTERVAL) {
        this.TIME_INTERVAL = TIME_INTERVAL;
    }

    public long getNumberOfScans() {
        return NUMBER_OF_SCANS;
    }

    public void setNumberOfScans(long number) {
        this.NUMBER_OF_SCANS = number;
    }

    public float getConfidenceFactor() {
        return CONFIDENCE_FACTOR;
    }

    public void setConfidenceFactor(float confidence) {
        this.CONFIDENCE_FACTOR = confidence;
    }

    public int getNumberOfNeighbours() {
        return NUMBER_OF_NEIGHBOURS;
    }

    public void setNumberOfNeighbours(int neighbours) {
        this.NUMBER_OF_NEIGHBOURS = neighbours;
    }

    public void setBufferTime(long bufferTime) {
        BUFFER_TIME = bufferTime;
    }

    public long getBufferTime() {
        return BUFFER_TIME;
    }

    public MATCH getMatching() {
        return MATCHING;
    }

    public void setMatching(MATCH MATCHING) {
        this.MATCHING = MATCHING;
    }

    public ALGORITHM getAlgorithm() {
        return METHOD;
    }

    public void setAlgorithm(ALGORITHM METHOD) {
        this.METHOD = METHOD;
    }

    public INTERPOLATE getInterpolation() {
        return IPOLATE;
    }

    public void setInterpolation(INTERPOLATE IPOLATE) {
        this.IPOLATE = IPOLATE;
    }

    public int getBinSize() {
        return BIN_SIZE;
    }

    public void setBinSize(int binSize) {
        this.BIN_SIZE = binSize;
    }

    public boolean hasOutlierDect() {
        return OUTLIER;
    }

    public void setOutlierDect(boolean OUTLIER) {
        this.OUTLIER = OUTLIER;
    }

    public String toString() {
        return "conf "+CONFIDENCE_FACTOR+" buf "+BUFFER_TIME +" numSc "+ NUMBER_OF_SCANS+" numN "+NUMBER_OF_NEIGHBOURS+" interv "+TIME_INTERVAL;
    }
}
