package org.indoor.plasmona.tracker.histowifi;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.indoor.plasmona.layer.TrackerLayerImpl;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import android.util.Log;

// TODO: Auto-generated Javadoc

/**
 * The Class HistoWifiXMLReader.
 */
public class HistoWifiXMLReader {

    /**
     * The tag.
     */
    private static String TAG = "HistoWifiXMLReader";

    /**
     * The histo layer.
     */
    private static HistoWifiLayer histoLayer = null;

    /**
     * The wifi ref file.
     */
    private static String WIFI_REF_FILE = "histo-wifi-references.xml";

    /**
     * The id.
     */
    public static int id = 0;

    /**
     * Load layer.
     *
     * @param _file the _file
     * @return the tracker layer impl
     */
    public static TrackerLayerImpl loadLayer(File _file) {
        Log.i(TAG, "loadLayer( " + _file.getAbsolutePath() + " )");

        if (_file.getName().equals(WIFI_REF_FILE) && _file.exists()) {
            try {

                SAXParserFactory spf = SAXParserFactory.newInstance();
                SAXParser sp = spf.newSAXParser();
                XMLReader xr = sp.getXMLReader();
                HistoWifiXMLParser xmlParser = new HistoWifiXMLParser();
                xr.setContentHandler(xmlParser);
                FileInputStream fis = new FileInputStream(_file);
                InputSource inputSource = new InputSource(fis);
                xr.parse(inputSource);
                id = xmlParser.getNewID();
                histoLayer = xmlParser.getLayer();

                return histoLayer;

            } catch (SAXException e) {
                Log.e(HistoWifiXMLReader.class.getSimpleName(), e.toString());
            } catch (ParserConfigurationException e) {
                Log.e(HistoWifiXMLReader.class.getSimpleName(), e.toString());
            } catch (IOException e) {
                Log.e(HistoWifiXMLReader.class.getSimpleName(), e.toString());
            } catch (NumberFormatException e) {
                Log.e(HistoWifiXMLReader.class.getSimpleName(), e.toString());
            }
        }

        id = 0;
        return new HistoWifiLayer();
    }
}
