package org.indoor.plasmona.tracker;

import java.util.List;

import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.models.MapData;

// TODO: Auto-generated Javadoc
/**
 * The Interface TrackerService.
 */
public interface TrackerService {

  /**
   * Gets the position.
   * 
   * @return the position
   */
  GeoPoint getPosition();

  /**
   * Update position.
   * 
   * @param pos
   *          the pos
   * @return the position
   */
  GeoPoint updatePosition(GeoPoint pos);

  /**
   * Register tracker.
   * 
   * @param tracker
   *          the tracker
   */
  void registerTracker(Tracker tracker);

  /**
   * Unregister tracker.
   * 
   * @param tracker
   *          the tracker
   */
  void unregisterTracker(Tracker tracker);

  /**
   * Register tracker.
   * 
   * @param name
   *          the name
   * @throws InstantiationException
   *           the instantiation exception
   * @throws IllegalAccessException
   *           the illegal access exception
   * @throws ClassNotFoundException
   *           the class not found exception
   */
  void registerTracker(String name) throws InstantiationException, IllegalAccessException, ClassNotFoundException;

  /**
   * Gets the trackers.
   * 
   * @return the trackers
   */
  List<Tracker> getTrackers();

  /**
   * Find tracker by name.
   * 
   * @param name
   *          the name
   * @return the tracker
   */
  Tracker findTrackerByName(String name);

  /**
   * Sets the map data.
   * 
   * @param data
   *          the new map data
   */
  void setMapData(MapData data);

  /**
   * Gets the map data.
   * 
   * @return the map data
   */
  MapData getMapData();
}
