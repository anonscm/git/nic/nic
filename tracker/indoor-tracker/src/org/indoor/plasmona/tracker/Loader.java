package org.indoor.plasmona.tracker;

import org.indoor.plasmona.models.GeoPoint;

// TODO: Auto-generated Javadoc
/**
 * The Interface Loader.
 */
public interface Loader {

  /**
   * Load.
   * 
   * @param pnt
   *          the pnt
   */
  public void load(GeoPoint pnt);

  /**
   * Reload.
   * 
   * @param pnt
   *          the pnt
   */
  public void reload(GeoPoint pnt);

  /**
   * Unload.
   * 
   * @param pnt
   *          the pnt
   */
  public void unload(GeoPoint pnt);
}
