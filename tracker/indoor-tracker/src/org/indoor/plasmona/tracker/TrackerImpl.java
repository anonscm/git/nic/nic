package org.indoor.plasmona.tracker;

import org.indoor.plasmona.layer.TrackerLayerImpl;
import org.indoor.plasmona.models.GeoPoint;

import android.os.Handler;

// TODO: Auto-generated Javadoc

/**
 * The Class TrackerImpl.
 */
public class TrackerImpl implements Tracker {

    /**
     * The pos.
     */
    protected GeoPoint pos;

    /**
     * The tracker service.
     */
    protected TrackerService trackerService;

    /**
     * The running.
     */
    protected boolean running = false;

    /**
     * The listener.
     */
    protected TrackerResultListener listener = null;

    /**
     * The capturer.
     */
    protected Capturer capturer = null;

    /**
     * The loader.
     */
    protected Loader loader = null;

    protected Configuration config = null;

    /**
     * The layer.
     */
    protected TrackerLayerImpl layer = null;


    protected Handler handler = null;

    /**
     * Instantiates a new tracker impl.
     *
     */
    public TrackerImpl() {
        this(TrackerServiceFactory.getLocationService());
        init();
    }

    /**
     * Instantiates a new tracker impl.
     *
     * @param trackerService the tracker service
     */
    public TrackerImpl(TrackerService trackerService) {
        pos = new GeoPoint();
        trackerService.registerTracker(this);
        init();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.indoor.plasmona.tracker.Tracker#setTrackerService(org.indoor.plasmona
     * .tracker.TrackerService)
     */
    @Override
    public void setTrackerService(TrackerService service) {
        trackerService = service;

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.indoor.plasmona.tracker.Tracker#unsetTrackerService(org.indoor.plasmona
     * .tracker.TrackerService)
     */
    @Override
    public void unsetTrackerService(TrackerService service) {
        trackerService = null;

    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#getTrackerName()
     */
    @Override
    public String getTrackerName() {
        return this.getClass().getSimpleName();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#getX()
     */
    @Override
    public float getX() {
        return pos.x();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#getY()
     */
    @Override
    public float getY() {
        return pos.y();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#getPosition()
     */
    @Override
    public GeoPoint getPosition() {
        return pos;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#start()
     */
    @Override
    public void start() {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#stop()
     */
    @Override
    public void stop() {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.indoor.plasmona.tracker.Tracker#setTrackerResultListener(org.indoor
     * .plasmona.tracker.TrackerResultListener)
     */
    @Override
    public void setTrackerResultListener(TrackerResultListener listener) {
        this.listener = listener;

    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#hasCapturer()
     */
    @Override
    public boolean hasCapturer() {
        return (capturer == null) ? false : true;

    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#hasLoader()
     */
    @Override
    public boolean hasLoader() {
        return (loader == null) ? false : true;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.indoor.plasmona.tracker.Tracker#registerCapturer(org.indoor.plasmona
     * .tracker.Capturer)
     */
    @Override
    public void registerCapturer(Capturer capturer) {
        this.capturer = capturer;

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.indoor.plasmona.tracker.Tracker#registerLoader(org.indoor.plasmona.
     * tracker.Loader)
     */
    @Override
    public void registerLoader(Loader loader) {
        this.loader = loader;

    }

    @Override
    public boolean hasConfiguration() {
        return (config == null) ? false : true;
    }

    @Override
    public void registerConfiguration(Configuration config) {
        this.config = config;

    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#getCapturer()
     */
    @Override
    public Capturer getCapturer() {
        return (capturer == null) ? null : capturer;
    }


    @Override
    public Configuration getConfiguration() {
        return (config == null) ? null : config;
    }

     /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#getLoader()
     */
    @Override
    public Loader getLoader() {
        return (loader == null) ? null : loader;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.indoor.plasmona.tracker.Tracker#registerLayer(org.indoor.plasmona.layer
     * .TrackerLayerImpl)
     */
    @Override
    public void registerLayer(TrackerLayerImpl layer) {
        this.layer = layer;

    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#hasLayer()
     */
    @Override
    public boolean hasLayer() {
        return (layer == null) ? false : true;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#getLayer()
     */
    @Override
    public TrackerLayerImpl getLayer() {
        return (layer == null) ? null : layer;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#unregisterLayer(java.lang.String)
     */
    @Override
    public void unregisterLayer(String string) {
        layer = null;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#init()
     */
    @Override
    public void init() {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     *
     * @see org.indoor.plasmona.tracker.Tracker#unregisterTracker()
     */
    @Override
    public void unregisterTracker() {
        trackerService.unregisterTracker(this);
        layer = null;
        loader = null;
        capturer = null;

    }

    @Override
    public void setHandler(Handler handler) {
        this.handler = handler;
    }

}
