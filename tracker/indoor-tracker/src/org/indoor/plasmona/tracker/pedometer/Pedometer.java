package org.indoor.plasmona.tracker.pedometer;

import java.util.Timer;

import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.tracker.TrackerImpl;
import org.indoor.plasmona.tracker.TrackerService;
import org.indoor.plasmona.tracker.TrackerServiceFactory;
import org.indoor.plasmona.utils.ApplicationContext;
import org.indoor.plasmona.utils.TName;

import android.os.Message;
import android.util.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class PedoTracker.
 */
public class Pedometer extends TrackerImpl implements StepTrigger {

  /** The Constant TAG. */
  private static final String TAG = "PedoTracker";

  /** The initialized. */
  private boolean initialized = false;

  /** The sd. */
  private StepDetection sd;

  /** The step width. */
  private static int STEP_WIDTH = 4;

  /** The capture. */
  private PedoCapturer capture;

  /** The x. */
  int x = 0;

  /** The y. */
  int y = 0;


  /** The timer. */
  protected Timer timer;

  /**
   * Instantiates a new pedo tracker.
   * 
   * @param handler
   *          the handler
   */
  public Pedometer() {
    this(TrackerServiceFactory.getLocationService());

  }

  /**
   * Instantiates a new pedo tracker.
   * 
   * @param handler
   *          the handler
   * @param locationService
   *          the location service
   */
  public Pedometer(TrackerService locationService) {
    super(locationService);

  }

  /*
   * (non-Javadoc)
   * 
   * @see org.indoor.plasmona.tracker.TrackerImpl#init()
   */
  public void init() {
    capture = new PedoCapturer();
    sd = new StepDetection(ApplicationContext.get(), this);
   // Log.i(TAG, "Class Name "+ this.getClass().getName());
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.indoor.plasmona.tracker.TrackerImpl#start()
   */
  @Override
  public void start() {
    Log.i(TAG, "pedometer start()");
    if (!running) {
      running = true;
      sd.load();
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.indoor.plasmona.tracker.TrackerImpl#stop()
   */
  @Override
  public void stop() {
    Log.i(TAG, "pedometer stop()");
    if (running)
      sd.unload();
    running = false;
  }

  /**
   * On activity result.
   * 
   * @param pnt
   *          the pnt
   */
  public void onActivityResult(GeoPoint pnt) {
    if (listener != null) {
      Log.i(TAG, "pedometer on tracker result: step detection");
      pnt.setType(TName.PEDOTRACKER);
      listener.onTrackerResult(pnt);
    }
    Message msg = new Message();
    msg.setTarget(handler);
    msg.what = 13;
    msg.sendToTarget();
  }

  /**
   * Checks if is initialized.
   * 
   * @return true, if is initialized
   */
  public boolean isInitialized() {
    return initialized;
  }

  /**
   * Sets the initialized.
   * 
   * @param initialized
   *          the new initialized
   */
  public void setInitialized(boolean initialized) {
    this.initialized = initialized;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.indoor.plasmona.tracker.pedometer.StepTrigger#trigger(long,
   * double)
   */
  @Override
  public void triggerStep(long now_ms, double compDir) {
    GeoPoint p = new GeoPoint();
    p.setData(compDir);
    onActivityResult(p);
  }


  /*
   * (non-Javadoc)
   * 
   * @see org.indoor.plasmona.tracker.pedometer.StepTrigger#dataHookComp(long,
   * double, double, double)
   */
  @Override
  public void triggerCompass(long now_ms, double angle) {

     Message msg = new Message();
     msg.setTarget(handler);
     msg.what = 14;
     msg.obj = angle;
     msg.sendToTarget();
  }
}
