package org.indoor.plasmona.tracker.pedometer;

// TODO: Auto-generated Javadoc
/**
 * An interface to be notified abuot detected steps and their directions. Also
 * there are hooks to to obtain values from sensors.
 *
 * 
 */
public interface StepTrigger {

  /**
   * Called each time a step is triggered.
   * 
   * @param now_ms
   *          the time stamp of the detected step
   * @param compDir
   *          the compass bearing
   */
  public void triggerStep(long now_ms, double compDir);

  
  public void triggerCompass(long now_ms, double x);

  
}
