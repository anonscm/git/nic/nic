/**
 * @author jens maiero
 *
 * @project plasmona
 *
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */
package org.indoor.plasmona.tracker.pedometer;

import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import org.indoor.plasmona.utils.ApplicationContext;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

// TODO: Auto-generated Javadoc

public class StepDetection {

    /**
     * ***** STEP DETECTION ***************
     */

    private static float PEAK_MIN = 0.05f;
    private static float PEAK_MAX = 0.2f;

    private static int TIMER_MIN = 4;
    private static int TIMER_MAX = 8;

    private static double NORMALIZE_MIN = -5;
    private static double NORMALIZE_MAX = 20;

    private long last_sensor_update = 0;

    private Vector<Double> buffer = new Vector<Double>(10);

    private boolean search_max = true;

    private long steptimer = 0l;

    /**
     * The Constant TAG.
     */
    private static final String TAG = "StepDetection";

    // Hold an interface to notify the outside world of detected steps
    /**
     * The st.
     */
    private StepTrigger st;
    // Context needed to get access to sensor service
    /**
     * The context.
     */
    private Context context;

    /**
     * The sm.
     */
    private static SensorManager sm; // Holds references to the SensorManager

    /**
     * The l sensor.
     */
    List<Sensor> lSensor; // List of all sensors

    /**
     * The is running.
     */
    public boolean isRunning = false;

    private double compassAngle = 0.0;

    private double local_min = 0.0;

    private double local_max = 0.0;

    public static final String PREFS_NAME = "PedoMeterPrefs";


    // magnetic field vector
    private float[] magnet = new float[3];

    // accelerometer vector
    private float[] accel = new float[3];

    /**
     * Instantiates a new step detection.
     *
     * @param context the context
     * @param st      the st
     */
    public StepDetection(Context context, StepTrigger st) {
        this.context = context;
        this.st = st;
    }

    /*****
     * NEW VALUES
     */
    /**
     * Handles sensor events. Updates the sensor
     */
    public SensorEventListener mySensorEventListener = new SensorEventListener() {
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }

        /*
         * (non-Javadoc)
         *
         * @see
         * android.hardware.SensorEventListener#onSensorChanged(android.hardware
         * .SensorEvent)
         */
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
                return;
            }

            switch (event.sensor.getType()) {
                case Sensor.TYPE_ACCELEROMETER:
                    // copy new accelerometer data into accel array and calculate
                    // orientation
                    System.arraycopy(event.values, 0, accel, 0, 3);

                    long stamp = event.timestamp;

                    event.values[0] = (float) ((event.values[0] - NORMALIZE_MIN) / (NORMALIZE_MAX - NORMALIZE_MIN));
                    event.values[1] = (float) ((event.values[1] - NORMALIZE_MIN) / (NORMALIZE_MAX - NORMALIZE_MIN));
                    event.values[2] = (float) ((event.values[2] - NORMALIZE_MIN) / (NORMALIZE_MAX - NORMALIZE_MIN));

                    onDetectStep(event.values[2], (stamp - last_sensor_update) / 1E6);

                    last_sensor_update = stamp;

                    break;

                case Sensor.TYPE_MAGNETIC_FIELD:
                    // copy new magnetometer data into magnet array
                    System.arraycopy(event.values, 0, magnet, 0, 3);

                    break;
            }
            onCompassChanged();
        }


    };

    /**
     * Enable step detection.
     */
    public void load() {
        SharedPreferences settings = ApplicationContext.get().getSharedPreferences(PREFS_NAME, 0);
        // Sensors
        sm = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        lSensor = sm.getSensorList(Sensor.TYPE_ALL);

        steptimer = 0;
        search_max = true;

        for (int i = 0; i < lSensor.size(); i++) {
            // Register only compass and accelerometer
            if (lSensor.get(i).getType() == Sensor.TYPE_ACCELEROMETER || lSensor.get(i).getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                sm.registerListener(mySensorEventListener, lSensor.get(i), 50000);
            }
        }
    }

    /**
     * Disable step detection.
     */
    public void unload() {
        isRunning = false;
        sm.unregisterListener(mySensorEventListener);

    }

    /**
     * On detect step.
     *
     * @param pvalue of all accelerometer data x+y+z for local peak detection
     * @param ptime  time of the last call to check if the last step occurs not too
     *               fast and not too slow
     *               <p/>
     */
    private void onDetectStep(double pvalue, double ptime) {

        buffer.insertElementAt(pvalue, 0);
        steptimer += ptime;
        if (buffer.size() > 10)
            buffer.remove(buffer.size() - 1);

        double max = (double) Collections.max(buffer);
        double min = (double) Collections.min(buffer);

        int index_max = buffer.indexOf(max);
        int index_min = buffer.indexOf(min);


//        Log.i("PEDO", "" + buffer);
        // search for the peak the local max should be found between 200 and 400 ms
        // (4*50ms and 8*50ms)
        // index_max-index_min has to be positive otherwise it is a min an the peak
        // has to be between 0.05 and 0.2 in the normalized data

        if (Math.abs(index_max - index_min) >= 4 && Math.abs(index_max - index_min) <= 8 && Math.abs(max - min) > 0.05 && Math.abs(max - min) < 0.2) {

            if (search_max && max > local_min) {
                local_max = max;
                buffer.clear();
                buffer.insertElementAt(max, 0);
                search_max = false;
                steptimer = 0;
            } else if (!search_max && min < local_max) {
                //stepText.setText("Max: " + ++counter);
                local_min = min;
                buffer.clear();
                buffer.insertElementAt(min, 0);
                search_max = true;
                steptimer = 0;
                st.triggerStep(System.currentTimeMillis(), compassAngle);
            }
        }

        if (!search_max && steptimer > 2200) {
            search_max = true;
            steptimer = 0;

        }

    }

    private void onCompassChanged() {
        if (accel != null && magnet != null) {
            float R[] = new float[9];
            float I[] = new float[9];


            boolean success = SensorManager.getRotationMatrix(R, I, accel, magnet);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                compassAngle = Math.toDegrees(orientation[0]); // orientation contains: azimuth, pitch and roll
                st.triggerCompass(System.currentTimeMillis(), compassAngle);

            }
        }
    }

    /**
     * Checks if is running.
     *
     * @return true, if is running
     */
    public boolean isRunning() {
        return isRunning;
    }

    /**
     * Sets the running.
     *
     * @param isRunning the new running
     */
    public void setRunning(boolean isRunning) {
        this.isRunning = isRunning;
    }
}
