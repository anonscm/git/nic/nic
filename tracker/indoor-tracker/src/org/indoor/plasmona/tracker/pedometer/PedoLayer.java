package org.indoor.plasmona.tracker.pedometer;

import org.indoor.plasmona.layer.TrackerLayerImpl;

// TODO: Auto-generated Javadoc
/**
 * The Class PedoLayer.
 */
public class PedoLayer extends TrackerLayerImpl {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7155368555566410855L;

  /** The Constant TAG. */
  private static final String TAG = "PedoLayer";

}
