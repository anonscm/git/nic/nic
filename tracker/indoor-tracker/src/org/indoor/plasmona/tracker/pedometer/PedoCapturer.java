package org.indoor.plasmona.tracker.pedometer;

import org.indoor.plasmona.models.GeoPoint;
import org.indoor.plasmona.tracker.Capturer;
import org.indoor.plasmona.tracker.TrackerServiceFactory;
import org.indoor.plasmona.utils.TName;

import android.util.Log;

public class PedoCapturer implements Capturer {

  private static final String TAG = "PedoCapturer";

  /**
   * Instantiates a new qR code capturer.
   */
  public PedoCapturer() {
    TrackerServiceFactory.getLocationService().findTrackerByName(TName.PEDOTRACKER).registerCapturer(this);

  }

  @Override
  public void captureAt(GeoPoint pnt) {
    Log.e(TAG, "Set North" + Float.parseFloat(pnt.getData().toString()));
    System.out.println(pnt.getData());
    System.out.println(pnt.getData().toString());
    System.out.println((Float) pnt.getData());

  }

  @Override
  public void write(GeoPoint pnt) {
    // TODO Auto-generated method stub

  }

  @Override
  public void update(GeoPoint pnt) {
    // TODO Auto-generated method stub

  }

  @Override
  public void delete(GeoPoint pnt) {
    // TODO Auto-generated method stub

  }

}
