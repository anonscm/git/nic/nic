package org.indoor.plasmona.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class TName.
 */
public class TName {

  /** The wifitracker. */
  public static final String HISTOTRACKER = "HistoWifi";

  /** The qrcodetracker. */
  public static final String QRCODETRACKER = "QRCode";

  public static final String PEDOTRACKER = "Pedometer";

}
