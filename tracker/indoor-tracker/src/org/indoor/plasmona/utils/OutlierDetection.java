package org.indoor.plasmona.utils;

import java.util.*;

import android.util.Log;
import com.google.zxing.client.result.VCardResultParser;
import org.indoor.plasmona.models.GeoPoint;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import org.indoor.plasmona.tracker.histowifi.HistoConfiguration;

// TODO: Auto-generated Javadoc

/**
 * The Class Cluster.
 */
public class OutlierDetection {

    /**
     * The Constant TAG.
     */
    private static final String TAG = "Cluster";

    /**
     * The candidates.
     */
    private ArrayList<GeoPoint> candidates = new ArrayList<GeoPoint>();

    //private Vector<Double> median = new Vector<Double>();
    /**
     * The centroid.
     */
    private GeoPoint centroid = new GeoPoint();

    private HashMap<Integer, Float> median = new HashMap<Integer, Float>();
    private HashMap<Integer, Float> absmedian = new HashMap<Integer, Float>();
    private HashMap<Integer, Float> centroidmapx = new HashMap<Integer, Float>();
    private HashMap<Integer, Float> centroidmapy = new HashMap<Integer, Float>();
    private int centroidcenter = 0;
    /**
     * The Constant MIN.
     */
    private final static double MIN = -1.0E-10;

    private int neighbours = 0;


    /**
     * Cluster the candidates by removing any outliers. The outliers might be fingerprints at the other end of the
     * building which, by chance, look similar to fingerprints near our actual position. But they are irrelevant and
     * should not distort our interpolation between the true neighbours.
     *
     * @return the array list of the candidates that belong to the cluster.
     */
    public ArrayList<GeoPoint> removeOutlier() {
        float valuemedian = 0.0f;
        float valuemedianx = 0.0f;
        float valuemediany = 0.0f;
        float absvaluemedian = 0.0f;

        // The candidates are those points, that were found to be the n closest neighbours/fingerprints.
        // If there's only one or two of them then we don't want to remove anything. The concept of outliers wouldn't
        // make sense in that case (which of two points is the "correct" one, and which one is the outlier?).
        if (candidates.size() > 2) {

            for (int i = 0; i < candidates.size(); i++) {
                centroidmapx.put(candidates.get(i).getId(), candidates.get(i).x());
                centroidmapy.put(candidates.get(i).getId(), candidates.get(i).y());
            }

            // TODO: If this is the default comparator (for floats) then why don't we use a SortedMap in the first place?
            centroidmapx = (HashMap) MapSort.sortByComparator(centroidmapx);
            centroidmapy = (HashMap) MapSort.sortByComparator(centroidmapy);

            // centroidcenter is half the candidate-size
            centroidcenter = centroidmapx.size() / 2;

            // The keyLists contain the IDs of the candidate-fingerprints, but each one is sorted according to its
            // coordinate, i.e. x or y respectively:
            List<Integer> keyListx = new ArrayList<Integer>(centroidmapx.keySet());
            List<Integer> keyListy = new ArrayList<Integer>(centroidmapy.keySet());

            if (centroidmapx.size() % 2 == 1) {
                // For an uneven number of candidates the median is just the value in the middle of the sorted list:
                valuemedianx = centroidmapx.get(keyListx.get(centroidcenter));
                valuemediany = centroidmapy.get(keyListy.get(centroidcenter));
            } else {
                // For an even number of candidates there is no single middle-element, so we take the average of the
                // two middle-elements:
                valuemedianx = 0.5f * (centroidmapx.get(keyListx.get(centroidcenter)) + centroidmapx.get(keyListx.get(centroidcenter - 1)));
                valuemediany = 0.5f * (centroidmapy.get(keyListy.get(centroidcenter)) + centroidmapy.get(keyListy.get(centroidcenter - 1)));
            }

            // The centroid is the virtual center of our cluster, having independent median-coordinates in
            // both dimensions:
            centroid.setPosition(valuemedianx, valuemediany);

            // For each candidate calculate and store its distance to the centroid:
            for (int i = 0; i < candidates.size(); i++) {
                median.put(candidates.get(i).getId(), (float) candidates.get(i).getDistanceTo(centroid));
            }

            // Sort the distance-map, so that candidates closest to the center of the cluster come first. The
            // potential outliers are then located at the end.
            // TODO: that looks dangerous. The HashMap is not guaranteed to preserve the ordering. Again, we should just us a SortedMap.
            median = (HashMap) MapSort.sortByComparator(median);

            // TODO: The number of candidates hasn't changed, so this should always be identical to the centroidcenter.
            int center = median.size() / 2;

            List<Integer> keyList = new ArrayList<Integer>(median.keySet());

            // The valuemedian is the median-distance-to-centroid of all the candidates, again calculated for even and
            // uneven sizes:
            if (median.size() % 2 == 1) {
                valuemedian = median.get(keyList.get(center));
            } else {
                valuemedian = 0.5f * (median.get(keyList.get(center)) + median.get(keyList.get(center - 1)));
            }

            // The absmedian-map stores, for each candidate, the absolute difference between its own centroid-distance
            // and the median-centroid-distance. That means that candidates far away from the cluster will have a
            // large absmedian. And the candidates that are much closer to the centroid than the median will also have
            // a large absmedian.
            for (int j = 0; j < median.size(); j++) {
                absmedian.put(keyList.get(j), Math.abs(median.get(keyList.get(j)) - valuemedian));
            }

            // TODO: again, we should use a SortedMap.
            absmedian = (HashMap) MapSort.sortByComparator(absmedian);

            // TODO: again, this will be the same index as it was before because the number hasn't changed, only the
            //       the values and the ordering:
            int abscenter = absmedian.size() / 2;

            List<Integer> abskeyList = new ArrayList<Integer>(absmedian.keySet());

            // Now we get the median divergence from the median distance... that can probably be seen as a measure of
            // how dense our cluster is. (Although a ring of points would have a very low value here as well, because
            // all points would have the same distance to the centroid and therefore the same divergence (zero) from
            // the median distance.)
            if (absmedian.size() % 2 == 1) {
                absvaluemedian = absmedian.get(abskeyList.get(abscenter));
            } else {
                absvaluemedian = 0.5f * (absmedian.get(abskeyList.get(abscenter)) + absmedian.get(abskeyList.get(abscenter - 1)));

            }

            // These will be values that describe the minimal and maximal divergence from the absvaluemedian among
            // all the candidates. They are not distances, but quotients (factors?).
            float min = Float.MAX_VALUE;
            float max = Float.MIN_VALUE;

            // TODO: if the map were sorted, why would we need loop over it? Are the min and max divergences from the
            //       absvaluemedian not always at the end/beginning of the absmedian-map?
            for (int k = 0; k < absmedian.size(); k++) {
                //Log.i(TAG, "test value median "+ abskeyList.get(k)+" = "+ absmedian.get(abskeyList.get(k)) / absvaluemedian);

                if (absmedian.get(abskeyList.get(k)) / absvaluemedian < min) {
                    min = absmedian.get(abskeyList.get(k)) / absvaluemedian;
                }
                if (absmedian.get(abskeyList.get(k)) / absvaluemedian > max) {
                    max = absmedian.get(abskeyList.get(k)) / absvaluemedian;
                }
            }

            for (int h = 0; h < absmedian.size(); h++) {
                //Log.i(TAG, "test value median            ++++ " + ((absmedian.get(abskeyList.get(h)) / absvaluemedian) - min) / (max - min));
                // TODO: This "if" is invariant, relative to the enclosing loop, and should be pulled out. If min and
                //       max are the same we don't need to enter the loop at all.
                if ((max - min) != 0)
                    // The tricky part :-) Throw out the candidates that aren't close enough to the cluster.
                    // I don't understand the formula, though. Too much medianism ;-)
                    if (((absmedian.get(abskeyList.get(h)) / absvaluemedian) - min) / (max - min) > 0.8) {
                        //Log.i(TAG, "test value median remove " + abskeyList.get(h));
                        candidates.remove(abskeyList.get(h));
                    }
            }
            //Log.i(TAG, "test value median " + min + " " + max);
        }
        return candidates;
    }


    /**
     * Gets the candidates.
     *
     * @return the candidates
     */
    public ArrayList<GeoPoint> getCandidates() {
        return candidates;
    }

    /**
     * Sets the candidates.
     *
     * @param _candidates the new candidates
     */
    public void setCandidates(ArrayList<GeoPoint> _candidates) {
        candidates.addAll(_candidates);
    }

    /**
     * Gets the centroid.
     *
     * @return the centroid
     */
    public GeoPoint getCentroid() {
        return centroid;
    }

    /**
     * Sets the centroid.
     *
     * @param _centroid the new centroid
     */
    public void setCentroid(GeoPoint _centroid) {
        centroid = _centroid;
    }

}
