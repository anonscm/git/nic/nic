package org.indoor.plasmona.utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

// TODO: Auto-generated Javadoc
/**
 * The Class MapSort.
 */
public class MapSort {
  /*
   * Paramterized method to sort Map e.g. HashMap or Hashtable in Java throw
   * NullPointerException if Map contains null key
   */
  /**
   * Sort by keys.
   * 
   * @param <K>
   *          the key type
   * @param <V>
   *          the value type
   * @param map
   *          the map
   * @return the map
   */
  public static <K extends Comparable, V extends Comparable> Map<K, V> sortByKeys(Map<K, V> map) {
    List<K> keys = new LinkedList<K>(map.keySet());
    Collections.sort(keys);

    // LinkedHashMap will keep the keys in the order they are inserted
    // which is currently sorted on natural ordering
    Map<K, V> sortedMap = new TreeMap<K, V>();
    for (K key : keys) {
      sortedMap.put(key, map.get(key));
    }

    return sortedMap;
  }

  /*
   * Java method to sort Map in Java by value e.g. HashMap or Hashtable throw
   * NullPointerException if Map contains null values It also sort values even
   * if they are duplicates
   */
  /**
   * Sort by values.
   * 
   * @param <K>
   *          the key type
   * @param <V>
   *          the value type
   * @param map
   *          the map
   * @return the map
   */
  public static Map sortByComparator(Map unsortMap) {

    List list = new LinkedList(unsortMap.entrySet());

    // sort list based on comparator
    Collections.sort(list, new Comparator() {
      public int compare(Object o1, Object o2) {
	return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
      }
    });

    // put sorted list into map again
    // LinkedHashMap make sure order in which keys were inserted
    Map sortedMap = new LinkedHashMap();
    for (Iterator it = list.iterator(); it.hasNext();) {
      Map.Entry entry = (Map.Entry) it.next();
      sortedMap.put(entry.getKey(), entry.getValue());
    }
    return sortedMap;
  }

}
