package org.indoor.plasmona.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class Prefs.
 */
public class Prefs {
	
	/** The Constant PROJECT. */
	public static final String PROJECT = "Project";
	
	/** The plasmona. */
	public static String PLASMONA = "Plasmona";
	
	/** The nav mode. */
	public static String NAV_MODE = "NavigationMode";
	
	/** The nav mode locate. */
	public static int NAV_MODE_LOCATE = 0;
	
	/** The nav mode edit. */
	public static int NAV_MODE_EDIT = 1;

    /**
     * Static path to the histo-wifi-references.xml file.
     */
    private static String pathToHistoWifi;

    /**
     * Determines whether or not the fingerprint should be shown. Default value is true.
     */
    private static boolean fingerprintVisible = true;

    public static String getPathToHistoWifi() {
        return pathToHistoWifi;
    }

    public static void setPathToHistoWifi(final String newPathToHistoWifi) {
        pathToHistoWifi = newPathToHistoWifi;
    }

    public static void setFingerprintVisible(final boolean newFingerprintVisible) {
        fingerprintVisible = newFingerprintVisible;
    }

    public static boolean isFingerprintVisible() {
        return fingerprintVisible;
    }
}
