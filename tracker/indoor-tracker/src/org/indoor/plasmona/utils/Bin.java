package org.indoor.plasmona.utils;

// TODO: Auto-generated Javadoc

import android.util.Log;
import org.indoor.plasmona.tracker.histowifi.HistoConfiguration;

/**
 * The Class Bin is used to quantize/group measured signal-levels into bins. E.g. in some fingerprint-files it can be
 * seen that the levels, which are recorded in the fingerprints, are always multiples of 4.
 * This cuts off small variations in the signal-strength. Maybe that is done to get nicer probabilities (by reducing
 * noise). It does, however, reduce the precision, as it is reported by the device (!).
 */
public class Bin {

    /**
     * The bin min db. Every level below this value will be recorded as -98
     */
    public static int BIN_MIN_DB = -100;

    /**
     * The bin max db. Every level above this value will be recorded as -1
     */
    public static int BIN_MAX_DB = -30;

    /**
     * Gets the bin.
     *
     * @param level the level
     * @return the bin
     */
    public static int getBin(int level) {
        int bin = -1;
        int binSize = HistoConfiguration.getInstance().getBinSize();
        // Couldn't this whole loop be replaced by: "level-((level+1)%binSize)-1" ?!
        for (int i = BIN_MIN_DB; i <= BIN_MAX_DB; i += binSize) {

            if (level >= i && level < i + binSize)
                bin = (int) (i + (binSize / 2));

        }
        return bin;
    }
}
