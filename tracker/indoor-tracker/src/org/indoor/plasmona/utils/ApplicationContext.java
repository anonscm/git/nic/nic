package org.indoor.plasmona.utils;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.content.Context;

// TODO: Auto-generated Javadoc
/**
 * The Class ApplicationContext.
 */
public class ApplicationContext {

  /** The context. */
  private static WeakReference<Context> context;

  /** The activity. */
  private static WeakReference<Activity> activity;

  /**
   * Gets the activity.
   * 
   * @return the activity
   */
  public static Activity getActivity() {
    return activity.get();
  }

  /**
   * Sets the activity.
   * 
   * @param a
   *          the new activity
   */
  public static void setActivity(Activity a) {
    activity = new WeakReference<Activity>(a);
  }

  /**
   * Gets the.
   * 
   * @return the context
   */
  public static Context get() {
    return context.get();
  }

  /**
   * Sets the.
   * 
   * @param c
   *          the c
   */
  public static void set(Context c) {
    context = new WeakReference<Context>(c);
  }

}