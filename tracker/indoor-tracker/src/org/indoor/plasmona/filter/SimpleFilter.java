/**
 * @author jens maiero
 *
 * @project plasmona
 *
 * PlaSMoNa (platform for social mobile navigation) is a research project started in January 2012.
 */

package org.indoor.plasmona.filter;

import java.util.*;

import java.util.Vector;


import android.util.FloatMath;
import org.indoor.plasmona.models.GeoPoint;

import android.util.Log;
import org.indoor.plasmona.utils.MapSort;

/**
 * The Class SimpleFilter. This class represent a simple filter class for
 * smoothing data, based on a weighted average filter. The static HISTORY stands
 * for the size of the history which the filter takes into account
 */
public class SimpleFilter {


    /**
     * The Constant TAG.
     */
    private static final String TAG = "SimpleFilter";

    /**
     * The history.
     */
    private static int HISTORY = 40;

    /**
     * The history.
     */
    private Vector<GeoPoint> history;

    /**
     * The smooth.
     */
    private Vector<GeoPoint> smooth;


    /**
     * The result.
     */
    private GeoPoint result = null;
    private float meter = 5;
    private Vector<Double> distance;
    private Vector<Double> distanceParticles;
    private Random rand;
    private Vector<Double> angle;
    private final static double MIN = -1.0E-10;
    private HashMap<Integer, Float> median = new HashMap<Integer, Float>();

    /**
     * The centroid.
     */
    private GeoPoint centroid = new GeoPoint();

    /**
     * The smooth vec.
     */
    private GeoPoint smoothVec = null;

    /**
     * Instantiates a new simple filter.
     */
    public SimpleFilter() {
        history = new Vector<GeoPoint>(HISTORY);
        smooth = new Vector<GeoPoint>();
        rand = new Random();
        distance = new Vector<Double>(HISTORY);
        angle = new Vector<Double>(HISTORY);
        distanceParticles = new Vector<Double>(HISTORY);

        smoothVec = new GeoPoint();
    }


    public GeoPoint onFilterMedian(GeoPoint _pos) {
        if (history.size() >= HISTORY)
            history.remove(0);
        history.add(_pos);

        if (history.size() >= 1)
            return onFilterMedian();
        else return _pos;
    }


    private GeoPoint onFilterMedian() {

        float dist;
        //Log.i(TAG, "median kk "+history);
        for (int i = 0; i < history.size(); i++) {
          //  Log.i(TAG, "median kk "+i+" "+history.get(i)+" "+history.lastElement() );
            dist = (float) history.get(i).getDistanceTo(history.lastElement());
         //   Log.i(TAG, "median kk"+dist );
            median.put(i, dist);
        }

        median = (HashMap) MapSort.sortByComparator(median);
       // Log.i(TAG, "median "+median);

        int center = median.size() / 2;

        List<Integer> keyList = new ArrayList<Integer>(median.keySet());

        //Log.i(TAG, "median "+keyList);

        GeoPoint p;
        GeoPoint res = new GeoPoint();

        p = history.get(keyList.get(center));

        if (median.size() % 2 == 1) {
            res = p;
        } else {
            GeoPoint q;
            q = history.get(keyList.get(center - 1));

            if (median.get(center) == median.get(center - 1))
                res = q;
            else {
                res.setX(0.5f *(p.x() + q.x()));
                res.setY(0.5f *(p.y() + q.y()));
            }
        }

        return res;

    }

    /**
     * On filter average.
     *
     * @param _pos the _pos
     * @return the position
     */
    public GeoPoint onFilterAverage(GeoPoint _pos) {
        result = new GeoPoint();
        if (history.size() >= HISTORY)
            history.remove(0);
        history.add(_pos);

        float x = 0.0f;
        float y = 0.0f;

        for (GeoPoint p : history) {
            x += p.x();
            y += p.y();
        }

        result.setPosition(x / history.size(), y / history.size());

        return result;
    }


    double var(double mean, Vector<Double> list) {

        double temp = 0;
        for (double a : list)
            temp += (mean - a) * (mean - a);
        return temp / list.size();
    }

    /**
     * On smooth data.
     *
     * @param _pos the _pos
     * @return the position
     */
    public GeoPoint onSmoothData(GeoPoint _pos) {
        float x, y;
        if (smooth.size() >= 2)
            smooth.remove(0);
        smooth.add(_pos);

        if (smooth.size() >= 2) {

            // 0 old 1 new
           //Log.i(TAG, "smoothed test "+smooth.get(0).getDistanceTo(smooth.get(1))+" \n "+smooth.get(0)+" \n "+smooth.size());
           //Log.i(TAG, "smoothed test "+Math.sqrt(0));
            if (smooth.get(0).getDistanceTo(smooth.get(1)) > meter) {
                float dx = 0.5f*(smooth.get(0).x() + smooth.get(1).x());
                float dy = 0.5f*(smooth.get(0).y() + smooth.get(1).y());
                //Log.i(TAG, "smoothed "+dx+" "+dy);
                if (dx != 0 && dx != 0) {
                    //x = (meter / FloatMath.sqrt(dx * dx + dy * dy)) * dx;
                    //y = (meter / FloatMath.sqrt(dx * dx + dy * dy)) * dy;

                    //_pos.setX(smooth.get(0).x() - x);
                    _pos.setX(dx);
                    _pos.setY(dy);
                    //_pos.setY(smooth.get(0).y() - y);
                }
            } else _pos = smooth.get(1);

            return _pos;
        } else {

            return _pos;
        }
    }

    public void setPixelMeterRelation(float f) {
        meter = f;
    }


}
