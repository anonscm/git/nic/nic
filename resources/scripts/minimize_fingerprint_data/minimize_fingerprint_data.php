<?php

/**
 * Fingerprint data file minimization script.
 *
 * This script processes two methods to minimize the size of any fingerprint_data file given the following JSON format:
 *
 * ------------------------------------------------------------
 *
 *  [
 *
 *      {
 *          "histogram": {
 *              "00:8e:f2:fe:9b:3f": {
 *                  "-67": 0.25,
 *                  "-66": 0.25,
 *                  "-64": 0.25,
 *                  "-62": 0.25
 *          },
 *          "id": "FP-1",
 *          "point": {
 *              "divergence": 0,
 *              "mAltitude": 0,
 *              "mLatitudeE6": 51254012,
 *              "mLongitudeE6": 6748470
 *          }
 *      },
 *
 *      [...]
 *
 *  ]
 *
 * ------------------------------------------------------------
 *
 * Currently this script expects a file with the name configured in "$fingerprints_data_file_name" in the same folder.
 *
 * Usage:
 *  php -f minimize_fingerprint_data.php
 *
 * @version     PHP 5.4.9-4ubuntu2
 * @author      Andreas Grau <a.grau@tarent.de>
 *
 */

/*
 * 0. Set parameters here
 */

// Input file name
$fingerprints_data_file_name = "fingerprints_data.json";

// Output file name
$fingerprints_data_file_minimized_name = "fingerprints_data_new.json";

// Rounding precision
$rounding_precision = 2;

// Threshold for deleting measurements
$measurement_threshold = -70;


/*
 * 1. Read fingerprints
 */

// Load resource file content into JSON object
$fingerprints_data_json = json_decode(file_get_contents($fingerprints_data_file_name), true);


/*
 * 2. Minimize size and save fingerprints
 */

// Process minimization tasks
foreach ($fingerprints_data_json as $fingerprint_key => $fingerprint_value) {

    // Go into each histogram element and iterate over all access points
    foreach ($fingerprint_value['histogram'] as $access_point_key => $access_point_value) {

        // Go into each access point element and iterate over all measurements
        foreach ($access_point_value as $measurement_key => $measurement_value) {

            // a) For every measurement, round measurement floating-point value
            $fingerprints_data_json[$fingerprint_key]['histogram'][$access_point_key][$measurement_key] = round($measurement_value, $rounding_precision);

            // b) For every measurement, remove complete measurement if it is beyond the threshold value for weak measurements
            if ($measurement_key < $measurement_threshold) {
                unset ($fingerprints_data_json[$fingerprint_key]['histogram'][$access_point_key][$measurement_key]);
            }

            // c) Remove complete access point if it has no measurements left through optimization
            if (empty($fingerprints_data_json[$fingerprint_key]['histogram'][$access_point_key])) {
                unset ($fingerprints_data_json[$fingerprint_key]['histogram'][$access_point_key]);
            }

        }


    }

}


/*
 * 3. Save minimized fingerprints
 */

// Save minimized JSON object into result file
file_put_contents($fingerprints_data_file_minimized_name, json_encode($fingerprints_data_json))

?>

