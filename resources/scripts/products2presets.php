<?php

// This script acts as a filter. It reads products on STDIN, one per line,
// and prints xml-items suitable for josm presets-files to STDOUT.
// The input format is: productname;barcode;price
// Whitespace around the ';' is stripped.
// A ',' or '.' in the price is removed and leading zeros are stripped to convert prices to cents.

$groupStart = <<<'EOD'
<group name="Produkte">

EOD;

$groupEnd = <<<'EOD'
</group>

EOD;

$itemTemplate = <<<'EOD'
    <item name="#name" type="node">
      <key key="name" value="#name" />
      <key key="EAN-13" value="#barcode" />
      <key key="price" value="#price" />
    </item>

EOD;

$itemPatterns = array("/#name/", "/#barcode/", "/#price/" );

echo $groupStart;
while($line = fgets(STDIN)){
    if (preg_match("/(.+);(.+);(.+)/", $line, $matches)) {
        $name = trim($matches[1]);
        $barcode = trim($matches[2]);
        $price = ltrim(strtr(trim($matches[3]), array("," => "", "." => "")), "0");
        
        $item = preg_replace($itemPatterns, array($name, $barcode, $price), $itemTemplate);
        
        echo $item;
    }
}
echo $groupEnd;

?>
