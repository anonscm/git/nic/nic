package de.tarent.invio.entities;

/**
 * A base class for the POIs (point of interest). Contains all of the base requirements for each POI. There should never
 * be anything referencing to a specific type of POI in this class.
 */
public class POI {

    /**
     * The identifier to distinguish between POIs.
     */
    private long id;

    /**
     * The name of the POI.
     */
    private String name;

    /**
     * The map to which the POI belongs in case there is multiple maps.
     */
    private String mapId;

    /**
     * Constructor.
     *
     * @param id the POI id
     * @param name the POI name
     */
    public POI(final long id, final String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Constructor.
     *
     * @param id the POI id
     * @param name the POI name
     * @param mapId the id of the map to which the POI belongs
     */
    public POI(final long id, final String name, final String mapId) {
        this(id, name);
        this.mapId = mapId;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getMapId() {
        return mapId;
    }

    public void setMapId(final String mapId) {
        this.mapId = mapId;
    }
}
