package de.tarent.invio.entities;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HistogramTest {

    @Test
    public void testThatInsertReturnsCorrectValue() {
        final Map<Integer, Float> value = new HashMap<Integer, Float>();

        final Histogram histogramOne = new Histogram("one");
        histogramOne.put("one", value);
        histogramOne.put("two", value);
        histogramOne.put("three", value);

        final Histogram histogramTwo = new Histogram();
        histogramTwo.setId("two");
        histogramTwo.put("two", value);
        histogramTwo.put("three", value);
        histogramTwo.put("four", value);

        final Histogram expected = new Histogram("expected");
        expected.put("two", value);
        expected.put("three", value);

        final Set<String> commonKeys = Histogram.getCommonKeys(histogramOne, histogramTwo);
        histogramOne.keySet().retainAll(commonKeys);
        histogramTwo.keySet().retainAll(commonKeys);

        assertTrue(onlyContainsExpectedKeys(histogramOne, expected));
        assertTrue(histogramOne.getId().equals("one"));
        assertTrue(onlyContainsExpectedKeys(histogramTwo, expected));
        assertTrue(histogramTwo.getId().equals("two"));
    }

    @Test
    public void testThatConstructorWithHistogramCopiesBothIdAndKeyValues() {
        final Map<Integer, Float> value = new HashMap<Integer, Float>();

        final Histogram original = new Histogram("test");
        original.put("one", value);
        original.put("two", value);
        original.put("three", value);

        final Histogram copy = new Histogram(original);

        assertEquals(original, copy);
    }

    private boolean onlyContainsExpectedKeys(final Histogram histogram, final Histogram expected) {
        for (final String expectedKey : expected.keySet()) {
            if (!histogram.containsKey(expectedKey)) {
                return false;
            }
        }

        for (final String histogramKey : histogram.keySet()) {
            if (!expected.containsKey(histogramKey)) {
                return false;
            }
        }

        return true;
    }
}