package de.tarent.invio.android.admin;

import android.app.Activity;
import android.content.res.Resources;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = "../admin/AndroidManifest.xml")
public class VersionDialogTest {

    @Test
    public void testBuildVersionInformationDialog() {
        final Activity activity = Robolectric.buildActivity(Activity.class).create().get();
        final Activity activitySpy = spy(activity);
        final Resources resources = mock(Resources.class);
        final VersionDialog versionDialog = new VersionDialog(activitySpy);

        final String testString = "testString";

        when(activitySpy.getResources()).thenReturn(resources);
        when(resources.getString(any(Integer.class))).thenReturn(testString);

        final VersionDialog versionDialogSpy = spy(versionDialog);
        versionDialogSpy.buildVersionInformationDialog();

        verify(versionDialogSpy, times(1)).getVersionMessage();
    }
}