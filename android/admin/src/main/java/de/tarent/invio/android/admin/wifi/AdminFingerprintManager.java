package de.tarent.invio.android.admin.wifi;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.widget.Toast;
import de.tarent.invio.android.admin.MapActivity;
import de.tarent.invio.android.admin.R;
import de.tarent.invio.android.base.AbstractMapActivity;
import de.tarent.invio.android.base.position.InvioGeoPointImpl;
import de.tarent.invio.android.base.sensor.SensorCollector;
import de.tarent.invio.android.base.wifi.BluetoothLECapturer;
import de.tarent.invio.android.base.wifi.CaptureCallback;
import de.tarent.invio.android.base.wifi.FingerprintItem;
import de.tarent.invio.android.base.wifi.FingerprintManager;
import de.tarent.invio.android.base.wifi.WifiCapturer;
import de.tarent.invio.entities.Fingerprint;
import de.tarent.invio.entities.Histogram;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.tracker.wifi.HistogramConsumer;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;

import java.util.ArrayList;
import java.util.List;

import static de.tarent.invio.android.base.utils.ScreenOrientation.lockScreenOrientation;
import static de.tarent.invio.android.base.utils.ScreenOrientation.unlockScreenOrientation;


/**
 * The FingerprintManager holds the wifi-fingerprints and the overlay to display them.
 * It is also responsible for uploading and downloading fingerprints to and from the mapserver.
 */
public class AdminFingerprintManager extends FingerprintManager implements HistogramConsumer, CaptureCallback {
    public static final int CAPTURE_WIFI = 0;
    public static final int CAPTURE_BLUETOOTH_LE = 1;

    /**
     * This is the point where the user wants to create the next fingerprint. It has to be stored somewhere because
     * the actual creation is triggered later, asynchronously by the wifiCapturer.
     */
    private IGeoPoint fingerprintLocation;


    private int captureIteration = -1;
    private FingerprintItem currentFingerprintItem = null;

    private List<FingerprintItem> newCreatedFingerprintItems = new ArrayList<FingerprintItem>();

    private SensorCollector currentSensorCollector = null;

    /**
     * Construct a new FingerprintManager.
     *
     * @param activity        the parent activity, from which we need the Context and FragmentManager
     * @param mapServerClient the {@link MapServerClient}
     * @param mapView         the {@link MapView}
     * @param mapName         the name of the map, as it is used in the mapserver-URLs
     */
    public AdminFingerprintManager(final AbstractMapActivity activity,
                                   final MapServerClient mapServerClient,
                                   final MapView mapView, String mapName) {
        super(activity, mapServerClient, mapView, mapName);
    }

    /**
     * Construct a new FingerprintManager with possibility not to download the fingerprints.
     *
     * @param activity             the parent activity, from which we need the Context and FragmentManager
     * @param mapServerClient      the {@link MapServerClient}
     * @param mapView              the {@link MapView}
     * @param mapName              the name of the map, as it is used in the mapserver-URLs
     * @param downloadFingerprints flag in case we want to construct without downloading the fingerprints
     */
    public AdminFingerprintManager(final AbstractMapActivity activity,
                                   final MapServerClient mapServerClient,
                                   final MapView mapView, String mapName,
                                   final boolean downloadFingerprints) {
        super(activity, mapServerClient, mapView, mapName, downloadFingerprints, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override // NOSONAR - This method cannot be shortened any more than it already has been.
    public boolean onItemSingleTapUp(final int index, final FingerprintItem item) {
        final FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
        final Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        final DialogFragment dialog = new FingerprintDialog(AdminFingerprintManager.this, item);
        dialog.show(ft, "dialog");
        return true;
    }


    /**
     * Creates a wifi fingerprint by using the users finger position.
     *
     * @param point          the {@link IGeoPoint}
     */
    public void createWifiFingerprint(final IGeoPoint point) {
        fingerprintLocation = point;
        if (currentFingerprintItem == null) {
            currentFingerprintItem = new FingerprintItem();
        }
        captureIteration = CAPTURE_WIFI;

        final WifiCapturer wifiCapturer = new WifiCapturer(activity);
        wifiCapturer.makeFingerprint(this, this);
        currentSensorCollector = wifiCapturer;
        // and now we wait for the WifiCapturer to do its work, ending with the call to our addHistogram(Histogram).
    }

    /**
     * Creates a bluetooth le fingerprint by using the users finger position.
     *
     * @param point          the {@link IGeoPoint}
     * @return true if bluetooth is supported and enabled, otherwise false
     */
    public boolean createBluetoothLEFingerprint(final IGeoPoint point) {
        //only create bluetooth le fingerprints if we are on a device that has bluetooth le (Nexus 7, SGS4, ...)
        if (isBluetoothEnabled()) {
            fingerprintLocation = point;
            if (currentFingerprintItem == null) {
                currentFingerprintItem = new FingerprintItem();
            }

            captureIteration = CAPTURE_BLUETOOTH_LE;
            final BluetoothLECapturer btleCapturer = new BluetoothLECapturer(activity);
            btleCapturer.makeFingerprint(this, this);
            currentSensorCollector = btleCapturer;
            // and now we wait for the BluetoothLECapturer to do its work, ending with the call to our
            // addHistogram(Histogram).
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     * Add a new FingerprintItem to the overlay. It will be visible immediately at the position that was saved when
     * the user initially clicked on the FingerprintCaptureOverlay.
     */
    @Override
    public void addHistogram(Histogram histogram) {
        if(currentFingerprintItem != null) {
            currentFingerprintItem
                    .setGeoLocation(fingerprintLocation.getLatitudeE6(), fingerprintLocation.getLongitudeE6());

            //FIXME: overlay.size() is not the correct solution for determining the fingerprint count
            // it works for now because the fingerprint overlay really only stores one overlayitem for one group of
            // fingerprints
            histogram.setId("FP-" + (getOverlay().size() + 1));
            final Fingerprint fingerprint = new Fingerprint(histogram, new InvioGeoPointImpl(fingerprintLocation));

            if (captureIteration == CAPTURE_WIFI) {
                currentFingerprintItem.setWifiFingerprint(fingerprint);
            } else if (captureIteration == CAPTURE_BLUETOOTH_LE) {
                currentFingerprintItem.setBluetoothLEFingerprint(fingerprint);
            }

            //check if we have already added the current fingerprint to our overlay so that we dont add it twice
            if (!newCreatedFingerprintItems.contains(currentFingerprintItem)) {
                newCreatedFingerprintItems.add(currentFingerprintItem);
                overlay.addItem(currentFingerprintItem);
            }

            mapView.postInvalidate();
        }
    }

    /**
     * Upload the current list of Fingerprints to the mapserver. The old list on the server will be overwritten.
     */
    public void uploadFingerprints() {
        if (isNetworkAvailable()) {
            // Upload the fingerprints in the background:
            // TODO: we need a timeout here as well. Maybe, now that we're back at httpcomponents 4.0.1, we can
            //       have a "normal" setTimeout-method in our client-lib.
            new UploadFingerprintsTask(this, activity, mapServerClient, mapName).execute();
        } else {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(activity, "No network: upload failed", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * Create an overlay to display icons for the fingerprints. In the admin-app we want to see the fingerprints,
     * so we need a different overlay than what we use in the customer-apps. This one here simply works like a normal
     * ItemizedIconOverlay, letting it draw its icons as it was meant to be.
     *
     * @param activity the activity, used for the context that the DefaultResourceProxyImpl of the overlay needs.
     * @return the new ItemizedIconOverlay
     */
    protected ItemizedIconOverlay<FingerprintItem> createFingerprintOverlay(final Activity activity) {
        return new ItemizedIconOverlay<FingerprintItem>(new ArrayList<FingerprintItem>(),
                activity.getResources().getDrawable(R.drawable.ips_histo),
                this,
                new DefaultResourceProxyImpl(activity)) {
        };
    }

    /**
     * Stops the fingerprint from capturing by stopping the running sensor and deleting the fingerprint item.
     */
    public void stopCapturingFingerprint() {
        if (currentSensorCollector != null) {
            currentSensorCollector.stopSensors();
        }

        if (currentFingerprintItem != null) {
            newCreatedFingerprintItems.remove(currentFingerprintItem);
            getOverlay().removeItem(currentFingerprintItem);
        }

        disableFingerprinting();
    }

    @Override
    public void onCaptureStart() {
        lockScreenOrientation(activity);
    }

    @Override
    public void onCaptureIteration(final int iteration, final int maxIterations) {
        final MapActivity mapActivity = ((MapActivity)activity);
        mapActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final ProgressDialog dialog = mapActivity.getProgressDialog();
                if(dialog != null) {
                    dialog.setProgress(100 / maxIterations * (iteration + 1));
                }
            }
        });
    }

    @Override
    public void onCaptureStop() {
        if (captureIteration == CAPTURE_WIFI) {
            if (isBluetoothEnabled()) {
                createBluetoothLEFingerprint(fingerprintLocation);
                captureIteration = CAPTURE_BLUETOOTH_LE;
            } else {
                disableFingerprinting();
            }
        } else if(captureIteration == CAPTURE_BLUETOOTH_LE) {
            disableFingerprinting();
        }
    }

    private void disableFingerprinting() {
        final MapActivity mapActivity = ((MapActivity)activity);
        currentFingerprintItem = null;
        currentSensorCollector = null;
        unlockScreenOrientation(mapActivity);
        mapView.postInvalidate();
        mapActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final ProgressDialog dialog = mapActivity.getProgressDialog();
                if(dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }
}
