package de.tarent.invio.android.admin;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import de.tarent.invio.android.admin.wifi.AdminFingerprintManager;
import de.tarent.invio.android.base.wifi.FingerprintManager;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.Projection;

/**
 * The CaptureMapView encapsulates the MapView so that we can add our own touch events to the view.
 */
public class CaptureMapView extends MapView {
    GestureDetector gestureDetector;
    private MapActivity mapActivity;
    private boolean longPressed = false;

    /**
     * Constructor.
     *
     * @param context the {@link Context}
     * @param attrs the {@link AttributeSet}
     */
    public CaptureMapView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        mapActivity = (MapActivity) context;
        createGestureDetector();
    }

    private void createGestureDetector() {
        gestureDetector = new GestureDetector(mapActivity, new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onDown(final MotionEvent e) {
                return false;
            }

            @Override
            public void onLongPress(final MotionEvent e) {
                if (!mapActivity.isTracking()) {
                    longPressed = true;
                    moveCrosshair(e.getX(), e.getY());
                    mapActivity.getCrosshairView().setVisibility(View.VISIBLE);
                }
            }

            @Override
            public boolean onSingleTapUp(final MotionEvent e) {
                return false;
            }
        });
    }

    @Override
    public boolean onTouchEvent(final MotionEvent e) {
        gestureDetector.onTouchEvent(e);

        if (longPressed && e.getAction() == MotionEvent.ACTION_UP) {
            mapActivity.getCrosshairView().setVisibility(View.INVISIBLE);
            final Projection projection = getProjection();

            final IGeoPoint currentPoint = projection.fromPixels((int) e.getX(), (int) e.getY());
            startCapturing(currentPoint);
            longPressed = false;
            return true;
        }

        if (longPressed && e.getAction() == MotionEvent.ACTION_MOVE) {
            moveCrosshair(e.getX(), e.getY());
        }

        return false;
    }

    private void moveCrosshair(final float x, final float y) {
        final ImageView crosshairView = mapActivity.getCrosshairView();

        crosshairView.setX((x - crosshairView.getWidth() / 2f));
        crosshairView.setY((y - crosshairView.getHeight() / 2f));
    }

    /**
     * Start the fingerprint capture process.
     *
     * @param currentPoint the current point where the user pressed
     */
    public void startCapturing(final IGeoPoint currentPoint) {
        final WifiManager wifi = (WifiManager) App.getContext().getSystemService(Context.WIFI_SERVICE);

        if (wifi.isWifiEnabled() && wifi.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
            final FingerprintManager fingerprintManager = mapActivity.getFingerprintManager();
            if (fingerprintManager != null) {
                ((AdminFingerprintManager) fingerprintManager).createWifiFingerprint(currentPoint);
                buildProgressDialog();
            }
        }
    }

    private ProgressDialog buildProgressDialog() {
        // prepare for a progress bar dialog
        // TODO: can't we move this to the wifiscanprogress.xml?
        final ProgressDialog progressDialog = new ProgressDialog(mapActivity);
        progressDialog.setCancelable(true);
        progressDialog.setMessage("Scanning...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setProgress(0);
        progressDialog.setMax(100);
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(true);
        progressDialog.setOnCancelListener(mapActivity);
        mapActivity.setProgressDialog(progressDialog);

        return progressDialog;
    }
}
