package de.tarent.invio.android.admin.wifi;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import de.tarent.invio.android.admin.App;
import de.tarent.invio.android.admin.R;
import de.tarent.invio.android.base.wifi.FingerprintItem;
import de.tarent.invio.entities.Fingerprint;

import java.util.Map;

/**
 * The FingerprintDialog shows a data stored in a fingerprint and allows the user to delete the fingerprint.
 */
public class FingerprintDialog extends DialogFragment {

    private static FingerprintItem lastFingerprintItem = null;

    /**
     * We need the manager if the user wants to remove the fingerprint.
     */
    private AdminFingerprintManager manager;

    /**
     * The dialog belongs to one specific fingerprint.
     */
    private FingerprintItem item;

    /**
     * Default Constructor for the FragmentManager
     */
    public FingerprintDialog() {
        this.item = lastFingerprintItem;
    }

    /**
     * Construct a new FingerprintDialog.
     *
     * @param manager the FingerprintManager which manages the fingerprint.
     * @param item    the item that is to be shown in this dialog.
     */
    public FingerprintDialog(final AdminFingerprintManager manager, final FingerprintItem item) {
        this.manager = manager;
        this.item = item;
        lastFingerprintItem = item; //NOSONAR - It's fine if the static field is overridden.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final String message = buildMessage();
        final String title = buildTitle();

        manager.stopCapturingFingerprint();

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setMessage(message)
                .setNeutralButton(R.string.dialog_fingerprint_button_ok, createOkListener())
                .setNegativeButton(R.string.dialog_fingerprint_button_delete, createDeleteListener());
        return builder.create();
    }


    /**
     * This listener is called when the user clicks "delete" and it removes the fingerprint from the manager without
     * any further confirmation.
     *
     * @return the OnClickListener
     */
    private DialogInterface.OnClickListener createDeleteListener() {
        return new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                manager.removeFingerprintFromOverlay(item);
            }
        };
    }

    /**
     * This listener is called when the user clicks "OK" but it doesn't do anything. Just close the dialog.
     *
     * @return the OnClickListener
     */
    private DialogInterface.OnClickListener createOkListener() {
        return new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                // Dismissed...
            }
        };
    }

    /**
     * The title of the dialog shows the ID of the fingerprint and its coordinates.
     *
     * @return the title string
     */
    private String buildTitle() {
        final Fingerprint fingerprint = getFingerprint();

        if (fingerprint == null) {
            dismiss();
            return null;
        }

        final String title = fingerprint.getId() + ": " +
                fingerprint.getPoint().getLatitudeE6() + ", " +
                fingerprint.getPoint().getLongitudeE6();

        return title;
    }

    private Fingerprint getFingerprint() {
        // Try and get a wifi fingerprint first.
        Fingerprint fingerprint = item.getWifiFingerprint();

        if (fingerprint == null) {
            // If the wifi fingerprint is null, it might mean that the fingerprint is actually a bluetooth fingerprint.
            fingerprint = item.getBluetoothLEFingerprint();
        }
        return fingerprint;
    }

    /**
     * The message is the body of the dialog. It shows the list of access points and their signal strength levels
     * at this point.
     *
     * @return the message body
     */
    private String buildMessage() {
        final StringBuilder message = new StringBuilder();

        if (item.getWifiFingerprint() != null) {
            message.append(App.getContext().getString(R.string.dialog_fingerprint_wifi_title)).append("\n\n");
            appendFingerprint(message, item.getWifiFingerprint());
            if (item.getWifiFingerprint().getHistogram().isEmpty()) {
                message.append(App.getContext().getString(R.string.dialog_fingerprint_no_wifi_signals_found))
                        .append("\n\n");
            }
        }

        if (item.getBluetoothLEFingerprint() != null) {
            message.append("\n").append(App.getContext().getString(R.string.dialog_fingerprint_bluetooth_title))
                    .append("\n\n");
            appendFingerprint(message, item.getBluetoothLEFingerprint());
            if (item.getBluetoothLEFingerprint().getHistogram().isEmpty()) {
                message.append(App.getContext().getString(R.string.dialog_fingerprint_no_bluetooth_signals_found));
            }
        }

        return message.toString();
    }

    private void appendFingerprint(final StringBuilder appendTo, final Fingerprint fingerprint) {
        for (final String bssid : fingerprint.getHistogram().keySet()) {
            appendTo.append(bssid).append("\n");
            final Map<Integer, Float> levels = fingerprint.getHistogram().get(bssid);
            for (final Map.Entry<Integer, Float> level : levels.entrySet()) {
                appendTo.append("  ").append(level.getKey()).append(App.getContext()
                        .getString(R.string.dialog_fingerprint_dB_tag))
                        .append(level.getValue()).append("\n");
            }
        }
    }

}
