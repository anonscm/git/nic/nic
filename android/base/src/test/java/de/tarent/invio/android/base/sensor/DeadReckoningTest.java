package de.tarent.invio.android.base.sensor;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import de.tarent.invio.android.base.R;
import de.tarent.invio.android.base.task.DownloadMapDataTask;
import de.tarent.invio.android.base.task.DownloadTask;
import de.tarent.invio.android.base.task.OsmParserKeys;
import de.tarent.invio.entities.InvioGeoPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@Config(manifest = "../base/AndroidManifest.xml")
@RunWith(RobolectricTestRunner.class)
public class DeadReckoningTest {

    // The Object under Test:
    DeadReckoning deadReckoning;

    @Mock
    private Activity activity;

    @Mock
    private SensorManager sensorManager;

    @Mock
    private PackageManager packageManager;

    @Mock
    private TimerTask timerTask;

    @Mock
    private SensorBuffer compassBuffer;

    @Before
    public void setUp() {
        initMocks(this);

        when(activity.getSystemService(Activity.SENSOR_SERVICE)).thenReturn(sensorManager);
        when(activity.getPackageManager()).thenReturn(packageManager);
        when(activity.getResources()).thenReturn(Robolectric.application.getResources());

        deadReckoning = new DeadReckoning(activity);

        deadReckoning.onAccuracyChanged(null, 0);

    }

    @Test
    public void testDetectMovement() {
        deadReckoning.valuesLinearAcceleration[0] = 1.7f;
        deadReckoning.valuesLinearAcceleration[1] = 0.f;
        deadReckoning.valuesLinearAcceleration[2] = 0.f;
        deadReckoning.movementDetectionSensitivity = 2.25f;

        assertTrue(deadReckoning.detectMovement());
    }

    @Test
    public void testNoDetectMovement() {
        deadReckoning.valuesLinearAcceleration[0] = 0.3f;
        deadReckoning.valuesLinearAcceleration[1] = 1.2f;
        deadReckoning.valuesLinearAcceleration[2] = 0.8f;
        deadReckoning.movementDetectionSensitivity = 2.25f;

        assertFalse(deadReckoning.detectMovement());
    }

    @Test
    public void testInvalidBaseAngleData() {
        DownloadTask task = mock(DownloadMapDataTask.class);
        Map<String, Collection> data = new HashMap<String, Collection>();

        deadReckoning.onDownloadFinished(task, true, data);
        assertTrue(0 == deadReckoning.baseAngle);

        deadReckoning.onDownloadFinished(task, false, data);
        assertTrue(0 == deadReckoning.baseAngle);

        List<Integer> list = new ArrayList<Integer>();
        data.put(OsmParserKeys.NORTH_ANGLE, list);

        deadReckoning.onDownloadFinished(task, true, data);
        assertTrue(0 == deadReckoning.baseAngle);

        list.add(null);
        data.put(OsmParserKeys.NORTH_ANGLE, list);

        deadReckoning.onDownloadFinished(task, true, data);
        assertTrue(0 == deadReckoning.baseAngle);
    }

    @Test
    public void testCalculateBaseAngle() {
        DownloadTask task = mock(DownloadMapDataTask.class);
        Map<String, Collection> data = new HashMap<String, Collection>();
        List<Integer> list = new ArrayList<Integer>();
        list.add(90);
        data.put(OsmParserKeys.NORTH_ANGLE, list);

        deadReckoning.onDownloadFinished(task, true, data);

        assertEquals(Math.PI / 2, deadReckoning.baseAngle, 0.01f);
    }

    @Test
    public void testDeltaNoMovement() {
        deadReckoning.delta[0] = 10.f;
        deadReckoning.delta[1] = 12.f;
        deadReckoning.moving = false;
        double x = deadReckoning.delta[0];
        double y = deadReckoning.delta[1];
        deadReckoning.calculateDeltaLoop();
        assertTrue(x == deadReckoning.delta[0]);
        assertTrue(y == deadReckoning.delta[1]);
    }

    @Test
    public void testDeltaNoAzimuth() {
        deadReckoning.delta[0] = 10.f;
        deadReckoning.delta[1] = 12.f;
        deadReckoning.azimuth = null;
        double x = deadReckoning.delta[0];
        double y = deadReckoning.delta[1];
        deadReckoning.calculateDeltaLoop();
        assertTrue(x == deadReckoning.delta[0]);
        assertTrue(y == deadReckoning.delta[1]);
    }

    @Test
    public void testCalculationDelta() {
        deadReckoning.moving = true;
        deadReckoning.azimuth = 1.57f;
        deadReckoning.motionModel = 1.f;
        deadReckoning.timestep = 100;
        deadReckoning.baseAngle = 1.57f;

        deadReckoning.calculateDeltaLoop();

        assertEquals(0.f, deadReckoning.delta[0], 0.02f);
        assertEquals(1.f, deadReckoning.delta[1], 0.02f);
    }

    @Test
    public void testClearDelta() {
        deadReckoning.delta[0] = 10.f;
        deadReckoning.delta[1] = 12.f;
        deadReckoning.getDelta();
        assertTrue(0.f == deadReckoning.delta[0]);
        assertTrue(0.f == deadReckoning.delta[1]);
    }

    @Test
    public void testStopWithoutStart() {
        deadReckoning.stopSensors();
        deadReckoning.stopSensors();
    }

    @Test
    public void testDoubleStart() {
        deadReckoning.startSensors();
        deadReckoning.startSensors();
    }

    @Test
    public void testTimerCancelled() {
        deadReckoning.startSensors();
        deadReckoning.timerTask = timerTask;
        deadReckoning.stopSensors();

        verify(timerTask).cancel();
    }

    @Test
    public void testDisableCompassFilter() {
        deadReckoning.setCompassFilterEnabled(false);
        assertEquals(1f, ((CompassFilter) deadReckoning.compassBuffer.getFilter()).getWeight(), 0.01);
    }

    @Test
    public void testEnableCompassFilter() {
        deadReckoning.setCompassFilterEnabled(true);
        assertEquals(Robolectric.application.getResources().getInteger(R.integer.compass_low_pass_filter_weight) / 100.f,
                ((CompassFilter) deadReckoning.compassBuffer.getFilter()).getWeight(), 0.01);
    }

    // For this test it is assumed that the orientation of the map image matches the real world perfectly.
    @Test
    public void testCalculationDeltaNoBaseAngle() {
        deadReckoning.moving = true;
        deadReckoning.motionModel = 1.f;
        deadReckoning.timestep = 100;

        deadReckoning.baseAngle = (float) Math.toRadians(0f);

        deadReckoning.azimuth = (float) Math.toRadians(0f);
        deadReckoning.calculateDeltaLoop();
        InvioGeoPoint delta = deadReckoning.getDelta();
        assertEquals(0.f, delta.getX(), 0.02f);
        assertEquals(1000.f, delta.getY(), 0.02f);

        deadReckoning.azimuth = (float) Math.toRadians(45f);
        deadReckoning.calculateDeltaLoop();
        delta = deadReckoning.getDelta();
        assertEquals(1000.f / Math.sqrt(2), delta.getX(), 0.02f);
        assertEquals(1000.f / Math.sqrt(2), delta.getY(), 0.02f);

        deadReckoning.azimuth = (float) Math.toRadians(90f);
        deadReckoning.calculateDeltaLoop();
        delta = deadReckoning.getDelta();
        assertEquals(1000.f, delta.getX(), 0.02f);
        assertEquals(0.f, delta.getY(), 0.02f);

        deadReckoning.azimuth = (float) Math.toRadians(180f);
        deadReckoning.calculateDeltaLoop();
        delta = deadReckoning.getDelta();
        assertEquals(0.f, delta.getX(), 0.02f);
        assertEquals(-1000.f, delta.getY(), 0.02f);

        deadReckoning.azimuth = (float) Math.toRadians(270f);
        deadReckoning.calculateDeltaLoop();
        delta = deadReckoning.getDelta();
        assertEquals(-1000.f, delta.getX(), 0.02f);
        assertEquals(0.f, delta.getY(), 0.02f);
    }

    // 45 means that the map-image would have to be rotated 45° clockwise to match the real world.
    @Test
    public void testCalculationDeltaBaseAnglePlus45() {
        deadReckoning.moving = true;
        deadReckoning.motionModel = 1.f;
        deadReckoning.timestep = 100;

        deadReckoning.baseAngle = (float) Math.toRadians(45f);

        deadReckoning.azimuth = (float) Math.toRadians(0f);
        deadReckoning.calculateDeltaLoop();
        InvioGeoPoint delta = deadReckoning.getDelta();
        assertEquals(-1000.f / Math.sqrt(2), delta.getX(), 0.02f);
        assertEquals(1000.f / Math.sqrt(2), delta.getY(), 0.02f);

        deadReckoning.azimuth = (float) Math.toRadians(45f);
        deadReckoning.calculateDeltaLoop();
        delta = deadReckoning.getDelta();
        assertEquals(0.f, delta.getX(), 0.02f);
        assertEquals(1000.f, delta.getY(), 0.02f);

        deadReckoning.azimuth = (float) Math.toRadians(90f);
        deadReckoning.calculateDeltaLoop();
        delta = deadReckoning.getDelta();
        assertEquals(1000.f / Math.sqrt(2), delta.getX(), 0.02f);
        assertEquals(1000.f / Math.sqrt(2), delta.getY(), 0.02f);

        deadReckoning.azimuth = (float) Math.toRadians(180f);
        deadReckoning.calculateDeltaLoop();
        delta = deadReckoning.getDelta();
        assertEquals(1000.f / Math.sqrt(2), delta.getX(), 0.02f);
        assertEquals(-1000.f / Math.sqrt(2), delta.getY(), 0.02f);

        deadReckoning.azimuth = (float) Math.toRadians(270f);
        deadReckoning.calculateDeltaLoop();
        delta = deadReckoning.getDelta();
        assertEquals(-1000.f / Math.sqrt(2), delta.getX(), 0.02f);
        assertEquals(-1000.f / Math.sqrt(2), delta.getY(), 0.02f);
    }

    // -45 means that the map-image would have to be rotated 45° counterclockwise to match the real world.
    // This is similar to our tarent-office-maps which where measured to be at -40.
    @Test
    public void testCalculationDeltaBaseAngleMinus45() {
        deadReckoning.moving = true;
        deadReckoning.motionModel = 1.f;
        deadReckoning.timestep = 100;

        deadReckoning.baseAngle = (float) Math.toRadians(-45f);

        deadReckoning.azimuth = (float) Math.toRadians(0f);
        deadReckoning.calculateDeltaLoop();
        InvioGeoPoint delta = deadReckoning.getDelta();
        assertEquals(1000.f / Math.sqrt(2), delta.getX(), 0.02f);
        assertEquals(1000.f / Math.sqrt(2), delta.getY(), 0.02f);

        deadReckoning.azimuth = (float) Math.toRadians(45f);
        deadReckoning.calculateDeltaLoop();
        delta = deadReckoning.getDelta();
        assertEquals(1000.f, delta.getX(), 0.02f);
        assertEquals(0.f, delta.getY(), 0.02f);

        deadReckoning.azimuth = (float) Math.toRadians(90f);
        deadReckoning.calculateDeltaLoop();
        delta = deadReckoning.getDelta();
        assertEquals(1000.f / Math.sqrt(2), delta.getX(), 0.02f);
        assertEquals(-1000.f / Math.sqrt(2), delta.getY(), 0.02f);

        deadReckoning.azimuth = (float) Math.toRadians(180f);
        deadReckoning.calculateDeltaLoop();
        delta = deadReckoning.getDelta();
        assertEquals(-1000.f / Math.sqrt(2), delta.getX(), 0.02f);
        assertEquals(-1000.f / Math.sqrt(2), delta.getY(), 0.02f);

        deadReckoning.azimuth = (float) Math.toRadians(270f);
        deadReckoning.calculateDeltaLoop();
        delta = deadReckoning.getDelta();
        assertEquals(-1000.f / Math.sqrt(2), delta.getX(), 0.02f);
        assertEquals(1000.f / Math.sqrt(2), delta.getY(), 0.02f);
    }

    @Test
    public void testAddMultipleDeltasBeforeResetting() {
        deadReckoning.moving = true;
        deadReckoning.motionModel = 1.f;
        deadReckoning.timestep = 100;
        deadReckoning.baseAngle = 0f;

        assertEquals(0.f, deadReckoning.delta[0], 0.002f);
        assertEquals(0.f, deadReckoning.delta[1], 0.002f);

        deadReckoning.azimuth = (float) Math.toRadians(90f);
        deadReckoning.calculateDeltaLoop();
        assertEquals(1.f, deadReckoning.delta[0], 0.002f);
        assertEquals(0.f, deadReckoning.delta[1], 0.002f);

        deadReckoning.azimuth = (float) Math.toRadians(45f);
        deadReckoning.calculateDeltaLoop();
        assertEquals(1.707f, deadReckoning.delta[0], 0.002f);
        assertEquals(0.707f, deadReckoning.delta[1], 0.002f);


        InvioGeoPoint delta = deadReckoning.getDelta();
        assertEquals(1707.f, delta.getX(), 0.2f);
        assertEquals(707.f, delta.getY(), 0.2f);

        assertEquals(0.f, deadReckoning.delta[0], 0.002f);
        assertEquals(0.f, deadReckoning.delta[1], 0.002f);
    }

    @Test
    public void testThatCreatingNewDeadReckoningObjectThatHasGyroDoesNotRaiseMovementDirectionSensitivity() {
        final Activity activity = mock(Activity.class);
        final Resources resources = mock(Resources.class);

        when(activity.getSystemService(Activity.SENSOR_SERVICE)).thenReturn(sensorManager);
        when(activity.getPackageManager()).thenReturn(packageManager);
        when(activity.getResources()).thenReturn(resources);
        when(resources.getInteger(any(Integer.class))).thenReturn(0);
        when(packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE)).thenReturn(true);

        final DeadReckoning deadReckoning = new DeadReckoning(activity);

        assertEquals(0.0f, deadReckoning.movementDetectionSensitivity, 0.0f);
    }

    @Test
    public void testThatOnSensorChangedAccelerometerCallsFilterSensorDataWithCorrectValuesNoGyro()
            throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchFieldException {

        final Activity activity = mock(Activity.class);
        final Resources resources = mock(Resources.class);

        when(activity.getSystemService(Activity.SENSOR_SERVICE)).thenReturn(sensorManager);
        when(activity.getPackageManager()).thenReturn(packageManager);
        when(activity.getResources()).thenReturn(resources);
        when(resources.getInteger(any(Integer.class))).thenReturn(0);
        when(packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE)).thenReturn(false);

        final DeadReckoning deadReckoning = new DeadReckoning(activity);

        final SensorEvent event = setupSensorEvent();
        final Sensor sensor = mock(Sensor.class);
        event.sensor = sensor;
        when(sensor.getType()).thenReturn(Sensor.TYPE_ACCELEROMETER);

        final DeadReckoning deadReckoningSpy = spy(deadReckoning);

        when(deadReckoningSpy.calculateAzimuth()).thenReturn(0.0f);

        deadReckoningSpy.onSensorChanged(event);

        verify(deadReckoningSpy, times(1)).filterSensorData(event.values, deadReckoningSpy.accBuffer);
        verify(deadReckoningSpy, times(1)).filterSensorData(event.values, deadReckoningSpy.linearAccBuffer);
    }

    @Test
    public void testThatOnSensorChangedAccelerometerCallsFilterSensorDataWithCorrectValuesWithGyro()
            throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchFieldException {

        final Activity activity = mock(Activity.class);
        final Resources resources = mock(Resources.class);

        when(activity.getSystemService(Activity.SENSOR_SERVICE)).thenReturn(sensorManager);
        when(activity.getPackageManager()).thenReturn(packageManager);
        when(activity.getResources()).thenReturn(resources);
        when(resources.getInteger(any(Integer.class))).thenReturn(0);
        when(packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE)).thenReturn(true);

        final DeadReckoning deadReckoning = new DeadReckoning(activity);

        final SensorEvent event = setupSensorEvent();
        final Sensor sensor = mock(Sensor.class);
        event.sensor = sensor;
        when(sensor.getType()).thenReturn(Sensor.TYPE_ACCELEROMETER);

        final DeadReckoning deadReckoningSpy = spy(deadReckoning);

        when(deadReckoningSpy.calculateAzimuth()).thenReturn(0.0f);

        deadReckoningSpy.onSensorChanged(event);

        verify(deadReckoningSpy, times(1)).filterSensorData(event.values, deadReckoningSpy.accBuffer);
        verify(deadReckoningSpy, times(0)).filterSensorData(event.values, deadReckoningSpy.linearAccBuffer);
    }

    @Test
    public void testThatOnSensorChangedMagneticFieldCallsFilterSensorDataWithCorrectValues()
            throws InvocationTargetException, NoSuchMethodException, NoSuchFieldException, InstantiationException, IllegalAccessException {

        final SensorEvent event = setupSensorEvent();
        final Sensor sensor = mock(Sensor.class);
        event.sensor = sensor;
        when(sensor.getType()).thenReturn(Sensor.TYPE_MAGNETIC_FIELD);

        final DeadReckoning deadReckoningSpy = spy(deadReckoning);

        when(deadReckoningSpy.calculateAzimuth()).thenReturn(0.0f);

        deadReckoningSpy.onSensorChanged(event);

        verify(deadReckoningSpy, times(1)).filterSensorData(event.values, deadReckoningSpy.magnBuffer);
    }

    @Test
    public void testThatOnSensorChangedLinearAccelerationCallsFilterSensorDataWithCorrectValues()
            throws InvocationTargetException, NoSuchMethodException, NoSuchFieldException, InstantiationException, IllegalAccessException {

        final SensorEvent event = setupSensorEvent();
        final Sensor sensor = mock(Sensor.class);
        event.sensor = sensor;
        when(sensor.getType()).thenReturn(Sensor.TYPE_LINEAR_ACCELERATION);

        final DeadReckoning deadReckoningSpy = spy(deadReckoning);

        when(deadReckoningSpy.calculateAzimuth()).thenReturn(0.0f);

        deadReckoningSpy.onSensorChanged(event);

        verify(deadReckoningSpy, times(1)).filterSensorData(event.values, deadReckoningSpy.linearAccBuffer);
    }

    @Test
    public void testThatOnSensorChangedGravityDoesNotCallFilterSensorData()
            throws InvocationTargetException, NoSuchMethodException, NoSuchFieldException, InstantiationException, IllegalAccessException {

        final SensorEvent event = setupSensorEvent();
        final Sensor sensor = mock(Sensor.class);
        event.sensor = sensor;
        when(sensor.getType()).thenReturn(Sensor.TYPE_GRAVITY);

        final DeadReckoning deadReckoningSpy = spy(deadReckoning);

        when(deadReckoningSpy.calculateAzimuth()).thenReturn(0.0f);

        deadReckoningSpy.onSensorChanged(event);

        verify(deadReckoningSpy, times(0)).filterSensorData(any(float[].class), any(SensorBuffer.class));
    }

    private SensorEvent setupSensorEvent()
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {
        // Make the constructor in SensorEvent public so we can instantiate a new SensorEvent for our test.
        final Constructor<SensorEvent> c = SensorEvent.class.getDeclaredConstructor();
        c.setAccessible(true);

        // Create a new instance with the now public constructor.
        final SensorEvent event = c.newInstance();

        // Get the field for the values and set the value to what we want.
        final Field f = SensorEvent.class.getField("values");
        f.setAccessible(true);
        f.set(event, new float[]{0.0f, 0.0f, 0.0f});

        return event;
    }
}