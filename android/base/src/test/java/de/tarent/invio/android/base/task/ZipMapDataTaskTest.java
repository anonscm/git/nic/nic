package de.tarent.invio.android.base.task;

import de.tarent.invio.entities.Edge;
import de.tarent.invio.entities.InvioGeoPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
public class ZipMapDataTaskTest {

    @Mock
    DownloadListener listener;

    OsmParser parser;

    File unzippedMapDir;

    ZipMapDataTask toTest;

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);
        String mapPath = "/tmp/ZipMapDataTaskTest/someMap/";
        unzippedMapDir = new File(mapPath);
        File unzippedMapDataDir = new File(mapPath + "data/");
        if (!unzippedMapDataDir.exists()) {
            unzippedMapDataDir.mkdirs();
        }
        createOsmFile(mapPath);
        parser = new InvioOsmParser();
        toTest = new ZipMapDataTask(listener, unzippedMapDir, parser);
    }

    @Test
    public void testIfDoInBackgroundReturnsTheCorrectData() throws Exception {
        Map<String, Collection> result = toTest.doInBackground();

        Edge resultEdge = (Edge) ((ArrayList) result.get("edges")).get(0);
        InvioGeoPoint resultPointA = resultEdge.getPointA();
        InvioGeoPoint resultPointB = resultEdge.getPointB();

        HashMap resultNamespaceList = (HashMap) ((ArrayList) result.get("namespace")).get(0);

        assertTrue(result.get("northAngle").contains(60));
        assertTrue(resultPointA.getLatitudeE6() == 51255477);
        assertTrue(resultPointB.getLatitudeE6() == 51254981);
        assertTrue(resultPointA.getLongitudeE6() == 6755884);
        assertTrue(resultPointB.getLongitudeE6() == 6756741);
        assertTrue(resultPointA.getDivergence() == 0.0);
        assertTrue(resultPointB.getDivergence() == 0.0);
        assertTrue(result.get("multilevelOrder").contains(2));
        assertTrue(result.get("indoorScale").contains((float) 0.4));
        assertTrue(resultNamespaceList.get("namespace_group_name").equals("tarent AG"));
        assertTrue(resultNamespaceList.get("namespace_short_name").equals("1"));
        assertTrue(resultNamespaceList.get("namespace_map_name").equals("1. Obergeschoss"));
    }

    private void createOsmFile(String mapPath) throws IOException {
        String OsmPath = mapPath + "data/someMap.osm";
        File OsmFile = new File(OsmPath);
        if (OsmFile.exists()) {
            OsmFile.delete();
        }
        OsmFile.createNewFile();
        PrintWriter writer = new PrintWriter(OsmPath);
        writer.print("<?xml version='1.0' encoding='UTF-8'?>\n" +
                "<osm version='0.6' upload='true' generator='JOSM'>\n" +
                "    <node id='-4' action='modify' visible='true' lat='50.722137440298205' lon='7.061636158928462'>\n" +
                "        <tag k='indoor' v='north' />\n" +
                "        <tag k='namespace_group_name' v='tarent AG' />\n" +
                "        <tag k='namespace_map_name' v='1. Obergeschoss' />\n" +
                "        <tag k='namespace_short_name' v='1' />\n" +
                "        <tag k='indoor_scale' v='0.4' />\n" +
                "        <tag k='north_angle' v='60' />\n" +
                "        <tag k='multilevel_order' v='2' />\n" +
                "    </node>\n" +
                "  <node id='-794' action='modify' visible='true' lat='51.25567034941198' lon='6.757231408520558' />\n" +
                "  <node id='-792' action='modify' visible='true' lat='51.25498038781923' lon='6.756741436298513' />\n" +
                "  <node id='-791' action='modify' visible='true' lat='51.25547869445262' lon='6.755883984909925' />\n" +
                "  <node id='-10' action='modify' visible='true' lat='51.25417504040519' lon='6.748673467159792' />\n" +
                "  <node id='-8' action='modify' visible='true' lat='51.25400181445516' lon='6.748673467159792' />\n" +
                "  <node id='-6' action='modify' visible='true' lat='51.25400823024272' lon='6.748909240339734' />\n" +
                "  <node id='-4' action='modify' visible='true' lat='51.25418787193108' lon='6.748909240339734' />\n" +
                "  <way id='-793' action='modify' visible='true'>\n" +
                "    <nd ref='-791' />\n" +
                "    <nd ref='-792' />\n" +
                "    <nd ref='-794' />\n" +
                "    <nd ref='-791' />\n" +
                "    <tag k='indoor' v='Gang' />\n" +
                "  </way>\n" +
                "  <way id='-394' action='modify' visible='true'>\n" +
                "    <nd ref='-10' />\n" +
                "    <nd ref='-8' />\n" +
                "    <nd ref='-6' />\n" +
                "    <nd ref='-4' />\n" +
                "    <nd ref='-10' />\n" +
                "    <tag k='indoor' v='Gang' />\n" +
                "  </way>\n" +
                "</osm>\n");
        writer.close();

    }
}
