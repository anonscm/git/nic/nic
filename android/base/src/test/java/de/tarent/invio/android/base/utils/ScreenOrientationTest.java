package de.tarent.invio.android.base.utils;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.view.Display;
import android.view.WindowManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_SENSOR;
import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;
import static android.content.res.Configuration.ORIENTATION_PORTRAIT;
import static android.view.Surface.ROTATION_0;
import static android.view.Surface.ROTATION_180;
import static android.view.Surface.ROTATION_270;
import static android.view.Surface.ROTATION_90;
import static de.tarent.invio.android.base.utils.ScreenOrientation.getDeviceDefaultOrientation;
import static de.tarent.invio.android.base.utils.ScreenOrientation.lockScreenOrientation;
import static de.tarent.invio.android.base.utils.ScreenOrientation.unlockScreenOrientation;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Désirée Amling <d.amling@tarent.de>
 */
@RunWith(RobolectricTestRunner.class)
public class ScreenOrientationTest {

    @Test
    public void testAllDefaultScreenOrientationOptions() {
        final Activity landscape1 = activitySetup(ORIENTATION_LANDSCAPE, ROTATION_0);
        final Activity landscape2 = activitySetup(ORIENTATION_LANDSCAPE, ROTATION_180);
        final Activity landscape3 = activitySetup(ORIENTATION_PORTRAIT, ROTATION_90);
        final Activity landscape4 = activitySetup(ORIENTATION_PORTRAIT, ROTATION_270);

        assertEquals(ORIENTATION_LANDSCAPE, getDeviceDefaultOrientation(landscape1));
        assertEquals(ORIENTATION_LANDSCAPE, getDeviceDefaultOrientation(landscape2));
        assertEquals(ORIENTATION_LANDSCAPE, getDeviceDefaultOrientation(landscape3));
        assertEquals(ORIENTATION_LANDSCAPE, getDeviceDefaultOrientation(landscape4));

        final Activity portrait1 = activitySetup(ORIENTATION_LANDSCAPE, ROTATION_90);
        final Activity portrait2 = activitySetup(ORIENTATION_LANDSCAPE, ROTATION_270);
        final Activity portrait3 = activitySetup(ORIENTATION_PORTRAIT, ROTATION_0);
        final Activity portrait4 = activitySetup(ORIENTATION_PORTRAIT, ROTATION_180);

        assertEquals(ORIENTATION_PORTRAIT, getDeviceDefaultOrientation(portrait1));
        assertEquals(ORIENTATION_PORTRAIT, getDeviceDefaultOrientation(portrait2));
        assertEquals(ORIENTATION_PORTRAIT, getDeviceDefaultOrientation(portrait3));
        assertEquals(ORIENTATION_PORTRAIT, getDeviceDefaultOrientation(portrait4));
    }

    @Test
    public void testLockScreenRotationPortrait0Degrees() {
        final Activity activity = activitySetup(ORIENTATION_PORTRAIT, ROTATION_0);

        lockScreenOrientation(activity);

        verify(activity, times(1)).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Test
    public void testLockScreenRotationPortrait90Degrees() {
        final Activity activity = activitySetup(ORIENTATION_PORTRAIT, ROTATION_90);

        lockScreenOrientation(activity);

        verify(activity, times(1)).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
    }

    @Test
    public void testLockScreenRotationPortrait180Degrees() {
        final Activity activity = activitySetup(ORIENTATION_PORTRAIT, ROTATION_180);

        lockScreenOrientation(activity);

        verify(activity, times(1)).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
    }

    @Test
    public void testLockScreenRotationPortrait270Degrees() {
        final Activity activity = activitySetup(ORIENTATION_PORTRAIT, ROTATION_270);

        lockScreenOrientation(activity);

        verify(activity, times(1)).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Test
    public void testLockScreenRotationLandscape0Degrees() {
        final Activity activity = activitySetup(ORIENTATION_LANDSCAPE, ROTATION_0);

        lockScreenOrientation(activity);

        verify(activity, times(1)).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Test
    public void testLockScreenRotationLandscape90Degrees() {
        final Activity activity = activitySetup(ORIENTATION_LANDSCAPE, ROTATION_90);

        lockScreenOrientation(activity);

        verify(activity, times(1)).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Test
    public void testLockScreenRotationLandscape180Degrees() {
        final Activity activity = activitySetup(ORIENTATION_LANDSCAPE, ROTATION_180);

        lockScreenOrientation(activity);

        verify(activity, times(1)).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
    }

    @Test
    public void testLockScreenRotationLandscape270Degrees() {
        final Activity activity = activitySetup(ORIENTATION_LANDSCAPE, ROTATION_270);

        lockScreenOrientation(activity);

        verify(activity, times(1)).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
    }

    @Test
    public void testUnlockScreenOrientation() {
        final Activity activity = activitySetup(ORIENTATION_PORTRAIT, ROTATION_0);

        unlockScreenOrientation(activity);

        verify(activity, times(1)).setRequestedOrientation(SCREEN_ORIENTATION_SENSOR);
    }

    private Activity activitySetup(final int orientation, final int rotation) {
        final Activity activity = mock(Activity.class);

        final WindowManager windowManager = mock(WindowManager.class);
        final Resources resources = mock(Resources.class);
        final Configuration configuration = new Configuration();
        final Display display = mock(Display.class);
        configuration.orientation = orientation;

        when(activity.getSystemService(Activity.WINDOW_SERVICE)).thenReturn(windowManager);
        when(activity.getResources()).thenReturn(resources);
        when(resources.getConfiguration()).thenReturn(configuration);
        when(windowManager.getDefaultDisplay()).thenReturn(display);
        when(display.getRotation()).thenReturn(rotation);

        return activity;
    }
}
