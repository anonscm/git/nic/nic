package de.tarent.invio.android.base.position;

import de.tarent.invio.entities.InvioGeoPoint;
import org.junit.Test;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.util.GeoPoint;

import static de.tarent.invio.android.base.position.InvioGeoPointImpl.LATITUDE_LINE_DISTANCE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @author Désirée Amling <d.amling@tarent.de>
 * @author Dino Omanovic <d.omanovic@tarent.de>
 */
public class InvioGeoPointImplTest {

    /**
     * Delta is the floatingpoint tolerance for Latitude/Longitude value differences.
     */
    private final double delta = 0.01;


    @Test
    public void testThatInvioGeoPointImplHoldsCorrectValuesWhenGivenAnOldGeoPoint() {
        int longitude = 12345;
        int latitude = 54321;
        IGeoPoint geoPoint = new GeoPoint(latitude, longitude);

        InvioGeoPoint invioGeoPoint = new InvioGeoPointImpl(geoPoint);

        assertEquals(longitude, invioGeoPoint.getLongitudeE6());
        assertEquals(latitude, invioGeoPoint.getLatitudeE6());
    }

    /**
     * 3 different degree tests on the {@link InvioGeoPointImpl#getX()} method. (0, 1, 15)
     */
    @Test
    public void testGetXWhenLatitudeAndLongitudeAre0Degrees() {
        InvioGeoPointImpl invioGeoPoint = new InvioGeoPointImpl(0, 0);

        final double expectedResult = 0;

        assertEquals(expectedResult, invioGeoPoint.getX(), 0);
    }

    @Test
    public void testGetXWhenLatitudeAndLongitudeAre1Degree() {
        InvioGeoPoint invioGeoPointDecimal = new InvioGeoPointImpl(1.0, 1.0);
        InvioGeoPoint invioGeoPointE6 = new InvioGeoPointImpl(1000000, 1000000);

        final double expectedResult = 111302.616;

        assertEquals(expectedResult, invioGeoPointDecimal.getX(), delta);
        assertEquals(expectedResult, invioGeoPointE6.getX(), delta);
    }

    @Test
    public void testGetXWhenLatitudeAndLongitudeAre15Degrees() {
        InvioGeoPoint invioGeoPointDecimal = new InvioGeoPointImpl(15.0, 15.0);
        InvioGeoPoint invioGeoPointE6 = new InvioGeoPointImpl(15000000, 15000000);

        final double expectedResult = 107550.455 * 15;

        assertEquals(expectedResult, invioGeoPointDecimal.getX(), delta);
        assertEquals(expectedResult, invioGeoPointE6.getX(), delta);
    }

    /**
     * 3 different degree tests on the {@link InvioGeoPointImpl#getY()} method. (0, 1, 15)
     */
    @Test
    public void testGetYWhenLatitudeIs0Degrees() throws NoSuchFieldException {
        InvioGeoPoint invioGeoPoint = new InvioGeoPointImpl(0, 0);

        final double expectedResult = LATITUDE_LINE_DISTANCE * 0;

        assertEquals(expectedResult, invioGeoPoint.getY(), 0);
    }

    @Test
    public void testGetYWhenLatitudeIs1Degree() throws NoSuchFieldException {
        InvioGeoPoint invioGeoPointDecimal = new InvioGeoPointImpl(1.0, 1.0);
        InvioGeoPoint invioGeoPointE6 = new InvioGeoPointImpl(1000000, 1000000);

        final double expectedResult = LATITUDE_LINE_DISTANCE * 1;

        assertEquals(expectedResult, invioGeoPointDecimal.getY(), delta);
        assertEquals(expectedResult, invioGeoPointE6.getY(), delta);
    }

    @Test
    public void testGetYWhenLatitudeIs15Degrees() {
        InvioGeoPoint invioGeoPointDecimal = new InvioGeoPointImpl(15.0, 15.0);
        InvioGeoPoint invioGeoPointE6 = new InvioGeoPointImpl(15000000, 15000000);

        final double expectedResult = LATITUDE_LINE_DISTANCE * 15;

        assertEquals(expectedResult, invioGeoPointDecimal.getY(), delta);
        assertEquals(expectedResult, invioGeoPointE6.getY(), delta);
    }

    /**
     * Tests both {@link InvioGeoPointImpl#getX()} and {@link InvioGeoPointImpl#getY()} methods with the exact Bonn
     * coordinates.
     */
    @Test
    public void testGetXAndGetYWhenLatitudeAndLongitudeEqualBonnWorldCoordinates() {
        final double bonnLatitude = 50.733992;
        final double bonnLongitude = 7.099814;

        InvioGeoPoint invioGeoPoint = new InvioGeoPointImpl(bonnLatitude, bonnLongitude);

        final double expectedY = LATITUDE_LINE_DISTANCE * bonnLatitude;
        final double expectedX = 70598.276 * bonnLongitude;

        assertEquals(expectedY, invioGeoPoint.getY(), delta);
        assertEquals(expectedX, invioGeoPoint.getX(), delta);
    }

    /**
     * Sets the {@link InvioGeoPoint} location to the Bonn coordinates and then parses them from X and Y back
     * into Long Lat and then asserts them.
     */
    @Test
    public void testSetXYWithBonnCoordinates() {
        final double bonnLatitude = 50.733992;
        final double bonnLongitude = 7.099814;

        final InvioGeoPoint invioGeoPoint = new InvioGeoPointImpl(bonnLatitude, bonnLongitude);

        invioGeoPoint.setXY(invioGeoPoint.getX(), invioGeoPoint.getY());

        final int expectedLatitude = (int) (bonnLatitude * 1E6);
        final int expectedLongitude = (int) (bonnLongitude * 1E6);

        assertEquals(expectedLatitude, invioGeoPoint.getLatitudeE6());
        assertEquals(expectedLongitude, invioGeoPoint.getLongitudeE6());
    }

    @Test
    public void testSetXYWithChangingValues() {
        final InvioGeoPoint invioGeoPoint = new InvioGeoPointImpl(0, 0);

        invioGeoPoint.setXY(1234000, 4321000);

        final int expectedLatitude = 38816025;
        final int expectedLongitude = 14208363;

        assertEquals(expectedLatitude, invioGeoPoint.getLatitudeE6());
        assertEquals(expectedLongitude, invioGeoPoint.getLongitudeE6());
    }

    @Test
    public void testDistanceCalculationWithNormalValues() {

        final InvioGeoPoint pointA = new InvioGeoPointImpl();
        final InvioGeoPoint pointB = new InvioGeoPointImpl();

        pointA.setXY(10, 6);
        pointB.setXY(7, 10);

        double expected = 5.0;

        double result = pointA.calculateDistanceTo(pointB);

        assertEquals(expected, result, delta);
    }

    @Test
    public void testDistanceCalculationWithZeros() {

        final InvioGeoPoint pointA = new InvioGeoPointImpl();
        final InvioGeoPoint pointB = new InvioGeoPointImpl();

        pointA.setXY(0, 0);
        pointB.setXY(0, 0);

        double expected = 0.0;

        double result = pointA.calculateDistanceTo(pointB);

        assertEquals(expected, result, delta);
    }

    @Test
    public void testEquals() {
        final InvioGeoPoint pointA = new InvioGeoPointImpl();
        pointA.setXY(100, 200);

        assertTrue(pointA.equals(pointA));
        assertFalse(pointA.equals(null));
        assertFalse(pointA.equals("hallo!?"));

        final InvioGeoPoint pointB = new InvioGeoPointImpl();

        // Identical Points
        pointB.setXY(100, 200);
        assertTrue(pointA.equals(pointB));
        assertTrue(pointB.equals(pointA));

        // X is different
        pointB.setXY(101, 200);
        assertFalse(pointA.equals(pointB));
        assertFalse(pointB.equals(pointA));

        // Y is different
        pointB.setXY(100, 201);
        assertFalse(pointA.equals(pointB));
        assertFalse(pointB.equals(pointA));

        // Divergence is different.
        pointB.setXY(100, 200);
        pointB.setDivergence(1);
        assertFalse(pointA.equals(pointB));
        assertFalse(pointB.equals(pointA));

        // Identical Points
        pointA.setDivergence(1);
        assertTrue(pointA.equals(pointB));
        assertTrue(pointB.equals(pointA));
    }

    @Test
    public void testToString() {
        final InvioGeoPoint invioGeoPoint = new InvioGeoPointImpl();
        invioGeoPoint.setXY(1234000, 4321000);
        invioGeoPoint.setDivergence(3.14159);

        String description = invioGeoPoint.toString();
        // We just look for the expected string-fragments, to see if the string-representation contains all that we
        // want.
        assertTrue(description.contains("123"));
        assertTrue(description.contains("432"));
        assertTrue(description.contains("38816025"));
        assertTrue(description.contains("14208363"));
        assertTrue(description.contains("3.14159"));
    }

    @Test
    public void testCompare() {
        final InvioGeoPoint pointA = new InvioGeoPointImpl(1, 1);
        final InvioGeoPoint pointB = new InvioGeoPointImpl(0, 0);

        // Note: if the divergence is the same then the result is dependent on the coordinates - but should really
        //       be treated as undefined. We don't care which one comes first in this case! But just don't want the
        //       result to be 0, because that would be inconsistent with equals().
        assertEquals(1, pointA.compareTo(pointB));

        pointA.setDivergence(42);
        pointB.setDivergence(42);
        assertEquals(1, pointA.compareTo(pointB));
        assertEquals(-1, pointB.compareTo(pointA));

        pointA.setDivergence(41);
        assertEquals(-1, pointA.compareTo(pointB));
        assertEquals(1, pointB.compareTo(pointA));

        assertEquals(0, pointA.compareTo(pointA));
    }


}
