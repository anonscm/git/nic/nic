package de.tarent.invio.android.base;

import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.ViewGroup;
import de.tarent.invio.android.base.sensor.DeadReckoning;
import de.tarent.invio.android.base.wifi.FingerprintManager;
import de.tarent.invio.android.base.wifi.UserLocator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.osmdroid.tileprovider.MapTileProviderBase;
import org.osmdroid.views.MapView;

import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;


@Config(manifest = "../base/AndroidManifest.xml")
@RunWith(RobolectricTestRunner.class)
public class AbstractMapActivityTest {

    private TestAbstractMapActivity activity;

    @Before
    public void setup() {
        // We have to make sure that Robolectric knows that it's awaiting an HTTPResponse.
        Robolectric.addPendingHttpResponse(200, "OK");

        // We have to tell Robolectric to not run any background tasks (AsyncTasks) because we do not have an actual
        // activity.
        Robolectric.getBackgroundScheduler().pause();

        // The activity is created with Robolectric.
        activity = Robolectric.buildActivity(TestAbstractMapActivity.class).create().get();

        activity.askedToEnableWifi = false;
    }

    @Test
    public void testThatGetDeadReckoningNeverReturnsNullAndIsAlwaysSameInstance() {
        final DeadReckoning firstCall = TestAbstractMapActivity.getDeadReckoning(activity);
        assertNotNull(firstCall);

        final DeadReckoning secondCall = TestAbstractMapActivity.getDeadReckoning(activity);
        assertNotNull(secondCall);

        assertSame(firstCall, secondCall);

        final DeadReckoning fromInstance = activity.getDeadReckoning(activity);
        assertSame(firstCall, fromInstance);
        assertSame(secondCall, fromInstance);
    }

    @Test
    public void testThatStartScanStartsUserLocatorTracking() {
        final UserLocator userLocator = mock(UserLocator.class);
        doNothing().when(userLocator).startTracking();
        activity.userLocator = userLocator;

        // We want to make sure that the activity wasn't scanning first.
        activity.scanning = false;

        final boolean wasNotYetScanning = activity.startScan();
        final boolean wasAlreadyScanning = activity.startScan();

        assertFalse(wasNotYetScanning);
        assertTrue(wasAlreadyScanning);
        verify(userLocator, times(2)).startTracking();
        verifyNoMoreInteractions(userLocator);
    }

    @Test
    public void testThatStopScanStopsUserLocatorTracking() {
        final UserLocator userLocator = mock(UserLocator.class);
        doNothing().when(userLocator).startTracking();
        activity.userLocator = userLocator;

        // We want to make sure that the activity was scanning first.
        activity.scanning = true;

        final boolean wasScanning = activity.stopScan();
        final boolean hadAlreadyStoppedScanning = activity.stopScan();

        assertTrue(wasScanning);
        assertFalse(hadAlreadyStoppedScanning);
        verify(userLocator, times(2)).stopTracking();
        verifyNoMoreInteractions(userLocator);
    }

    @Test
    public void testThatOnStopDetachesNotNullFingerPrintManagerAndTileProvider() {
        final MapTileProviderBase tileProvider = mock(MapTileProviderBase.class);
        activity.tileProvider = tileProvider;

        final FingerprintManager fingerprintManager = mock(FingerprintManager.class);
        activity.fingerprintManager = fingerprintManager;

        doNothing().when(tileProvider).detach();
        doNothing().when(fingerprintManager).detach();

        activity.onStop();
        verify(tileProvider, times(1)).detach();
        verify(fingerprintManager, times(1)).detach();
        verifyNoMoreInteractions(tileProvider);
        verifyNoMoreInteractions(fingerprintManager);
    }

    @Test
    public void testThatOnStopDoesNotTryToDetachNullFingerprintManagerAndTileProvider() {
        // make sure they're both null for this test.
        activity.tileProvider = null;
        activity.fingerprintManager = null;

        activity.onStop();
        // TODO: What can we actually assert or verify here? For now it just doesn't throw a nullpointer.
    }

    @Test
    public void testThatResetParticleFilterMapChangeOnlyResetsParticleFilterIfUserLocatorIsNotNull() {
        final UserLocator userLocator = mock(UserLocator.class);
        doNothing().when(userLocator).resetParticleFilter();
        activity.userLocator = userLocator;

        activity.resetParticleFilterMapchange();

        activity.userLocator = null;
        activity.resetParticleFilterMapchange();

        verify(userLocator, times(1)).resetParticleFilter();
        verifyNoMoreInteractions(userLocator);
    }

    @Test
    public void testOnCreateMultilevelMap() {
        final int mapViewLayoutId = R.string.default_map_name_not_found;
        final int osmMapViewId = R.string.default_map_name_not_found;
        final Bundle bundle = mock(Bundle.class);

        final AbstractMapActivity activitySpy = spy(activity);
        final MapView mapView = mock(MapView.class);
        activitySpy.mapView = mapView;
        activitySpy.multiLevelMap = true;

        doNothing().when(activitySpy).handleWifiStatus();
        final ViewGroup viewGroup = mock(ViewGroup.class);
        doReturn(viewGroup).when(activitySpy).getMapLayout();
        doReturn(mapView).when(activitySpy).configureMapView();
        doReturn("nothing").when(activitySpy).findMapName();

        activitySpy.onCreate(bundle, mapViewLayoutId, osmMapViewId);

        assertEquals(mapViewLayoutId, activitySpy.getMapViewLayoutId());
        assertEquals(osmMapViewId, activitySpy.getOsmMapViewId());
        verify(activitySpy, times(1)).onBaseCreate(bundle);
        verify(activitySpy, times(1)).createMultiLevelMap(bundle);
        verify(activitySpy, never()).createOrRecoverFingerprintManager(bundle);
        assertNotNull(activitySpy.userLocator);
        assertNotNull(activitySpy.mapServerClient);
    }

    @Test
    public void testOnCreateSingleMap() {
        final int mapViewLayoutId = R.string.default_map_name_not_found;
        final int osmMapViewId = R.string.default_map_name_not_found;
        final Bundle bundle = mock(Bundle.class);

        final AbstractMapActivity activitySpy = spy(activity);
        final MapView mapView = mock(MapView.class);
        activitySpy.mapView = mapView;
        activitySpy.multiLevelMap = true;

        doNothing().when(activitySpy).handleWifiStatus();
        final ViewGroup viewGroup = mock(ViewGroup.class);
        doReturn(viewGroup).when(activitySpy).getMapLayout();
        doReturn(mapView).when(activitySpy).configureMapView();

        activitySpy.onCreate(bundle, mapViewLayoutId, osmMapViewId);

        assertEquals(mapViewLayoutId, activitySpy.getMapViewLayoutId());
        assertEquals(osmMapViewId, activitySpy.getOsmMapViewId());
        verify(activitySpy, times(1)).onBaseCreate(bundle);
        verify(activitySpy, never()).createMultiLevelMap(bundle);
        verify(activitySpy, times(1)).createOrRecoverFingerprintManager(bundle);
        assertNotNull(activitySpy.userLocator);
        assertNotNull(activitySpy.mapServerClient);
    }

    @Test
    public void testHandleWifiStatusOn() {
        final AbstractMapActivity activitySpy = spy(activity);
        final Context context = mock(Context.class);
        final WifiManager wifiManager = mock(WifiManager.class);
        doReturn(context).when(activitySpy).getApplicationContext();
        doReturn(wifiManager).when(context).getSystemService(Context.WIFI_SERVICE);
        doReturn(WifiManager.WIFI_STATE_ENABLED).when(wifiManager).getWifiState();

        activitySpy.handleWifiStatus();

        verify(activitySpy, never()).showWifiNotEnabledDialog();
    }

    @Test
    public void testHandleWifiStatusDisabled() {
        final AbstractMapActivity activitySpy = spy(activity);
        final Context context = mock(Context.class);
        final WifiManager wifiManager = mock(WifiManager.class);
        doReturn(context).when(activitySpy).getApplicationContext();
        doReturn(wifiManager).when(context).getSystemService(Context.WIFI_SERVICE);
        doReturn(WifiManager.WIFI_STATE_DISABLED).when(wifiManager).getWifiState();
        doNothing().when(activitySpy).showWifiNotEnabledDialog();

        activitySpy.handleWifiStatus();

        verify(activitySpy, times(1)).showWifiNotEnabledDialog();
    }

    @Test
    public void testHandleWifiStatusDisabling() {
        final AbstractMapActivity activitySpy = spy(activity);
        final Context context = mock(Context.class);
        final WifiManager wifiManager = mock(WifiManager.class);
        doReturn(context).when(activitySpy).getApplicationContext();
        doReturn(wifiManager).when(context).getSystemService(Context.WIFI_SERVICE);
        doReturn(WifiManager.WIFI_STATE_DISABLING).when(wifiManager).getWifiState();
        doNothing().when(activitySpy).showWifiNotEnabledDialog();

        activitySpy.handleWifiStatus();

        verify(activitySpy, times(1)).showWifiNotEnabledDialog();
    }

    @Test
    public void testHandleWifiStatusUnknown() {
        final AbstractMapActivity activitySpy = spy(activity);
        final Context context = mock(Context.class);
        final WifiManager wifiManager = mock(WifiManager.class);
        doReturn(context).when(activitySpy).getApplicationContext();
        doReturn(wifiManager).when(context).getSystemService(Context.WIFI_SERVICE);
        doReturn(WifiManager.WIFI_STATE_UNKNOWN).when(wifiManager).getWifiState();
        doNothing().when(activitySpy).showWifiNotEnabledDialog();

        activitySpy.handleWifiStatus();

        verify(activitySpy, times(1)).showWifiNotEnabledDialog();
    }

    @Test
    public void testThatShowProgressDialogCreatesAndShowsNewDialogWhenNullAndShowsDialogWhenCalledAgain() {
        //First run to see if a progressDialog is created when null.
        activity.progressDialog = null;
        activity.showProgressDialog();
        assertNotNull(activity.progressDialog);

        // Set the progressDialog to our spy so we can watch it and run again to see what happens and make sure the
        // dialog was not just recreated, but the existing one used.
        final ProgressDialog progressDialog = spy(activity.progressDialog);
        activity.progressDialog = progressDialog;
        activity.showProgressDialog();
        verify(progressDialog, times(1)).show();
        // Dialog should not be recreated
        verify(progressDialog, never()).setCancelable(any(Boolean.class));
        verify(progressDialog, never()).setMessage(any(String.class));
        verify(progressDialog, never()).setProgressStyle(any(Integer.class));
        verify(progressDialog, never()).setCanceledOnTouchOutside(any(Boolean.class));
    }

    @Test
    public void testDismissProgressDialog() {
        activity.progressDialog = null;
        activity.dismissProgressDialog(); // This should not cause any errors because it is null.

        final ProgressDialog progressDialogNotShowing = mock(ProgressDialog.class);
        doReturn(false).when(progressDialogNotShowing).isShowing();
        activity.progressDialog = progressDialogNotShowing;
        activity.dismissProgressDialog();
        verify(progressDialogNotShowing, never()).cancel();

        final ProgressDialog progressDialogShowing = mock(ProgressDialog.class);
        doReturn(true).when(progressDialogShowing).isShowing();
        activity.progressDialog = progressDialogShowing;
        activity.dismissProgressDialog();
        verify(progressDialogShowing, times(1)).cancel();
    }
}