package de.tarent.invio.android.base.wifi;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import de.tarent.invio.android.base.position.InvioGeoPointImpl;
import de.tarent.invio.entities.Fingerprint;
import de.tarent.invio.entities.Histogram;
import de.tarent.invio.mapserver.MapServerClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.osmdroid.views.MapView;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Desiree Amling <d.amling@tarent.de>
 */
@Config(manifest = "../base/AndroidManifest.xml")
@RunWith(RobolectricTestRunner.class)
public class FingerprintManagerTest {

    private FingerprintManager fingerprintManager;
    private FingerprintManager fingerprintManagerNoDownload;

    @Mock
    private Activity activity;

    @Mock
    private MapServerClient mapServerClient;

    @Mock
    private MapView mapView;

    @Mock
    private PackageManager packageManager;

    private String mapName = "testMap";

    @Before
    public void setup() {
        Robolectric.getBackgroundScheduler().pause();

        initMocks(this);
        when(activity.getResources()).thenReturn(Robolectric.application.getResources());
        when(activity.getPackageManager()).thenReturn(packageManager);
        when(packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)).thenReturn(false);

        fingerprintManager = new FingerprintManager(activity, mapServerClient, mapView, mapName);
        fingerprintManagerNoDownload = new FingerprintManager(activity, mapServerClient, mapView, mapName, false, true);
    }

    @Test
    public void testRemoveFingerprintFromOverlay() {
        final FingerprintItem fingerprintItem = mock(FingerprintItem.class);
        fingerprintManager.getOverlay().addItem(fingerprintItem);
        fingerprintManager.overlay = spy(fingerprintManager.getOverlay());

        fingerprintManager.removeFingerprintFromOverlay(fingerprintItem);

        verify(fingerprintManager.getOverlay(), times(1)).removeItem(fingerprintItem);
        verify(fingerprintManager.mapView, times(1)).postInvalidate();
    }

    @Test
    public void testRemoveAllFingerprintFromOverlay() {
        fingerprintManager.overlay = spy(fingerprintManager.getOverlay());

        fingerprintManager.removeAllFingerprintFromOverlay();

        verify(fingerprintManager.getOverlay(), times(1)).removeAllItems();
        verify(fingerprintManager.mapView, times(1)).postInvalidate();
    }

    @Test
    public void testThatGetFingerprintsReturnsEmptyListWhenOverlayIsNull() {
        fingerprintManager.overlay = null;

        final List<Fingerprint> fingerprints = fingerprintManager.getWifiFingerprints();

        assertTrue(fingerprints.isEmpty());
    }

    @Test
    public void testThatGetWifiFingerprintsReturnsAllFingerprintsWhenOverlayIsNotNull() {
        // Fill the overlay with some FingerprintItems.
        Fingerprint fingerprint1 = new Fingerprint(new Histogram("1"), new InvioGeoPointImpl(1, 1));
        Fingerprint fingerprint2 = new Fingerprint(new Histogram("2"), new InvioGeoPointImpl(2, 2));
        Fingerprint fingerprint3 = new Fingerprint(new Histogram("3"), new InvioGeoPointImpl(3, 3));

        FingerprintItem item = new FingerprintItem();
        item.setWifiFingerprint(fingerprint1);
        fingerprintManager.getOverlay().addItem(item);

        item = new FingerprintItem();
        item.setWifiFingerprint(fingerprint2);
        fingerprintManager.getOverlay().addItem(item);

        item = new FingerprintItem();
        item.setWifiFingerprint(fingerprint3);
        fingerprintManager.getOverlay().addItem(item);

        final List<Fingerprint> fingerprints = fingerprintManager.getWifiFingerprints();

        assertTrue(fingerprints.contains(fingerprint1));
        assertTrue(fingerprints.contains(fingerprint2));
        assertTrue(fingerprints.contains(fingerprint3));
    }

    @Test
    public void testThatGetBluetoothLEFingerprintsReturnsAllFingerprintsWhenOverlayIsNotNull() {
        // Fill the overlay with some FingerprintItems.
        Fingerprint fingerprint1 = new Fingerprint(new Histogram("1"), new InvioGeoPointImpl(1, 1));
        Fingerprint fingerprint2 = new Fingerprint(new Histogram("2"), new InvioGeoPointImpl(2, 2));
        Fingerprint fingerprint3 = new Fingerprint(new Histogram("3"), new InvioGeoPointImpl(3, 3));

        FingerprintItem item = new FingerprintItem();
        item.setBluetoothLEFingerprint(fingerprint1);
        fingerprintManager.getOverlay().addItem(item);

        item = new FingerprintItem();
        item.setBluetoothLEFingerprint(fingerprint2);
        fingerprintManager.getOverlay().addItem(item);

        item = new FingerprintItem();
        item.setBluetoothLEFingerprint(fingerprint3);
        fingerprintManager.getOverlay().addItem(item);

        final List<Fingerprint> fingerprints = fingerprintManager.getBluetoothLEFingerprints();

        assertTrue(fingerprints.contains(fingerprint1));
        assertTrue(fingerprints.contains(fingerprint2));
        assertTrue(fingerprints.contains(fingerprint3));
    }

    @Test
    public void testThatGetWifiFingerprintsJsonReturnsCorrectJson() {
        // Fill the overlay with some FingerprintItems.
        Fingerprint fingerprint1 = new Fingerprint(new Histogram("1"), new InvioGeoPointImpl(1, 1));
        Fingerprint fingerprint2 = new Fingerprint(new Histogram("2"), new InvioGeoPointImpl(2, 2));
        Fingerprint fingerprint3 = new Fingerprint(new Histogram("3"), new InvioGeoPointImpl(3, 3));

        FingerprintItem item = new FingerprintItem();
        item.setWifiFingerprint(fingerprint1);
        fingerprintManager.getOverlay().addItem(item);

        item = new FingerprintItem();
        item.setWifiFingerprint(fingerprint2);
        fingerprintManager.getOverlay().addItem(item);

        item = new FingerprintItem();
        item.setWifiFingerprint(fingerprint3);
        fingerprintManager.getOverlay().addItem(item);

        final String expectedJsonString = "[{\"histogram\":{},\"point\":{\"divergence\":0.0,\"mLongitudeE6\":1,\"" +
                "mLatitudeE6\":1,\"mAltitude\":0},\"id\":\"1\"},{\"histogram\":{},\"point\":{\"divergence\":0.0,\"" +
                "mLongitudeE6\":2,\"mLatitudeE6\":2,\"mAltitude\":0},\"id\":\"2\"},{\"histogram\":{},\"point\":{\"" +
                "divergence\":0.0,\"mLongitudeE6\":3,\"mLatitudeE6\":3,\"mAltitude\":0},\"id\":\"3\"}]";

        final String actualJsonString = fingerprintManager.getWifiFingerprintsJson();

        assertEquals(expectedJsonString, actualJsonString);
    }

    @Test
    public void testThatGetBluetoothLEFingerprintsJsonReturnsCorrectJson() {
        // Fill the overlay with some FingerprintItems.
        Fingerprint fingerprint1 = new Fingerprint(new Histogram("1"), new InvioGeoPointImpl(1, 1));
        Fingerprint fingerprint2 = new Fingerprint(new Histogram("2"), new InvioGeoPointImpl(2, 2));
        Fingerprint fingerprint3 = new Fingerprint(new Histogram("3"), new InvioGeoPointImpl(3, 3));

        FingerprintItem item = new FingerprintItem();
        item.setBluetoothLEFingerprint(fingerprint1);
        fingerprintManager.getOverlay().addItem(item);

        item = new FingerprintItem();
        item.setBluetoothLEFingerprint(fingerprint2);
        fingerprintManager.getOverlay().addItem(item);

        item = new FingerprintItem();
        item.setBluetoothLEFingerprint(fingerprint3);
        fingerprintManager.getOverlay().addItem(item);

        final String expectedJsonString = "[{\"histogram\":{},\"point\":{\"divergence\":0.0,\"mLongitudeE6\":1,\"" +
                "mLatitudeE6\":1,\"mAltitude\":0},\"id\":\"1\"},{\"histogram\":{},\"point\":{\"divergence\":0.0,\"" +
                "mLongitudeE6\":2,\"mLatitudeE6\":2,\"mAltitude\":0},\"id\":\"2\"},{\"histogram\":{},\"point\":{\"" +
                "divergence\":0.0,\"mLongitudeE6\":3,\"mLatitudeE6\":3,\"mAltitude\":0},\"id\":\"3\"}]";

        final String actualJsonString = fingerprintManager.getBluetoothLEFingerprintsJson();

        assertEquals(expectedJsonString, actualJsonString);
    }

    @Test
    public void testSetWifiFingerprintsJsonWithEmptyJsonStringAndRandomJsonString() {
        fingerprintManager.overlay = spy(fingerprintManager.getOverlay());

        fingerprintManager.setWifiFingerprintsJson("");
        fingerprintManager.setWifiFingerprintsJson("dfsgadkhaskfguiagfafasf");

        verify(fingerprintManager.getOverlay(), never()).addItem(any(FingerprintItem.class));
    }

    @Test
    public void testSetBluetoothLEFingerprintsJsonWithEmptyJsonStringAndRandomJsonString() {
        fingerprintManager.overlay = spy(fingerprintManager.getOverlay());

        fingerprintManager.setBluetoothLEFingerprintsJson("");
        fingerprintManager.setBluetoothLEFingerprintsJson("dfsgadkhaskfguiagfafasf");

        verify(fingerprintManager.getOverlay(), never()).addItem(any(FingerprintItem.class));
    }

    @Test
    public void testSetWifiFingerprintsJsonWithActualJsonString() {
        final String jsonString = "[{\"histogram\":{},\"point\":{\"divergence\":0.0,\"mLongitudeE6\":1,\"" +
                "mLatitudeE6\":1,\"mAltitude\":0},\"id\":\"1\"},{\"histogram\":{},\"point\":{\"divergence\":0.0,\"" +
                "mLongitudeE6\":2,\"mLatitudeE6\":2,\"mAltitude\":0},\"id\":\"2\"},{\"histogram\":{},\"point\":{\"" +
                "divergence\":0.0,\"mLongitudeE6\":3,\"mLatitudeE6\":3,\"mAltitude\":0},\"id\":\"3\"}]";
        fingerprintManager.overlay = spy(fingerprintManager.getOverlay());

        fingerprintManager.setWifiFingerprintsJson(jsonString);

        verify(fingerprintManager.getOverlay(), times(3)).addItem(any(FingerprintItem.class));
    }

    @Test
    public void testSetBluetoothLEFingerprintsJsonWithActualJsonString() {
        final String jsonString = "[{\"histogram\":{},\"point\":{\"divergence\":0.0,\"mLongitudeE6\":1,\"" +
                "mLatitudeE6\":1,\"mAltitude\":0},\"id\":\"1\"},{\"histogram\":{},\"point\":{\"divergence\":0.0,\"" +
                "mLongitudeE6\":2,\"mLatitudeE6\":2,\"mAltitude\":0},\"id\":\"2\"},{\"histogram\":{},\"point\":{\"" +
                "divergence\":0.0,\"mLongitudeE6\":3,\"mLatitudeE6\":3,\"mAltitude\":0},\"id\":\"3\"}]";
        fingerprintManager.overlay = spy(fingerprintManager.getOverlay());

        fingerprintManager.setBluetoothLEFingerprintsJson(jsonString);

        verify(fingerprintManager.getOverlay(), times(3)).addItem(any(FingerprintItem.class));
    }

    @Test
    public void testThatSizeReturnsOverlaySize() {
        int expectedSize = 0;

        Fingerprint fingerprint1 = new Fingerprint(new Histogram("1"), new InvioGeoPointImpl(1, 1));
        Fingerprint fingerprint2 = new Fingerprint(new Histogram("2"), new InvioGeoPointImpl(2, 2));
        Fingerprint fingerprint3 = new Fingerprint(new Histogram("3"), new InvioGeoPointImpl(3, 3));
        FingerprintItem item = new FingerprintItem();
        item.setWifiFingerprint(fingerprint1);

        fingerprintManager.getOverlay().addItem(item);
        expectedSize++;
        item = new FingerprintItem();
        item.setWifiFingerprint(fingerprint2);
        fingerprintManager.getOverlay().addItem(item);
        expectedSize++;
        item = new FingerprintItem();
        item.setBluetoothLEFingerprint(fingerprint3);
        fingerprintManager.getOverlay().addItem(item);
        expectedSize++;

        final int actualSize = fingerprintManager.size();

        assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testThatDetachDoesNothing() {
        final FingerprintManager fingerprintManagerSpy = spy(fingerprintManager);

        fingerprintManagerSpy.detach();

        verify(fingerprintManagerSpy, times(1)).detach();
        verifyNoMoreInteractions(fingerprintManagerSpy);
    }

    @Test
    public void testIsNetWorkAvailableReturnsFalseIfNetworkInfoNull() {
        final ConnectivityManager manager = mock(ConnectivityManager.class);
        when(activity.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(manager);
        when(manager.getActiveNetworkInfo()).thenReturn(null);

        assertFalse(fingerprintManager.isNetworkAvailable());
    }

    @Test
    public void testIsNetWorkAvailableReturnsFalseIfNetworkInfoNotConnected() {
        final ConnectivityManager manager = mock(ConnectivityManager.class);
        final NetworkInfo netInfo = mock(NetworkInfo.class);
        when(activity.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(manager);
        when(manager.getActiveNetworkInfo()).thenReturn(netInfo);
        when(netInfo.isConnected()).thenReturn(false);

        assertFalse(fingerprintManager.isNetworkAvailable());
    }

    @Test
    public void testIsNetWorkAvailableReturnsTrueIfNetworkInfoConnected() {
        final ConnectivityManager manager = mock(ConnectivityManager.class);
        final NetworkInfo netInfo = mock(NetworkInfo.class);
        when(activity.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(manager);
        when(manager.getActiveNetworkInfo()).thenReturn(netInfo);
        when(netInfo.isConnected()).thenReturn(true);

        assertTrue(fingerprintManager.isNetworkAvailable());
    }

    @Test
    public void testOnItemSingleTapUpAndOnItemLongPressReturnsFalseAndDoesNothing() {
        final FingerprintItem item = mock(FingerprintItem.class);
        final Boolean tapUp = fingerprintManager.onItemSingleTapUp(0, item);
        final Boolean longPress = fingerprintManager.onItemLongPress(0, item);

        assertFalse(tapUp);
        assertFalse(longPress);
    }
}
