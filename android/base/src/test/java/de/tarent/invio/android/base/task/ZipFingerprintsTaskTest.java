package de.tarent.invio.android.base.task;

import de.tarent.invio.android.base.map.IndoorMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
public class ZipFingerprintsTaskTest {

    @Mock
    DownloadListener listener;

    @Mock
    IndoorMap indoorMap;

    ZipFingerprintsTask toTest;
    String testString;

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);
        File testDir = new File("/tmp/ZipFingerprintsTaskTest/" + File.separator);
        File testJSON = new File("/tmp/ZipFingerprintsTaskTest/" + File.separator + "fingerprints" + File.separator + "fingerprints_data.json");
        File testJSONDir = new File("/tmp/ZipFingerprintsTaskTest/" + File.separator + "fingerprints" + File.separator);
        testString = "This represents the contents of the JSON-File";
        if (!testDir.exists()) {
            testDir.mkdirs();
        }
        if (!testJSONDir.exists()) {
            testJSONDir.mkdirs();
        }
        if (testJSON.exists()) {
            testJSON.delete();
        }
        testJSON.createNewFile();
        PrintWriter writer = new PrintWriter(testJSON);
        writer.print(testString);
        writer.close();
        when(indoorMap.getMapDirectory()).thenReturn(testDir);
        toTest = new ZipFingerprintsTask(listener, indoorMap);
    }

    @Test
    public void testIfDoInBackgroundReadsTheJSONFile() throws Exception {
        String result = toTest.doInBackground();
        assertEquals(testString, result);
    }
}
