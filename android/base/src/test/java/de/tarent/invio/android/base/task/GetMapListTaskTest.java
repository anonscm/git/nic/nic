package de.tarent.invio.android.base.task;


import android.app.ProgressDialog;
import android.widget.ArrayAdapter;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.response.MapListResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
public class GetMapListTaskTest {

    @Before
    public void setup() throws Exception {
        Robolectric.getBackgroundScheduler().pause();
    }
    @Test
    public void testThatTwoMapsListResponse() throws Exception {
        MapServerClient client = mock(MapServerClient.class);

        List<String> mapList = new ArrayList<String>();
        mapList.add("tarent 1OG");
        mapList.add("tarent 2OG");

        MapListResponse response = mock(MapListResponse.class);
        when(response.getMapList()).thenReturn(mapList);
        when(client.getMapList(5000)).thenReturn(response);

        ProgressDialog progressDialog = mock(ProgressDialog.class);

        ArrayAdapter arrayAdapter = mock(ArrayAdapter.class);

        GetMapListTask task = new GetMapListTask(client, progressDialog, arrayAdapter);
        task.execute();

        Robolectric.runBackgroundTasks();

        assertEquals(2, task.mapList.size());
    }

    @Test
    public void testEmptyMapResponse() throws Exception {
        MapServerClient client = mock(MapServerClient.class);

        List<String> mapList = new ArrayList<String>();

        MapListResponse response = mock(MapListResponse.class);
        when(response.getMapList()).thenReturn(mapList);
        when(client.getMapList(5000)).thenReturn(response);

        ProgressDialog progressDialog = mock(ProgressDialog.class);

        ArrayAdapter arrayAdapter = mock(ArrayAdapter.class);

        GetMapListTask task = new GetMapListTask(client, progressDialog, arrayAdapter);
        task.execute();

        Robolectric.runBackgroundTasks();

        assertEquals(0, task.mapList.size());
    }

    @Test
    public void testExceptionHandling() throws Exception {
        MapServerClient client = mock(MapServerClient.class);

        List<String> mapList = new ArrayList<String>();
        mapList.add("tarent 1OG");
        mapList.add("tarent 2OG");

        MapListResponse response = mock(MapListResponse.class);
        when(response.getMapList()).thenReturn(mapList);

        when(client.getMapList(5000)).thenThrow(IOException.class);

        ProgressDialog progressDialog = mock(ProgressDialog.class);

        ArrayAdapter arrayAdapter = mock(ArrayAdapter.class);

        GetMapListTask task = new GetMapListTask(client, progressDialog, arrayAdapter);
        task.execute();

        Robolectric.runBackgroundTasks();

        assertEquals(0, task.mapList.size());
    }
}
