package de.tarent.invio.android.base.particlefilter;


import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import de.tarent.invio.android.base.position.UserPositionItem;
import de.tarent.invio.android.base.position.UserPositionManager;
import de.tarent.invio.entities.InvioGeoPoint;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.osmdroid.views.MapView;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@Config(manifest = "../base/AndroidManifest.xml")
@RunWith(RobolectricTestRunner.class)
public class ParticleFilterTest {

    // The Object under Test:
    ParticleFilter pf;

    @Mock
    private Context ctx;

    @Mock
    private UserPositionManager userPosManager;

    @Before
    public void setUp() {
        initMocks(this);
        when(ctx.getResources()).thenReturn(Robolectric.application.getResources());
    }

    @Test
    public void testInitialization() {

        pf = new ParticleFilterTestable();
        pf.setNumParticles(6);
        pf.setScale(1.f);
        pf.sigmaInit = 1.f;

        pf.initialize(0, 0);

        assertTrue(6 == pf.particleList.size());
        assertEquals(0.17f, pf.particleList.get(0).getWeight(), 0.01f);
        assertEquals(0.1f, pf.particleList.get(0).getX(), 0.01f);
        assertEquals(0.1f, pf.particleList.get(0).getY(), 0.01f);
    }

    @Test
    public void testResampling() {
        pf = new ParticleFilterTestable();
        pf.setNumParticles(4);

        Particle tmp = new Particle(1.f, 2.f, 0.25f);
        pf.particleList.add(tmp);

        tmp = new Particle(3.f, 4.f, 0.01f);
        pf.particleList.add(tmp);

        tmp = new Particle(5.f, 6.f, 0.73f);
        pf.particleList.add(tmp);

        tmp = new Particle(7.f, 8.f, 0.01f);
        pf.particleList.add(tmp);

        pf.sigmaSensor = 1.f;
        pf.setScale(1.f);

        pf.resampling();

        assertEquals(1.1f, pf.particleList.get(0).getX(), 0.01f);
        assertEquals(5.1f, pf.particleList.get(1).getX(), 0.01f);
        assertEquals(5.1f, pf.particleList.get(2).getX(), 0.01f);
        assertEquals(5.1f, pf.particleList.get(3).getX(), 0.01f);
    }

    @Test
    public void testUpdateAction() {
        double deltaX = 2.f;
        double deltaY = 0.5f;

        pf = new ParticleFilterTestable();
        pf.setNumParticles(2);

        Particle tmp = new Particle(1.f, 2.f, 0.6f);
        pf.particleList.add(tmp);

        tmp = new Particle(4.f, 5.f, 0.4f);
        pf.particleList.add(tmp);

        pf.sigmaAction = 1.f;
        pf.setScale(1.f);

        pf.updateAction(deltaX, deltaY);

        assertEquals(3.1f, pf.particleList.get(0).getX(), 0.01f);
        assertEquals(2.6f, pf.particleList.get(0).getY(), 0.01f);
        assertEquals(6.1f, pf.particleList.get(1).getX(), 0.01f);
        assertEquals(5.6f, pf.particleList.get(1).getY(), 0.01f);

    }

    @Test
    public void testUpdateSensor() {
        double wifiX = 1.f;
        double wifiY = 5.f;

        pf = new ParticleFilterTestable();
        pf.setNumParticles(2);

        Particle tmp = new Particle(2.f, 4.f, 0.5f);
        pf.particleList.add(tmp);

        tmp = new Particle(20.f, 40.f, 0.5f);
        pf.particleList.add(tmp);

        pf.updateSensor(wifiX, wifiY);

        assertEquals(0.966f, pf.particleList.get(0).getWeight(), 0.01f);
        assertEquals(0.034f, pf.particleList.get(1).getWeight(), 0.01f);

    }

    @Test
    public void testCalculatePosition() {
        pf = new ParticleFilterTestable();
        pf.setNumParticles(4);

        Particle tmp = new Particle(1.f, 2.f, 0.25f);
        pf.particleList.add(tmp);

        tmp = new Particle(3.f, 4.f, 0.01f);
        pf.particleList.add(tmp);

        tmp = new Particle(5.f, 6.f, 0.73f);
        pf.particleList.add(tmp);

        tmp = new Particle(7.f, 8.f, 0.01f);
        pf.particleList.add(tmp);

        InvioGeoPoint pos = pf.calculatePosition();

        assertEquals(4.f, pos.getX(), 0.2f);
        assertEquals(5.f, pos.getY(), 0.2f);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test0Particles() {
        final Context context = mock(Context.class);
        final UserPositionManager userPositionManager = mock(UserPositionManager.class);

        final Resources resources = mock(Resources.class);
        when(context.getResources()).thenReturn(resources);
        when(resources.getInteger(any(Integer.class))).thenReturn(0);

        final ParticleFilter particleFilter = new ParticleFilter(context, userPositionManager);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLessThan0Particles() {
        final Context context = mock(Context.class);
        final UserPositionManager userPositionManager = mock(UserPositionManager.class);

        final Resources resources = mock(Resources.class);
        when(context.getResources()).thenReturn(resources);
        when(resources.getInteger(any(Integer.class))).thenReturn(-10);

        final ParticleFilter particleFilter = new ParticleFilter(context, userPositionManager);
    }

    @Test
    public void testThatUpdateSensorDoesNotAllowDivisionBy0() {
        final Context context = mock(Context.class);
        final UserPositionManager userPositionManager = mock(UserPositionManager.class);

        final Resources resources = mock(Resources.class);
        when(context.getResources()).thenReturn(resources);
        when(resources.getInteger(any(Integer.class))).thenReturn(1);

        final ParticleFilter particleFilter = new ParticleFilter(context, userPositionManager);
        particleFilter.particleList.add(new Particle(0, 0, 0));
        particleFilter.updateSensor(0, 0);
    }

    @Test
    public void testThatClearParticlesClearsParticleList() {
        pf = new ParticleFilterTestable();

        pf.particleList.add(new Particle(0, 0, 0));
        assertFalse(pf.particleList.isEmpty());

        pf.clearParticles();
        Assert.assertTrue(pf.particleList.isEmpty());
    }

    @Test
    public void testThatShowParticlesSetsParticleListAsUserPositionManagerParticlesWhenShowParticlesTrue() {
        final Activity activity = mock(Activity.class);
        final MapView mapView = mock(MapView.class);
        final UserPositionItem item = mock(UserPositionItem.class);
        when(activity.getResources()).thenReturn(Robolectric.application.getResources());

        final UserPositionManager userPositionManager = new UserPositionManager(activity, mapView, item);
        pf = new ParticleFilterTestable();
        pf.userPositionManager = userPositionManager;

        pf.setShowParticles(true);
        pf.particleList.add(new Particle(0, 0, 0));
        pf.showParticles();

        assertEquals(pf.particleList, userPositionManager.getParticles());
    }

    @Test
    public void testThatShowParticlesSetsEmptyParticleListInUserPositionManagerWhenShowParticlesFalse() {
        final Activity activity = mock(Activity.class);
        final MapView mapView = mock(MapView.class);
        final UserPositionItem item = mock(UserPositionItem.class);
        when(activity.getResources()).thenReturn(Robolectric.application.getResources());

        final UserPositionManager userPositionManager = new UserPositionManager(activity, mapView, item);
        pf = new ParticleFilterTestable();
        pf.userPositionManager = userPositionManager;

        pf.setShowParticles(false);
        pf.particleList.add(new Particle(0, 0, 0));
        pf.showParticles();

        assertNotSame(pf.particleList, userPositionManager.getParticles());
        assertTrue(userPositionManager.getParticles().isEmpty());
    }

    /**
     * There are no asserts here because this test is just supposed to run and make sure nothing crashes while running.
     */
    @Test
    public void testThatResamplingDoesNotCrashWhenNextRandomIsFirst0() {
        pf = new ParticleFilterTestable(0);
        pf.setNumParticles(2);

        pf.particleList.add(new Particle(2, 2, 2));
        pf.particleList.add(new Particle(1, 1, 1));
        pf.particleList.add(new Particle(3, 3, 2));

        pf.sigmaSensor = 2.f;
        pf.setScale(2.f);

        pf.resampling();
    }

    class ParticleFilterTestable extends ParticleFilter {
        public double nextRandomValue = 0.5f;
        public double nextGaussianValue = 0.1f;
        private int times = 0;

        ParticleFilterTestable() {
            super(ctx, userPosManager);
        }

        ParticleFilterTestable(final double nextRandomValue) {
            super(ctx, userPosManager);
            this.nextRandomValue = nextRandomValue;
        }

        /**
         * There is the if in here to make this that a not-0 value is returned.
         * Added for {@link #testThatResamplingDoesNotCrashWhenNextRandomIsFirst0}
         */
        protected double nextRandom() {
            if (times == 0) {
                times++;
                return nextRandomValue;
            } else {
                return 0.1f;
            }
        }

        protected void setNextRandom(final double nextRandomValue) {
            this.nextRandomValue = nextRandomValue;
        }

        protected double nextGaussian() {
            return nextGaussianValue;
        }
    }

}
