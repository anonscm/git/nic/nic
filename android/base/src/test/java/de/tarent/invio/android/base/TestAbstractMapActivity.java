package de.tarent.invio.android.base;

import android.os.Bundle;
import de.tarent.invio.android.base.map.IndoorMap;

import java.util.List;

/**
 * Implementation of the {@link AbstractMapActivity} so that the class can be fully covered with unit tests.
 *
 * @author Désirée Amling <d.amling@tarent.de>
 */
public class TestAbstractMapActivity extends AbstractMapActivity {

    @Override
    protected void downloadMapInformation() {
    }

    @Override
    public String getMultilevelDefaultMapShortName() {
        return null;
    }

    @Override
    public void createLevelButtons(List<IndoorMap> mapsList) {

    }

    @Override
    public void configurePOISearch() {

    }

    @Override
    public void onMapSwitched(IndoorMap map) {

    }

    @Override
    protected void createFingerprintManager() {

    }

    @Override
    protected void restoreFingerprintManagerFromBundle(final Bundle savedInstanceState) {

    }

    @Override
    protected void createMultiLevelMap(final Bundle savedInstanceState) {

    }
}
