package de.tarent.invio.android.base.utils;

import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * @author Désirée Amling <d.amling@tarent.de>
 */
public class CurrencyTest {

    @Test
    public void testThatUKCurrencyIsFormattedCorrectly() {
        final Currency currency = new Currency(199);
        currency.localeWrapper = spy(currency.localeWrapper);
        when(currency.localeWrapper.getDefaultLocale()).thenReturn(Locale.UK);

        String actualCurrency = currency.getFormattedCurrencyForDevice();
        String expectedCurrency = "1.99 £";

        assertEquals(expectedCurrency, actualCurrency);

        currency.setPrice(99999);
        actualCurrency = currency.getFormattedCurrencyForDevice();
        expectedCurrency = "999.99 £";

        assertEquals(expectedCurrency, actualCurrency);
    }

    @Test
    public void testThatGermanCurrencyIsFormattedCorrectly() {
        final Currency currency = new Currency(199);
        currency.localeWrapper = spy(currency.localeWrapper);
        when(currency.localeWrapper.getDefaultLocale()).thenReturn(Locale.GERMANY);

        String actualCurrency = currency.getFormattedCurrencyForDevice();
        String expectedCurrency = "1,99 €";

        assertEquals(expectedCurrency, actualCurrency);

        currency.setPrice(99999);
        actualCurrency = currency.getFormattedCurrencyForDevice();
        expectedCurrency = "999,99 €";

        assertEquals(expectedCurrency, actualCurrency);
    }

    @Test
    public void testThatDefaultCurrencyIsFormattedAsUKCurrency() {
        final Currency currency = new Currency(199);
        currency.localeWrapper = spy(currency.localeWrapper);
        // Use US as a test because it is not implemented.
        when(currency.localeWrapper.getDefaultLocale()).thenReturn(Locale.US);

        String actualCurrency = currency.getFormattedCurrencyForDevice();
        String expectedCurrency = "1.99 £";

        assertEquals(expectedCurrency, actualCurrency);
    }

    @Test
    public void testThatUnknownCurrencyIsFormattedToUKWhichIsDefault() {

    }
}
