package de.tarent.invio.android.base.tileprovider;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.osmdroid.ResourceProxy;
import org.osmdroid.tileprovider.MapTile;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

/**
 * Tests for {@link de.tarent.invio.android.base.tileprovider.InvioTileSource}
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 */
@RunWith(RobolectricTestRunner.class)
public class InvioTileSourceTest {
    final String tileName = "name";
    final int minZoom = 0;
    final int maxZoom = 6;
    final String fileEnding = ".png";

    InvioTileSource toTest;

    @Before
    public void setup() {
        toTest = spy(new InvioTileSource(
                tileName,
                ResourceProxy.string.unknown,
                minZoom,
                maxZoom,
                256,
                fileEnding));
    }

    @Ignore
    @Test
    public void getTileRelativeFilenameString() {
        final MapTile tile = new MapTile(1, 13, 12);
        doReturn(tile.getY()).when(toTest).getYinTMSFormat(anyInt(), anyInt());

        final String result = toTest.getTileRelativeFilenameString(tile);

        assertEquals(
                tileName + "/" + tile.getZoomLevel() + "/" +
                        tile.getX() + "/" + toTest.getYinTMSFormat(tile.getZoomLevel(), tile.getY()) + fileEnding,

                result);
    }

    @Test
    public void getY_TMS() {
        assertEquals(0, toTest.getYinTMSFormat(0, 0));
        assertEquals(63, toTest.getYinTMSFormat(6, 0));
    }

}
