package de.tarent.invio.android.base.task;

import android.os.Environment;
import android.util.Log;
import de.tarent.invio.mapserver.MapServerClient;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Class for the task of downloading the group data.
 */
public class CachedDownloadGroupDataTask extends DownloadTask<Void, Void, File> {

    private static final String TAG = CachedDownloadGroupDataTask.class.getName();

    private String groupDataDirectory;

    private final String groupName;

    private final MapServerClient client;

    /**
     * Constructor.
     *
     * @param listener which will be notified when the complete zip download and unzipping were done
     * @param groupName name of the group (group of maps on the server)
     * @param client the {@link de.tarent.invio.mapserver.MapServerClient} instance
     * @param appFolderName name of the apps folder on the sd-card
     */
    public CachedDownloadGroupDataTask(final DownloadListener<File> listener,
                                       final String groupName,
                                       final MapServerClient client,
                                       final String appFolderName) {
        downloadListeners.add(listener);
        this.groupName = groupName;
        this.client = client;
        groupDataDirectory =  Environment.getExternalStorageDirectory() + File.separator + appFolderName;
    }

    @Override
    protected File doInBackground(final Void... params) {

        final File groupDataFile = new File(groupDataDirectory + File.separator + groupName + "_data.zip");
        ensureDirExists(groupDataFile);
        final File unzipDir = new File(groupDataDirectory + File.separator + groupName);
        ensureDirExists(unzipDir);
        try {
            final InputStream in = client.getGroupData(groupName, 6000);
            final OutputStream os = new FileOutputStream(groupDataFile);
            IOUtils.copy(in, os);
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(os);
            unzipFile(groupDataFile, unzipDir);
            success = true;
        } catch (IOException e) {
            Log.e(TAG, "Map group data download/persistence or unzip failed! Falling back to cache (if such exists).");
        }

        // If we don't have any maps inside the unzipped group directory, then something went wrong and we must signal
        // it.
        if(unzipDir.listFiles() == null || unzipDir.listFiles().length == 0 ) {
            success = false;
        } else {
            success = true;
        }

        return unzipDir;
    }

    private void ensureDirExists(final File directory){
        final File groupDataFileParent = directory.getParentFile();
        if(!groupDataFileParent.exists()) {
            if(groupDataFileParent.mkdirs()) {
                Log.d(TAG, "Created directory: " + directory.getPath());
            } else {
                Log.e(TAG, "Could not create directory: " + directory.getPath());
            }
        }
    }

    /**
     * Unzips the downloaded zip file to the apps directory.
     *
     * @param fileToUnzip file to unzip
     * @param directory apps directory
     * @throws IOException if something went wrong with reading / writing of the files
     */
    protected void unzipFile(final File fileToUnzip, final File directory) throws IOException {
        final ZipFile zipFile = new ZipFile(fileToUnzip);
        try {
            final Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                final ZipEntry entry = entries.nextElement();
                final File entryFile = new File(directory, entry.getName());
                ensureDirExists(entryFile);
                if (entry.isDirectory()) {
                    entryFile.mkdirs(); //NOSONAR - We do not need the return value of this particular method
                } else {
                    final InputStream is = zipFile.getInputStream(entry);
                    final OutputStream os = new FileOutputStream(entryFile);
                    IOUtils.copy(is, os);
                    IOUtils.closeQuietly(is);
                    IOUtils.closeQuietly(os);
                }
            }
            zipFile.close();
        } catch (final IOException e) {
            zipFile.close();
            throw new IOException(e);
        }
    }
}
