package de.tarent.invio.android.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import de.tarent.invio.android.base.config.Config;
import de.tarent.invio.android.base.config.ValueProvider;
import de.tarent.invio.android.base.map.IndoorMap;
import de.tarent.invio.android.base.mapmatching.WayManager;
import de.tarent.invio.android.base.position.UserPositionManager;
import de.tarent.invio.android.base.sensor.DeadReckoning;
import de.tarent.invio.android.base.task.CachedDownloadMapDataTask;
import de.tarent.invio.android.base.task.CachedDownloadMapResourceTask;
import de.tarent.invio.android.base.task.InvioOsmParser;
import de.tarent.invio.android.base.task.UploadPositionTask;
import de.tarent.invio.android.base.tileprovider.TileProviderFactory;
import de.tarent.invio.android.base.wifi.FingerprintManager;
import de.tarent.invio.android.base.wifi.UserLocator;
import de.tarent.invio.android.base.wifi.WifiCapturer;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.MapServerClientImpl;
import org.osmdroid.tileprovider.MapTileProviderBase;
import org.osmdroid.views.MapView;

import java.util.List;

import static de.tarent.invio.android.base.config.Property.MAP_PROVIDER_SCHEMA;


/**
 * TODO: Translate
 * Jede {@link android.app.Activity} des Invio-Projektes sollte von
 * dieser Klasse erben! Sie beinhaltet nötige Initialisierungs-
 * schritte und Methoden, die allgemein von Bedeutung sind.
 */
public abstract class AbstractMapActivity extends BaseActivity {
    /**
     * The key under which we store our fingerprints in the instance-state-bundle.
     */
    protected static final String WIFI_FINGERPRINTS_JSON = "wifiFingerprintsJson";
    protected static final String BLUETOOTH_FINGERPRINTS_JSON = "bluetoothFingerprintsJson";

    protected static final String EDGES_JSON = "edgesJson";

    /**
     * The {@link UserLocator} is used to do the fingerprint-matching/tracking of our current position.
     */
    protected static UserLocator userLocator; //NOSONAR - Cannot be package protected.

    /**
     * Static variable so that the user is only asked once to turn on his/her wifi during the entire lifecycle.
     */
    protected static boolean askedToEnableWifi = false; //NOSONAR - Cannot be package protected.

    /**
     * The {@link WayManager} is static so that HomeActivity can access it, even before the AbstractMapActivity is
     * instantiated, to pass it as a DownloadListener to the MapDatadownloadTask.
     */
    private static final WayManager WAY_MANAGER = new WayManager();

    private static UploadPositionTask uploadPositionTask;

    /**
     * the {@link DeadReckoning} tracks the user's movement until a new wifi-position is available. It is static so
     * that HomeActivity can access it, even before the AbstractMapActivity is instantiated, to pass it as a
     * DownloadListener to the MapDatadownloadTask.
     */
    private static DeadReckoning deadReckoning;

    /**
     * The tag for log-messages from this class:
     */
    private static final String TAG = "GetMapListTask";

    /**
     * The name of the map the current map. Will be used in the URL from which to download tiles, fingerprints,
     * etc. from the server.
     */
    protected String mapName;

    /**
     * The {@link MapView}from osmdroid is the main-feature of this activity.
     */
    protected MapView mapView;

    /**
     * The {@link FingerprintManager}.
     */
    protected FingerprintManager fingerprintManager;

    /**
     * The {@link de.tarent.invio.android.base.wifi.WifiCapturer} builds the scan-buffer for the tracking-mode:
     */
    protected WifiCapturer wifiCapturer;

    protected ProgressDialog progressDialog;

    protected MapTileProviderBase tileProvider;

    /**
     * This flag stores whether the UserLocator is currently scanning (using all its available sensors).
     */
    protected boolean scanning = false;

    protected boolean multiLevelMap;

    private int mapViewLayoutId;

    private int osmMapViewId;

    private ValueProvider mConfig;

    private boolean wifiOff;

    private String mapUploadLocation;

    /**
     * Constructor.
     */
    protected AbstractMapActivity() {
        initialiseConfig();

        this.mConfig = Config.getInstance();
    }


    /**
     * Get the global WayManager. (Not a very nice solution to the problem that the waymanager needs to be created
     * before the activity that needs it, because we just happen to download the data earlier...).
     *
     * @return the WayManager
     */
    public static WayManager getWayManager() {
        return WAY_MANAGER;
    }

    /**
     * Get the {@link MapView} - the main feature of this activity.
     *
     * @return the {@link MapView}
     */
    public MapView getMapView() {
        return mapView;
    }

    /**
     * Get the DeadReckoning-instance, creating it if it doesn't exist.
     *
     * @param activity the activity where the DeadReckoning, if it has to be created, can get its resources from
     * @return the static DeadReckoning
     */
    public static DeadReckoning getDeadReckoning(final Activity activity) {
        if (deadReckoning == null) {
            deadReckoning = new DeadReckoning(activity);
        }
        return deadReckoning;
    }

    /**
     * Get the {@link de.tarent.invio.android.base.wifi.FingerprintManager}
     *
     * @return the {@link de.tarent.invio.android.base.wifi.FingerprintManager}
     */
    public FingerprintManager getFingerprintManager() {
        return fingerprintManager; //NOSONAR Initialized in the implemented activities
    }

    /**
     * Get the {@link WifiCapturer}-instance, creating it if it doesn't exist.
     *
     * @param activity the activity where the {@link WifiCapturer}, if it has to be created, can get its resources
     * @return the {@link WifiCapturer}-instance
     */
    public WifiCapturer getWifiCapturer(final Activity activity) {
        initSensorCollectors(userLocator);

        return wifiCapturer;
    }

    /**
     * (Re-)start scanning.
     * @return true = activity was already scanning; false = activity was not scanning (but is now)
     */
    public boolean startScan() {
        final boolean wasScanning = scanning;
        scanning = true;
        userLocator.startTracking();

        return wasScanning;
    }

    /**
     * Stop scanning.
     * @return true = activity was not scanning; false = activity was  scanning (but has now stopped)
     */
    public boolean stopScan() {
        final boolean wasScanning = scanning;
        scanning = false;
        userLocator.stopTracking();

        return wasScanning;
    }

    /**
     * Detach all tileProviders on stop to avoid leaks
     */
    @Override
    protected void onStop() {
        super.onStop();

        if (tileProvider != null) {
            tileProvider.detach();
        }

        if (fingerprintManager != null) { // NOSONAR - Unwritten public or protected field - Not always null
            fingerprintManager.detach();
        }

        dismissProgressDialog();
    }

    /**
     * Resets Particle Filter
     */
    public void resetParticleFilterMapchange() {
        if (userLocator != null) {
            userLocator.resetParticleFilter();
        }
    }

    /**
     * This onCreate method is called when the need to distinguish between layouts is present. For example the
     * admin-app requires a toggle button in the {@link MapView} whereas the kunden-app does not.
     *
     * @param savedInstanceState the {@link Bundle}
     * @param mapViewLayoutId the layout id for the map view
     * @param osmMapViewId the is for the osm map view
     */
    protected void onCreate(final Bundle savedInstanceState, final int mapViewLayoutId, final int osmMapViewId) {
        this.mapViewLayoutId = mapViewLayoutId;
        this.osmMapViewId = osmMapViewId;

        handleWifiStatus();
        onBaseCreate(savedInstanceState);
    }

    /**
     * Configure map. Download or restore fingerprints and ways. IMPORTANT: fingerprints will be restored if the map is
     * not an multilevel map.
     *
     * @param savedInstanceState the state that was saved in onSaveInstanceState
     */
    protected void onBaseCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mapName = findMapName();

        setContentView(getMapLayout());
        configureMapView();

        if (multiLevelMap) {
            createMultiLevelMap(savedInstanceState);
        } else {
            createOrRecoverFingerprintManager(savedInstanceState);
        }

        final UserPositionManager userPositionManager = new UserPositionManager(this, mapView);

        if (userLocator != null) { //NOSONAR - This is a multi-threading problem but will be fixed after we make a plan.
            userLocator.stopTracking();
        }
        userLocator = new UserLocator(this, fingerprintManager, userPositionManager, WAY_MANAGER);
        initSensorCollectors(userLocator);
    }

    /**
     * Checks for Wifi-Status and then informs the user if wifi is disabled.
     */
    protected void handleWifiStatus() {
        final Context ctx = this.getApplicationContext();
        final WifiManager wifiManager = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        final int wifiState = wifiManager.getWifiState();

        switch (wifiState) {
            // For all three states (disabled, disabling and unknown) show the same toast
            case WifiManager.WIFI_STATE_DISABLED:
            case WifiManager.WIFI_STATE_DISABLING:
            case WifiManager.WIFI_STATE_UNKNOWN:
                wifiOff = true;
                if (!askedToEnableWifi) {
                    askedToEnableWifi = true;
                    showWifiNotEnabledDialog();
                }
                break;

            default:
                // Everything  is fine, if WiFi is enabled
        }
    }

    /**
     * Show dialog if WiFi is disabled.
     */
    protected void showWifiNotEnabledDialog() {
        final AlertDialog alertDialog = createWifiNotEnabledDialog();
        alertDialog.show();
    }

    private AlertDialog createWifiNotEnabledDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK).create();
        alertDialog.setTitle(getString(R.string.wifi_not_enabled_dialog_title));
        alertDialog.setMessage(getString(R.string.wifi_not_enabled_dialog_message));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                getString(R.string.activate_wifi),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activateWifi();
                        wifiOff = false;
                        showProgressDialog();
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getString(R.string.dialog_button_no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        wifiOff = true;
                        dialog.dismiss();
                    }
                });
        return alertDialog;
    }

    private boolean isOnline() {
        final ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnected());
    }

    private void activateWifi() {
        final WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(true);
    }

    private void initSensorCollectors(UserLocator myUserLocator) {
        if (wifiCapturer == null) {
            wifiCapturer = new WifiCapturer(this,
                    myUserLocator,
                    getResources().getInteger(R.integer.tracker_scanbuffer_maxage));

            myUserLocator.addSensorCollector(wifiCapturer);
        }

        if (myUserLocator.getLocalizationMethod() != myUserLocator.LOCALIZATION_MODE_WIFI) {
            myUserLocator.addSensorCollector(getDeadReckoning(this));
        }
    }

    public boolean isWifiOff() {
        return wifiOff;
    }

    public void setWifiOff(boolean wifiState) {
        wifiOff = wifiState;
    }

    /**
     * show ProgressDialog
     */
    public void showProgressDialog() {
        if (progressDialog == null) {
            createProgressDialog();
        }
        progressDialog.show();

    }

    private void createProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);
        progressDialog.setMessage(this.getString(R.string.please_wait_message));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
    }

    /**
     * dismiss ProgressDialog
     */
    public void dismissProgressDialog() {
        if(progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    /**
     * Creates or recovers the fingerprint manager.
     *
     * @param savedInstanceState the saved instance state {@link Bundle}
     */
    protected void createOrRecoverFingerprintManager(final Bundle savedInstanceState) {
        //TODO: Need to save map resources in the Bundle
        new CachedDownloadMapResourceTask(this, mapServerClient, mapView, mapName).execute();
        if (savedInstanceState == null){
            // For single-level-maps we can download the resources here, but for multi-level-maps this will
            // be handled by the MultiMap.
            createFingerprintManager();
            // Crude hack: maybe the WayManager already has edges, downloaded from some other activity (e.g.
            // HomeActivity in sellfio-customer-app). But maybe it doesn't (e.g. admin-app). In that case do download
            // them now:
            if (WAY_MANAGER.getEdges().size() == 0) {
                new CachedDownloadMapDataTask(WAY_MANAGER, mapServerClient, mapName, new InvioOsmParser()).execute();
            }
        } else {
            //If savedInstanceState is not null then we can restore fingerprints and ways from Bundle and don't
            //need to download it again.
            final String edgesJson = savedInstanceState.getString(EDGES_JSON);
            WAY_MANAGER.setEdgesJson(edgesJson);
            restoreFingerprintManagerFromBundle(savedInstanceState);
        }
    }

    /**
     * Each subclass should return the name of the short map which will be shown if no level were detected initially.
     *
     * @return short name of the default map (e.g. the entrance of the multilevel building)
     */
    public abstract String getMultilevelDefaultMapShortName();

    /**
     * Create level buttons depending on the list of the maps.
     *
     * @param mapsList containing indoor maps
     */
    public abstract void createLevelButtons(final List<IndoorMap> mapsList);


    /**
     * Each subclass has to configure it own POI (products) search functionality.
     */
    public abstract void configurePOISearch();

    /**
     * Gets called when a map finished loading and is displayed on the screen.
     *
     * @param map which should be shown
     */
    public abstract void onMapSwitched(final IndoorMap map);

    /**
     * This method is called before an activity may be killed to restore the state in the future. This is the case when
     * user presses the home button
     * IMPORTANT: please note that this method will NOT be called it user navigates back from activity or another
     * activity is launched in front of this activity, because there is no need to it.
     * @param   outState the Bundle, in which to save our state
     */
    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        if (fingerprintManager != null) { // NOSONAR this field is written by the child-classes only and that's ok
            outState.putString(WIFI_FINGERPRINTS_JSON, fingerprintManager.getWifiFingerprintsJson());
            outState.putString(BLUETOOTH_FINGERPRINTS_JSON, fingerprintManager.getBluetoothLEFingerprintsJson());
        }
        if (WAY_MANAGER != null) { // NOSONAR this field is written by the child-classes only and that's ok
            outState.putString(EDGES_JSON, WAY_MANAGER.getEdgesJson());
        }

        super.onSaveInstanceState(outState);
    }

    /**
     * Create a {@link FingerprintManager} using the savedInstanceState.
     */
    protected abstract void createFingerprintManager();

    /**
     * Create new fingerprintManager instance and put the saved fingerprints from bundle.
     * @param savedInstanceState the Bundle from which to read the data that the FingerprintManager needs.
     */
    protected abstract void restoreFingerprintManagerFromBundle(final Bundle savedInstanceState);

    /**
     * Create a MultiMap using the savedInstanceState.
     *
     * @param savedInstanceState the {@link Bundle} with the savedInstanceState
     */
    protected abstract void createMultiLevelMap(final Bundle savedInstanceState);

    /**
     * Liefert den zu verwendenden {@link ValueProvider}.
     *
     * @return den zu verwendenden {@link ValueProvider}
     */
    protected ValueProvider config() {
        return mConfig;
    }


    /**
     * Find the name of the map in the intent that started this activity. The relevant intent-extra will have been set
     * by the MapSelectionActivity, which comes before this activity to let the user chose a map:
     * Here we also detect whether this is a multi-level-map.
     *
     * @return the name of the map
     */
    protected String findMapName() {
        final Intent intent = getIntent();
        final String mapName = intent.getStringExtra("MapName");
        multiLevelMap = intent.getBooleanExtra("multilevelMap", false);

        return mapName;
    }

    /**
     * Gets the layout of the map view.
     *
     * @return the map view's layout
     */
    protected ViewGroup getMapLayout() {
        final LayoutInflater inflater = getLayoutInflater();
        final View v = inflater.inflate(mapViewLayoutId, null);

        return (ViewGroup) v;
    }

    /**
     * Configures the {@link #mapView}.
     *
     * @return the newly configured {@link #mapView}
     */
    protected MapView configureMapView() {
        mapView = (MapView)findViewById(osmMapViewId);

        // For multi level maps the tileprovider will later be set by the MultiMap.
        // We set it here anyways to disable the default osmdroid world tiles that will show
        // since osmdroid 4.2 and on some devices pre 4.2
        tileProvider = buildTileProvider();
        mapView.setTileSource(tileProvider.getTileSource());

        mapView.setBuiltInZoomControls(true);
        mapView.setMultiTouchControls(true);    //zoom with finger

        return mapView;
    }

    private MapTileProviderBase buildTileProvider() {
        final TileProviderFactory factory = new TileProviderFactory(this);
        final MapTileProviderBase provider = factory.buildWebTileProvider(
                config().getPropertyValue(MAP_PROVIDER_SCHEMA),
                mapName);

        return provider;
    }

    private void initialiseConfig() {
        Config.getInstance().setContext(this);
    }

    /**
     * Starts the position upload task.
     * TODO: Add try catch for when mapUploadLocation wasn't set.
     */
    protected void startPositionUpload() {
        uploadPositionTask = UploadPositionTask.getInstance(mapServerClient, mapUploadLocation, this);
        if (uploadPositionTask.getStatus() == AsyncTask.Status.PENDING) {
            try {
                uploadPositionTask.execute();
            } catch (IllegalStateException e) {
                Log.e(TAG, "UploadPositionTask executed more than once" + e);
            }
        }
    }

    /**
     * Stops the position upload task.
     */
    protected void stopPositionUpload() {
        uploadPositionTask.cancel(true);
    }

    public void setMapUploadLocation(final String mapUploadLocation) {
        this.mapUploadLocation = mapUploadLocation;
    }

    public int getMapViewLayoutId() {
        return mapViewLayoutId;
    }

    public int getOsmMapViewId() {
        return osmMapViewId;
    }

}
