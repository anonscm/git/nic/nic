package de.tarent.invio.android.base.wifi;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import de.tarent.invio.android.base.sensor.SensorCollector;
import de.tarent.invio.entities.Histogram;
import de.tarent.invio.entities.WifiScanResult;
import de.tarent.invio.tracker.wifi.HistogramBuilder;
import de.tarent.invio.tracker.wifi.HistogramConsumer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The BluetoothLECapturer captures BluetoothLE-histograms.
 * The BluetoothLECapturer has two modes:
 * - fingerprint-mode, for the offline-phase, where it does a number of scans, produces one histogram, and then stops.
 * - continuous-mode, for the online-phase, where it goes on scanning and producing histograms until it is stopped.
 * The mode is selected via the ProgressDialog-parameter. If it is null then continuous-mode is selected.
 * <p/>
 * TODO:
 * - the number of scans could be made configurable.
 * - there should be a configurable timeout, in case the scanning takes much longer than expected.
 */
public class BluetoothLECapturer implements SensorCollector {

    public static final String TAG = "WifiCapturer";

    /**
     * How many scans we want to do, unless we are in "continuous-scanning-mode" (i.e. have not ProgressDialog):
     */
    public static final int NUMBER_SCANS = 5;

    /**
     * The interval in which the scans will be updated.
     */
    public static final int UPDATE_INTERVAL = 1000;

    private BluetoothAdapter.LeScanCallback callback;

    private Context context;

    /**
     * We will hand the new histograms to this consumer. If we have a ProgressDialog then this will happen at the end.
     * If we don't have a ProgressDialog we are in continuous-background-scanning-mode and will produce a new histogram
     * after each scan.
     */
    private HistogramConsumer fingerprintHistogramConsumer;

    private HistogramConsumer trackinghistogramConsumer = null;

    private HistogramBuilder histogramBuilder;

    private BluetoothAdapter bluetoothAdapter;

    private Handler handler;

    private int scanCount = 0;

    /**
     * The maximum age in milliseconds that a scanresult may have to be included in the histogram.
     * A maxAge of 0 is used to disable age-filtering.
     */
    private int maxAge;

    private boolean bluetoothEnabled = false;
    private CaptureCallback captureCallback;

    /**
     * Construct a new BluetoothLECapturer for a specific Activity. This constructor cannot be used for tracking, only
     * for making fingerprints!
     *
     * @param context the Activity which will be used for Context and to run UI-stuff on.
     */
    public BluetoothLECapturer(final Context context) {
        this(context, null, 0);
    }

    /**
     * Construct a new BluetoothLECapturer for a specific Activity.
     *
     * @param context  the Activity which will be used for Context and to run UI-stuff on.
     * @param consumer the HistogramConsumer that needs the results that we generate periodically.
     * @param maxAge   the maximum age in milliseconds that a scanresult may have to be included in the histogram.
     *                 A maxAge of 0 is used to disable age-filtering.
     */
    public BluetoothLECapturer(final Context context, final HistogramConsumer consumer, final int maxAge) {
        this.context = context;
        this.trackinghistogramConsumer = consumer;
        this.maxAge = maxAge;
        createCallback();
        enableBluetoothLE();
        handler = new Handler();
    }

    /**
     * Start the scanning-process. Should not be called a second time before the asynchronous scanning-process has
     * finished or has been stopped explicitly.
     *
     * @param captureCallback TODO
     * @param consumer the HistogramConsumer that will be called with the collected histogram.
     * TODO: if startScan is called a second time before stopScan we might leak a WifiReceiver.
     */
    public void makeFingerprint(final CaptureCallback captureCallback, final HistogramConsumer consumer) {
        this.captureCallback = captureCallback;
        this.fingerprintHistogramConsumer = consumer;

        startSensors();
        captureCallback.onCaptureStart();
    }

    @Override
    public void startSensors() {
        if (bluetoothEnabled && bluetoothAdapter != null) {
            histogramBuilder = new HistogramBuilder("Bluetooth LE Histogram " + new Date().toString(), maxAge);

            scanCount = 0;

            postScan();
        }
    }

    @Override
    public void stopSensors() {
        if (bluetoothEnabled && bluetoothAdapter != null) {
            bluetoothAdapter.stopLeScan(callback);
        }
    }

    private void enableBluetoothLE() {
        if (isBluetoothLESupported(context)) {
            final BluetoothManager bluetoothManager =
                    (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
            bluetoothAdapter = bluetoothManager.getAdapter();

            if (bluetoothAdapter.isEnabled()) {
                bluetoothEnabled = true;
            }
        }
    }

    private boolean isBluetoothLESupported(final Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
        }
        return false;
    }

    private void postScan() {
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (scanCount < NUMBER_SCANS || captureCallback == null) {
                    doScan();
                } else {
                    //stop the scan, because of wifi/bluetooth bug in Nexus 7 see wiki Bluetooth Evaluation
                    stopScan();
                }
                scanCount++;
            }
        }, UPDATE_INTERVAL);
    }

    private void doScan() {
        bluetoothAdapter.stopLeScan(callback);
        bluetoothAdapter.startLeScan(callback);

        if (captureCallback == null) {
            final Histogram histogram = histogramBuilder.build();
            trackinghistogramConsumer.addHistogram(histogram);
        } else {
            captureCallback.onCaptureIteration(scanCount, NUMBER_SCANS);
        }
        postScan();
    }

    private void stopScan() {
        bluetoothAdapter.stopLeScan(callback);

        fingerprintHistogramConsumer.addHistogram(histogramBuilder.build());
        captureCallback.onCaptureStop();
    }

    private void createCallback() {
        callback = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(BluetoothDevice bluetoothDevice, int level, byte[] advertisingData) {
                final List<WifiScanResult> btleScanResults = new ArrayList<WifiScanResult>();
                IBeacon beacon = IBeacon.fromAdvertisementData(advertisingData);
                if(beacon != null) {
                    btleScanResults.add(new WifiScanResult(beacon.getProximityUUID().toString() + "_" + String.valueOf
                            (beacon.getMajor()) + "_" + String.valueOf
                            (beacon.getMinor()), level));
                    histogramBuilder.addScanResults(btleScanResults);
                }
            }
        };
    }
}
