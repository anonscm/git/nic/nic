package de.tarent.invio.android.base.position;

import de.tarent.invio.entities.InvioGeoPoint;
import de.tarent.invio.tracker.geopoint.PointList;

import java.util.LinkedList;
import java.util.List;

/**
 * Contains a sliding-window for the {@link InvioGeoPoint}s so that we can keep multiple {@link InvioGeoPoint}s up to a
 * configurable number. Once that number is reached, the eldest {@link InvioGeoPoint} is removed.
 *
 * @author Désirée Amling <d.amling@tarent.de>
 */
public class History {

    /**
     * The maximum number of {@link InvioGeoPoint}s that the History should hold.
     */
    private final int maxGeoPointHistory;

    /**
     * The list of the {@link InvioGeoPoint}s that will act as our sliding-window.
     */
    private final List<InvioGeoPoint> geoPoints;

    /**
     * Simple constructor that instantiates the {@link #geoPoints} as a {@link LinkedList}.
     *
     * @param maxGeoPointHistory the given {@link #maxGeoPointHistory}
     */
    public History(final int maxGeoPointHistory) {
        this.maxGeoPointHistory = maxGeoPointHistory;
        geoPoints = new LinkedList<InvioGeoPoint>();
    }

    /**
     * Adds a {@link InvioGeoPoint} to the {@link #geoPoints} history. If the length of the {@link #geoPoints}
     * reaches {@link #maxGeoPointHistory}, then the oldest {@link InvioGeoPoint} will be removed.
     *
     * @param geoPoint the {@link InvioGeoPoint} to add to the list of {@link #geoPoints}
     */
    public void add(final InvioGeoPoint geoPoint) {
        if (geoPoints.size() == maxGeoPointHistory) {
            removeOldestGeoPoint();
        }
        geoPoints.add(geoPoint);
    }

    /**
     * Gets the median {@link InvioGeoPoint} from the list of {@link #geoPoints}.
     *
     * @return the median {@link InvioGeoPoint}
     */
    public InvioGeoPoint getMedianPoint() {
        final PointList pointList = new PointList(new InvioGeoPointFactory(), geoPoints);

        return pointList.getMedianPoint();
    }

    /**
     * Gets the average {@link InvioGeoPoint} from the list of {@link #geoPoints}.
     *
     * @return the average {@link InvioGeoPoint}
     */
    public InvioGeoPoint getAveragePoint() {
        final PointList pointList = new PointList(new InvioGeoPointFactory(), geoPoints);

        return pointList.getAveragePoint();
    }

    private void removeOldestGeoPoint() {
        geoPoints.remove(0);
    }

    public List<InvioGeoPoint> getGeoPoints() {
        return geoPoints;
    }
}
