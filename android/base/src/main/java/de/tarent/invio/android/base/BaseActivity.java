package de.tarent.invio.android.base;

import android.app.Activity;
import android.os.Bundle;
import de.tarent.invio.android.base.config.MapProperties;
import de.tarent.invio.android.base.task.DownloadMapPropertiesTask;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.MapServerClientImpl;

import java.util.concurrent.ExecutionException;

/**
 * This class contains information that all of the activities will need.
 */
public class BaseActivity extends Activity {

    /**
     * The {@link MapProperties}.
     */
    public static MapProperties mapProperties;

    /**
     * True if the app being started is the admin app. False if not. This will prevent the properties being downloaded
     * for the admin app as it's not actually required.
     */
    public static boolean adminApp = false;

    /**
     * True if there was an error fetching the properties from the server and the default properties had to be loaded.
     * False if there were no problems and the default properties were not loaded.
     */
    protected static boolean defaultProperties = false;

    /**
     * The {@link MapServerClient}.
     */
    protected static MapServerClient mapServerClient;

    private static final MapPropertiesManager MAP_PROPERTY_MANAGER = new MapPropertiesManager();

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createMapServerClient();
        downloadMapInformation();
    }

    protected void createMapServerClient() {
        if (mapServerClient == null) {
            final String serverEndpoint = getResources().getString(R.string.server_endpoint);
            mapServerClient = new MapServerClientImpl(serverEndpoint);
        }
    }

    protected void downloadMapInformation() {
        if (!adminApp && mapProperties == null) {
            try {
                new DownloadMapPropertiesTask(MAP_PROPERTY_MANAGER, mapServerClient, getString(R.string.map_properties)).execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            mapProperties = MAP_PROPERTY_MANAGER.getMapProperties();
            verifyMapProperties();
        }
    }

    private void verifyMapProperties() {
        if (mapProperties == null) {
            mapProperties = new MapProperties();
        }

        if (!mapProperties.allPropertiesSet()) {
            mapProperties.setGroupName(getString(R.string.map_group_name));
            mapProperties.setShortName(getString(R.string.multilevel_default_map_short_name));
            mapProperties.setUploadLocation(getString(R.string.map_cart_upload));
            defaultProperties = true;
        }
    }
}
