package de.tarent.invio.android.base.position;

import de.tarent.invio.tracker.geopoint.PointFactory;

/**
 * The InvioGeoPointFactory is a PointFactory which creates instances of InvioGeoPointImpl. It is the right factory to use
 * on android, where the points need to be compatible with osmdroid. In other places (e.g. on the server), where we
 * don't want android-dependencies, the {@link de.tarent.invio.tracker.geopoint.XYPointFactory} might be better.
 */
public class InvioGeoPointFactory extends PointFactory<InvioGeoPointImpl> {

    /**
     * {@inheritDoc}
     */
    @Override
    public InvioGeoPointImpl newPoint() {
        return new InvioGeoPointImpl();
    }

}
