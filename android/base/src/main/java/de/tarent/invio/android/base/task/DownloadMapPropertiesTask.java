package de.tarent.invio.android.base.task;

import android.util.Log;
import de.tarent.invio.android.base.config.MapProperties;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.response.MapPropertiesResponse;

import java.io.IOException;

/**
 * The task to download the map properties asynchronously.
 */
public class DownloadMapPropertiesTask extends DownloadTask<Void, Void, MapProperties> {

    private static final String TAG = "DownloadMapPropertiesTask";

    private MapServerClient mapServerClient;

    private String mapPropertiesName;

    /**
     * Constructor.
     *
     * @param downloadListener the {@link DownloadListener}
     * @param mapServerClient the {@link MapServerClient}
     * @param mapPropertiesName the map properties name, which is found in the config.xml
     */
    public DownloadMapPropertiesTask(final DownloadListener<MapProperties> downloadListener,
                                     final MapServerClient mapServerClient,
                                     final String mapPropertiesName) {
        downloadListeners.add(downloadListener);
        this.mapServerClient = mapServerClient;
        this.mapPropertiesName = mapPropertiesName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected MapProperties doInBackground(final Void... params) {
        final MapProperties result = new MapProperties();
        try {
            final MapPropertiesResponse response = mapServerClient.downloadMapProperties(mapPropertiesName);
            result.putAll(response.getMapProperties());
            success = true;
            onPostExecute(result); // has to be invoked manually since the UI thread is blocked with get()
        } catch (final IOException e) {
            Log.e(TAG, "Couldn't connect to the server. Reverting to default properties.");
            success = false;
        } catch (final InvioException e) {
            Log.e(TAG, "Couldn't download the map properties. Reverting to default properties.");
            success = false;
        }

        return result;
    }
}
