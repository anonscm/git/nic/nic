package de.tarent.invio.android.base.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.response.MapListResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the AsyncTask which fetches the list of maps from the server, in the background. After it has finished
 * it will add the names to our list-adapter.
 */
public class GetMapListTask extends AsyncTask {

    /**
     * The tag for log-messages from this class:
     */
    private static final String TAG = "GetMapListTask";

    /**
     * In this list we will store the map-names that we got from the server. This will happen in the background.
     * Then, onPostExecute will be called automatically, from the main-thread, and then the mapList will be applied.
     */
    protected List<String> mapList = new ArrayList<String>();

    private final MapServerClient mapServerClient;

    private ProgressDialog progressDialog;

    private ArrayAdapter<String> adapter;

    /**
     * Constructor.
     *
     * @param mapServerClient the {@link MapServerClient}
     * @param progressDialog the {@link #progressDialog}
     * @param adapter        the {@link #adapter}
     */
    public GetMapListTask(final MapServerClient mapServerClient, final ProgressDialog progressDialog,
                          final ArrayAdapter<String> adapter) {

        this.mapServerClient = mapServerClient;
        this.progressDialog = progressDialog;
        this.adapter = adapter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object doInBackground(final Object... params) {
        try {
            final MapListResponse response = mapServerClient.getMapList(5000);
            mapList = response.getMapList();
        } catch (IOException e) {
            Log.e(TAG, "Failed to download the map-list: " + e);
        }
        progressDialog.dismiss();

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPostExecute(final Object result) {
        adapter.addAll(mapList);
        adapter.notifyDataSetChanged();
    }

    /**
     * Scan a list of map names and identify those that represent different levels of the same building.
     * Add virtual multilevel map names to the list.
     * @param mapList   the list of simple map-names, as it comes from the server
     */

}
