package de.tarent.invio.android.base.wrapper;

import java.util.Locale;

/**
 * Wrapper class for the {@link Locale} class for easier testing.
 *
 * @author Désirée Amling <d.amling@tarent.de>
 */
public class LocaleWrapper {

    /**
     * Default constructor.
     */
    public LocaleWrapper() {}

    /**
     * Returns the device's {@link Locale}.
     *
     * @return the device's {@link Locale}
     */
    public Locale getDefaultLocale() {
        return Locale.getDefault();
    }
}
