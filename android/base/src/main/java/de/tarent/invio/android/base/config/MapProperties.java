package de.tarent.invio.android.base.config;

import java.util.Properties;

/**
 * Properties class for the map.
 */
public class MapProperties extends Properties {
    private static final String GROUP_NAME_KEY = "namespace_group_name";
    private static final String SHORT_NAME_KEY = "namespace_short_name";
    private static final String UPLOAD_LOCATION_KEY = "upload_location";

    /**
     * Gets the group name from the properties.
     *
     * @return the group name
     */
    public String getGroupName() {
        return getProperty(GROUP_NAME_KEY);
    }

    /**
     * Sets the property: group name.
     *
     * @param value the group name
     */
    public void setGroupName(final String value) {
        put(GROUP_NAME_KEY, value);
    }

    /**
     * Gets the short name from the properties.
     *
     * @return the short name
     */
    public String getShortName() {
        return getProperty(SHORT_NAME_KEY);
    }

    /**
     * Sets the property: short name.
     *
     * @param value the value to set
     */
    public void setShortName(final String value) {
        put(SHORT_NAME_KEY, value);
    }

    /**
     * Gets the upload location from the properties.
     *
     * @return the upload location
     */
    public String getUploadLocation() {
        return getProperty(UPLOAD_LOCATION_KEY);
    }

    /**
     * Sets the property: upload location.
     *
     * @param value the value
     */
    public void setUploadLocation(final String value) {
        put(UPLOAD_LOCATION_KEY, value);
    }

    /**
     * Checks if every property value was set. If one or more return null, the method returns false.
     *
     * @return returns false if one or more properties returns null
     */
    public boolean allPropertiesSet() {
        if (getGroupName() == null || getShortName() == null || getUploadLocation() == null) {
            return false;
        }

        return true;
    }
}
