package de.tarent.invio.android.base.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import de.tarent.invio.android.base.position.InvioGeoPointImpl;
import de.tarent.invio.entities.InvioGeoPoint;

import java.lang.reflect.Type;

/**
 * This is a simple deserializer for our InvioGeoPoint-interface which will be called by gson as needed.
 */
public class InvioGeoPointDeserializer implements JsonDeserializer<InvioGeoPoint> { // NOSONAR, gson wants instances.
    @Override
    public InvioGeoPoint deserialize(JsonElement jsonElement,
                                   Type type,
                                   JsonDeserializationContext jsonDeserializationContext) {

        final JsonObject jsonObject = jsonElement.getAsJsonObject();
        return new InvioGeoPointImpl(jsonObject.get("mLatitudeE6").getAsInt(),
                jsonObject.get("mLongitudeE6").getAsInt());
    }
}