package de.tarent.invio.android.base.wifi;

/**
 * Callback for the capturing of fingerprints.
 */
public interface CaptureCallback {

    /**
     * Called when the capturing is started.
     */
    void onCaptureStart();

    /**
     * Called when each new capture iteration starts.
     *
     * @param iteration the current iteration
     * @param maxIterations the maximum number of iterations
     */
    void onCaptureIteration(final int iteration, final int maxIterations);

    /**
     * Called when the capturing is stopped.
     */
    void onCaptureStop();

}
