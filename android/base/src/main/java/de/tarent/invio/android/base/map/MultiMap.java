package de.tarent.invio.android.base.map;

import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.view.Display;
import android.widget.Toast;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import de.tarent.invio.android.base.AbstractMapActivity;
import de.tarent.invio.android.base.R;
import de.tarent.invio.android.base.json.InvioGeoPointDeserializer;
import de.tarent.invio.android.base.mapmatching.WayManager;
import de.tarent.invio.android.base.task.DownloadListener;
import de.tarent.invio.android.base.task.DownloadTask;
import de.tarent.invio.android.base.task.InvioOsmParser;
import de.tarent.invio.android.base.task.ZipFingerprintsTask;
import de.tarent.invio.android.base.task.ZipMapDataTask;
import de.tarent.invio.android.base.task.ZipMapResourceTask;
import de.tarent.invio.android.base.tileprovider.TileProviderFactory;
import de.tarent.invio.android.base.wifi.FingerprintManager;
import de.tarent.invio.android.base.wifi.UserLocator;
import de.tarent.invio.android.base.wifi.WifiCapturer;
import de.tarent.invio.entities.Fingerprint;
import de.tarent.invio.entities.Histogram;
import de.tarent.invio.entities.InvioGeoPoint;
import de.tarent.invio.tracker.wifi.HistogramConsumer;
import de.tarent.invio.tracker.wifi.RouterLevelHistogram;
import de.tarent.invio.tracker.wifi.divergence.KullbackLeibler;
import de.tarent.invio.tracker.wifi.divergence.RouterLevelDivergence;
import org.osmdroid.tileprovider.MapTileProviderBase;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.views.MapView;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static de.tarent.invio.android.base.map.MapParams.setLevel;

/**
 * A MultiMap bundles one or more instances of IndoorMap together to form a multi-level building.
 */
public class MultiMap implements DownloadListener, HistogramConsumer {

    private static final int DOWNLOADS_PER_MAP = 3;

    protected File groupDir;

    protected AbstractMapActivity activity;

    protected MapView mapView;

    protected Map<String, IndoorMap> maps;

    protected List<IndoorMap> mapsList;

    IndoorMap userSelectedMap;

    private MapTileProviderBase provider;

    private int successfulPartDownloads = 0;

    private int partDownloads = 0;

    private WifiCapturer wifiCapturer;

    /**
     * Constructor.
     *
     * @param groupDir directory of the unzipped group data file
     * @param activity the calling activity for context
     * @param mapView the corresponding mapview
     */
    public MultiMap(final File groupDir, final AbstractMapActivity activity, final MapView mapView) {
        this.groupDir = groupDir;
        this.activity = activity;
        this.mapView = mapView;
        setLevel(0);
        createMaps();
    }

    /**
     * Detaches the created provider. The calling activity must call this method on every onPause() to avoid leaks.
     */
    public void detach(){
        if(provider != null) {
            provider.detach();
        }
    }

    public int getLevel() {
        return MapParams.getLevel();
    }

    /**
     * Create indoor maps per directory inside the unzipped group data directory.
     */
    private void createMaps() {
        mapsList = new ArrayList<IndoorMap>();
        if(groupDir.exists() && groupDir.isDirectory()) {
            final File[] mapDirs = groupDir.listFiles();
            for(int i = 0; i < mapDirs.length; i++) {
                final IndoorMap map = new IndoorMap(mapDirs[i]);
                mapsList.add(map);
            }

            for(IndoorMap map : mapsList) {
                final ZipMapDataTask mapDataTask = new ZipMapDataTask(map, map.getMapDirectory(), new InvioOsmParser());
                mapDataTask.addDownloadListener(this);
                mapDataTask.execute();
                final ZipMapResourceTask mapResourceTask = new ZipMapResourceTask(this, map);
                mapResourceTask.execute();
                final ZipFingerprintsTask fingerprintsTask = new ZipFingerprintsTask(map, map);
                fingerprintsTask.addDownloadListener(this);
                fingerprintsTask.execute();
            }
        }
    }

    /**
     * Switches the map in the UI and sets all data needed for tracking (fingerprints, ways, scale ..)
     *
     * @param map {@link de.tarent.invio.android.base.map.IndoorMap} to show
     */
    private void switchMapForReal(final IndoorMap map){
        final Handler mainHandler = new Handler(Looper.getMainLooper());
        mainHandler.post(getNewRunnable(map));
    }

    /**
     * Finds the map based on the given short name.
     *
     * @param shortName short id of map
     * @return map
     */
    public IndoorMap findMapByName(final String shortName){
        return maps.get(shortName);
    }

    // TODO: strange name...
    private Runnable getNewRunnable(final IndoorMap map) {
        return new Runnable() {
            @Override
            public void run() {
                initializeBaseAngle(map);
                initializeScale(map);
                initializeFingerprints(map);
                initializeEdges(map);
                configureMapView(map);
                mapView.invalidate();

                activity.setWifiOff(false);
                activity.dismissProgressDialog();
                activity.onMapSwitched(map);
            }
        };
    }

    private void initializeEdges(final IndoorMap map) {
        final WayManager wayManager = AbstractMapActivity.getWayManager();
        if (wayManager != null) {
            wayManager.setEdges(map.getEdges());
        }
    }

    private void initializeFingerprints(final IndoorMap map) {
        final FingerprintManager fingerprintManager = activity.getFingerprintManager();
        if(fingerprintManager != null) {
            fingerprintManager.removeAllFingerprintFromOverlay();
            fingerprintManager.setWifiFingerprintsJson(map.getFingerprintsJson());
        }
    }

    private void initializeScale(final IndoorMap map) {
        final Float scale = map.getScale();
        if(scale != null) {
            final float mapScaleDivisor =
                    (float) activity.getResources().getInteger(R.integer.mapscale_divisor);
            UserLocator.setScale(scale / mapScaleDivisor);
        }
    }

    private void initializeBaseAngle(final IndoorMap map) {
        final Integer angle = map.getBaseAngle();
        if(angle != null) {
            AbstractMapActivity.getDeadReckoning(activity).setBaseAngle(map.getBaseAngle());
        }
    }

    /**
     * Switch to the level of the building on which we currently are. Includes autodetection.
     *
     * @param map {@link de.tarent.invio.android.base.map.IndoorMap} to show
     */
    public void switchMap(IndoorMap map) {
        if (!activity.isWifiOff()) {
            activity.showProgressDialog();
        }
        userSelectedMap = map;
        activity.stopScan();
        // Do a single scan, as if we were tracking:
        wifiCapturer = new WifiCapturer(activity,
                this,
                activity.getResources().getInteger(de.tarent.invio.android.base.R.integer.tracker_scanbuffer_maxage));
        wifiCapturer.startSensors();
    }

    /**
     * Return an {@link IndoorMap} by its short name
     *
     * @param shortName of the map
     * @return the {@link IndoorMap}
     */
    public IndoorMap getMapByShortName(final String shortName) {
        return maps.get(shortName);
    }

    /**
     * Adding a histogram will compare this histogram to all the fingerprints on all levels and then switch to the
     * level that most closely matched the histogram.
     * If this method is called while there are still pending downloads it will do nothing.
     *
     * @param histogram the current histogram
     */
    @Override
    public void addHistogram(final Histogram histogram) {
        wifiCapturer.stopSensors();

        final RouterLevelHistogram rlh = RouterLevelHistogram.makeAverageHistogram(histogram);
        final SortedMap<Float, Integer> divergenceMap = new TreeMap<Float, Integer>();

        for(int i = 0; i < mapsList.size(); i++) {
            calculateLevelDivergence(i, rlh, divergenceMap);
        }

        if (userSelectedMap == null) {
            handleNoUserSelectedMap(divergenceMap);
        } else {
            handleUserSelectedMap(divergenceMap);
        }
    }

    private void handleUserSelectedMap(SortedMap<Float, Integer> divergenceMap) {
        switchMapForReal(userSelectedMap);
        if ((!divergenceMap.isEmpty()) && (userSelectedMap == mapsList.get(findClosestLevel(divergenceMap)))) {
            activity.resetParticleFilterMapchange();
            activity.startScan();
        } else {
            activity.stopScan();
        }
    }

    private void handleNoUserSelectedMap(SortedMap<Float, Integer> divergenceMap) {
        if (divergenceMap.isEmpty()) {
            IndoorMap defaultMap = maps.get(activity.getMultilevelDefaultMapShortName());
            if(defaultMap == null) {
                defaultMap = mapsList.get(0);
                Toast.makeText(activity.getApplicationContext(),
                        activity.getString(R.string.default_map_name_not_found), Toast.LENGTH_LONG).show();
            }
            switchMapForReal(defaultMap);
        } else {
            setLevel(findClosestLevel(divergenceMap));
            switchMapForReal(mapsList.get(MapParams.getLevel()));
        }
        activity.resetParticleFilterMapchange();
        activity.startScan();
    }

    /**
     * Calculate the divergence from one histogram to all the fingerprints on one level and add them to a map.
     * @param level the level to analyse
     * @param rlh the current histogram
     * @param divergenceMap map of divergence -> level where we add the data for the level
     */
    private void calculateLevelDivergence(final int level,
                                          final RouterLevelHistogram rlh,
                                          final SortedMap<Float, Integer> divergenceMap) {

        final List<Fingerprint> fingerprints = parseJson(mapsList.get(level).getFingerprintsJson());
        if(fingerprints == null || fingerprints.isEmpty()) {
            return;
        }
        final RouterLevelDivergence divergenceAlgorithm = new KullbackLeibler();

        for (Fingerprint fingerprint : fingerprints) {
            final RouterLevelHistogram fingerprintRlh =
                    RouterLevelHistogram.makeAverageHistogram(fingerprint.getHistogram());
            divergenceAlgorithm.init(rlh, fingerprintRlh);
            // A confidence < 0.4 is not considered relevant enough:
            if (divergenceAlgorithm.getConfidence() > 0.4f) {
                // TODO: in case of identical divergence this will overwrite the old value!!
                divergenceMap.put(divergenceAlgorithm.getDivergence(), level);
            }
        }
    }

    /**
     * Find the level that we most probably are on. At the moment that is the first level to reach 5 fingerprints,
     * counted from the beginning of the divergenceMap.
     * @param divergenceMap a sorted mapping of divergence to level number
     * @return the current level number
     */
    private int findClosestLevel(final SortedMap<Float, Integer> divergenceMap) {
        final Map<Integer, Integer> hitList = new HashMap<Integer, Integer>();


        for (Integer level : divergenceMap.values()) {
            Integer count = hitList.get(level);
            if (count == null) {
                count = 0;
            }
            hitList.put(level, ++count);
            if (count == 5) {
                return level;
            }
        }

        // Too few fingerprints for any level to reach 3!?
        return divergenceMap.get(divergenceMap.firstKey());
    }

    /**
     * Create Fingerprints from json-string.
     *
     * @param json the json that contains the serialized fingerprints
     * @return the List of Fingerprints
     */
    //TODO Refactoring: THIS CODE IS STRICTLY DUPLICATE FROM FingerprintManager
    protected List<Fingerprint> parseJson(final String json) {
        // We need this TypeToken-thingy because it is not possible to have a Class-object for a generic type.
        // But we must tell gson what kind of object it is supposed to create. That's how we can do is:
        final Type genericFingerprintArrayListType = new TypeToken<ArrayList<Fingerprint>>() {
        }.getType();
        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(InvioGeoPoint.class, new InvioGeoPointDeserializer());

        final List<Fingerprint> fingerprints = gsonBuilder.create().fromJson(json, genericFingerprintArrayListType);
        return fingerprints;
    }

    private void configureMapView(final IndoorMap map) {
        mapView.setTileSource(makeTileSource(map));
        mapView.setMinZoomLevel(map.getMinZoomLevel());
        mapView.setMaxZoomLevel(map.getMaxZoomLevel());
        // TODO: does it even make sense to arrive at this place without a boundingbox? Maybe we get called
        //       too early, if that happens? Anyway, the MapView doesn't like "null"...
        final BoundingBoxE6 boundingBox = map.getBoundingBox();
        if (boundingBox != null) {
            mapView.setMinZoomLevel(getZoomLevel(map.getMinZoomLevel()));
            mapView.zoomToBoundingBox(boundingBox);
            mapView.setScrollableAreaLimit(boundingBox);
        }
    }

    private int getZoomLevel(int minZoomLevel) {
        final Display display = activity.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        int screenSize = Math.min(size.x, size.y);
        int zoomLvl = 1; // added 1 -> map does not fit completely into display, but nicer this way

        while ( screenSize > 512 ) {
            screenSize = screenSize/2;
            zoomLvl++;
        }

        return minZoomLevel + zoomLvl;
    }

    /**
     * Get the appropriate tile source for the current level.
     *
     * @return the ITileSource for the current level
     */
    private ITileSource makeTileSource(final IndoorMap map) {
        // TODO: maybe we need to dispose of the old provider/source?
        final TileProviderFactory factory = new TileProviderFactory(activity);
        provider = factory.buildWebTileProvider(
                activity.getResources().getString(de.tarent.invio.android.base.R.string.map_provider_url_schema),
                map.getMapDirectory().getName());
        return provider.getTileSource();
    }


    @Override
    public void onDownloadFinished(DownloadTask task, boolean success, Object data) {
        //Count all part downloads - map data, fingerprints and tilemapresource, no matter if it was successful or not.
        partDownloads++;

        //Then count only the successful downloads
        if(success) {
            if(task instanceof ZipMapDataTask) {
                successfulPartDownloads++;
            } else if(task instanceof ZipMapResourceTask) {
                successfulPartDownloads++;
            } else if(task instanceof ZipFingerprintsTask) {
                successfulPartDownloads++;
            }
        }

        //Inform the user, when some parts are missing!
        //TODO Multilevel: what to do if some fingerprints are missing?
        if(partDownloads == (mapsList.size() * DOWNLOADS_PER_MAP)) {
            if(successfulPartDownloads < partDownloads) {
                //TODO: Some parts are missing! Things may not work properly
                Toast.makeText(activity, "Some parts are missing! Things may not work as expected!", 7000);
            }

            //Configure searchview in order to be able to search for POIs (products or exhibitors)
            activity.configurePOISearch();

            createHashMapFromMapsList();
            switchMap(null);
            //Create the level buttons in the map view
            activity.createLevelButtons(mapsList);
        }
    }

    /**
     * Now that we have downloaded and parsed short names, create a map with short name as a key
     *
     * and {@link IndoorMap} objects as values.
     */
    private void createHashMapFromMapsList() {
        maps = new HashMap<String, IndoorMap>();
        for(final IndoorMap map : mapsList) {
            maps.put(map.getShortName(), map);
        }
    }
}
