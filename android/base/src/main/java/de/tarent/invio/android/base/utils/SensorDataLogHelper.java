package de.tarent.invio.android.base.utils;

import android.hardware.Sensor;
import android.os.Environment;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.tarent.invio.entities.Histogram;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * This class provides static functionality for sensor data logging.
 * It can append histograms to an existing log file, creating it if there is none.
 * Also other sensor data can be dumped here for the same purpose
 */
public final class SensorDataLogHelper {

    public static final String TAG = "SensorDataLogHelper";
    private static boolean isActive = false;//NOSONAR - is used outside of this class and needs to be public
    private static String logDirectory = "invioLogs";
    private static int idCount = 1;

    /**
     * No initialization needed.
     */
    private SensorDataLogHelper() {

    }

    /**
     * Appends the given histogram to a central log file
     *
     * @param histogram the histogram to be logged
     */
    public static void logHistogram(final Histogram histogram) {
        final Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();
        //TODO: All the stuff added to the string should be removed after the parser is modified to work without them
        final String json = "{\"histogram\":"+ gson.toJson(histogram)
                +",\"id\":\"FP-" + idCount + "\",\"point\":{\"divergence\":0.0,\"mAltitude\":0,\"mLatitudeE6\":0,\"mLongitudeE6\":0}" + "},";
        final String fileName = "histogramLog.json";
        writeLog(json, fileName);
        idCount++;
    }

    /**
     * Appends the given sensor data to a central log file
     *
     * @param sensorData float array containing the sensor results
     * @param type       {@link android.hardware.Sensor}-type value determining
     *                   which type of sensor is providing the information
     */
    public static void logSensorData(final float[] sensorData, int type) {
        String logEntry = System.currentTimeMillis()+" "+type;
        for (float measurement : sensorData) {
            logEntry += " "+measurement;
        }
        logEntry+="\n";

        switch (type) {
            case Sensor.TYPE_ACCELEROMETER:
                writeLog(logEntry, "accelerometerLog.txt");
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                writeLog(logEntry, "linearAccelerationLog.txt");
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                writeLog(logEntry, "magneticFieldLog.txt");
                break;
            default:
                break;
        }



    }

    private static void writeLog(String line, String fileName) {//NOSONAR splitting this up would not improve readability
        final File logDirectory = new File(Environment.getExternalStorageDirectory() + File.separator +
                SensorDataLogHelper.logDirectory);
        final File logFile = new File(Environment.getExternalStorageDirectory() + File.separator +
                SensorDataLogHelper.logDirectory + File.separator + fileName);

        if (!logDirectory.exists()) {
            logDirectory.mkdirs();
        }

        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                Log.e(TAG, "Log file could not be written!");
            }
        }

        try {
            final FileOutputStream out = new FileOutputStream(logFile, true);
            final PrintWriter printWriter = new PrintWriter(out);
            printWriter.print(line);
            printWriter.flush();
            printWriter.close();
            out.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Log file could not be found!");
        } catch (IOException e) {
            Log.e(TAG, "Could not close FileOutputStream to Log File!");
        }
    }

    private static void removeLogs() {
        final File logDirectory = new File(Environment.getExternalStorageDirectory() + File.separator +
                SensorDataLogHelper.logDirectory);
        if (logDirectory.isDirectory()) {
            for (File file : logDirectory.listFiles()) {
                file.delete();
            }
        }
    }

    /**
     * Prepares the logging and marks this Helper as active.
     */
    public static void startLogging() {
        removeLogs();
        writeLog("[", "histogramLog.json");
        setIsActive(true);
    }

    /**
     * Finishes the logging and changes the histogram log so that it can be parsed properly.
     */
    public static void stopLogging() {
        try {
            //Remove the last "," from the histogram Log and place a "]" there so that it can be parsed properly
            final File histogramLog = new File(Environment.getExternalStorageDirectory() + File.separator +
                    SensorDataLogHelper.logDirectory + File.separator + "histogramLog.json");
            final StringBuilder fileContents = new StringBuilder((int)histogramLog.length());
            final Scanner scanner = new Scanner(histogramLog);
            while(scanner.hasNextLine()) {
                fileContents.append(scanner.nextLine());
            }
            String histogramLogString = fileContents.toString();
            histogramLogString = histogramLogString.substring(0, histogramLogString.length()-1)+"]";
            histogramLog.delete();
            writeLog(histogramLogString, "histogramLog.json");
            idCount=1;

        } catch (FileNotFoundException e) {
            Log.e(TAG, "Log file could not be found!");
        }
        setIsActive(false);
    }

    public static boolean isActive() {
        return isActive;
    }

    private static void setIsActive(boolean isActive) {
        SensorDataLogHelper.isActive = isActive;
    }
}
