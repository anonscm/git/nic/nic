package de.tarent.invio.android.base.task;

import android.util.Log;
import de.tarent.invio.android.base.map.IndoorMap;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 */
public class ZipFingerprintsTask extends DownloadTask<Void, Void, String> {

    private static final String TAG = ZipFingerprintsTask.class.getCanonicalName();

    private final IndoorMap indoorMap;


    /**
     * Gets the fingerprints json from the unziped directory.
     *
     * @param listener which will be notified when the work is done
     * @param indoorMap containing fingerprints
     */
    public ZipFingerprintsTask(final DownloadListener listener, final IndoorMap indoorMap) {
        addDownloadListener(listener);
        this.indoorMap = indoorMap;
    }

    @Override
    protected String doInBackground(final Void... params) {
        String jsonString = null;
        try {
            jsonString = getJsonString();
            success = true;
        } catch (IOException e) {
            success = false;
            Log.e(TAG, e.getMessage());
        }
        return jsonString;
    }

    /**
     * Get string from json file.
     *
     * @return fingerprings json string
     * @throws IOException if something went wrong with reading / writing of the file
     */
    protected String getJsonString() throws IOException {
        final File fingerprintsFile = new File(indoorMap.getMapDirectory()
                + File.separator + "fingerprints" + File.separator + "fingerprints_data.json");
        if(fingerprintsFile.exists()) {
            final String result = FileUtils.readFileToString(fingerprintsFile, "UTF-8");
            return result;
        } else {
            throw new FileNotFoundException("ERROR: No fingerprints file found! " +
                    "Was expected here: " + fingerprintsFile.getPath());
        }
    }
}
