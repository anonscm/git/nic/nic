package de.tarent.invio.android.base.position;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import de.tarent.invio.android.base.R;
import de.tarent.invio.android.base.particlefilter.Particle;
import de.tarent.invio.entities.InvioGeoPoint;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.Projection;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * The UserPositionManager holds the {@link UserPositionItem} and
 * {@link org.osmdroid.views.overlay.ItemizedIconOverlay} for the purpose of displaying them.
 */
public class UserPositionManager {

    /**
     * The overlay displays the users position and some additional diagnostic/debug info.
     */
    protected ItemizedIconOverlay<UserPositionItem> overlay;

    private Activity activity;

    private MapView mapView;

    private SortedSet<InvioGeoPoint> neighbours = new TreeSet<InvioGeoPoint>();

    private SortedSet<InvioGeoPoint> outliers = new TreeSet<InvioGeoPoint>();

    private SortedSet<InvioGeoPoint> neighboursWithOutliers;

    private List<Particle> particles = new ArrayList<Particle>();


    /**
     * Constructs a new UserPositionManager.
     *
     * @param activity the parent {@link android.app.Activity}, from which we need the
     *  {@link android.content.Context} and {@link android.app.FragmentManager}
     * @param mapView the {@link org.osmdroid.views.MapView}
     */
    public UserPositionManager(final Activity activity, final MapView mapView) {
        this.activity = activity;
        this.mapView = mapView;
        final Bitmap bm = BitmapFactory.decodeResource(activity.getResources(), R.drawable.user_position_icon);
        final Drawable icon = new BitmapDrawable(activity.getResources(), bm);
        overlay = new UserPositionOverlay(icon);
        mapView.getOverlays().add(overlay);
    }


    /**
     * Constructs a new UserPositionManager.
     *
     * @param activity the parent {@link android.app.Activity}, from which we need the
     *  {@link android.content.Context} and {@link android.app.FragmentManager}
     * @param mapView the {@link org.osmdroid.views.MapView}
     * @param userPositionItem the {@link UserPositionItem} to be placed on the overlay.
     */
    public UserPositionManager(final Activity activity, final MapView mapView,
                               final UserPositionItem userPositionItem) {
        this(activity, mapView);
        overlay.addItem(userPositionItem);
    }


    /**
     * Set the actual neighbours and calculate the outliers, which are the neighboursWithOutliers minus the neighbours.
     *
     * @param neighbours the neighbouring points; the first one is special because it will be this one which has the
     *                   neighbour-lines attached to it.
     */
    public void setNeighbours(final SortedSet<InvioGeoPoint> neighbours) {
        this.neighbours = neighbours;
        if ( neighbours != null ) {
            outliers = new TreeSet<InvioGeoPoint>(neighboursWithOutliers);
            outliers.removeAll(neighbours);
        }
    }

    public void setNeighboursWithOutliers(final SortedSet<InvioGeoPoint> neighboursWithOutliers) {
        this.neighboursWithOutliers = neighboursWithOutliers;
    }

    public void setParticles(List<Particle> particles) {
        this.particles = new ArrayList(particles);
    }

    public List<Particle> getParticles() {
        return particles;
    }

    /**
     * Exchange the current UserPositionItems with a new ones. The first one will be treated as a special one, because
     * it is this which will have the lines to its fingerprint neighbours. Therefore you should use the first item as
     * the wifi-only-item.
     *
     * @param userPositionItems the new positions
     */
    public void updateItems(final List<UserPositionItem> userPositionItems) {
        overlay.removeAllItems();
        overlay.addItems(userPositionItems);
        mapView.postInvalidate();
    }

    /**
     * This method removes the position-icon, i.e. makes the whole layer invisible.
     */
    public void disablePosition() {
        overlay.removeAllItems();
        mapView.postInvalidate();
    }

    public SortedSet<InvioGeoPoint> getNeighbours() {
        return neighbours;
    }

    public SortedSet<InvioGeoPoint> getOutliers() {
        return outliers;
    }

    /**
     * Gets center of bounding box. We use the "scrollable area" for this, not what the MapView calls bounding box,
     * because we set the scrollable area ourselves, to the correct value, whereas the bounding box is calculated
     * in some strange way that we do not understand.
     * 
     * @return the center of the bounding box
     */
    public InvioGeoPoint getBoundingBoxCenter() {
        final BoundingBoxE6 boundingBox = mapView.getScrollableAreaLimit();
        InvioGeoPoint center = null;
        if (boundingBox != null) {
            final IGeoPoint originalCenter = boundingBox.getCenter();
            center = new InvioGeoPointImpl(originalCenter);
        }
        return center;
    }

    /**
     * The UserPositionOverlay displays the user's position and some additional diagnostic/debug info.
     * TODO: the ItemizedIconOverlay is probably not the best type of overlay for this usecase. The userposition is not
     *       an immutable item and it doesn't need to be clicked, etc. A custom Overlay might be more fitting.
     */
    public class UserPositionOverlay extends ItemizedIconOverlay<UserPositionItem> {

        /**
         * Construct a new UserPositionOverlay with the specific icon for the user.
         *
         * @param icon some icon (e.g. BitmapDrawable)
         */
        public UserPositionOverlay(final Drawable icon) {
            super(new ArrayList<UserPositionItem>(), icon, null, new DefaultResourceProxyImpl(activity));
        }

        @Override
        protected void draw(final Canvas canvas, final MapView mapView, final boolean shadow) {
            if (size() > 0) {
                if (shadow) {
                    if (activity.getClass().getCanonicalName().equals("de.tarent.invio.android.admin.MapActivity")) {
                        drawLinesBetweenUserPositionAndNeighbours(Color.BLUE, neighbours, canvas);
                        drawLinesBetweenUserPositionAndNeighbours(Color.RED, outliers, canvas);
                        drawParticles(Color.RED, canvas);
                    }
                } else {
                    super.draw(canvas, mapView, shadow);
                }
            }

        }


        private void drawLinesBetweenUserPositionAndNeighbours(final int color, final SortedSet<InvioGeoPoint> neighbours,
                                                               final Canvas canvas) {
            final Projection projection = mapView.getProjection();
            final Point userPoint = projection.toPixels(getItem(0).getGeoPoint(), null);
            final Paint sp = new Paint();
            sp.setColor(color);
            sp.setStrokeWidth(2);

            if (neighbours != null && neighbours.size() > 0) {
                for (InvioGeoPoint neighbour : neighbours) {
                    final Point fingerprintPoint = projection.toPixels((IGeoPoint)neighbour, null);
                    canvas.drawLine(userPoint.x, userPoint.y, fingerprintPoint.x, fingerprintPoint.y, sp);
                }
            }
        }

        private void drawParticles(final int color, final Canvas canvas) {
            final Projection projection = mapView.getProjection();
            final Paint sp = new Paint();
            sp.setColor(color);
            sp.setStrokeWidth(4);

            if (particles != null && particles.size() > 0) {
                for (Particle particle : particles) {
                    final InvioGeoPoint geoParticle = new InvioGeoPointImpl();
                    geoParticle.setXY(particle.getX(), particle.getY());
                    final Point particlePoint = projection.toPixels((IGeoPoint)geoParticle, null);
                    canvas.drawPoint(particlePoint.x, particlePoint.y, sp);
                }
            }
        }
    }

}
