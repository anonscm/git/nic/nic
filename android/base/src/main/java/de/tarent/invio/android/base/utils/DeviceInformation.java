package de.tarent.invio.android.base.utils;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Class that for now generates the device id.
 */
public class DeviceInformation {

    private static final String TAG = DeviceInformation.class.getCanonicalName();

    private final Context context;

    /**
     * Constructs a new {@link de.tarent.invio.android.base.utils.DeviceInformation} class.
     *
     * @param context the application context
     */
    public DeviceInformation(final Context context) {
        this.context = context;
    }

    /**
     * Here we try to produce a unique id for each device combining several identifiers into one string:
     * <ul>
     *     <li>androidID - can be null without Google account</li>
     *     <li>buildSerial - can be null or redundant</li>
     *     <li>wifi mac address - can be null if wifi is off</li>
     * </ul>
     * As you can see some cases the combined ID can be null.
     *
     * @return combined device ID or null if all identifiers have returned null.
     */
    public String getCombinedDeviceIdAsSHA1String() {
        final String androidID = Settings.Secure.getString(context.getContentResolver(),  Settings.Secure.ANDROID_ID);
        final String buildSerial = android.os.Build.SERIAL;
        final WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        final String wifiMacAddress =  wifiManager.getConnectionInfo().getMacAddress();

        final StringBuilder result = new StringBuilder();
        if (androidID != null) {
            result.append(androidID);
        }
        if (buildSerial != null) {
            result.append(buildSerial);
        }
        if (wifiMacAddress != null) {
            result.append(wifiMacAddress.replaceAll(":", "_"));
        }

        //If sha1 was not null, then return this as a result. 
        final String sha1 = generateSHA1String(result.toString());
        if (sha1 != null) {
            return sha1;
        }

        return result.toString();
    }

    /**
     * Produce a SHA1 string from string input.
     *
     * @param input string as an input
     * @return SHA1 string
     */
    private String generateSHA1String (final String input) {
        MessageDigest digest;
        byte[] data;
        try {
            digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            data = digest.digest(input.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "No such algorithm: SHA-1.");
            return null;
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "UTF-8 is not supported.");
            return null;
        }

        final String result = String.format("%0" + (data.length*2) + "X", new BigInteger(1, data));
        return result;
    }
}
