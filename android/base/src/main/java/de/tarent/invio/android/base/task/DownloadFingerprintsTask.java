package de.tarent.invio.android.base.task;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;
import de.tarent.invio.android.base.R;
import de.tarent.invio.android.base.wifi.FingerprintManager;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.exception.InvioException;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;


/**
 * This is the AsyncTask which downloads the fingerprints from the server, in the background.
 */
public class DownloadFingerprintsTask extends DownloadTask<Void, Void, Void> {

    public static final String TAG = "DownloadFingerprintsTask";

    private static final int FINGERPRINT_WIFI = 0;
    private static final int FINGERPRINT_BLUETOOTH = 1;

    protected final String mapName;

    protected final MapServerClient mapServerClient;

    private FingerprintManager fingerprintManager;

    private Activity activity;

    /**
     * Construct a new DownloadFingerprintsTask, which belongs to a specific FingerprintManager
     *
     * @param fingerprintManager the owner of this task
     * @param activity           the activity, to get the R and for UI-access
     * @param mapServerClient    the client which should be used for talking to the map-server
     * @param mapName            the name of the map for which we shall download the fingerprints
     */
    public DownloadFingerprintsTask(final FingerprintManager fingerprintManager,
                                    final Activity activity,
                                    final MapServerClient mapServerClient,
                                    final String mapName) {
        this.fingerprintManager = fingerprintManager;
        this.activity = activity;
        this.mapServerClient = mapServerClient;
        this.mapName = mapName;
    }

    @Override
    protected Void doInBackground(final Void... params) {
        try {
            fingerprintManager.setWifiFingerprintsJson(
                    inputStreamToString(getWifiFingerprintInputStream()));
            fingerprintManager.setBluetoothLEFingerprintsJson(
                    inputStreamToString(getBluetoothLEFingerprintInputStream()));
            success = true;
        } catch (final UnsupportedEncodingException e) {
            success = false;
            Log.e(TAG, "Unsupported encoding " + e);
        } catch (final FileNotFoundException e) {
            success = false;
            Log.e(TAG, "File not found " + e);
        }

        showToast();
        return null;
    }

    private String inputStreamToString(final InputStream inputStream) {
        final StringWriter stringWriter = new StringWriter();
        try {
            IOUtils.copy(inputStream, stringWriter, "UTF-8");
        } catch (IOException e) {
            success = false;
            Log.e(TAG, "Failed to read fingerprints: " + e);
        }
        return stringWriter.toString();
    }


    private void showToast() {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                final String msg = activity.getString(R.string.toast_localisation_started);
                Toast.makeText(activity,
                        msg,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    // TODO: give notification in case of failure. Don't just log the errors. Do something :-)
    private String downloadFingerprints(final int type) {
        String json = "[]";
        try {
            if (type == FINGERPRINT_BLUETOOTH) {
                json = mapServerClient.downloadBluetoothLEFingerprintsData(mapName);
            } else {
                json = mapServerClient.downloadWifiFingerprintsData(mapName);
            }
            success = true;
        } catch (InvioException e) {
            success = false;
            Log.e(TAG, "Failed to download fingerprints: " + e);
        } catch (IOException e) {
            success = false;
            Log.e(TAG, "Failed to download fingerprints: " + e);
        }

        return json;
    }

    /**
     * Get the {@link InputStream} with the wifi fingerprints from somewhere. In this case: from the mapserver.
     * But you can override this method to get it from somewhere else.
     *
     * @return the InputStream from which the fingerprints can be read.
     * @throws UnsupportedEncodingException when the given encoding is invalid
     * @throws FileNotFoundException        when overwriting methods cannot find the existing fingerprints data file
     */
    protected InputStream getWifiFingerprintInputStream() throws UnsupportedEncodingException, FileNotFoundException {
        final String json = downloadFingerprints(FINGERPRINT_WIFI);
        final InputStream stream = new ByteArrayInputStream(json.getBytes("UTF-8"));
        return stream;
    }

    /**
     * Get the {@link InputStream} with the bluetooth le fingerprints from somewhere. In this case: from the mapserver.
     * But you can override this method to get it from somewhere else.
     *
     * @return the InputStream from which the fingerprints can be read.
     * @throws UnsupportedEncodingException when the given encoding is invalid
     * @throws FileNotFoundException        when overwriting methods cannot find the existing fingerprints data file
     */
    protected InputStream getBluetoothLEFingerprintInputStream()
            throws UnsupportedEncodingException, FileNotFoundException {
        final String json = downloadFingerprints(FINGERPRINT_BLUETOOTH);
        final InputStream stream = new ByteArrayInputStream(json.getBytes("UTF-8"));
        return stream;
    }

}
