package de.tarent.invio.android.base.map;

/**
 * This class provides static information needed across various parts of the application concerning map properties.
 */
public final class MapParams {

    static int level;
    private static MapParams instance = new MapParams();

    private MapParams() {
    }

    public static MapParams getInstance() {
        return instance;
    }

    public static int getLevel() {
        return level;
    }

    public static void setLevel(int level) {
        MapParams.level = level;
    }
}
