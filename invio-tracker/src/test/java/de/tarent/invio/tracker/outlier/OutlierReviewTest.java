package de.tarent.invio.tracker.outlier;

import de.tarent.invio.entities.InvioGeoPoint;
import de.tarent.invio.tracker.exception.InvioTrackerException;
import de.tarent.invio.tracker.geopoint.XYPoint;
import de.tarent.invio.tracker.geopoint.XYPointFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;


public class OutlierReviewTest {

    private CentroidMedianEliminator cme;

    @Before
    public void setup() {
        // variable setup configuration
        cme = new CentroidMedianEliminator(new XYPointFactory(), 2);
    }

    @Test
    public void test1() throws InvioTrackerException {
        Set<InvioGeoPoint> points = new HashSet<InvioGeoPoint>();
        points.add(createXYPoint(498651.86,	5646389.29));
        points.add(createXYPoint(498656.88,	5646395.19));
        points.add(createXYPoint(498652.17,	5646398.64));
        points.add(createXYPoint(498651.45,	5646400.98));
        points.add(createXYPoint(498657.63,	5646382.84));

        Set<InvioGeoPoint> outlier = new HashSet<InvioGeoPoint>(points);
        outlier.removeAll(cme.removeOutliers(points));
        System.out.println("1 reduced set: "+outlier);
    }

    @Test
    public void test2() throws InvioTrackerException {
        Set<InvioGeoPoint> points = new HashSet<InvioGeoPoint>();
        points.add(createXYPoint(498651.86,5646389.29));
        points.add(createXYPoint(498656.88,5646395.19));
        points.add(createXYPoint(498657.63,5646382.84));
        points.add(createXYPoint(498661.42,5646389.07));
        points.add(createXYPoint(498651.45,5646400.98));

        Set<InvioGeoPoint> outlier = new HashSet<InvioGeoPoint>(points);
        outlier.removeAll(cme.removeOutliers(points));
        System.out.println("2 reduced set: "+outlier);
    }

    @Test
    public void test3() throws InvioTrackerException {
        Set<InvioGeoPoint> points = new HashSet<InvioGeoPoint>();
        points.add(createXYPoint(498651.86,5646389.29));
        points.add(createXYPoint(498656.88,5646395.19));
        points.add(createXYPoint(498657.63,5646382.84));
        points.add(createXYPoint(498652.17,5646398.64));
        points.add(createXYPoint(498663.47,5646377.16));

        Set<InvioGeoPoint> outlier = new HashSet<InvioGeoPoint>(points);
        outlier.removeAll(cme.removeOutliers(points));
        System.out.println("3 reduced set: "+outlier);
    }

    @Test
    public void test4() throws InvioTrackerException {
        Set<InvioGeoPoint> points = new HashSet<InvioGeoPoint>();
        points.add(createXYPoint(498651.86,5646389.29));
        points.add(createXYPoint(498656.88,5646395.19));
        points.add(createXYPoint(498657.63,5646382.84));
        points.add(createXYPoint(498661.42,5646389.07));
        points.add(createXYPoint(498654.0, 5646376.49));

        Set<InvioGeoPoint> outlier = new HashSet<InvioGeoPoint>(points);
        outlier.removeAll(cme.removeOutliers(points));
        System.out.println("4 reduced set: "+outlier);
    }


    @Test
    public void test5() throws InvioTrackerException {
        Set<InvioGeoPoint> points = new HashSet<InvioGeoPoint>();
        points.add(createXYPoint(498651.86,5646389.29));
        points.add(createXYPoint(498651.45,5646400.98));
        points.add(createXYPoint(498656.88,5646395.19));
        points.add(createXYPoint(498657.63,5646382.84));
        points.add(createXYPoint(498660.6,5646411));

        Set<InvioGeoPoint> outlier = new HashSet<InvioGeoPoint>(points);
        outlier.removeAll(cme.removeOutliers(points));
        System.out.println("5 reduced set: "+outlier);
        System.out.println("5 original   : "+points);
    }



    private InvioGeoPoint createXYPoint(double x, double y){
        InvioGeoPoint point = new XYPoint();
        point.setXY(x,y);
        return point;
    }
}
