package de.tarent.invio.tracker.geopoint;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

public class XYPointTest {

    @Test
    public void testThatEqualsIsWorkingCorrectly() {
        XYPoint pointA = new XYPoint(2, 2);
        XYPoint pointB = null;

        assertFalse(pointA.equals(pointB));
        assertTrue(pointA.equals(pointA));
        assertFalse(pointA.equals(new Integer(123)));

        pointB = new XYPoint(2, 1);
        assertFalse(pointA.equals(pointB));

        pointB = new XYPoint(1, 2);
        assertFalse(pointA.equals(pointB));

        pointB = new XYPoint(2, 2);
        XYPoint pointBSpy = spy(pointB);
        doReturn(pointA.getDivergence() + 1).when(pointBSpy).getDivergence();
        assertFalse(pointA.equals(pointBSpy));

        assertTrue(pointA.equals(pointB));
    }

    @Test
    public void testCompareTo() {
        final XYPoint pointA = new XYPoint();
        final XYPoint pointB = new XYPoint();

        // Test the same point
        assertEquals(0, pointA.compareTo(pointB));

        // Test that pointA has larger values than pointB, thus comes first.
        pointA.setXY(5, 5);
        assertEquals(1, pointA.compareTo(pointB));

        // Test that pointB has larger values than pointA, thus comes first.
        pointB.setXY(20, 20);
        assertEquals(-1, pointA.compareTo(pointB));
    }
}