package de.tarent.invio.tracker.outlier;

import de.tarent.invio.entities.InvioGeoPoint;
import de.tarent.invio.tracker.exception.InvioTrackerException;
import de.tarent.invio.tracker.geopoint.XYPointFactory;
import de.tarent.invio.tracker.outlier.fixtures.CandidateFixtures;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * @author Désirée Amling <d.amling@tarent.de>
 */
public class DoubleCentroidMedianEliminatorTest {

    private DoubleCentroidMedianEliminator dcme;

    @Before
    public void setup() {
        dcme = new DoubleCentroidMedianEliminator(new XYPointFactory());
    }

    @Test
    public void testThatNoPointIsEliminatedWhenTheTotalAmountOfPointsIsToSmall() throws InvioTrackerException {
        final Set<InvioGeoPoint> geoPointsIn = CandidateFixtures.createCandidatesTooSmallAmount();

        final Set<InvioGeoPoint> expected = CandidateFixtures.createCandidatesTooSmallAmountExpected();

        final Set<InvioGeoPoint> geoPointsOut = dcme.removeOutliers(geoPointsIn);

        assertEquals(expected, geoPointsOut);
    }

    @Test
    public void testThatTheOutlierIsRemovedWhenItIsObviousThatItIsAnOutlierWithVaryingGeoPoints() throws InvioTrackerException {
        final Set<InvioGeoPoint> geoPointsIn = CandidateFixtures.createCandidatesWithAnObviousOutlier();

        final Set<InvioGeoPoint> expected = CandidateFixtures.createCandidatesWithAnObviousOutlierExpected();

        final Set<InvioGeoPoint> geoPointsOut = dcme.removeOutliers(geoPointsIn);

        assertEquals(expected, geoPointsOut);
    }

    @Test
    public void testThatTheOutlierIsRemovedWhenItIsObviousThatItIsAnOutlierWithTheOutlierAddedInTheMiddleOfTheMap() throws InvioTrackerException {
        final Set<InvioGeoPoint> geoPointsIn = CandidateFixtures.createCandidatesWithAnObviousOutlierInTheMiddleOfTheMap();

        final Set<InvioGeoPoint> expected = CandidateFixtures.createCandidatesWithAnObviousOutlierInTheMiddleOfTheMapExpected();

        final Set<InvioGeoPoint> geoPointsOut = dcme.removeOutliers(geoPointsIn);

        assertEquals(expected, geoPointsOut);
    }

    @Test
    public void testThatGeoPointsPlacedInASquareHaveNoOutliers() throws InvioTrackerException {
        final Set<InvioGeoPoint> geoPointsIn = CandidateFixtures.createCandidatesPlacedInASquare();

        final Set<InvioGeoPoint> expected = CandidateFixtures.createCandidatesPlacedInASquareExpected();

        final Set<InvioGeoPoint> geoPointsOut = dcme.removeOutliers(geoPointsIn);

        assertEquals(expected, geoPointsOut);
    }

    @Test
    public void testThatFourGeoPointsPlacedInASquareAndOnePlacedInTheMiddleHaveNoOutliers() throws InvioTrackerException {
        final Set<InvioGeoPoint> geoPointsIn = CandidateFixtures.createCandidatesPlacedInASquareAndOnePlacedInTheMiddle();

        final Set<InvioGeoPoint> expected = CandidateFixtures.createCandidatesPlacedInASquareAndOnePlacedInTheMiddleExpected();

        final Set<InvioGeoPoint> geoPointsOut = dcme.removeOutliers(geoPointsIn);

        assertEquals(expected, geoPointsOut);
    }

    @Test
    public void testThatAnOutlierFarAwayFromGeoPointsPlacedInASquareIsDetectedProperly() throws InvioTrackerException {
        final Set<InvioGeoPoint> geoPointsIn = CandidateFixtures.createCandidatesPlacedInASquareAndOnePlacedFarAway();

        final Set<InvioGeoPoint> expected = CandidateFixtures.createCandidatesPlacedInASquareAndOnePlacedFarAwayExpected();

        final Set<InvioGeoPoint> geoPointsOut = dcme.removeOutliers(geoPointsIn);

        assertEquals(expected, geoPointsOut);
    }

    @Test
    public void testRasterCoordinates() throws InvioTrackerException {
        final Set<InvioGeoPoint> geoPointsIn = CandidateFixtures.createCandidatesRasterCoordinates();

        final Set<InvioGeoPoint> expected = CandidateFixtures.createCandidatesRasterCoordinatesExpected();

        final Set<InvioGeoPoint> geoPointsOut = dcme.removeOutliers(geoPointsIn);

        assertEquals(expected, geoPointsOut);
    }

    // TODO: What is tested?
    @Test
    public void test() throws InvioTrackerException {
        final Set<InvioGeoPoint> geoPointsIn = CandidateFixtures.createCandidatesTest();

        final Set<InvioGeoPoint> expected = CandidateFixtures.createCandidatesTestExpected();

        final Set<InvioGeoPoint> geoPointsOut = dcme.removeOutliers(geoPointsIn);

        assertEquals(expected, geoPointsOut);
    }

    @Test
    public void testThat2PointsFarAwayFromAnother2PointsHaveNoOutliers() throws InvioTrackerException {
        final Set<InvioGeoPoint> geoPointsIn = CandidateFixtures.createCandidatesTwoPointsFarAwayFromAnotherTwoPoints();

        final Set<InvioGeoPoint> expected = CandidateFixtures.createCandidatesTwoPointsFarAwayFromAnotherTwoPointsExpected();

        final Set<InvioGeoPoint> geoPointsOut = dcme.removeOutliers(geoPointsIn);

        assertEquals(expected, geoPointsOut);
    }

    @Test
    public void testThat2PointsFarAwayFrom3PointsNearToEachAreDetectedAsOutliers() throws InvioTrackerException {
        final Set<InvioGeoPoint> geoPointsIn = CandidateFixtures.createCandidatesTwoPointsFarAwayFromThreePointsNearToEachOther();

        final Set<InvioGeoPoint> expected = CandidateFixtures.createCandidatesTwoPointsFarAwayFromThreePointsNearToEachOtherExpected();

        final Set<InvioGeoPoint> geoPointsOut = dcme.removeOutliers(geoPointsIn);

        assertEquals(expected, geoPointsOut);
    }

    @Test
    public void testThatOutliersFromARealMapAreDetected() throws InvioTrackerException {
        final Set<InvioGeoPoint> geoPointsIn = CandidateFixtures.createCandidatesOutliersFromARealMap();

        final Set<InvioGeoPoint> expected = CandidateFixtures.createCandidatesOutliersFromARealMapExpected();

        final Set<InvioGeoPoint> geoPointsOut = dcme.removeOutliers(geoPointsIn);

        assertEquals(expected, geoPointsOut);
    }
}
