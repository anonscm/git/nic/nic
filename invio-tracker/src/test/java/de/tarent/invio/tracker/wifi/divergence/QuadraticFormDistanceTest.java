package de.tarent.invio.tracker.wifi.divergence;

import de.tarent.invio.entities.Histogram;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class QuadraticFormDistanceTest {

    private QuadraticFormDistance quadraticFormDistance;

    @Before
    public void setup() {
        quadraticFormDistance = new QuadraticFormDistance();
    }

    @Test
    public void testBothEmpty() {
        Histogram p = new Histogram();
        Histogram q = new Histogram();

        double qfDist = quadraticFormDistance.compareHistograms(p, q);

        assertEquals(0.f, qfDist, 0.0000001f);
    }

    @Test
    public void testOneEmpty() {
        Histogram p = new Histogram();
        Map<Integer, Float> tmpHist = new HashMap<Integer, Float>();
        tmpHist.put(-30, 25.f);
        tmpHist.put(-40, 30.f);
        p.put("00:01:02:03:04:05", tmpHist);
        Histogram q = new Histogram();

        double qfDist = quadraticFormDistance.compareHistograms(p, q);

        assertEquals(0.f, qfDist, 0.0000001f);
    }

    @Test
    public void testIdentity() {
        Histogram p = new Histogram();
        Map<Integer, Float> tmpHist = new HashMap<Integer, Float>();
        tmpHist.put(-30, 25.f);
        tmpHist.put(-40, 30.f);
        p.put("00:01:02:03:04:05", tmpHist);

        double qfDist = quadraticFormDistance.compareHistograms(p, p);

        assertEquals(0.f, qfDist, 0.0000001f);
    }

    @Test
    public void testSimpleHistograms() {
        Histogram p = new Histogram();
        Map<Integer, Float> tmpHist = new HashMap<Integer, Float>();
        tmpHist.put(-20, 25.f);
        tmpHist.put(-40, 30.f);
        p.put("02:00:00:00:00:01", tmpHist);

        Map<Integer, Float> tmpHist2 = new HashMap<Integer, Float>();
        Histogram q = new Histogram();
        tmpHist2.put(-20, 10.f);
        tmpHist2.put(-40, 20.f);
        tmpHist2.put(-80, 10.f);
        q.put("02:00:00:00:00:01", tmpHist2);

        double qfDist = quadraticFormDistance.compareHistograms(p, q);

        assertEquals(23.6f, qfDist, 0.1f);
    }

    @Test
    public void testMultipleHistograms() {
        Histogram p = new Histogram();
        Map<Integer, Float> tmpHist = new HashMap<Integer, Float>();
        tmpHist.put(-20, 25.f);
        tmpHist.put(-40, 30.f);
        p.put("02:00:00:00:00:01", tmpHist);
        Map<Integer, Float> tmpHist2 = new HashMap<Integer, Float>();
        tmpHist2.put(-50, 10.0f);
        tmpHist2.put(-70, 20.0f);
        p.put("00:01:02:03:04:05", tmpHist2);

        Map<Integer, Float> tmpHist3 = new HashMap<Integer, Float>();
        Histogram q = new Histogram();
        tmpHist3.put(-20, 10.f);
        tmpHist3.put(-40, 20.f);
        tmpHist3.put(-80, 10.f);
        q.put("02:00:00:00:00:01", tmpHist3);
        Map<Integer, Float> tmpHist4 = new HashMap<Integer, Float>();
        tmpHist4.put(-50, 10.0f);
        q.put("00:01:02:03:04:05", tmpHist4);

        double qfDist = quadraticFormDistance.compareHistograms(p, q);

        assertEquals(21.8f, qfDist, 0.1f);
    }

    @Test
    public void testCompare() {
        Histogram p = new Histogram();
        Map<Integer, Float> tmpHist = new HashMap<Integer, Float>();
        tmpHist.put(-20, 25.f);
        tmpHist.put(-40, 30.f);
        p.put("02:00:00:00:00:01", tmpHist);

        Map<Integer, Float> tmpHist2 = new HashMap<Integer, Float>();
        Histogram q = new Histogram();
        tmpHist2.put(-20, 10.f);
        tmpHist2.put(-40, 20.f);
        tmpHist2.put(-80, 10.f);
        q.put("02:00:00:00:00:01", tmpHist2);

        Map<Integer, Float> tmpHist3 = new HashMap<Integer, Float>();
        Histogram r = new Histogram();
        tmpHist3.put(-50, 50.f);
        tmpHist3.put(-60, 20.f);
        tmpHist3.put(-70, 30.f);
        r.put("02:00:00:00:00:01", tmpHist3);

        double qfDist = quadraticFormDistance.compareHistograms(p, q);
        assertEquals(23.6f, qfDist, 0.1f);

        double qfDist2 = quadraticFormDistance.compareHistograms(p, r);
        assertEquals(74.5f, qfDist2, 0.1f);

        double qfDist3 = quadraticFormDistance.compareHistograms(q, r);
        assertEquals(68.3f, qfDist3, 0.1f);

        // These proportions are pretty obvious:
        assertTrue(qfDist < qfDist2);
        assertTrue(qfDist < qfDist3);
        assertTrue(qfDist3 < qfDist2);
    }

    @Test
    public void testThatOriginalHistogramsWereNotChangedInCompareHistograms() {
        final Map<Integer, Float> value = new HashMap<Integer, Float>();
        value.put(-10, 10f);

        final Histogram histogramOne = new Histogram("one");
        histogramOne.put("one", value);
        histogramOne.put("two", value);
        histogramOne.put("three", value);
        final Histogram histogramOneExpected = new Histogram("one");
        histogramOneExpected.putAll(histogramOne);

        final Histogram histogramTwo = new Histogram("two");
        histogramTwo.put("two", value);
        histogramTwo.put("three", value);
        histogramTwo.put("four", value);
        final Histogram histogramTwoExpected = new Histogram("two");
        histogramTwoExpected.putAll(histogramTwo);

        quadraticFormDistance.compareHistograms(histogramOne, histogramTwo);

        assertEquals(histogramOneExpected, histogramOne);
        assertEquals(histogramTwoExpected, histogramTwo);
    }
}
