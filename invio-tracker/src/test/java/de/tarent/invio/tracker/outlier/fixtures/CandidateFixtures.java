package de.tarent.invio.tracker.outlier.fixtures;

import de.tarent.invio.entities.InvioGeoPoint;
import de.tarent.invio.tracker.geopoint.XYPoint;

import java.util.HashSet;
import java.util.Set;

public class CandidateFixtures {

    public static Set<InvioGeoPoint> createCandidatesTooSmallAmount() {
        final Set<InvioGeoPoint> geoPointsIn = new HashSet<InvioGeoPoint>();
        geoPointsIn.add(makeGeoPoint(5, 4));
        geoPointsIn.add(makeGeoPoint(16, 15));
        return geoPointsIn;
    }

    public static Set<InvioGeoPoint> createCandidatesTooSmallAmountExpected() {
        final Set<InvioGeoPoint> expected = new HashSet<InvioGeoPoint>();
        expected.add(makeGeoPoint(5, 4));
        expected.add(makeGeoPoint(16, 15));
        return expected;
    }

    public static Set<InvioGeoPoint> createCandidatesWithAnObviousOutlier() {
        final Set<InvioGeoPoint> geoPointsIn = new HashSet<InvioGeoPoint>();
        geoPointsIn.add(makeGeoPoint(5, 4));
        geoPointsIn.add(makeGeoPoint(7, 6));
        geoPointsIn.add(makeGeoPoint(9, 3));
        geoPointsIn.add(makeGeoPoint(16, 15));
        return geoPointsIn;
    }

    public static Set<InvioGeoPoint> createCandidatesWithAnObviousOutlierExpected() {
        final Set<InvioGeoPoint> expected = new HashSet<InvioGeoPoint>();
        expected.add(makeGeoPoint(5, 4));
        expected.add(makeGeoPoint(7, 6));
        expected.add(makeGeoPoint(9, 3));
        return expected;
    }


    public static Set<InvioGeoPoint> createCandidatesWithAnObviousOutlierInTheMiddleOfTheMap() {
        final Set<InvioGeoPoint> geoPointsIn = new HashSet<InvioGeoPoint>();
        geoPointsIn.add(makeGeoPoint(5, 4));
        geoPointsIn.add(makeGeoPoint(9, 3));
        geoPointsIn.add(makeGeoPoint(16, 15));
        geoPointsIn.add(makeGeoPoint(7, 6));
        return geoPointsIn;
    }

    public static Set<InvioGeoPoint> createCandidatesWithAnObviousOutlierInTheMiddleOfTheMapExpected() {
        final Set<InvioGeoPoint> expected = new HashSet<InvioGeoPoint>();
        expected.add(makeGeoPoint(5, 4));
        expected.add(makeGeoPoint(9, 3));
        expected.add(makeGeoPoint(7, 6));
        return expected;
    }


    public static Set<InvioGeoPoint> createCandidatesPlacedInASquare() {
        final Set<InvioGeoPoint> geoPointsIn = new HashSet<InvioGeoPoint>();
        geoPointsIn.add(makeGeoPoint(0, 0));
        geoPointsIn.add(makeGeoPoint(0, 2));
        geoPointsIn.add(makeGeoPoint(2, 0));
        geoPointsIn.add(makeGeoPoint(2, 2));
        return geoPointsIn;
    }

    public static Set<InvioGeoPoint> createCandidatesPlacedInASquareExpected() {
        final Set<InvioGeoPoint> expected = new HashSet<InvioGeoPoint>();
        expected.add(makeGeoPoint(0, 0));
        expected.add(makeGeoPoint(0, 2));
        expected.add(makeGeoPoint(2, 0));
        expected.add(makeGeoPoint(2, 2));
        return expected;
    }


    public static Set<InvioGeoPoint> createCandidatesPlacedInASquareAndOnePlacedInTheMiddle() {
        final Set<InvioGeoPoint> geoPointsIn = new HashSet<InvioGeoPoint>();
        geoPointsIn.add(makeGeoPoint(0, 0));
        geoPointsIn.add(makeGeoPoint(0, 2));
        geoPointsIn.add(makeGeoPoint(2, 0));
        geoPointsIn.add(makeGeoPoint(2, 2));
        geoPointsIn.add(makeGeoPoint(1, 1));
        return geoPointsIn;
    }

    public static Set<InvioGeoPoint> createCandidatesPlacedInASquareAndOnePlacedInTheMiddleExpected() {
        final Set<InvioGeoPoint> expected = new HashSet<InvioGeoPoint>();
        expected.add(makeGeoPoint(0, 0));
        expected.add(makeGeoPoint(0, 2));
        expected.add(makeGeoPoint(2, 0));
        expected.add(makeGeoPoint(2, 2));
        expected.add(makeGeoPoint(1, 1));
        return expected;
    }


    public static Set<InvioGeoPoint> createCandidatesPlacedInASquareAndOnePlacedFarAway() {
        final Set<InvioGeoPoint> geoPointsIn = new HashSet<InvioGeoPoint>();
        geoPointsIn.add(makeGeoPoint(0, 0));
        geoPointsIn.add(makeGeoPoint(0, 2));
        geoPointsIn.add(makeGeoPoint(2, 0));
        geoPointsIn.add(makeGeoPoint(2, 2));
        geoPointsIn.add(makeGeoPoint(15, 15));
        return geoPointsIn;
    }

    public static Set<InvioGeoPoint> createCandidatesPlacedInASquareAndOnePlacedFarAwayExpected() {
        final Set<InvioGeoPoint> expected = new HashSet<InvioGeoPoint>();
        expected.add(makeGeoPoint(0, 0));
        expected.add(makeGeoPoint(0, 2));
        expected.add(makeGeoPoint(2, 0));
        expected.add(makeGeoPoint(2, 2));
        return expected;
    }


    public static Set<InvioGeoPoint> createCandidatesRasterCoordinates() {
        final Set<InvioGeoPoint> geoPointsIn = new HashSet<InvioGeoPoint>();
        geoPointsIn.add(makeGeoPoint(2, 2));
        geoPointsIn.add(makeGeoPoint(2, 4));
        geoPointsIn.add(makeGeoPoint(4, 2));
        geoPointsIn.add(makeGeoPoint(4, 4));
        geoPointsIn.add(makeGeoPoint(6, 2));
        geoPointsIn.add(makeGeoPoint(6, 4));
        geoPointsIn.add(makeGeoPoint(6, 6));
        geoPointsIn.add(makeGeoPoint(2, 6));
        geoPointsIn.add(makeGeoPoint(4, 6));
        geoPointsIn.add(makeGeoPoint(8, 8));
        return geoPointsIn;
    }

    public static Set<InvioGeoPoint> createCandidatesRasterCoordinatesExpected() {
        final Set<InvioGeoPoint> expected = new HashSet<InvioGeoPoint>();
        expected.add(makeGeoPoint(2, 2));
        expected.add(makeGeoPoint(2, 4));
        expected.add(makeGeoPoint(4, 2));
        expected.add(makeGeoPoint(4, 4));
        expected.add(makeGeoPoint(6, 2));
        expected.add(makeGeoPoint(6, 4));
        expected.add(makeGeoPoint(6, 6));
        expected.add(makeGeoPoint(2, 6));
        expected.add(makeGeoPoint(4, 6));
        return expected;
    }


    public static Set<InvioGeoPoint> createCandidatesTest() {
        final Set<InvioGeoPoint> geoPointsIn = new HashSet<InvioGeoPoint>();
        geoPointsIn.add(makeGeoPoint(11, 2));
        geoPointsIn.add(makeGeoPoint(14, 6));
        geoPointsIn.add(makeGeoPoint(17, 2));
        geoPointsIn.add(makeGeoPoint(14, 3));
        geoPointsIn.add(makeGeoPoint(14, 10));
        return geoPointsIn;
    }

    public static Set<InvioGeoPoint> createCandidatesTestExpected() {
        final Set<InvioGeoPoint> expected = new HashSet<InvioGeoPoint>();
        expected.add(makeGeoPoint(11, 2));
        expected.add(makeGeoPoint(14, 6));
        expected.add(makeGeoPoint(17, 2));
        expected.add(makeGeoPoint(14, 3));
        return expected;
    }


    public static Set<InvioGeoPoint> createCandidatesTwoPointsFarAwayFromAnotherTwoPoints() {
        final Set<InvioGeoPoint> geoPointsIn = new HashSet<InvioGeoPoint>();
        geoPointsIn.add(makeGeoPoint(2, 1));
        geoPointsIn.add(makeGeoPoint(4, 1));
        geoPointsIn.add(makeGeoPoint(6, 15));
        geoPointsIn.add(makeGeoPoint(8, 15));
        return geoPointsIn;
    }

    public static Set<InvioGeoPoint> createCandidatesTwoPointsFarAwayFromAnotherTwoPointsExpected() {
        final Set<InvioGeoPoint> expected = new HashSet<InvioGeoPoint>();
        expected.add(makeGeoPoint(2, 1));
        expected.add(makeGeoPoint(4, 1));
        expected.add(makeGeoPoint(6, 15));
        expected.add(makeGeoPoint(8, 15));
        return expected;
    }


    public static Set<InvioGeoPoint> createCandidatesTwoPointsFarAwayFromThreePointsNearToEachOther() {
        final Set<InvioGeoPoint> geoPointsIn = new HashSet<InvioGeoPoint>();
        geoPointsIn.add(makeGeoPoint(10, 1));
        geoPointsIn.add(makeGeoPoint(11, 1));
        geoPointsIn.add(makeGeoPoint(11, 2));
        geoPointsIn.add(makeGeoPoint(1, 15));
        geoPointsIn.add(makeGeoPoint(20, 20));
        return geoPointsIn;
    }

    public static Set<InvioGeoPoint> createCandidatesTwoPointsFarAwayFromThreePointsNearToEachOtherExpected() {
        final Set<InvioGeoPoint> expected = new HashSet<InvioGeoPoint>();
        expected.add(makeGeoPoint(10, 1));
        expected.add(makeGeoPoint(11, 1));
        expected.add(makeGeoPoint(11, 2));
        return expected;
    }


    public static Set<InvioGeoPoint> createCandidatesOutliersFromARealMap() {
        final Set<InvioGeoPoint> geoPointsIn = new HashSet<InvioGeoPoint>();
        geoPointsIn.add(new XYPoint(50722124, 7061466));
        geoPointsIn.add(new XYPoint(50722124, 7061587));
        geoPointsIn.add(new XYPoint(50722033, 7061589));
        geoPointsIn.add(new XYPoint(50722032, 7061460));
        geoPointsIn.add(new XYPoint(50722087, 7061526));
        geoPointsIn.add(new XYPoint(50721550, 7061636));
        geoPointsIn.add(new XYPoint(50721494, 7061554));
        return geoPointsIn;
    }

    public static Set<InvioGeoPoint> createCandidatesOutliersFromARealMapExpected() {
        final Set<InvioGeoPoint> expected = new HashSet<InvioGeoPoint>();
        expected.add(new XYPoint(50722124, 7061466));
        expected.add(new XYPoint(50722124, 7061587));
        expected.add(new XYPoint(50722033, 7061589));
        expected.add(new XYPoint(50722032, 7061460));
        expected.add(new XYPoint(50722087, 7061526));
        return expected;
    }


    private static InvioGeoPoint makeGeoPoint(final double x, final double y) {
        final InvioGeoPoint invioGeoPoint = new XYPoint(0, 0);
        invioGeoPoint.setXY(x, y);

        return invioGeoPoint;
    }
}
