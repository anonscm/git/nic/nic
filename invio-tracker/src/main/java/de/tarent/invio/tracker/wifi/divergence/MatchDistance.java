package de.tarent.invio.tracker.wifi.divergence;

import de.tarent.invio.entities.Histogram;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Implements the match distance for comparing the cumulative histograms of two measurements router by router
 */
public class MatchDistance {

    /**
     * Empty constructor
     */
    public MatchDistance() {

    }

    /**
     * Compares the histograms of the routers found by both, the current signal measurement and a fingerprint
     * @param p current measurement
     * @param q fingerprint measurement
     * @return the normalized sum of the match distances of each router comparison
     */
    public double compareHistograms(final Histogram p, final Histogram q) {

        double sumMatchDist = 0.f;

        // First we get a list of the common keys from p and q.
        final Set commonKeys = Histogram.getCommonKeys(p, q);

        // Secondly we create copies of both histograms so we do not end up changing the originals.
        final Histogram cutP = new Histogram(p);
        final Histogram cutQ = new Histogram(q);

        // Then we remove all the values that are not present in both histograms.
        cutP.keySet().retainAll(commonKeys);

        // for all routers hashmap <string, <dB, weight>>.. get map out of it
        for ( String accessPoint : cutP.keySet() ) {
            // compare histograms for real
            sumMatchDist += computeMatchDistance(cutP.get(accessPoint), cutQ.get(accessPoint)); //NOSONAR
        }

        if (cutP.keySet().size() > 0) {
            sumMatchDist = sumMatchDist/cutP.keySet().size();
        }

        return sumMatchDist;
    }

    private double computeMatchDistance(Map<Integer, Float> p, Map<Integer, Float> q) {
        final Integer[] sortedKeys = getAllSortedKeys(p, q);
        final int size = sortedKeys.length;
        double dist = 0.f;
        float pVal;
        float qVal;
        float pValSum = 0.f;
        float qValSum = 0.f;

        // calculate match distance with formula: sum(|cumH -cumK|)
        for (int i = 0; i < size; i++) {
            pVal = p.containsKey(sortedKeys[i]) ? p.get(sortedKeys[i]) : 0.f;
            qVal = q.containsKey(sortedKeys[i]) ? q.get(sortedKeys[i]) : 0.f;

            // calculate cumulative histogram values
            pValSum += pVal;
            qValSum += qVal;

            dist += Math.abs(pValSum - qValSum);
        }

        return dist;
    }

    private Integer[] getAllSortedKeys(Map<Integer, Float> p, Map<Integer, Float> q) {
        final Set<Integer> allKeys = new HashSet<Integer>();
        allKeys.addAll(p.keySet());
        allKeys.addAll(q.keySet());
        final Integer[] allKeysSort = allKeys.toArray(new Integer[allKeys.size()]);
        Arrays.sort(allKeysSort, Collections.reverseOrder());

        return allKeysSort;
    }

}
