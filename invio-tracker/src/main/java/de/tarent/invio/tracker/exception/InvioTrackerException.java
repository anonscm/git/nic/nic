package de.tarent.invio.tracker.exception;

/**
 * This exception should be used when any exception is thrown while tracking.
 *
 * @author Désirée Amling <d.amling@tarent.de>
 */
public class InvioTrackerException extends Exception {

    /**
     * Default constructor.
     */
    public InvioTrackerException() {
        super();
    }

    /**
     * Constructor.
     *
     * @param message the message to be shown
     */
    public InvioTrackerException(final String message) {
        super(message);
    }

    /**
     * Constructor.
     *
     * @param cause the cause of the exception
     */
    public InvioTrackerException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     *
     * @param message the message to be shown
     * @param cause the cause of the exception
     */
    public InvioTrackerException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
