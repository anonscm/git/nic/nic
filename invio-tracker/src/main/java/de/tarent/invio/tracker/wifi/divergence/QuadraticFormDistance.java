package de.tarent.invio.tracker.wifi.divergence;


import de.tarent.invio.entities.Histogram;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * Implements the quadratic form distance for comparing the histograms of two measurements router by router
 */
public class QuadraticFormDistance {

    private boolean useLogScale = false;

    /**
     * Empty constructor
     */
    public QuadraticFormDistance() {

    }

    /**
     * Compares the histograms of the routers found by both, the current signal measurement and a fingerprint
     * @param p current measurement
     * @param q fingerprint histogram
     * @return the normalized sum of the quadratic form distances of each router comparison
     */
    public double compareHistograms(Histogram p, Histogram q) {

        double sumQFDist = 0.f;

        // First we get a list of the common keys from p and q.
        final Set commonKeys = Histogram.getCommonKeys(p, q);

        // Secondly we create copies of both histograms so we do not end up changing the originals.
        final Histogram cutP = new Histogram(p);
        final Histogram cutQ = new Histogram(q);

        // Then we remove all the values that are not present in both histograms.
        cutP.keySet().retainAll(commonKeys);
        cutQ.keySet().retainAll(commonKeys);

        // for all routers hashmap <string, <dB, weight>>.. get map out of it
        for ( String accessPoint : cutP.keySet() ) {
            final double[][] groundDistanceMatrix = calculateGroundDistanceMatrix(cutP.get(accessPoint), cutQ.get(accessPoint)); //NOSONAR
            final double[][] similarityMatrix = calculateSimilarityMatrix(groundDistanceMatrix);

            // compare histograms for real
            sumQFDist += computeQFDistance(cutP.get(accessPoint), cutQ.get(accessPoint), similarityMatrix); //NOSONAR
        }

        if (cutP.keySet().size() > 0) {
            sumQFDist = sumQFDist/cutP.keySet().size();
        }

        return sumQFDist;
    }

    private double[][] calculateGroundDistanceMatrix(Map<Integer, Float> p, Map<Integer, Float> q) {

        final Integer[] allKeysSort = getAllSortedKeys(p, q);
        final double[][] groundDistances = new double[allKeysSort.length][allKeysSort.length];

        // calculate L1-metric for the dBs
        for (int i = 0; i < allKeysSort.length; i++) {
            for (int j = 0; j < allKeysSort.length; j++) {
                if (!isLogarithmic()) {
                    groundDistances[i][j] = Math.abs(allKeysSort[i] - allKeysSort[j]);
                } else {
                    // or correctly with a logarithmic scaling!
                    groundDistances[i][j] = Math.abs(castToLogScale(allKeysSort[i]) - castToLogScale(allKeysSort[j]));
                }
            }
        }

        return groundDistances;
    }

    private double[][] calculateSimilarityMatrix(double[][] groundDistanceMatrix) {
        final int sizeI = groundDistanceMatrix.length;
        final int sizeJ = groundDistanceMatrix[0].length;
        double max = 0;

        // formula: a_ij = 1 - d_ij/d_max
        final double[][] similarityMatrix = new double[sizeI][sizeJ];

        // get max value in ground distance matrix
        for (int i = 0; i < sizeI; i++) {
            for (int j = 0; j < sizeJ; j++) {
                if (groundDistanceMatrix[i][j] > max) {
                    max = groundDistanceMatrix[i][j];
                }
            }
        }

        // construct similarity matrix by means of ground distance matrix
        for (int i = 0; i < sizeI; i++) {
            for (int j = 0; j < sizeJ; j++) {
                similarityMatrix[i][j] = 1 - (groundDistanceMatrix[i][j] / max);
            }
        }

        return similarityMatrix;
    }

    private double computeQFDistance(Map<Integer, Float> p, Map<Integer, Float> q, double[][] similarityMatrix) {
        final Integer[] sortedKeys = getAllSortedKeys(p, q);
        final int size = sortedKeys.length;
        final float[] dist = new float[size];
        final float[] tmpQFDist = new float[size];
        float pVal;
        float qVal;

        for (int i = 0; i < size; i++) {
            pVal = p.containsKey(sortedKeys[i]) ? p.get(sortedKeys[i]) : 0.f;
            qVal = q.containsKey(sortedKeys[i]) ? q.get(sortedKeys[i]) : 0.f;

            dist[i] = pVal - qVal;
        }

        // matrix multiplication formula: sqrt((p-q)T*A*(p-q))
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                tmpQFDist[i] += dist[j] * similarityMatrix[i][j];
            }
        }

        double qfDist = 0.f;

        for (int i = 0; i < size; i++) {
                qfDist += tmpQFDist[i] * dist[i];
        }

        qfDist = Math.sqrt(qfDist);

        return qfDist;
    }

    private Integer[] getAllSortedKeys(Map<Integer, Float> p, Map<Integer, Float> q) {
        final Set<Integer> allKeys = new HashSet<Integer>();
        allKeys.addAll(p.keySet());
        allKeys.addAll(q.keySet());
        final Integer[] allKeysSort = allKeys.toArray(new Integer[allKeys.size()]);
        Arrays.sort(allKeysSort, Collections.reverseOrder());

        return allKeysSort;
    }

    private double castToLogScale(float db) {
        final double logVal = Math.sqrt(Math.pow(10.f,(db/10.f)));

        return logVal;
    }

    public boolean isLogarithmic() {
        return useLogScale;
    }

    public void setLogarithmic(boolean b) {
        this.useLogScale = b;
    }
}
