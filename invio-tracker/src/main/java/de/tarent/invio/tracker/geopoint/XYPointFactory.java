package de.tarent.invio.tracker.geopoint;


/**
 * The XYPointFactory is a PointFactory which, surprisingly, creates XYPoints :-) It should be used in all places except
 * on android, where the {@link de.tarent.invio.android.base.position.InvioGeoPointFactory} should be used.
 */
public class XYPointFactory extends PointFactory<XYPoint> {

    /**
     * {@inheritDoc}
     */
    @Override
    public XYPoint newPoint() {
        return new XYPoint();
    }

}
