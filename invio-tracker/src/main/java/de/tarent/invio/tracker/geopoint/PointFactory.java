package de.tarent.invio.tracker.geopoint;

import de.tarent.invio.entities.InvioGeoPoint;

/**
 * A PointFactory can be used to create new instances of InvioGeoPoint. Concrete subclasses can decide which
 * implementation they want to produce. Pass the factory of your choice to a class that needs to create points for
 * you but which should not know about the different InvioGeoPoints.
 * This is the solution to the problem that InvioGeoPointImpl must extend org.osmdroid.util.GeoPoint, in order to be
 * usable with all of osmdroid, but that we want to keep android-code like that out of the tracker.
 * @param <T> the concrete type that the factory will instantiate.
 */
public abstract class PointFactory<T extends InvioGeoPoint> {

    /**
     * Make a new point, with default coordinates (could be (0,0) but don't depend on that!).
     * @return a new instance of an implementation of InvioGeoPoint
     */
    public abstract T newPoint();

}
