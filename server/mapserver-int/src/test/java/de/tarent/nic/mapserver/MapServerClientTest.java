package de.tarent.invio.mapserver;

import com.google.gson.Gson;
import de.tarent.invio.entities.Fingerprint;
import de.tarent.invio.entities.Histogram;
import de.tarent.invio.entities.InvioGeoPoint;
import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.response.MapDataResponse;
import de.tarent.invio.mapserver.response.MapImageResponse;
import de.tarent.invio.mapserver.response.MapListResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MapServerClientTest {

    static final String A_IMAGE = "/kitty1.png";
    static final String A_MAPDATA = "/kitty1.png";
    static final String A_MAPNAME = "mapName";

    MapServerClient client;

    MapServerClient unreachableHost;
    MapServerClient wrongEndpoint;

    HttpClient httpClient = new DefaultHttpClient();

    @Before
    public void setup() throws InvioException {
        // This system property is set by the surefire-plugin that runs our test. It sets it to the maven property
        // selected by the active maven profile. That way we can have different values locally and for jenkins.
        String endpoint = System.getProperty("mapserverEndpoint");
        client = new MapServerClientImpl("http://"+endpoint+":8080/mapserver", httpClient);
        unreachableHost = new MapServerClientImpl("http://localhost:1312/doesntExist", httpClient);
        wrongEndpoint = new MapServerClientImpl(client.getEndpoint() + "/doesntExist", httpClient);
    }

    @Test
    public void testMapDataUpload_InputStream() throws IOException {
        final MapDataResponse response = client.uploadMapData(
                A_MAPNAME, "mapDataName",
                getClass().getResourceAsStream(A_MAPDATA));

        assertEquals(200, response.getStatus());
        assertEquals("successful", response.getStatusMessage());
    }

    @Test
    public void testMapDataUpload_File() throws IOException {
        final File mapData = new File(getClass().getResource(A_MAPDATA).getPath());
        final MapDataResponse response = client.uploadMapData(
                A_MAPNAME, "mapDataName", mapData);

        assertEquals(200, response.getStatus());
        assertEquals("successful", response.getStatusMessage());
    }

    @Test(expected = IOException.class)
    public void testMapDataUpload_unreachable() throws IOException {
        final File mapData = new File(getClass().getResource(A_MAPDATA).getPath());
        unreachableHost.uploadMapData(A_MAPNAME, "mapDataName", mapData);
    }

    @Test
    public void testMapDataUpload_WrongEndpoint() throws IOException {
        final File mapData = new File(getClass().getResource(A_MAPDATA).getPath());
        final MapDataResponse response = wrongEndpoint.uploadMapData(
                A_MAPNAME, "mapDataName", mapData);

        assertEquals(404, response.getStatus());
        assertTrue(response.getStatusMessage().contains("Internal Server Error"));
    }

    // TODO: this test (or testMapImageUpload_File()) should verify that the server created the correct tiles for
    //       the given boundingbox-coordinates.
    // TODO: the boundingbox-coordinates are crappy and should be more realistic.
    @Test
    public void testMapImageUpload_InputStream() throws IOException {
        final MapImageResponse response = client.uploadMapImage(
                A_MAPNAME,
                getClass().getResourceAsStream(A_IMAGE), 1f, 2f, 2f, 1f);

        assertEquals(200, response.getStatus());
        assertEquals("image-upload successful", response.getStatusMessage());
    }

    @Test
    public void testMapImageUpload_File() throws IOException, InvioException {
        final File mapImage = new File(getClass().getResource(A_IMAGE).getPath());
        final MapImageResponse response = client.uploadMapImage(
                A_MAPNAME, mapImage, 1f, 2f, 2f, 1f);

        assertEquals(200, response.getStatus());
        assertEquals("image-upload successful", response.getStatusMessage());

        // TODO: move this to its own test, like testMapList:
        // Now we try to download the tilemapresource.xml which should have been generated:
        String xml = client.getTilemapresourceXml(A_MAPNAME);

        assertTrue(xml.startsWith("<?xml version=\"1.0\" encoding=\"utf-8\"?>"));
        // Note: gdal doesn't use our given coordinates because it needs to snap them to tile-boundaries and the tiles
        //       are defined as fixed grids over the earth. TODO: We should use better/realistic coordinates in this
        //       test anyway.
        // Note: the names of the coordinates are not what we would expect, see our wiki: "Karten_und_Koordinaten"
        assertTrue(xml.contains("<BoundingBox minx=\"0.99939198548315\" miny=\"0.99999999999999\" "+
                                "maxx=\"2.00000000000000\" maxy=\"1.99958297449991\"/>"));
        // TODO: verify the zoomlevels as well
    }

    @Test(expected = IOException.class)
    public void testMapImageUpload_unreachable() throws IOException {
        final File mapImage = new File(getClass().getResource(A_IMAGE).getPath());
        unreachableHost.uploadMapImage("mapName", mapImage, 1f, 2f, 2f, 1f);
    }

    @Test
    public void testMapImageUpload_WrongEndpoint() throws IOException {
        final File mapImage = new File(getClass().getResource(A_IMAGE).getPath());
        final MapImageResponse response = wrongEndpoint.uploadMapImage(
                A_MAPNAME, mapImage, 1f, 2f, 2f, 1f);

        assertEquals(404, response.getStatus());
        assertTrue(response.getStatusMessage().contains("Internal Server Error"));
    }

    @Test
    public void testFingerprintUpAndDownload() throws IOException, JSONException, InvioException {
        List<Fingerprint> fingerprints = new ArrayList<Fingerprint>();
        Fingerprint fingerprint = new Fingerprint(new Histogram("h1"), new InvioGeoPoint() {
            @Override
            public int getLongitudeE6() {
                // We want a different fingerprint for each test-run, so that we don't download some old file and
                // think it was our new one.
                return (int)System.currentTimeMillis();
            }

            @Override
            public void setLongitudeE6(int lon) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public int getLatitudeE6() {
                return 0;
            }

            @Override
            public void setLatitudeE6(int lat) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public double getX() {
                return 0;
            }

            @Override
            public double getY() {
                return 0;
            }

            @Override
            public void setXY(double x, double y) {

            }

            @Override
            public double calculateDistanceTo(InvioGeoPoint invioGeoPoint) {
                return 0;
            }

            @Override
            public double getDivergence() {
                return 0;  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void setDivergence(double divergence) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public int compareTo(InvioGeoPoint another) {
                return 0;  //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        fingerprints.add(fingerprint);
        Gson gson = new Gson();
        String json = gson.toJson(fingerprints);

        // Upload stuff for two different maps:
        client.uploadFingerprintsData("mapName1", json);
        client.uploadFingerprintsData("mapName2", "almost empty test data");

        // Download again and see if it matches:
        String response = client.downloadFingerprintsData("mapName1");
        assertEquals(json, response);
        response = client.downloadFingerprintsData("mapName2");
        assertEquals("almost empty test data", response);
    }

    @Test
    public void testMapList() throws IOException, InvioException {
        testMapImageUpload_File();
        final MapListResponse response = client.getMapList();
        assertEquals(200, response.getStatus());
        assertFalse(response.getMapList().isEmpty());
        assertTrue(response.getMapList().contains(A_MAPNAME));
    }
}
