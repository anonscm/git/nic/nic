package de.tarent.invio.mapserver.controller;

import de.tarent.invio.mapserver.ResourceConstants;
import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.gdal.Gdal;
import de.tarent.invio.mapserver.management.DirectoryManager;
import de.tarent.invio.mapserver.response.MapImageResponse;
import de.tarent.invio.mapserver.response.Response;
import de.tarent.invio.mapserver.response.ResponseBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import javax.servlet.ServletRequest;
import java.io.File;
import java.io.IOException;

/**
 * The MapImageController is used to manage tiled maps.
 */
@Controller
@RequestMapping(ResourceConstants.MAP_IMAGE_ROOT)
public class MapImageController implements ResourceConstants {

    private static final Logger LOGGER = Logger.getLogger(MapImageController.class);

    private static final String MEGA_SCRIPT = "/var/lib/tomcat7/webapps/scripts/mega_script.sh";

    private final ServletRequest request;

    private final Gdal gdal;

    private final DirectoryManager directoryManager;

    /**
     * Create a new MapImageController. We use springs constructor-injection. That way the class itself can be used
     * just like any normal java-class and it becomes easier to fill a test-instance with mock-objects. As long
     * as these dependency-objects do not need to be mutable this is the simplest solution.
     *
     * @param request          the servlet request will be used to build new URLs the way the client expects them
     * @param gdal             gdal will be used to cut uploaded images into tiles
     * @param directoryManager contains diretory/path-helper-methods, for moving upload/result-files around
     */
    @Inject
    public MapImageController(final ServletRequest request, final Gdal gdal, final DirectoryManager directoryManager) {
        this.request = request;
        this.gdal = gdal;
        this.directoryManager = directoryManager;
    }

    /**
     * Receive an uploaded image and process it with GDAL.
     * The image is split into tiles which can then be downloaded.
     *
     * @param mapName  the name of the map.
     * @param left the longitude of left edge of the boundingbox for this map
     * @param upper the latitude of upper edge of the boundingbox for this map
     * @param right the longitude of right edge of the boundingbox for this map
     * @param lower the latitude of lower edge of the boundingbox for this map
     * @param newImage the uploaded image as a {@link MultipartFile}
     * @return a status response string
     * @throws InvioException tmp file handling and gdal processing could fail
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody // The method will provide its own literal response-body. No spring-jsp magic is required.
    protected ResponseEntity<String> uploadImage(
            @RequestParam(PARAM_MAP_NAME) final String mapName,
            @RequestParam(MAP_IMAGE_PARAM_LEFT) final Float left,
            @RequestParam(MAP_IMAGE_PARAM_UPPER) final Float upper,
            @RequestParam(MAP_IMAGE_PARAM_RIGHT) final Float right,
            @RequestParam(MAP_IMAGE_PARAM_LOWER) final Float lower,
            @RequestParam(MAP_IMAGE_PARAM_MAP_IMAGE) final MultipartFile newImage) throws InvioException {

        final File mapTilesDirectory;
        try {
            mapTilesDirectory = handleImageUploadAndTilesCreation(mapName, newImage, left, upper, right, lower);
        } catch (final IOException e) {
            throw new InvioException("Error while uploading or providing the maptiles.", e);
        }

        final ResponseEntity<String> responseEntity = buildResponseEntity(mapTilesDirectory);
        return responseEntity;
    }

    /**
     * Handles the image upload and the tile creation.
     *
     * @param mapName  the name of the map
     * @param newImage the new image to be uploaded
     * @param left the longitude of left edge of the boundingbox for this map
     * @param upper the latitude of upper edge of the boundingbox for this map
     * @param right the longitude of right edge of the boundingbox for this map
     * @param lower the latitude of lower edge of the boundingbox for this map
     * @return the map tiles location
     * @throws IOException if the map tiles directory couldn't be created
     */
    private File handleImageUploadAndTilesCreation(final String mapName, final MultipartFile newImage,
                                                   final float left, final float upper,
                                                   final float right, final float lower)
            throws IOException, InvioException {
        final File mapTilesDirectory = directoryManager.getMapTilesDirectory(mapName);
        final File tmpFile = transferImageToTempDirectory(newImage);
        final String tilesPath = gdal.makeRasterTiles(tmpFile.getAbsolutePath(), left, upper, right, lower);
        updateTiles(tilesPath, mapTilesDirectory);

        final int megaScriptReturnCode = runMegaScript(new File(tilesPath+".tif"), mapName);
        if (megaScriptReturnCode != 0) {
            throw new InvioException(MEGA_SCRIPT + " failed with code " + megaScriptReturnCode);
        }

        return mapTilesDirectory;
    }

    /**
     * Mega script executes following scripts in a row:
     *
     * - add_zoom_level.sh
     * - generate_border_image.sh
     * - make_jpg.sh
     * - make_border_tiles.php
     *
     * @param tmpFile GTiff of the uploaded map image
     * @param mapName the name of the map
     * @return returns 0 if successful
     * @throws IOException if something went wrong with IO
     * @throws InvioException if scripts is missing or were interrupted
     */
    protected int runMegaScript(final File tmpFile, final String mapName) throws IOException, InvioException {
        final File megaScript = new File(MEGA_SCRIPT); //NOSONAR - An absolute path is fine here for the time being.

        int returnCode = 1;

        if (megaScript.exists()) {
            final String[] cmd = {"sh", MEGA_SCRIPT, tmpFile.getAbsolutePath(), mapName};
            final Process process = Runtime.getRuntime().exec(cmd);

            try {
                returnCode = process.waitFor();
            } catch (final InterruptedException e) {
                throw new InvioException(MEGA_SCRIPT + " was interrupted!", e);
            }
        } else {
            throw new InvioException(MEGA_SCRIPT + " is missing!");
        }

        return returnCode;
    }

    /**
     * Transfers the image to a temporary directory and then returns the location the image was saved to.
     *
     * @param newImage the map image
     * @return the temp location of the image
     * @throws IOException if the temp file couldn't be created
     */
    private File transferImageToTempDirectory(final MultipartFile newImage) throws IOException {
        final File tmpFile = directoryManager.getTempFile();
        newImage.transferTo(tmpFile);
        return tmpFile;
    }

    private ResponseEntity<String> buildResponseEntity(File mapTilesDirectory) {
        final Response response = new MapImageResponse()
                .setImageURL(generateTilesUrl(mapTilesDirectory))
                .setStatusMessage("image-upload successful");
        final ResponseEntity<String> responseEntity = ResponseBuilder.buildResponseEntity(response);
        return responseEntity;
    }

    /**
     * Generates the URL of the specified map path.
     *
     * @param mapTilesDirectory the path to the map tiles directory
     * @return the map URL
     */
    protected String generateTilesUrl(final File mapTilesDirectory) {
        final String mapPath = getMapPath(mapTilesDirectory);
        final String mapUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
                + "/" + mapPath + "/";
        return mapUrl;
    }

    /**
     * Gets the path to the given map directory after "webapps".
     * <p/>
     * eg. .../webapps/maps/<mapName> (Everything after the webapps is returned)
     *
     * @param mapTilesDirectory the path to the map tiles directory
     * @return The map path
     */
    private String getMapPath(final File mapTilesDirectory) {
        final String path = mapTilesDirectory.getAbsolutePath();
        final String webappsName = "webapps";
        final int index = path.indexOf(webappsName + "/") + webappsName.length() + 1;
        //cut up the root path
        final String mapPath = mapTilesDirectory.getAbsolutePath().substring(index);
        return mapPath;
    }

    /**
     * Update the existing map tiles
     *
     * @param tilesPath         the new map tiles folder
     * @param mapTilesDirectory the existing map tiles folder
     * @throws IOException if the old tiles directory can't be replaced with the new one
     */
    protected void updateTiles(final String tilesPath, final File mapTilesDirectory) throws IOException {
        final File tilesDir = new File(tilesPath);

        // TODO: Vielleicht sollen wir die Tiles direkt im $TOMCAT/webapps/maps/ generieren?
        //remove old version
        try {
            FileUtils.deleteDirectory(mapTilesDirectory);
            FileUtils.copyDirectory(tilesDir, mapTilesDirectory);
            FileUtils.deleteDirectory(tilesDir);
        } catch (IOException e) {
            LOGGER.error("An error occurred while copying the maptiles directory into the maps directory", e);
        }
    }
}
