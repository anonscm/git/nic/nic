package de.tarent.invio.mapserver.controller;

import de.tarent.invio.mapserver.ResourceConstants;
import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.management.DirectoryManager;
import de.tarent.invio.mapserver.response.MapListResponse;
import de.tarent.invio.mapserver.response.Response;
import de.tarent.invio.mapserver.response.ResponseBuilder;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This controller is responsible for delivering a list of map names.
 * These are used inside the app, so that the user can select the map most useful to him/her.
 */

@Controller
@RequestMapping()
public class MapListController {

    private static final Logger LOGGER = Logger.getLogger(MapListController.class);

    private final DirectoryManager directoryManager;


    /**
     * Creates a controller which returns a simple list of all existing maps on the mapserver.
     * Because no server side modifications are done, we use only get requests.
     * @param directoryManager contains diretory/path-helper-methods, to get the map (directory) names
     */
    @Inject
    public MapListController(final DirectoryManager directoryManager) {
        this.directoryManager = directoryManager;
    }

    /**
     * GET's the list of directory names for the various maps.
     *
     * @return ResponseEntity Response with JSON string containing map names
     * @throws InvioException Exception in case of a missing read permission for the file system
     */
    @RequestMapping(value = ResourceConstants.MAP_LIST_ROOT, method = RequestMethod.GET)
    public ResponseEntity<String> getMapList() throws InvioException {

        final List<String> mapList;
        mapList = directoryManager.getMapList();

        final Response response = new MapListResponse().setMapList(mapList);
        final ResponseEntity<String> responseEntity = ResponseBuilder.buildResponseEntity(response);

        return responseEntity;
    }

    /**
     * GET's the list of map/directory names for a specific group.
     *
     * @param groupName The group for which we want to find all the maps
     * @return the response
     *
     * @throws InvioException Exception in case of a missing read permission for the file system
     * @throws ParserConfigurationException if a DocumentBuilder cannot be created
     * @throws SAXException If any parse errors occur
     * @throws IOException IOException if:
     *          1. the directory could not be created
     *          2. parse error occurs
     */
    @RequestMapping(value = ResourceConstants.MULTILEVEL_MAP_LIST, method = RequestMethod.GET)
    public ResponseEntity<String> getMultilevelMapList(@RequestParam("group") String groupName)
            throws InvioException, ParserConfigurationException, SAXException, IOException {

        final List<String> mapList = new ArrayList<String>();

        final List<String> availableMaps = directoryManager.getMapList();
        getMapsForGroup(groupName, mapList, availableMaps);

        final Response response = new MapListResponse().setMapList(mapList);
        final ResponseEntity<String> responseEntity = ResponseBuilder.buildResponseEntity(response);

        return responseEntity;
    }

    private void getMapsForGroup(String groupName, List<String> mapList, List<String> allMaps)
            throws IOException, ParserConfigurationException, SAXException {
        for (String mapName : allMaps) {
            final File mapDataDirectory = directoryManager.getMapDataDirectory(mapName);
            final File mapDataFile = new File(mapDataDirectory + File.separator + mapName + ".osm");
            if (mapDataFile.exists()) {
                handleMap(groupName, mapList, mapName, mapDataFile);
            } else {
                LOGGER.error(mapName + ".osm not found");
            }
        }
    }

    private void handleMap(String groupName, List<String> mapList, String mapName, File mapDataFile)
            throws ParserConfigurationException, SAXException, IOException {
        final Document doc = parseXmlFile(mapDataFile);

        final NodeList nodes = doc.getElementsByTagName("tag");

        for (int i = 0; i < nodes.getLength(); i++) {
            final Node nNode = nodes.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                final Element eElement = (Element) nNode;
                if (eElement.getAttribute("k").equalsIgnoreCase("namespace_group_name")
                        && eElement.getAttribute("v").equalsIgnoreCase(groupName.toLowerCase())) {
                    mapList.add(mapName);
                }
            }
        }
    }

    private Document parseXmlFile(File mapDataFile) throws ParserConfigurationException, SAXException, IOException {
        final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        final Document doc = dBuilder.parse(mapDataFile);
        doc.getDocumentElement().normalize();
        return doc;
    }
}
