package de.tarent.invio.mapserver.controller;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import de.tarent.invio.mapserver.ResourceConstants;
import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.management.DirectoryManager;
import de.tarent.invio.mapserver.response.MapDataResponse;
import de.tarent.invio.mapserver.response.ResponseBuilder;

/**
 * This controller is responsible for the uploadImage of map-data-files.
 * In these files, the points-of-interests and so on are stored.
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 */
@Controller
@RequestMapping(ResourceConstants.MAP_DATA_ROOT)
public class MapDataController implements ResourceConstants {

    private final DirectoryManager directoryManager;


    /**
     * Create a MapDataController. We use springs constructor-injection. That way the class itself can be used
     * just like any normal java-class and it becomes easier to fill a test-instance with mock-objects. As long
     * as these dependency-objects do not need to be mutable this is the simplest solution.
     * @param directoryManager contains diretory/path-helper-methods, for moving upload/result-files around
     */
    @Inject
    public MapDataController(final DirectoryManager directoryManager) {
        this.directoryManager = directoryManager;
    }


    /**
     * This will handle a incoming map-data-file.
     *
     * @param mapName     The name of the map for which file is this content
     * @param mapDataName the special name of the map data
     * @param mapData     the map-data-file
     * @return the response
     * @throws InvioException if the map-data-file could not be stored successfully.
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody // The method will provide its own literal response-body. No spring-jsp-magic is required.
    public ResponseEntity<String> upload(
            @RequestParam(PARAM_MAP_NAME) final String mapName,
            @RequestParam(MAP_DATA_NAME) final String mapDataName,
            @RequestParam(MAP_DATA) final MultipartFile mapData) throws InvioException {

        try {
            createOrUpdateMapData(mapName, mapDataName, mapData);
        } catch (IOException e) {
            throw new InvioException("Could not move uploaded map data file!", e);
        }

        final ResponseEntity<String> responseEntity = ResponseBuilder.buildResponseEntity(new MapDataResponse());
        return responseEntity;
    }

    /**
     * Find the correct place for this mapData and move it there, possibly overwriting an old mapData-file.
     *
     * @param mapName the name of the map
     * @param mapDataName the name of the map data file
     * @param mapData the map data
     * @throws IOException if the target directory can neither be found nor created
     */
    protected void createOrUpdateMapData(final String mapName, final String mapDataName, final MultipartFile mapData)
            throws IOException {

        final File targetDestination = getTargetDestination(mapName, mapDataName);
        final File tmpFile = directoryManager.getTempFile();

        mapData.transferTo(tmpFile);

        directoryManager.moveFile(tmpFile, targetDestination);
    }

    /**
     * TODO: TRANSLATE
     * @param mapName the name of the map
     * @param mapDataName the name of the map data file
     * @return the target destination
     * @throws IOException if the target directory can neither be found nor created
     */
    protected File getTargetDestination(final String mapName, final String mapDataName) throws IOException {
        final File root = directoryManager.getMapDataDirectory(mapName);
        final File targetDestination = new File(root, mapDataName);

        return targetDestination;
    }
}
