package de.tarent.invio.mapserver.controller;

import de.tarent.invio.mapserver.ResourceConstants;
import de.tarent.invio.mapserver.management.DirectoryManager;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * This controller handles requests concerning the users position data.
 */
@Controller
@RequestMapping(ResourceConstants.POSITION_ROOT)
public class PositionController implements ResourceConstants {

    private final DirectoryManager directoryManager;

    /**
     * Spring constructor
     *
     * @param directoryManager this is where we get the paths from
     */
    @Inject
    public PositionController(DirectoryManager directoryManager) {
        this.directoryManager = directoryManager;
    }

    /**
     * POST-method for creating a position-file for a given user
     *
     * @param longitude position to be written to the file
     * @param latitude  position to be written to the file
     * @param deviceId  the distinct name of the device
     * @param mapName   the relevant map name
     * @param level the current level/floor
     * @throws JSONException is thrown if something goes wrong while creating the json
     * @throws IOException   is thrown if something goes wrong while writing the file
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public void setPosition(
            @RequestParam(POSITION_PARAM_LONGITUDE) final long longitude,
            @RequestParam(POSITION_PARAM_LATITUDE) final long latitude,
            @RequestParam(POSITION_PARAM_DEVICE_ID) final String deviceId,
            @RequestParam(PARAM_MAP_NAME) final String mapName,
            @RequestParam(POSITION_PARAM_LEVEL) final int level) throws JSONException, IOException {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd_HH:mm:ss.SSS");
        final Date date = new Date();
        date.setTime(System.currentTimeMillis());
        final String currentTime = dateFormat.format(date);
        final JSONObject json = new JSONObject();
        json.put("time", currentTime);
        json.put("long", longitude);
        json.put("lat", latitude);
        json.put("level", level);
        final File positionDirectory = directoryManager.getPositionDataDirectory(mapName);
        if (!positionDirectory.exists()) {
            positionDirectory.mkdirs(); // NOSONAR - We don't need the returned boolean here.
        }
            final FileWriter fileWriter = new FileWriter(positionDirectory.getPath()//NOSONAR default encoding is fine
                    + File.separator + deviceId + ".json");                         //NOSONAR default encoding is fine
            fileWriter.write(json.toString());
            fileWriter.flush();
            fileWriter.close();
    }

    /**
     * GET-method for user positions
     *
     * @param deviceId the distinct user name
     * @param mapName  the relevant map name
     * @return a json string containing the position
     * @throws IOException if something goes wrong while reading the file
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String getPosition(
            @RequestParam(POSITION_PARAM_DEVICE_ID) final String deviceId,
            @RequestParam(PARAM_MAP_NAME) final String mapName) throws IOException {
        final File jsonFile = new File(directoryManager.getPositionDataDirectory(mapName)
                + File.separator + deviceId + ".json");
        if ((jsonFile.exists())) {
            final String jsonString = new Scanner(jsonFile).next(); //NOSONAR default encoding is fine
            return jsonString;
        } else {
            return null;
        }
    }
}
