package de.tarent.invio.mapserver.controller;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import de.tarent.invio.mapserver.ResourceConstants;
import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.management.DirectoryManager;
import de.tarent.invio.mapserver.response.CartDataResponse;
import de.tarent.invio.mapserver.response.ResponseBuilder;

/**
 * A controller used for uploading shopping cart data.
 * 
 * @author Timo Kanera, tarent solutions GmbH
 */
@Controller
@RequestMapping(ResourceConstants.CART_DATA_ROOT)
public class CartDataController implements ResourceConstants {

	private static final Logger LOGGER = Logger.getLogger(CartDataController.class);
	
    private final DirectoryManager directoryManager;


    /**
     * Creates a new CartDataController.
     * 
     * @param directoryManager the directory manager
     */
    @Inject
    public CartDataController(final DirectoryManager directoryManager) {
        this.directoryManager = directoryManager;
    }
	
	/**
	 * Handles an incoming shopping cart.
	 * 
	 * @param clientID the unique ID of the client
	 * @param map the name of the map
	 * @param cart the file describing the shopping cart
	 * 
	 * @return the response of the call
	 * @throws InvioException if an error occurred during execution
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<String> uploadCart(
			@RequestParam(CART_DATA_PARAM_CLIENT_ID) final String clientID, 
			@RequestParam(PARAM_MAP_NAME) final String map,
			@RequestParam(CART_DATA_PARAM_CART) final MultipartFile cart) throws InvioException {
		LOGGER.debug("Client: " + clientID + " is uploading a shopping cart for map: " + map);
		try {
			persist(clientID, map, cart);
        } catch (IOException ex) {
            throw new InvioException("Failed to persist uploaded shopping card for client ID: " + clientID, ex);
        }
		final ResponseEntity<String> response = ResponseBuilder.buildResponseEntity(new CartDataResponse());
		return response;
	}
	
	/**
	 * Persists the given shopping cart data.
	 * 
	 * @param clientID the unique ID of the client
	 * @param map the name of the map
	 * @param cart the file describing the shopping cart
	 * 
	 * @throws IOException if persisting the cart failed
	 */
	protected void persist(String clientID, String map, MultipartFile cart) throws IOException {
		final File tempFile = directoryManager.getTempFile();
		cart.transferTo(tempFile);
		
		final File targetFile = getTargetFile(clientID, map);
		directoryManager.moveFile(tempFile, targetFile);
	}
	
	/**
	 * Returns the target file used to store the shopping cart for the given client and map. 
	 * 
	 * @param clientID the unique ID of the client
	 * @param map the name of the map
	 * 
	 * @return the requested target file
	 * @throws IOException if the directory for the file doesn't exist and cannot be created
	 */
	protected File getTargetFile(String clientID, String map) throws IOException {
		final File directory = directoryManager.getCartDataDirectory(map);
		final File target = new File(directory, clientID + ResourceConstants.CART_DATA_FILE_SUFFIX);
		return target;
	}
}
