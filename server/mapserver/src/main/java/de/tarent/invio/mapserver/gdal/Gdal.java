package de.tarent.invio.mapserver.gdal;

import de.tarent.invio.mapserver.exception.GdalException;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.apache.commons.io.IOUtils.closeQuietly;

/**
 * The class Gdal is the interface to the external program "gdal2tiles.py".
 * It is used to cut a map-image into tiles according to the TileMapService-spec.
 *
 * @see <a href="http://wiki.osgeo.org/wiki/Tile_Map_Service_Specification">
 * http://wiki.osgeo.org/wiki/Tile_Map_Service_Specification
 * </a>
 * @see <a href="http://www.gdal.org/gdal2tiles.html">
 * http://www.gdal.org/gdal2tiles.html
 * </a>
 * <p/>
 * This method is unix-specific!
 */
@Component
public class Gdal {

    public static final String PYTHON = "/usr/bin/python";
    public static final String GDAL = "/usr/bin/gdal2tiles.py";
    public static final String GDAL_TRANSLATE = "/usr/bin/gdal_translate";
    public static final String OUTPUT_DIR = "/tmp";
    private static final Logger LOGGER = Logger.getLogger(Gdal.class);

    /**
     * The expected output from the gdal tile conversion.
     */
    private static final String EXPECTED_GDAL_OUTPUT =
            "Generating Base Tiles:0...10...20...30...40...50...60...70...80...90...100" +
            " - done.Generating Overview Tiles:0...10...20...30...40...50...60...70...80...90...100" +
            " - done.";

    /**
     * Split an image into tiles using the raster profile of GDAL.
     *
     * @param imageFileName the absolute name of the image file
     * @param left the longitude of left edge of the boundingbox for this map
     * @param upper the latitude of upper edge of the boundingbox for this map
     * @param right the longitude of right edge of the boundingbox for this map
     * @param lower the latitude of lower edge of the boundingbox for this map
     * @return the name of the folder that contains the tile hierarchy
     * @throws IOException there might be problems reading from the streams
     * @throws GdalException when the tile creation failes because of problems with the gdal jobs.
     */
    public String makeRasterTiles(final String imageFileName,
                                   final float left, final float upper, final float right, final float lower)
            throws IOException, GdalException {

        final String geoTiffName = makeGeoTiff(imageFileName, left, upper, right, lower);

        final String[] cmd = {PYTHON, GDAL, "--webviewer=none", "-r", "near", geoTiffName};
        final Process process = Runtime.getRuntime().exec(cmd, null, new File(OUTPUT_DIR));

        if (!readAndVerifyOutput(process)) {
            throw new GdalException("GDAL failed.");
        }

        waitForProcessFinish(process, "gdal2tiles.py");

        final String dst = FilenameUtils.removeExtension(FilenameUtils.getBaseName(imageFileName));
        return OUTPUT_DIR + "/" + dst;
    }


    private String makeGeoTiff(final String imageFileName,
                                final Float left, final Float upper, final Float right, final Float lower)
            throws IOException, GdalException {
        final String destinationFile = FilenameUtils.removeExtension(FilenameUtils.getBaseName(imageFileName)) + ".tif";
        final String[] cmd = {GDAL_TRANSLATE, "-of", "GTiff", "-a_ullr",
                              left.toString(), upper.toString(), right.toString(), lower.toString(),
                              "-a_srs", "EPSG:4326", imageFileName, destinationFile};
        final Process process = Runtime.getRuntime().exec(cmd, null, new File(OUTPUT_DIR));

        waitForProcessFinish(process, "gdal_translate");

        return destinationFile;
    }


    private void waitForProcessFinish(Process process, String scriptName) {
        try {
            final int returnCode = process.waitFor();
            if (returnCode != 0) {
                throw new GdalException(scriptName + " failed with code " + returnCode);
            }
        } catch (InterruptedException e) {
            throw new GdalException(scriptName + " was interrupted.", e);
        }
    }

    /**
     * Read from STDOUT and STDERR of our gdal-process and check, if the output indicates a successful conversion.
     * (Might be a bit redundant, because gdal should signal its failure via its return code (which should, by general
     *  convention, be 0 for "no errors" and != 0 for "fail"), which it probably does, but we haven't checked.)
     *
     * @param process the python {@link Process} running the gdal-script
     * @return true = everything fine; false = something went wrong
     * @throws IOException there might be problems reading from the streams
     */
    private boolean readAndVerifyOutput(final Process process) throws IOException {
        final BufferedReader stdOutput = new BufferedReader(new InputStreamReader(process.getInputStream(), "UTF-8"));
        final BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream(), "UTF-8"));
        try {
            return isExpectedGdalOutput(stdOutput) && !hasErrors(stdError);
        } finally {
            closeQuietly(stdOutput);
            closeQuietly(stdError);
        }
    }

    /**
     * Change the verification success status to false on error.
     *
     * @param stdError STDERR
     * @return the success status
     * @throws IOException if stdError is closed
     */
    private boolean hasErrors(final BufferedReader stdError) throws IOException {
        boolean hasErrors = false;
        String line;
        while ((line = stdError.readLine()) != null) {
            LOGGER.error(line);
            hasErrors = true;
        }
        return hasErrors;
    }

    /**
     * Read and verify the gdal output.
     *
     * @param stdInput STDIN
     * @return true if the gdal output same as expected
     * @throws IOException if the input can't be read
     */
    private boolean isExpectedGdalOutput(final BufferedReader stdInput) throws IOException {
        final StringBuilder stdOutput = new StringBuilder();
        String line;
        while ((line = stdInput.readLine()) != null) {
            stdOutput.append(line);
        }
        if (!stdOutput.toString().equals(EXPECTED_GDAL_OUTPUT)) {
            LOGGER.error("GDAL didn't print the expected output: " + stdOutput);
            return false;
        }
        return true;
    }
}
