package de.tarent.invio.mapserver.wrapper;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.io.OutputStreamWriter;

/**
 * Wrapper class for the methods from {@link IOUtils} used in the project.
 *
 * @author Désirée Amling <d.amling@tarent.de>
 */
@Component
public class IOUtilWrapper {

    /**
     * Wraps the static method {@link IOUtils#closeQuietly(java.io.Writer)}.
     *
     * @param outputStreamWriter the {@link OutputStreamWriter}
     */
    public void closeQuietly(final OutputStreamWriter outputStreamWriter) {
        IOUtils.closeQuietly(outputStreamWriter);
    }

    /**
     * Wraps the static method {@link IOUtils#closeQuietly(java.io.InputStream)}.
     *
     * @param inputStream the {@link InputStream}
     */
    public void closeQuietly(final InputStream inputStream) {
        IOUtils.closeQuietly(inputStream);
    }
}
