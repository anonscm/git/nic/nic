package de.tarent.invio.mapserver.controller;

import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.management.DirectoryManager;
import de.tarent.invio.mapserver.wrapper.IOUtilWrapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_DATA_FILENAME_BLUETOOTH_LE;
import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_DATA_PARAM_BLUETOOTH_LE;
import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_DATA_ROOT_BLUETOOTH_LE;
import static de.tarent.invio.mapserver.ResourceConstants.PARAM_MAP_NAME;

/**
 * Controller for the bluetooth fingerprints.
 */
@Controller
@RequestMapping(FINGERPRINTS_DATA_ROOT_BLUETOOTH_LE)
public class FingerprintsBluetoothLEDataController {

    @Inject
    private DirectoryManager directoryManager;

    @Inject
    private IOUtilWrapper ioUtilWrapper;

    /**
     * Upload the bluetooth fingerprints file to the server.
     *
     * @param mapName          the name of the map
     * @param fingerprintsJson the fingerprints in JSON-format
     * @return a status response string
     * @throws InvioException if the upload of the fingerprints file fails
     */
    @RequestMapping(method = RequestMethod.POST) //NOSONAR
    @ResponseBody // The method will provide its own literal response-body. No spring-jsp magic is required.
    public ResponseEntity uploadFingerprints(
            @RequestParam(PARAM_MAP_NAME) final String mapName,
            @RequestParam(FINGERPRINTS_DATA_PARAM_BLUETOOTH_LE) final String fingerprintsJson) throws InvioException {

        OutputStreamWriter outputStreamWriter = null;

        try {
            final FileOutputStream fileOutputStream = getFileOutputStreamForFingerprintsFromMapName(mapName);
            outputStreamWriter = new OutputStreamWriter(fileOutputStream, "UTF-8");
            outputStreamWriter.write(fingerprintsJson);
        } catch (final IOException e) {
            throw new InvioException("Could not save bluetooth fingerprints data file!", e);
        } finally {
            ioUtilWrapper.closeQuietly(outputStreamWriter);
        }

        return new ResponseEntity(HttpStatus.CREATED);
    }

    /**
     * Gets the fingerprints directory from the {@link #directoryManager} based on the map name and returns a
     * {@link FileOutputStream} for the fingerprints file in the fingerprints directory.
     *
     * @param mapName the map name
     * @return the {@link FileOutputStream}
     * @throws IOException if the directory could not be found or the file could not be written
     */
    private FileOutputStream getFileOutputStreamForFingerprintsFromMapName(final String mapName) throws IOException {
        final File fingerprintsDirectory = directoryManager.getMapFingerprintsDirectory(mapName);
        final File fingerprintsData = new File(fingerprintsDirectory, FINGERPRINTS_DATA_FILENAME_BLUETOOTH_LE);
        final FileOutputStream fileOutputStream = new FileOutputStream(fingerprintsData);

        return fileOutputStream;
    }
}
