package de.tarent.invio.mapserver.controller;

import de.tarent.invio.mapserver.management.DirectoryManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;

import static org.junit.Assert.*;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class PositionControllerTest {

    PositionController toTest;
    String testPath = "/tmp/positionControllerTest/";
    String testId = "deviceId";
    File testFile = new File(testPath+testId+".json");
    File testPathFile = new File(testPath);

    @Mock
    DirectoryManager directoryManager;

    @Before
    public void setUp() throws Exception {

        if (testPathFile.exists()) {
            if (testFile.exists()) {
                testFile.delete();
            }
            testPathFile.delete();
        }
        doReturn(new File(testPath)).when(directoryManager).getPositionDataDirectory(anyString());
        toTest = new PositionController(directoryManager);
    }

    @Test
    public void testSetPosition() throws Exception {
        toTest.setPosition(1L, 2L, testId, "someName", 0);
        assertTrue(testPathFile.exists());
        assertTrue(testFile.exists());
        toTest.setPosition(12L, 22L, testId, "someName", 0);
        assertTrue(testPathFile.exists());
        assertTrue(testFile.exists());
    }

    @Test
    public void testGetPosition() throws Exception {
        toTest.setPosition(1111L, 2222L, testId, "someName", 0);
        String result = toTest.getPosition(testId, "someName");
        assertTrue(result.contains("\"long\":1111"));
        assertTrue(result.contains("\"lat\":2222"));
    }
}
