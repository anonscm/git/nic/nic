package de.tarent.invio.mapserver.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.tarent.invio.mapserver.exception.InvioException;

public class ExceptionResolverTest {

    ExceptionResolver toTest;

    @Before
    public void setup() {
        toTest = spy(new ExceptionResolver());
    }

    @Test
    public void handleExceptions() {
        final InvioException aException = new InvioException("aMessage");

        final ResponseEntity<String> result = toTest.handleExceptions(aException);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
        assertTrue(result.getBody().contains(aException.getMessage()));
    }

}
