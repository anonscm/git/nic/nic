package de.tarent.invio.mapserver.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import de.tarent.invio.mapserver.ResourceConstants;
import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.management.DirectoryManager;

@RunWith(MockitoJUnitRunner.class)
public class CartDataControllerTest {
	
	private String map = "map name";
	private String client = "client id";
	private MultipartFile cart = mock(MultipartFile.class);
	
	@Mock
	private DirectoryManager directoryManager;

	private CartDataController controller;
	
	@Before
	public void setUp() {
		controller = spy(new CartDataController(directoryManager));
	}
	
	@Test(expected=InvioException.class)
	public final void testUploadCartException() throws InvioException, IOException {
		doThrow(new IOException("Boom")).when(controller).persist(eq(client), eq(map), eq(cart));
		
		controller.uploadCart(client, map, cart);
	}
	
	@Test
	public final void testUploadCart() throws InvioException, IOException {
		ResponseEntity<String> response = controller.uploadCart(client, map, cart);
		
		verify(controller, times(1)).persist(eq(client), eq(map), eq(cart));
		
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
	public final void testPersist() throws IOException {
		File target = mock(File.class);
		doReturn(target).when(controller).getTargetFile(eq(client), eq(map));
		File source = mock(File.class);
		doReturn(source).when(directoryManager).getTempFile();
		
		controller.persist(client, map, cart);
		verify(directoryManager, times(1)).getTempFile();
		verify(cart, times(1)).transferTo(eq(source));
		verify(directoryManager, times(1)).moveFile(eq(source), eq(target));
	}

	@Test
	public final void testGetTargetFile() throws IOException {
        final File cartDataRoot = new File("/tmp/");

        doReturn(cartDataRoot).when(directoryManager).getCartDataDirectory(eq(map));

        final File result = controller.getTargetFile(client, map);

        assertNotNull(result);
        assertEquals(new File(cartDataRoot, client + ResourceConstants.CART_DATA_FILE_SUFFIX).getAbsolutePath(),
                result.getAbsolutePath());
	}
}
