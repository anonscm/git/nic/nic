package de.tarent.invio.mapserver.controller;

import de.tarent.invio.mapserver.management.DirectoryManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by domano on 23.04.14.
 */
public class GroupDataControllerTest {

    GroupDataController toTest;
    List<String> mapNames = new ArrayList<String>();

    @Mock
    DirectoryManager directoryManager;

    @Mock
    MapListController mapListController;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        final String mapName = "aMapName";
        final File mapDataRoot = new File("/tmp/data/data");
        doReturn(mapDataRoot).when(directoryManager).getMapDataDirectory(anyString());
        doReturn(new File("/tmp/data" + "/groupData")).when(directoryManager).getGroupDataDirectory();
        mapNames.add("fakeMap1");
        mapNames.add("fakeMap2");

        //Create the spy and make the field groupName accessible, so that we can set "tarent AG"
        toTest = new GroupDataController(directoryManager, mapListController);
        Class classToTest = toTest.getClass();
        Field field = classToTest.getDeclaredField("groupName");
        field.setAccessible(true);
        toTest = spy(toTest);
        field.set(toTest, "tarent AG");
        createTestFiles();
    }

    @Test
    public void testIfZipFileIsCreated() throws Exception {
        List<String> mapNames = new ArrayList<String>();
        mapNames.add("fakeMap1");
        mapNames.add("fakeMap2");
        toTest.generateZipByteArray(mapNames);
        File zipFile = new File("/tmp/data/groupData/"+"tarent AG.zip");
        assertTrue(zipFile.exists());
    }

    private void createTestFiles() throws IOException {
        List<File> fakeFiles = new ArrayList<File>();
        File fakefile1= new File("/tmp/data/"+ "fakeMap1" + "/data/" + "fakeMap1" + ".osm");
        fakefile1.getParentFile().mkdirs();
        fakefile1.createNewFile();
        fakeFiles.add(fakefile1);
        PrintWriter out1 = new PrintWriter("/tmp/data/"+ "fakeMap1" + "/data/" + "fakeMap1" + ".osm");
        out1.println("<?xml version='1.0' encoding='UTF-8'?>\n" +
                "<osm version='0.6' upload='true' generator='JOSM'>\n" +
                "  <node id='-8' action='modify' visible='true' lat='50.72200569824068' lon='7.061640755679998'>\n" +
                "    <tag k='indoor' v='metadata' />\n" +
                "    <tag k='indoor_scale' v='2.4' />\n" +
                "    <tag k='namespace_group_name' v='tarent AG' />\n" +
                "    <tag k='namespace_map_name' v='fakeMap1' />\n" +
                "    <tag k='namespace_short_name' v='1' />\n" +
                "    <tag k='north_angle' v='-40' />\n" +
                "  </node>\n" +
                "</osm>\n");
        File fakefile2= new File("/tmp/data/"+ "fakeMap2" + "/data/" + "fakeMap2" + ".osm");
        fakefile2.getParentFile().mkdirs();
        fakefile2.createNewFile();
        fakeFiles.add(fakefile2);
        PrintWriter out2 = new PrintWriter("/tmp/data/"+ "fakeMap2" + "/data/" + "fakeMap2" + ".osm");
        out2.println("<?xml version='1.0' encoding='UTF-8'?>\n" +
                "<osm version='0.6' upload='true' generator='JOSM'>\n" +
                "  <node id='-8' action='modify' visible='true' lat='50.72200569824068' lon='7.061640755679998'>\n" +
                "    <tag k='indoor' v='metadata' />\n" +
                "    <tag k='indoor_scale' v='2.4' />\n" +
                "    <tag k='namespace_group_name' v='tarent AG' />\n" +
                "    <tag k='namespace_map_name' v='fakeMap2' />\n" +
                "    <tag k='namespace_short_name' v='2' />\n" +
                "    <tag k='north_angle' v='-40' />\n" +
                "  </node>\n" +
                "</osm>\n");
        fakeFiles.add(new File("/tmp/data/"+ "fakeMap1" + "/fingerprints/fingerprints_data.json"));
        fakeFiles.add(new File("/tmp/data/"+ "fakeMap2" + "/fingerprints/fingerprints_data.json"));
        fakeFiles.add(new File("/tmp/data/"+ "fakeMap1" + "/tiles/tilemapresource.xml"));
        fakeFiles.add(new File("/tmp/data/"+ "fakeMap2" + "/tiles/tilemapresource.xml"));
        for (File file : fakeFiles) {
            file.delete();
            file.setWritable(true,false);
            file.getParentFile().setWritable(true,false);
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
    }
}
