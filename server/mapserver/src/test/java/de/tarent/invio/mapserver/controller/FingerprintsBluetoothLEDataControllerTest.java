package de.tarent.invio.mapserver.controller;

import de.tarent.invio.mapserver.ResourceConstants;
import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.management.DirectoryManager;
import de.tarent.invio.mapserver.wrapper.IOUtilWrapper;
import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.util.ReflectionTestUtils.setField;

@RunWith(MockitoJUnitRunner.class)
public class FingerprintsBluetoothLEDataControllerTest {

    private FingerprintsBluetoothLEDataController fdc;

    @Mock
    private IOUtilWrapper ioUtilWrapper;

    @Mock
    private DirectoryManager directoryManager;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        final FingerprintsBluetoothLEDataController tempFdc = new FingerprintsBluetoothLEDataController();
        setField(tempFdc, "directoryManager", directoryManager);
        setField(tempFdc, "ioUtilWrapper", ioUtilWrapper);
        fdc = spy(tempFdc);
        when(directoryManager.getTempDirectory()).thenCallRealMethod();
        when(directoryManager.getTempFile()).thenCallRealMethod();
    }

    @After
    public void tearDown() throws IOException {
        final File fingerprintsData = new File(ResourceConstants.FINGERPRINTS_DATA_FILENAME_BLUETOOTH_LE);
        fingerprintsData.delete();
    }

    @Test
    public void testUploadWifiFingerprints() throws InvioException, IOException, JSONException {
        String json = "[{\"histogram\":{\"00:1f:3f:12:30:32\":{\"-90\":0.2,\"-89\":0.8}},\"id\":\"hello\"," +
                "\"point\":{\"mAltitude\":0,\"mLatitudeE6\":76516818,\"mLongitudeE6\":-147656250}}]";
        final ResponseEntity<String> result = fdc.uploadFingerprints("mapName", json);

        assertEquals(HttpStatus.CREATED.value(), result.getStatusCode().value());
        verify(directoryManager, times(1)).getMapFingerprintsDirectory(anyString());
        verifyNoMoreInteractions(directoryManager);
        verify(ioUtilWrapper, times(1)).closeQuietly(any(FileWriter.class));
        verifyNoMoreInteractions(ioUtilWrapper);
    }

    @Test(expected = InvioException.class)
    public void testUploadWifiFingerprintsThrowsInvioExceptionWhenDirectoryManagerReturnsFile() throws IOException, JSONException, InvioException {
        String json = "foo";
        File file = new File("file");
        when(directoryManager.getMapFingerprintsDirectory("mapName")).thenReturn(file);

        fdc.uploadFingerprints("mapName", json);

        verify(ioUtilWrapper).closeQuietly(any(FileWriter.class));
        verifyNoMoreInteractions(ioUtilWrapper);
    }

}