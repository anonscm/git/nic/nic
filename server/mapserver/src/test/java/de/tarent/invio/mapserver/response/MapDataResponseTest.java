package de.tarent.invio.mapserver.response;

import com.google.gson.Gson;
import junit.framework.TestCase;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mockito.Mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.TreeMap;

public class MapDataResponseTest extends TestCase {

    MapDataResponse mapDataResponse;

    int testStatusCode =1337;
    String testKey = "testKey";
    String testValue = "testValue";

    @Mock
    InputStream json;

    @Test
    public void testSetJsonValue() {
        mapDataResponse = new MapDataResponse();
        mapDataResponse.setJsonValue(testKey, testValue);
        Object result = mapDataResponse.getJsonBodyValue(testKey);
        assertEquals(testValue, result);
    }

    @Test
    public void testIfStatuscodeIsSet(){
        mapDataResponse = new MapDataResponse(testStatusCode);
        int result =mapDataResponse.getStatus();
        assertEquals(testStatusCode, result);
    }

    @Test(expected = IOException.class)
    public void testIfInputStreamIOExceptionIsThrown() throws IOException{
        try {
            mapDataResponse = new MapDataResponse(testStatusCode, json);
        } catch (IOException e) {
            //nothing
        }
    }

    @Test
    public void testIfInputStreamIsSet() throws IOException {
        Map<String, Object> jsonBody = new TreeMap<String, Object>();
        jsonBody.put(testKey, testValue);
        String jsonString = new Gson().toJson(jsonBody);
        InputStream json = IOUtils.toInputStream(jsonString);
        mapDataResponse = new MapDataResponse(testStatusCode, json);

        final String expected = "MapDataResponse [statusCode=1337, json={testKey=testValue}]";
        final String result = mapDataResponse.toString();

        assertEquals(expected, result);

    }

    @Test
    public void testIfJsonbodyIsSet(){
        Map<String, Object> jsonBody = new TreeMap<String, Object>();
        jsonBody.put(testKey, testValue);
        mapDataResponse = new MapDataResponse(testStatusCode, jsonBody);
        Object jsonBodyResult = mapDataResponse.getJsonBody();
        int statusResult =mapDataResponse.getStatus();
        assertEquals(testStatusCode, statusResult);
        assertEquals(jsonBody, jsonBodyResult);
    }

}
