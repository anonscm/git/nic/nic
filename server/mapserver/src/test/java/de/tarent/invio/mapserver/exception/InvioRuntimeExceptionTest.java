package de.tarent.invio.mapserver.exception;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class InvioRuntimeExceptionTest  {

    @Test
    public void testIfMessageIsSet(){
        String testMessage = "testmessage";
        InvioRuntimeException tempInvioRuntimeException = new InvioRuntimeException(testMessage);
        assertTrue(tempInvioRuntimeException.getMessage().equals(testMessage));
    }

    @Test
    public void testIfCauseIsSet(){
        Throwable testCause = new Throwable();
        InvioRuntimeException tempInvioRuntimeException = new InvioRuntimeException(testCause);
        assertTrue(tempInvioRuntimeException.getCause()==testCause);
    }


    @Test
    public void testIfMessageAndCauseAreSet(){
        String testMessage = "testmessage";
        Throwable testCause = new Throwable();
        InvioRuntimeException tempInvioException = new InvioRuntimeException(testMessage,testCause);
        assertTrue(tempInvioException.getMessage().equals(testMessage));
        assertTrue(tempInvioException.getCause()==testCause);
    }
}
