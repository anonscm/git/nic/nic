package de.tarent.invio.mapserver.gdal;

import de.tarent.invio.mapserver.exception.GdalException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

// TODO: we need to test more than just the output-directory-name...
public class GdalTest {

    // The object we want to test:
    Gdal gdal;


    @Before // Call this method before each of the @Test-methods:
    public void setUp() {
        gdal = new Gdal();
    }

    @Test
    public void testMakeRasterTilesPNG() throws IOException {
        String src = this.getClass().getResource("/kitty1.png").getPath();
        String dst = gdal.makeRasterTiles(src, 1f, 2f, 2f, 1f);
        assertEquals(Gdal.OUTPUT_DIR + "/kitty1", dst);
    }

    @Test
    public void testMakeRasterTilesJPG() throws IOException {
        String src = this.getClass().getResource("/kitty2.jpg").getPath();
        String dst = gdal.makeRasterTiles(src, 1f, 2f, 2f, 1f);
        assertEquals(Gdal.OUTPUT_DIR + "/kitty2", dst);
    }

    @Ignore("The gdal2tiles-script doesn't support GIF.")
    @Test
    public void testMakeRasterTilesGIF() throws IOException {
        String src = this.getClass().getResource("/kitty3.gif").getPath();
        String dst = gdal.makeRasterTiles(src, 1f, 2f, 2f, 1f);
        assertEquals(Gdal.OUTPUT_DIR + "/kitty3", dst);
    }

    @Test(expected = GdalException.class)
    public void testMakeRasterTilesFileNotFound() throws IOException {
        gdal.makeRasterTiles("foo", 1f, 2f, 2f, 1f);
    }

    @Test(expected = GdalException.class)
    public void testMakeRasterTilesNoImage() throws IOException {
        String src = this.getClass().getResource("/no-image").getPath();
        gdal.makeRasterTiles(src, 1f, 2f, 2f, 1f);
    }

}
