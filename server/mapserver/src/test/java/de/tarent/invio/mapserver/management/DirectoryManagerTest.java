package de.tarent.invio.mapserver.management;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.IOException;

import de.tarent.invio.mapserver.exception.InvioException;
import org.junit.Before;
import org.junit.Test;

public class DirectoryManagerTest {

    static final String TMP_DIR = System.getProperty("java.io.tmpdir");
    static final String MAP_ROOT = "root/";

    DirectoryManager toTest;

    @Before
    public void setup() throws InvioException {
        // This might cause problems as a {@link System} property is being
        // changed. It might be better to have this
        // line cleaned up after all tests have been run. (Maybe saving the
        // original value and then setting it after).
        System.setProperty("catalina.base", TMP_DIR);

        toTest = spy(new DirectoryManager(MAP_ROOT));
    }

    private String normalizePath(String path){
        return new File(path).getAbsolutePath();
    }

    @Test
    public void getTempFile() throws IOException {
        final File result = toTest.getTempFile();

        assertTrue(result.isFile());
        assertTrue(result.exists());
    }

    @Test
    public void getTempDirectory() throws IOException {
        final File result = toTest.getTempDirectory();

        assertTrue(result.isDirectory());
        assertTrue(result.exists());
    }

    @Test
    public void getMapTilesDirectory() throws IOException {
        final String mapName = "EG1";
        final File result = toTest.getMapTilesDirectory(mapName);

        final String expected = normalizePath(
                TMP_DIR + "/" + MAP_ROOT +
                mapName + DirectoryManager.SUB_DIR_TILES);

        assertEquals(expected, result.getAbsolutePath());
        assertTrue(result.isDirectory());
        assertTrue(result.exists());
    }

    @Test
    public void getMapDataDirectory() throws IOException {
        final String mapName = "EG1";
        final File result = toTest.getMapDataDirectory(mapName);

        final String expected = normalizePath(
                TMP_DIR + "/" + MAP_ROOT +
                mapName + DirectoryManager.SUB_DIR_DATA);

        assertEquals(expected, result.getAbsolutePath());
        assertTrue(result.isDirectory());
        assertTrue(result.exists());
    }

    @Test
    public void getMapFingerprintsDirectory() throws IOException {
        final String mapName = "EG1";
        final File result = toTest.getMapFingerprintsDirectory(mapName);

        final String expected = normalizePath(
                TMP_DIR + "/" + MAP_ROOT +
                mapName + DirectoryManager.SUB_DIR_FINGERPRINTS);

        assertEquals(expected, result.getAbsolutePath());
        assertTrue(result.isDirectory());
        assertTrue(result.exists());
    }

    @Test
    public void getCartsDataDirectory() throws IOException {
        final String mapName = "EG1";
        final File result = toTest.getCartDataDirectory(mapName);

        final String expected = normalizePath(
                TMP_DIR + "/" + MAP_ROOT +
                mapName + DirectoryManager.SUB_DIR_CARTS);

        assertEquals(expected, result.getAbsolutePath());
        assertTrue(result.isDirectory());
        assertTrue(result.exists());
    }

    @Test
    public void testMoveFile() throws IOException {
        final File currentLocation = File.createTempFile("tmp", ".tmp");
        currentLocation.deleteOnExit();

        final File destinationLocation = File.createTempFile("tmp", ".tmp");
        assertTrue("Vorbedingung nicht erfüllt!", destinationLocation.delete());
        destinationLocation.deleteOnExit();

        toTest.moveFile(currentLocation, destinationLocation);

        assertFalse(currentLocation.exists());
        assertTrue(destinationLocation.exists());
    }

    @Test(expected = IOException.class)
    public void testMoveFileFailedDeletion() throws IOException {
        final File currentLocation = File.createTempFile("tmp", ".tmp");
        currentLocation.deleteOnExit();
        final File destinationLocation = mock(File.class);

        doReturn(true).when(destinationLocation).exists();
        doReturn(false).when(destinationLocation).delete();

        toTest.moveFile(currentLocation, destinationLocation);
    }

    @Test
    public void moveFileExistingDestination() throws IOException {
        final File currentLocation = File.createTempFile("tmp", ".tmp");
        currentLocation.deleteOnExit();

        final File destinationLocation = File.createTempFile("tmp", ".tmp");
        destinationLocation.createNewFile();
        destinationLocation.deleteOnExit();

        toTest.moveFile(currentLocation, destinationLocation);

        assertFalse(currentLocation.exists());
        assertTrue(destinationLocation.exists());
    }
}
