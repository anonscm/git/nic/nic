#!/bin/sh
image=$1
mapname=$2
scriptdir=/var/lib/tomcat7/webapps/scripts

#Generate additional level
cd /tmp; bash $scriptdir/add_zoom_level.sh $image $mapname; cd $scriptdir

#Create Border.jpg
sh $scriptdir/generate_border_image.sh $image

#Retain color of the right upper pixel of the Border image
color=`convert Border.jpg[1x1+0+0] -depth 8 txt: | tail -n +2 | sed -n 's/^.*\(#[^ ]*\).*$/\1/p'`

#Convert previously gdal-generated PNG tiles to JPG
sh $scriptdir/make_jpg.sh $color

#Generate border tiles for the given map
php $scriptdir/make_border_tiles.php $mapname

#Clean up
rm Border.jpg

exit 0