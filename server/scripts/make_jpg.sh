#!/bin/sh
color=$1
for d in `\ls /var/lib/tomcat7/webapps/maps`; do if [ $(find /var/lib/tomcat7/webapps/maps/$d -name "*.jpg" | wc -l) -gt 0 ]; then echo "$d alreads has JPGs"; else echo "generating JPGs for $d..."; for f in `find /var/lib/tomcat7/webapps/maps/$d -name "*.png"`; do convert -quality 99 -fill "$color" -opaque none $f ${f%.*}.jpg; done; fi; done
