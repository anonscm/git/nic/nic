#!/bin/bash
#-
# Copyright © 2014
#	Thorsten Glaser <t.glaser@tarent.de>
#
# Provided that these terms and disclaimer and all copyright notices
# are retained or reproduced in an accompanying document, permission
# is granted to deal in this work without restriction, including un‐
# limited rights to use, publicly perform, distribute, sell, modify,
# merge, give away, or sublicence.
#
# This work is provided “AS IS” and WITHOUT WARRANTY of any kind, to
# the utmost extent permitted by applicable law, neither express nor
# implied; without malicious intent or gross negligence. In no event
# may a licensor, author or contributor be held liable for indirect,
# direct, other damage, loss, or other issues arising in any way out
# of dealing in the work, even if advised of the possibility of such
# damage or existence of a defect, except proven that it results out
# of said person’s immediate fault when using the work as intended.
#-
# Zu installieren: ed, xmlstarlet

mapbase=/var/lib/tomcat7/webapps/maps
if [[ -z $1 || -z $2 || $1 = -h || $1 = --help ]]; then
	echo >&2 "Syntax: $0 foo.tif Kitty"
	echo >&2 "where foo.tif is the map source and Kitty the target path"
	echo >&2 "    under $mapbase/"
	exit 1
fi

tiffile=$1
mapdir=$mapbase/$2/tiles

if [[ $tiffile != *.tif ]]; then
	echo >&2 'E: Syntax error: TIF file not *.tif'
	exit 1
fi
if [[ ! -s $tiffile ]]; then
	echo >&2 'E: TIF file does not exist'
	exit 1
fi
if [[ ! -s $mapdir/tilemapresource.xml ]]; then
	echo >&2 'E: tilemapresource.xml does not exist'
	exit 1
fi

lastzoomstufe=$(xmlstarlet sel -t -m '//TileSet[last()]' -v @href \
    <"$mapdir/tilemapresource.xml")
if [[ -z $lastzoomstufe ]]; then
	echo >&2 'E: could not figure out last Zoomstufe'
	exit 1
fi
if [[ $lastzoomstufe -lt 1 ]]; then
	echo >&2 "E: last Zoomstufe '$lastzoomstufe' too small"
	exit 1
fi
if (( lastzoomstufe >= 21 )); then
	echo >&2 "I: last Zoomstufe already $lastzoomstufe, nothing done"
	exit 0
fi
(( nextzoomstufe = lastzoomstufe + 1 ))

gdal2tiles.py -z $nextzoomstufe -w none -r near $tiffile 
e=$?
if (( e )); then
	echo >&2 "E: gdal exited with code $e"
	exit 1
fi
gdaldir=${tiffile%.tif}

if [[ ! -d $gdaldir || ! -s $gdaldir/tilemapresource.xml || \
    ! -d $gdaldir/$nextzoomstufe ]]; then
	echo >&2 "E: '$gdaldir' not a valid output dir"
	exit 1
fi

newtileset=$(fgrep '<TileSet ' "$gdaldir/tilemapresource.xml")
ed -s "$mapdir/tilemapresource.xml" <<EOF
/<\/TileSets>/i
$newtileset
.
w
q
EOF

mv "$gdaldir/$nextzoomstufe" "$mapdir/"
rm -rf "$gdaldir"
echo >&2 "I: all done"
exit 0
