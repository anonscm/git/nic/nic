<?php

// resolution of nexus 10: 2560x1600px
// size of tiles: 256x256px
// both display orientations -> 2560/256px=10 folders and tiles per zoom level
$maxTilesAndFolders = 10;

$images = `find /var/lib/tomcat7/webapps/maps/|grep tiles|grep -v png|sort`;
$lines = explode("\n", $images);

// Find maps...
$maps = array();
foreach ($lines as $line) {
   	 if (preg_match("/webapps\/maps\/(.+)\/tiles\/(\d\d)$/", $line, $matches)) {
     	   $maps[$matches[1]] = true;
 	   }
 	   if (preg_match("/webapps\/maps\/(.+)\/tiles\/done$/", $line, $matches)) {
  	      unset($maps[$matches[1]]);
   	 }
}

// Get the mapname as an input for the script and start producing new tiles
if (!isset($argv[1])) {
	echo "Needs map name as argument!\n";
	exit;
} else {
    	$maps = array($argv[1] => true);

	// Foreach map...
	foreach (array_keys($maps) as $map) {
	    $minZoom = 99;
	    $maxZoom = 1;

	    $mapPath = "/var/lib/tomcat7/webapps/maps/".$map."/tiles/";
	    echo "Filling $mapPath...\n";
	    // Find zoom levels
	    foreach ($lines as $line) {
		if (preg_match("/webapps\/maps\/".$map."\/tiles\/(\d+)$/", $line, $matches)) {
		    $level = $matches[1];
		    if ($level < $minZoom) $minZoom = $level;
		    if ($level > $maxZoom) $maxZoom = $level;
		}
	    }

	    echo "Zoomlevels from ".$minZoom." to ".$maxZoom."\n";

	    // Make files for each zoom level
	    for ($level = $minZoom; $level <= $maxZoom; $level++) {
		$firstFolder = 9999999;
		$lastFolder = 0;

		for ($lineNum = 0; $lineNum < count($lines); $lineNum++) {
		    $line = $lines[$lineNum];
		    if (preg_match("/webapps\/maps\/".$map."\/tiles\/".$level."\/(\d+)$/", $line, $matches)) {
			// Next folder...
			$folder = $matches[1];
			if ($folder > $lastFolder) $lastFolder = $folder;
			if ($folder < $firstFolder) $firstFolder = $folder;
			$nextLine = $lines[$lineNum+1];
			preg_match("/webapps\/maps\/".$map."\/tiles\/".$level."\/\d+\/(\d+).jpg$/", $nextLine, $matches);
			$firstFile = $matches[1];
			$lastFile = "";
			// Find last image in folder...
			for ($folderLineNum = $lineNum+1; $folderLineNum < count($lines); $folderLineNum++) {
			    $folderLine = $lines[$folderLineNum];
			    if (!preg_match("/webapps\/maps\/".$map."\/tiles\/".$level."\/\d+\/(\d+).jpg$/", $folderLine, $matches)) {
				//echo "-> $lastFile\n";
				break;
			    } else {
				$lastFile = $matches[1];
			    }
			}

			$numTiles = $lastFile - $firstFile + 1;
			$numNewTiles = $maxTilesAndFolders - $numTiles + 2;

			echo "Number of generated tiles top and bottom: ".$numNewTiles."\n";

			for ($y = 1; $y <= $numNewTiles; $y++) {
				$finalFile = $level."/".$folder."/".($lastFile+$y).".jpg";
				$zeroFile = $level."/".$folder."/".($firstFile-$y).".jpg";
				$cpCmd = "cp Border.jpg ".$mapPath.$zeroFile;
				//echo "== $cpCmd\n";
				`$cpCmd`;
				$cpCmd = "cp Border.jpg ".$mapPath.$finalFile;
				//echo "== $cpCmd\n";
				`$cpCmd`;
			}
		    }
		}

		$numFolders = $lastFolder - $firstFolder + 1;
		$numNewFolders = $maxTilesAndFolders - $numFolders + 2;

		echo "Number of generated tiles left and right: ".$numNewFolders."\n";

		for ($x = 1; $x <= $numNewFolders; $x++) {

			$zeroFolder = $firstFolder-$x;
			$finalFolder = $lastFolder+$x;
			$src = $mapPath.$level."/".$firstFolder;

			// Now we copy the images from one folder, to get the filenames, and then xargs-link all images to the Border.jpg:
			$dst = $mapPath.$level."/".$zeroFolder;
			$cpCmd = "cp -r ".$src." ".$dst;
			`$cpCmd`;
			$lnCmd = "find $dst | grep jpg | xargs -L1 ln -fT Border.jpg";
			`$lnCmd\n`;
		/*
			$dst = $mapPath.$level."/".($zeroFolder-1);
			$cpCmd = "cp -r ".$src." ".$dst;
			`$cpCmd`;
			$lnCmd = "find $dst | grep jpg | xargs -L1 ln -fT ~root/black.jpg";
			`$lnCmd\n`;
		*/
			$dst = $mapPath.$level."/".$finalFolder;
			$cpCmd = "cp -r ".$src." ".$dst;
			`$cpCmd`;
			$lnCmd = "find $dst | grep jpg | xargs -L1 ln -fT Border.jpg";
			`$lnCmd\n`;
		/*
			$dst = $mapPath.$level."/".($finalFolder+1);
			$cpCmd = "cp -r ".$src." ".$dst;
			`$cpCmd`;
			$lnCmd = "find $dst | grep jpg | xargs -L1 ln -fT ~root/black.jpg";
			`$lnCmd\n`;
		*/
		}
	    }
	    $touchCmd = "touch ".$mapPath."done";
	    `$touchCmd`;
	}
}

?>
