#!sh/bash

IMAGE=$1

# This crops the first pixel and stops imagemagick from creating all the other pixels
# as images as well.
convert $IMAGE -crop 1x1+0+0 +repage Border.jpg

# Transform the newly generated pixel image into a 256x256 image.
convert Border.jpg -resize 256x256 Border.jpg
