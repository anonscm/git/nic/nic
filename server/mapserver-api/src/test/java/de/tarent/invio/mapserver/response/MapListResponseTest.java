package de.tarent.invio.mapserver.response;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class MapListResponseTest {

    private MapListResponse mapListResponse;

    @Before
    public void setup() {
        mapListResponse = new MapListResponse();
    }

    @Test
    public void testThatSetMapListSetsTheListOfMapsInTheJsonBody() {
        final List<String> value = new ArrayList<String>();
        value.add("One");
        value.add("Two");
        value.add("Three");
        mapListResponse.setMapList(value);

        assertEquals(value, mapListResponse.getMapList());
    }

    @Test
    public void testConstructors() throws IOException {
        final int statusCode = 1;
        final Map<String, Object> jsonBody = new HashMap<String, Object>();
        jsonBody.put("test", "test");

        final MapListResponse responseStatusMap = new MapListResponse(statusCode, jsonBody);
        assertEquals(statusCode, responseStatusMap.getStatus());
        assertEquals("test", responseStatusMap.getJsonBodyValue("test"));
        assertNull(responseStatusMap.getStatusMessage());

        final Map<String, Object> nullJsonBody = null;
        final MapListResponse responseStatusMapNull = new MapListResponse(statusCode, nullJsonBody);
        assertEquals(statusCode, responseStatusMapNull.getStatus());
        assertEquals(Response.NO_JSON_MESSAGE, responseStatusMapNull.getStatusMessage());
        assertNotNull(responseStatusMapNull.getJsonBody());
    }

}