package de.tarent.invio.mapserver.response;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ExceptionResponseTest {

    private ExceptionResponse exceptionResponse;

    @Before
    public void setup() {
        exceptionResponse = new ExceptionResponse();
    }

    @Test
    public void testThatSetJsonValueSetsTheValueInTheJsonBody() {
        final String key = "key";
        final String value = "value";
        exceptionResponse.setJsonValue(key, value);

        assertEquals(value, exceptionResponse.getJsonBodyValue(key));
        assertEquals(ExceptionResponse.INTERNAL_SERVER_ERROR_STATUS, exceptionResponse.getStatus());
    }

    @Test
    public void testConstructors() throws IOException {
        final int statusCode = 1;
        final Map<String, Object> jsonBody = new HashMap<String, Object>();
        jsonBody.put("test", "test");

        final InputStream inputStream = this.getClass().getResource("/response.json").openStream();
        final ExceptionResponse responseStatusStream = new ExceptionResponse(statusCode, inputStream);
        assertEquals(statusCode, responseStatusStream.getStatus());
        assertEquals("test", responseStatusStream.getJsonBodyValue("test"));
        assertNull(responseStatusStream.getStatusMessage());

        final InputStream nullInputStream = null;
        final ExceptionResponse responseStatusStreamNull = new ExceptionResponse(statusCode, nullInputStream);
        assertEquals(statusCode, responseStatusStreamNull.getStatus());
        assertEquals(Response.NO_JSON_MESSAGE, responseStatusStreamNull.getStatusMessage());

        final ExceptionResponse responseStatusMap = new ExceptionResponse(statusCode, jsonBody);
        assertEquals(statusCode, responseStatusMap.getStatus());
        assertEquals("test", responseStatusMap.getJsonBodyValue("test"));
        assertNull(responseStatusMap.getStatusMessage());

        final Map<String, Object> nullJsonBody = null;
        final ExceptionResponse responseStatusMapNull = new ExceptionResponse(statusCode, nullJsonBody);
        assertEquals(statusCode, responseStatusMapNull.getStatus());
        assertEquals(Response.NO_JSON_MESSAGE, responseStatusMapNull.getStatusMessage());
        assertNotNull(responseStatusMapNull.getJsonBody());
    }

}