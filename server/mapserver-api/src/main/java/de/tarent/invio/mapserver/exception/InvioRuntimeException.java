package de.tarent.invio.mapserver.exception;

/**
 * Every {@link RuntimeException} that our code throws should be a {@link InvioRuntimeException} instead.
 * <p/>
 * Jede {@link RuntimeException} die wir in unserem Code werfen,
 * sollte eine {@link InvioRuntimeException} sein!
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 */
public class InvioRuntimeException extends RuntimeException {

    /**
     * Default constructor.
     */
    public InvioRuntimeException() {
        super();
    }

    /**
     * Constructor.
     *
     * @param message the message to be shown
     */
    public InvioRuntimeException(final String message) {
        super(message);
    }

    /**
     * Constructor.
     *
     * @param cause the cause of the exception
     */
    public InvioRuntimeException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     *
     * @param message the message to be shown
     * @param cause the cause of the exception
     */
    public InvioRuntimeException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
