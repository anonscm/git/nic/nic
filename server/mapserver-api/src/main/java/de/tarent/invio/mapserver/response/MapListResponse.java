package de.tarent.invio.mapserver.response;

import java.util.List;
import java.util.Map;

/**
 * This {@link Response} will be used by map list requests
 */
public class MapListResponse extends Response {

    private static final String MAP_LIST = "mapList";

    /**
     * Default constructor.
     */
    public MapListResponse() {
        super();
    }

    /**
     * TODO: We need to document why we MUST use this constructor.
     *       ...yes, why must we?
     * Constructor.
     *
     * @param status the status
     * @param body the json data body
     */
    public MapListResponse(final int status, final Map<String, Object> body) {
        super(status, body);
    }

    /**
     * Puts the list of the maps into the JSON body
     * @param mapList list of the existing maps on the server
     * @return MapListResponse containing a map list inside the JSON body
     */
    public final MapListResponse setMapList(List<String> mapList) {
        setJsonValue(MAP_LIST, mapList);
        return this;
    }

    public final List<String> getMapList(){
        return (List<String>) getJsonBodyValue(MAP_LIST);
    }

}
