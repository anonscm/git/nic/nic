package de.tarent.invio.mapserver.response;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * This {@link Response} will used by the map data requests.
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 *
 */
public class MapDataResponse extends Response {

    /**
     * Default constructor.
     */
    public MapDataResponse() {
        super();
    }

    /**
     * Constructor.
     *
     * @param statusCode the status code
     */
    public MapDataResponse(final int statusCode) {
        super(statusCode);
    }

    /**
     * Constructor.
     *
     * @param status the status code
     * @param json the {@link InputStream} from json
     * @throws IOException if something is wrong with the {@link InputStream}
     */
    public MapDataResponse(final int status, final InputStream json) throws IOException {
        super(status, json);
    }

    /**
     * Constructor.
     *
     * @param status the status
     * @param body the json data body
     */
    public MapDataResponse(final int status, final Map<String, Object> body) {
        super(status, body);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MapDataResponse setJsonValue(final String name, final Object value) {
        super.setJsonValue(name, value);
        return this;
    }
}
