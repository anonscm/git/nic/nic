package de.tarent.invio.mapserver.exception;

/**
 * Every new {@link Exception} that our code throws should be a {@link InvioException} instead.
 * <p/>
 * Jede neue {@link Exception} die wir in unserem Code werfen,
 * sollte eine {@link InvioException} sein!
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 */
public class InvioException extends Exception {

    /**
     * Default constructor.
     */
    public InvioException() {
        super();
    }

    /**
     * Constructor.
     *
     * @param message the message to be shown
     */
    public InvioException(final String message) {
        super(message);
    }

    /**
     * Constructor.
     *
     * @param cause the cause of the exception
     */
    public InvioException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     *
     * @param message the message to be shown
     * @param cause the cause of the exception
     */
    public InvioException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
