package de.tarent.invio.mapserver.response;

import java.util.Map;

/**
 * This {@link Response} will be used by fingerprint data requests.
 *
 * @author Atanas Alexandrov, tarent solutions GmbH
 * @author Désirée Amling <d.amling@tarent.de>
 */
public class FingerprintsDataResponse extends Response {

    /**
     * Default constructor.
     */
    public FingerprintsDataResponse() {
        super();
    }

    /**
     * Constructor.
     *
     * @param status the status
     * @param body the json data body
     */
    public FingerprintsDataResponse(final int status, final Map<String, Object> body) {
        super(status, body);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FingerprintsDataResponse setJsonValue(final String name, final Object value) {
        super.setJsonValue(name, value);
        return this;
    }

}
