package de.tarent.invio.mapserver;

/**
 * This interface includes all resource-constants.
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 *
 */
public interface ResourceConstants {

    /* ==================
     * Global
     * ==================
     */

    String PARAM_MAP_NAME = "map_name";

    /* ==================
     * MapList specific
     * ==================
     */

    String MAP_LIST_ROOT = "/map_list";
    String MULTILEVEL_MAP_LIST = "/multilevel_map_list";

    /* ==================
     * Position specific
     * ==================
     */

    String POSITION_ROOT = "/position";
    String POSITION_PARAM_LONGITUDE = "longitude";
    String POSITION_PARAM_LATITUDE = "latitude";
    String POSITION_PARAM_DEVICE_ID = "device_id";
    String POSITION_PARAM_LEVEL = "level";

    /* ==================
     * GroupData specific
     * ==================
     */

    String GROUP_DATA_ROOT = "/group_data";


    /* ==================
     * Mapdata specific
     * ==================
     */

    String MAP_DATA_ROOT	=	"/map_data";

    String MAP_DATA_NAME = "map_data_name";
    String MAP_DATA = "map_data";
    String MAPDATA_TEMPLATE = "/maps/{mapName}/data/";

    /* ==================
     * Mapimage specific
     * ==================
     */
    String MAP_IMAGE_ROOT =	"/map_images";

    String MAP_IMAGE_PARAM_MAP_IMAGE       =   "map_image";

    String MAP_IMAGE_PARAM_LOWER           =   "lower";
    String MAP_IMAGE_PARAM_UPPER           =   "upper";
    String MAP_IMAGE_PARAM_LEFT            =   "left";
    String MAP_IMAGE_PARAM_RIGHT           =   "right";

    String MAP_TILEMAPRESOURCE_TEMPLATE    = "/maps/{mapName}/tiles/tilemapresource.xml";


    /* ====================
     * Fingerprint specific
     * ====================
     */
    String FINGERPRINTS_DATA_ROOT_WIFI = "/fingerprints";
    String FINGERPRINTS_DATA_ROOT_BLUETOOTH_LE      = "/fingerprints_btle";
    String FINGERPRINTS_DATA_PARAM_WIFI     = "fingerprints_data";
    String FINGERPRINTS_DATA_PARAM_BLUETOOTH_LE = "fingerprints_data_btle";
    String FINGERPRINTS_DATA_FILENAME_WIFI = "fingerprints_data.json";
    String FINGERPRINTS_DATA_FILENAME_BLUETOOTH_LE = "fingerprints_btle_data.json";
    String FINGERPRINTS_TEMPLATE_WIFI       = "/maps/{mapName}/fingerprints/"+FINGERPRINTS_DATA_FILENAME_WIFI;
    String FINGERPRINTS_TEMPLATE_BLUETOOTH_LE = "/maps/{mapName}/fingerprints/"+FINGERPRINTS_DATA_FILENAME_BLUETOOTH_LE;

    /* ====================
     * Shopping Cart specific
     * ====================
     */
    String CART_DATA_ROOT = "/carts";
    String CART_DATA_PARAM_CLIENT_ID = "client_id";
    String CART_DATA_PARAM_CART = "cart";
    String CART_DATA_FILE_SUFFIX = ".json";

    /* ====================
     * Map properties specific
     * ====================
     */
    String MAP_PROPERTIES_ROOT = "/properties";
    String MAP_PROPERTIES_SUFFIX = ".properties";
}
