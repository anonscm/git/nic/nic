package de.tarent.invio.mapserver;

import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.exception.InvioRuntimeException;
import de.tarent.invio.mapserver.response.CartDataResponse;
import de.tarent.invio.mapserver.response.MapDataResponse;
import de.tarent.invio.mapserver.response.MapImageResponse;
import de.tarent.invio.mapserver.response.MapListResponse;
import de.tarent.invio.mapserver.response.MapPropertiesResponse;
import de.tarent.invio.mapserver.response.Response;
import de.tarent.invio.mapserver.response.ResponseBuilder;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.zip.GZIPInputStream;

/**
 * This class is responsible for the communication with the mapserver.
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 */
public class MapServerClientImpl implements MapServerClient, ResourceConstants {
    private static final int FINGERPRINT_WIFI = 0;
    private static final int FINGERPRINT_BLUETOOTH = 1;

    // TODO: don't define constants like these. Take them from someplace else. Some library is certain to have them.
    private static final String TEXT_PLAIN = "text/plain";
    private static final String APPLICATION_JSON = "application/json";
    private static final String UTF_8 = "UTF-8";

    private String endpoint;

    private HttpClient client;

    private final Object lock = new Object();
    /**
     * Create a {@link MapServerClient}.
     *
     * @param serverEndpoint the endpoint of the destination server.
     *                       For example: http://localhost:8080/mapserver
     */
    public MapServerClientImpl(String serverEndpoint) {
        this(serverEndpoint, new DefaultHttpClient());
    }

    /**
     * Create a {@link MapServerClient}.
     *
     * @param serverEndpoint the endpoint of the destination server.
     *                       For example: http://localhost:8080/mapserver
     * @param client the HttpClient that this MapServerClient is supposed to use.
     */
    public MapServerClientImpl(String serverEndpoint, HttpClient client) {
        this.client = client;

        try {
            checkEndpoint(serverEndpoint);
        } catch (final MalformedURLException e) {
            throw new InvioRuntimeException("Invalid endpoint!", e);
        }

        this.endpoint = serverEndpoint;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEndpoint() {
        return endpoint;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MapImageResponse uploadMapImage(String mapName, File mapImageFile,
                                           final float left, final float upper, final float right, final float lower)
            throws IOException {
        final InputStream is = new FileInputStream(mapImageFile);
        final MapImageResponse response = uploadMapImage(mapName, is, left, upper, right, lower);

        return response;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MapImageResponse uploadMapImage(final String mapName, final InputStream mapImage,
                                           final float left, final float upper, final float right, final float lower)
            throws IOException {
        final HttpPost postRequest = preparePostRequestForImageUpload(mapName, mapImage, left, upper, right, lower);
        final HttpResponse httpResponse = threadSafeExecute(postRequest);
        final MapImageResponse response = buildResponse(httpResponse, MapImageResponse.class);
        return response;
    }

    /**
     * Prepare the {@link HttpPost} request for the image upload.
     *
     * @param mapName  the map name
     * @param mapImage the map image as {@link InputStream}
     * @param left the left coordinate of the boundingbox
     * @param upper the upper coordinate of the boundingbox
     * @param right the right coordinate of the boundingbox
     * @param lower the lower coordinate of the boundingbox
     * @throws UnsupportedEncodingException should never really be thrown, because we hardcoded UTF_8 here.
     * @return the post request which will be send
     */
    private HttpPost preparePostRequestForImageUpload(final String mapName, final InputStream mapImage,
                                                      final Float left, final Float upper,
                                                      final Float right, final Float lower)
            throws UnsupportedEncodingException {

        final HttpPost postRequest = new HttpPost(getResourceEndpoint(MAP_IMAGE_ROOT));
        final InputStreamBody fileBody = new InputStreamBody(
                mapImage,
                "application/octet-stream",
                mapName); //it is very important to give it a "filename"
        final MultipartEntity reqEntity = new MultipartEntity();
        // TODO: we should probably not have a separate part for each string. Maybe one json-string with all the data?
        final Charset charset = Charset.forName(UTF_8);
        reqEntity.addPart(PARAM_MAP_NAME, new StringBody(mapName, TEXT_PLAIN, charset));
        reqEntity.addPart(MAP_IMAGE_PARAM_LEFT, new StringBody(left.toString(), TEXT_PLAIN, charset));
        reqEntity.addPart(MAP_IMAGE_PARAM_UPPER, new StringBody(upper.toString(), TEXT_PLAIN, charset));
        reqEntity.addPart(MAP_IMAGE_PARAM_RIGHT, new StringBody(right.toString(), TEXT_PLAIN, charset));
        reqEntity.addPart(MAP_IMAGE_PARAM_LOWER, new StringBody(lower.toString(), TEXT_PLAIN, charset));
        reqEntity.addPart(MAP_IMAGE_PARAM_MAP_IMAGE, fileBody);

        postRequest.setEntity(reqEntity);
        return postRequest;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MapDataResponse uploadMapData(final String mapName, final String mapDataName, final File mapData)
            throws IOException {

        final InputStream is = new FileInputStream(mapData);
        return uploadMapData(mapName, mapDataName, is);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public MapDataResponse uploadMapData(final String mapName, final String mapDataName, final InputStream mapData)
            throws IOException {
        final HttpPost postRequest = preparePostRequestForMapDataUpload(mapName, mapDataName, mapData);
        final HttpResponse response = threadSafeExecute(postRequest);
        return buildResponse(response, MapDataResponse.class);
    }

    /* (non-Javadoc)
     * @see MapServerClient#uploadCart(java.lang.String, java.lang.String, java.io.InputStream)
    */
	@Override
	public CartDataResponse uploadCartData(String clientID, String map, InputStream cart)
			throws IOException {
        final HttpPost postRequest = preparePostRequestForCartDataUpload(clientID, map, cart);
        final HttpResponse response = threadSafeExecute(postRequest);
        return buildResponse(response, CartDataResponse.class);
	}

    private HttpResponse threadSafeExecute(HttpPost postRequest) throws IOException {
        synchronized (lock) {
            return client.execute(postRequest);
        }
    }

    private HttpResponse threadSafeExecute(HttpGet getRequest) throws IOException {
        synchronized (lock) {
            return client.execute(getRequest);
        }
    }

    /* (non-Javadoc)
     * @see MapServerClient#uploadCart(java.lang.String, java.lang.String, java.io.File)
     */
	@Override
	public CartDataResponse uploadCartData(String clientID, String map, File cart)
			throws IOException {
        final InputStream is = new FileInputStream(cart);
        return uploadCartData(clientID, map, is);
	}

    /**
     * {@inheritDoc}
     *
     * This download has connection- and socket-timeouts of 4000 ms.
     */
    @Override
    public String downloadMapData(final String mapName) throws IOException, InvioException {
        return downloadMapData(mapName, 4000);
    }

    /**
     * Download the mapData from the server.
     *
     * @param mapName the map name
     * @param timeout connection- and socket-timeout in ms
     * @return the xml string containing the mapdata
     * @throws IOException TODO
     * @throws InvioException TODO
     */
    public String downloadMapData(final String mapName, int timeout) throws IOException, InvioException {
        // TODO: it's very bad to remove the "/mapserver"-path like this. The maps-directory should be under
        //       /mapserver as well, or we need another constant for the direct-download-files, or something like that.
        final String extension = ".osm";
        final String url = endpoint.replace("/mapserver", "") +
                MAPDATA_TEMPLATE.replace("{mapName}", mapName) + mapName + extension;
        final HttpGet request = new HttpGet(url);
        final HttpParams params = request.getParams();
        HttpConnectionParams.setConnectionTimeout(params, timeout);
        HttpConnectionParams.setSoTimeout(params, timeout);
        final HttpResponse response = client.execute(request);
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            throw new InvioException("Couldn't download the map data information: "+response.getStatusLine());
        }
        final String xmlData = getContentFromResponse(response);
        return xmlData;
    }

    @Override
    public InputStream getGroupData(final String groupName, final int timeout) throws IOException {
        final URL groupEndpoint = new URL(
                getResourceEndpoint(GROUP_DATA_ROOT) + "?group=" + URLEncoder.encode(groupName, UTF_8));
        return groupEndpoint.openStream();
    }

    @Override
    public MapPropertiesResponse downloadMapProperties(final String mapProperties)
            throws IOException, InvioException {
        final String url = getResourceEndpoint(MAP_PROPERTIES_ROOT) + "?map_properties="
                + URLEncoder.encode(mapProperties, UTF_8);
        final HttpGet request = new HttpGet(url);
        final HttpParams params = request.getParams();
        HttpConnectionParams.setConnectionTimeout(params, 4000);
        HttpConnectionParams.setSoTimeout(params, 4000);
        final HttpResponse response = client.execute(request);
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            throw new InvioException("Couldn't download the map properties: " + response.getStatusLine());
        }
        return buildResponse(response, MapPropertiesResponse.class);
    }

    /**
     * {@inheritDoc}
     */
    public void uploadFingerprintsData(final String mapName, int fingerprintType, String fingerprintsJson)
            throws IOException, InvioException {

        final HttpPost postRequest = preparePostRequestForFingerprintsDataUpload(
                mapName, fingerprintType, fingerprintsJson
        );
        final HttpResponse response = threadSafeExecute(postRequest);
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_CREATED) {
            throw new InvioException("Couldn't upload fingerprints: "+response.getStatusLine());
        }
        // Consume the content so that the connection can be reused again:
        response.getEntity().consumeContent();
    }

    @Override
    public void uploadWifiFingerprintsData(String mapName, String fingerprintsJson) throws IOException, InvioException {
        uploadFingerprintsData(mapName, FINGERPRINT_WIFI, fingerprintsJson);
    }

    @Override
    public void uploadBluetoothLEFingerprintsData(String mapName, String fingerprintsJson)
            throws IOException, InvioException {
        uploadFingerprintsData(mapName, FINGERPRINT_BLUETOOTH, fingerprintsJson);
    }

    /**
     * {@inheritDoc}
     * This download has connection- and socket-timeouts of 4000 ms.
     */
    @Override
    public String downloadWifiFingerprintsData(String mapName) throws IOException, InvioException {
        return downloadFingerprintsData(mapName, FINGERPRINT_WIFI, 4000);
    }

    /**
     * {@inheritDoc}
     * This download has connection- and socket-timeouts of 4000 ms.
     */
    @Override
    public String downloadBluetoothLEFingerprintsData(String mapName) throws IOException, InvioException {
        return downloadFingerprintsData(mapName, FINGERPRINT_BLUETOOTH, 4000);
    }

    /**
     * Download the fingerprintData from the server.
     *
     * @param mapName the map name
     * @param fingerprintType the fingerprint's type (eg. wifi/bluetooth)
     * @param timeout the timeout
     * @return the downloaded mapFingerprint string
     * @throws IOException if the client could not execute of the content from the response could not be prepared
     * @throws InvioException if the fingerprints could not be downloaded
     */
    public String downloadFingerprintsData(final String mapName, int fingerprintType, int timeout)
            throws IOException, InvioException {
        // TODO: it's very bad to remove the "/mapserver"-path like this. The maps-directory should be under
        //       /mapserver as well, or we need another constant for the direct-download-files, or something like that.

        String template = FINGERPRINTS_TEMPLATE_WIFI; //default behaviour is using wifi fingerprints
        if(fingerprintType == FINGERPRINT_BLUETOOTH){
            template = FINGERPRINTS_TEMPLATE_BLUETOOTH_LE;
        }
        final String url = endpoint.replace("/mapserver", "") +
                template.replace("{mapName}", mapName);
        final HttpGet request = new HttpGet(url);
        final HttpParams params = request.getParams();
        request.setHeader("Accept-Encoding", "gzip");
        HttpConnectionParams.setConnectionTimeout(params, timeout);
        HttpConnectionParams.setSoTimeout(params, timeout);

        final HttpResponse response = threadSafeExecute(request);
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            throw new InvioException("Couldn't download fingerprints: "+response.getStatusLine());
        }
        final String json = getContentFromResponse(response);
        return json;
    }

    /**
     * Uploads position data to the server.
     *
     * @param mapName the map name
     * @param deviceId the unique device name
     * @param latE6 position
     * @param longE6 position
     * @param level the current level/floor
     * @throws IOException when the client cannot execute the POST request
     * @throws InvioException when something bad happens (TODO: what might that be?)
     */
    public void uploadPositionData(final String mapName, final String deviceId, final int latE6, final int longE6,
                                   final int level) throws IOException, InvioException {
        final HttpPost postRequest =
                preparePostRequestForPositionDataUpload(mapName, latE6, longE6, deviceId, level);
        //TODO: Multi-threading? client field is not good when more than one thing want to access it at the same time.
        // eg. shopping cart + position upload.
        final HttpResponse response = threadSafeExecute(postRequest);
        if (response.getStatusLine().getStatusCode() >= HttpStatus.SC_NON_AUTHORITATIVE_INFORMATION) {
            throw new InvioException("Couldn't upload position: "+response.getStatusLine());
        }
        // Consume the content so that the connection can be reused again:
        response.getEntity().consumeContent();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MapListResponse getMapList(int timeout) throws IOException {
        final HttpGet httpGetRequest = new HttpGet(getResourceEndpoint(MAP_LIST_ROOT));
        final HttpParams params = httpGetRequest.getParams();
        HttpConnectionParams.setConnectionTimeout(params, timeout);
        HttpConnectionParams.setSoTimeout(params, timeout);

        final HttpResponse response = threadSafeExecute(httpGetRequest);
        return buildResponse(response, MapListResponse.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MapListResponse getMapList() throws IOException {
        return getMapList(0);
    }

    @Override
    public String getTilemapresourceXml(String mapName) throws IOException, InvioException {
        return getTilemapresourceXml(mapName, 4000);
    }

    /**
     * Download the tilemapresource.xml with a parametrized timeout.
     *
     * @param mapName the name of the map for which the resources are to be downloaded
     * @param timeout the connection- and socket-timeout in milliseconds
     * @return the xml
     * @throws IOException on timeouts
     * @throws InvioException on http-status != OK/200
     */
    public String getTilemapresourceXml(String mapName, int timeout) throws IOException, InvioException {
        // TODO: it's very bad to remove the "/mapserver"-path like this. The maps-directory should be under
        //       /mapserver as well, or we need another constant for the direct-download-files, or something like that.
        final String url = endpoint.replace("/mapserver", "") +
                MAP_TILEMAPRESOURCE_TEMPLATE.replace("{mapName}", mapName);
        final HttpGet request = new HttpGet(url);
        final HttpParams params = request.getParams();
        HttpConnectionParams.setConnectionTimeout(params, timeout);
        HttpConnectionParams.setSoTimeout(params, timeout);
        final HttpResponse response = threadSafeExecute(request);
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            throw new InvioException("Couldn't download /tilemapresource.xml: "+response.getStatusLine());
        }

        final String xml = getContentFromResponse(response);
        return xml;
    }

    /**
     * Prepares the {@link HttpPost} request for the map data upload.
     *
     * @param mapName     the name of the map
     * @param mapDataName the name of the map data
     * @param mapData     the map data {@link InputStream}
     * @throws UnsupportedEncodingException should never really be thrown, because we hardcoded UTF_8 here.
     * @return the post request which will be send
     */
    private HttpPost preparePostRequestForMapDataUpload(final String mapName, final String mapDataName,
                                                        final InputStream mapData) throws UnsupportedEncodingException {
        final HttpPost postRequest = new HttpPost(getResourceEndpoint(MAP_DATA_ROOT));
        final InputStreamBody fileBody = new InputStreamBody(
                mapData,
                "application/octet-stream",
                mapDataName); //it is very important to give it a "filename"
        final MultipartEntity reqEntity = new MultipartEntity();
        final Charset charset = Charset.forName(UTF_8);
        reqEntity.addPart(PARAM_MAP_NAME, new StringBody(mapName, TEXT_PLAIN, charset));
        reqEntity.addPart(MAP_DATA_NAME, new StringBody(mapDataName, TEXT_PLAIN, charset));
        reqEntity.addPart(MAP_DATA, fileBody);
        postRequest.setEntity(reqEntity);
        return postRequest;
    }

    private HttpPost preparePostRequestForCartDataUpload(final String clientID, final String map,
    		final InputStream cart) throws UnsupportedEncodingException {
        final HttpPost postRequest = new HttpPost(getResourceEndpoint(CART_DATA_ROOT));
        final InputStreamBody fileBody = new InputStreamBody(
                cart,
                "application/octet-stream",
                clientID); //it is very important to give it a "filename"
        final  MultipartEntity reqEntity = new MultipartEntity();
        final Charset charset = Charset.forName(UTF_8);
        reqEntity.addPart(CART_DATA_PARAM_CLIENT_ID, new StringBody(clientID, TEXT_PLAIN, charset));
        reqEntity.addPart(PARAM_MAP_NAME, new StringBody(map, TEXT_PLAIN, charset));
        reqEntity.addPart(CART_DATA_PARAM_CART, fileBody);
        postRequest.setEntity(reqEntity);
        return postRequest;
    }

    /**
     * Prepares the {@link HttpPost} request for the map data upload.
     *
     * @param mapName          the name of the map
     * @param fingerprintsJson the fingerprints data in JSON-format
     * @throws UnsupportedEncodingException should never really be thrown, because we hardcoded UTF_8 here.
     * @return the post request which will be send
     */
    private HttpPost preparePostRequestForFingerprintsDataUpload(final String mapName,
                                                                 int fingerprintType,
                                                                 final String fingerprintsJson)
            throws UnsupportedEncodingException {

        final  MultipartEntity reqEntity = new MultipartEntity();
        final Charset charset = Charset.forName(UTF_8);
        HttpPost postRequest;

        if (fingerprintType == FINGERPRINT_BLUETOOTH) {
            postRequest = new HttpPost(getResourceEndpoint(FINGERPRINTS_DATA_ROOT_BLUETOOTH_LE));
            reqEntity.addPart(PARAM_MAP_NAME, new StringBody(mapName, TEXT_PLAIN, charset));
            reqEntity.addPart(FINGERPRINTS_DATA_PARAM_BLUETOOTH_LE, new StringBody(
                    fingerprintsJson, APPLICATION_JSON, charset
            ));
        } else {
            postRequest = new HttpPost(getResourceEndpoint(FINGERPRINTS_DATA_ROOT_WIFI));
            reqEntity.addPart(PARAM_MAP_NAME, new StringBody(mapName, TEXT_PLAIN, charset));
            //default behaviour is using wifi fingerprints
            reqEntity.addPart(FINGERPRINTS_DATA_PARAM_WIFI, new StringBody(
                    fingerprintsJson, APPLICATION_JSON, charset
            ));
        }
        postRequest.setEntity(reqEntity);
        return postRequest;
    }

    /**
     * Prepares the {@link HttpPost} request for the map data upload.
     *
     * @param mapName the name of the map the positions belong to
     * @param latitudeE6 position
     * @param longitudeE6 position
     * @param deviceId the unique identifier of the device
     * @param level the current level/floor
     * @return the constructed POST
     * @throws java.io.UnsupportedEncodingException if StringBody-creation fails due to bad encoding
     */
    protected HttpPost preparePostRequestForPositionDataUpload(final String mapName,
                                                             final int latitudeE6,
                                                             final int longitudeE6,
                                                             final String deviceId,
                                                             final int level)
            throws UnsupportedEncodingException {

        final HttpPost postRequest = new HttpPost(getResourceEndpoint(POSITION_ROOT));
        final  MultipartEntity reqEntity = new MultipartEntity();
        final Charset charset = Charset.forName(UTF_8);
        reqEntity.addPart(PARAM_MAP_NAME, new StringBody(mapName, TEXT_PLAIN,charset));
        reqEntity.addPart(POSITION_PARAM_LATITUDE, new StringBody(latitudeE6+"", APPLICATION_JSON, charset));
        reqEntity.addPart(POSITION_PARAM_LONGITUDE, new StringBody(longitudeE6+"", APPLICATION_JSON, charset));
        reqEntity.addPart(POSITION_PARAM_DEVICE_ID, new StringBody(deviceId, APPLICATION_JSON, charset));
        reqEntity.addPart(POSITION_PARAM_LEVEL, new StringBody(level+"", APPLICATION_JSON, charset));
        postRequest.setEntity(reqEntity);
        return postRequest;
    }

    /**
     * Extract the UTF-8 string content from the response object. Also wraps gzip-content, if necessary.
     * @param response the response from the mapserver
     * @return the string content
     * @throws IOException when stream problems occur
     */
    private String getContentFromResponse(HttpResponse response) throws IOException {
        String content = "";
        final HttpEntity responseEntity = response.getEntity();
        if (responseEntity != null) {
            InputStream instream = responseEntity.getContent();
            final Header contentEncoding = response.getFirstHeader("Content-Encoding");
            if (contentEncoding != null && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
                instream = new GZIPInputStream(instream);
            }
            final StringWriter writer = new StringWriter();
            IOUtils.copy(instream, writer, UTF_8);
            content = writer.toString();
        }
        return content;
    }

    /**
     * Checks the syntax of the server endpoint URL string. No connection check is made.
     *
     * @param serverEndpoint the server endpoint
     * @throws MalformedURLException if the URL is malformed
     */
    private void checkEndpoint(final String serverEndpoint) throws MalformedURLException {
        final String protocol = new URL(serverEndpoint).getProtocol();

        // TODO Falls es nicht mit http anfängt, kommt es niemals bis hierher, daher für mich überflüssig
        if (!protocol.startsWith("http")) {
            throw new IllegalArgumentException("The endpoint must use the HTTP!");
        }
    }

    private String getResourceEndpoint(final String resource) {
        return endpoint + resource;
    }

    // TODO: this class has one rather simple line of code. Why would we want to not use that code directly,
    //       everywhere this method is used?
    private <T extends Response> T buildResponse(final HttpResponse response, final Class<T> responseType)
            throws IOException {
        final T result = ResponseBuilder.build(response, responseType);

        return result;
    }

}