package de.tarent.invio.mapserver;

import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.exception.InvioRuntimeException;
import de.tarent.invio.mapserver.response.CartDataResponse;
import de.tarent.invio.mapserver.response.MapDataResponse;
import de.tarent.invio.mapserver.response.MapImageResponse;
import de.tarent.invio.mapserver.response.MapListResponse;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.mime.MultipartEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MapServerClientImplTest {

    private MapServerClient mapServerClient;

    @Mock
    HttpClient httpClient;

    @Mock
    HttpResponse httpResponse;

    @Mock
    HttpEntity httpEntity;

    @Mock
    Header contentType;

    @Mock
    StatusLine statusLine;

    @Before
    public void setUp() throws Exception {
        String serverEndpoint = "http://notreallylocalhost:8080/mapserver";
        mapServerClient = new MapServerClientImpl(serverEndpoint, httpClient);
    }

    @Test
    public void testConstructor() throws InvioException {
        String serverEndpoint = "http://notreallylocalhost:8080/mapserver";
        MapServerClientImpl mapServerClient = new MapServerClientImpl(serverEndpoint);
        String result = mapServerClient.getEndpoint();
        assertEquals("http://notreallylocalhost:8080/mapserver", result);
    }

    @Test
    public void testCreateInstanceFailsOnMalformedEndpointUrl() throws InvioException {
        String serverEndpoint = "localhost:8080/mapserver";
        try {
            new MapServerClientImpl(serverEndpoint, httpClient);
            fail();
        } catch (InvioRuntimeException e) {
            assertEquals(e.getMessage(), "Invalid endpoint!");
        }
    }

    @Test
    public void testConstructorWrongEndpointProtocol() throws InvioException {
        String serverEndpoint = "ftp://localhost:8080/mapserver";
        try {
            new MapServerClientImpl(serverEndpoint, httpClient);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "The endpoint must use the HTTP!");
        }
    }

    @Test
    public void testUploadMapImage() throws IOException {
        String fileName = this.getClass().getResource("/kitty1.png").getPath();
        File file = new File(fileName);
        preparePostRequestStubs();

        MapImageResponse response = mapServerClient.uploadMapImage("testimage", file, 0f, 0f, 1f, 1f);

        String actualUri = captureUri();
        assertEquals("http://notreallylocalhost:8080/mapserver" + ResourceConstants.MAP_IMAGE_ROOT, actualUri);
        assertNotNull(response);
        verify(httpClient, times(1)).execute(any(HttpPost.class));
    }

    @Test
    public void testUploadMapData() throws IOException {
        String fileName = this.getClass().getResource("/no-image").getPath();
        File file = new File(fileName);
        preparePostRequestStubs();

        MapDataResponse response = mapServerClient.uploadMapData("mapName", "mapDataName", file);

        String actualUri = captureUri();
        assertEquals("http://notreallylocalhost:8080/mapserver" + ResourceConstants.MAP_DATA_ROOT, actualUri);
        assertNotNull(response);
        verify(httpClient, times(1)).execute(any(HttpPost.class));
    }

    @Test
    public void testUploadCartData() throws IOException {
        String fileName = this.getClass().getResource("/no-image").getPath();
        File file = new File(fileName);
        preparePostRequestStubs();

        CartDataResponse response = mapServerClient.uploadCartData("client ID", "map name", file);

        String actualUri = captureUri();
        assertEquals("http://notreallylocalhost:8080/mapserver" + ResourceConstants.CART_DATA_ROOT, actualUri);
        assertNotNull(response);
        verify(httpClient, times(1)).execute(any(HttpPost.class));
    }

    @Test
    public void testUploadFingerprintsData() throws IOException, InvioException {
        preparePostRequestStubs();
        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_CREATED);

        mapServerClient.uploadWifiFingerprintsData("mapName", "{test:json}");

        String actualUri = captureUri();
        assertEquals("http://notreallylocalhost:8080/mapserver" + ResourceConstants.FINGERPRINTS_DATA_ROOT_WIFI, actualUri);
        verify(httpClient, times(1)).execute(any(HttpPost.class));
    }

    @Test
    public void testUploadFingerprintsDataThrowsException() throws IOException, InvioException {
        preparePostRequestStubs();
        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_OK);

        try {
            mapServerClient.uploadWifiFingerprintsData("mapName", "{test:json}");
            fail();
        } catch (InvioException e) {
            verify(httpClient, times(1)).execute(any(HttpPost.class));
        }
    }

    @Test
    public void testDownloadFingerprintsData() throws IOException, InvioException {
        testUploadFingerprintsData();
        prepareGetRequestStubs();
        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_OK);

        String fingerprints = mapServerClient.downloadWifiFingerprintsData("mapName");

        assertNotNull(fingerprints);
    }

    @Test
    public void testDownloadFingerprintsDataThrowsException1() throws IOException, InvioException {
        testUploadFingerprintsData();
        prepareGetRequestStubs();
        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_CREATED);
        String fingerprints = null;
        try {
            fingerprints = mapServerClient.downloadWifiFingerprintsData("mapName");
        } catch (InvioException e) {
        }

        assertNull(fingerprints);
    }

    @Test
    public void testDownloadFingerprintsDataWithResponseEntityNull() throws IOException, InvioException {
        testUploadFingerprintsData();
        when(httpClient.execute(any(HttpGet.class))).thenReturn(httpResponse);
        when(httpResponse.getEntity()).thenReturn(null);
        when(httpEntity.getContentType()).thenReturn(contentType);
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_OK);
        String fingerprints = null;
        try {
            fingerprints = mapServerClient.downloadWifiFingerprintsData("mapName");
        } catch (InvioException e) {
        }

        assertNotNull(fingerprints);
    }

    @Test
    public void testGetMapList() throws IOException {
        prepareGetRequestStubs();
        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_OK);

        MapListResponse mapList = mapServerClient.getMapList();

        assertNotNull(mapList);
        assertEquals(HttpStatus.SC_OK, mapList.getStatus());
    }

    @Test
    public void testGetTilemapresourceXml() throws IOException, InvioException {
        prepareGetRequestStubs();
        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_OK);

        String result = mapServerClient.getTilemapresourceXml("mapName");

        assertNotNull(result);
    }

    @Test
    public void testGetTilemapresourceXmlFailed() throws IOException, InvioException {
        prepareGetRequestStubs();
        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_BAD_REQUEST);

        String result = null;
        try {
            result = mapServerClient.getTilemapresourceXml("mapName");
            fail();
        } catch (InvioException e) {
            assertNull(result);
        }
    }

    @Test
    public void testIfPositionPostContainsAllInformation() throws IOException {
        HttpPost post = ((MapServerClientImpl)mapServerClient).preparePostRequestForPositionDataUpload("someName", 1337, 42, "someCoolDeviceId", 0);
        MultipartEntity entity = (MultipartEntity) post.getEntity();
        ByteArrayOutputStream contentStream = new ByteArrayOutputStream();
        entity.writeTo(contentStream);
        String content = new String(contentStream.toByteArray());

        assertTrue(content.contains("someName"));
        assertTrue(content.contains("1337"));
        assertTrue(content.contains("42"));
        assertTrue(content.contains("someCoolDeviceId"));
        assertTrue(content.contains("0"));
    }



    private void prepareGetRequestStubs() throws IOException {
        when(httpClient.execute(any(HttpGet.class))).thenReturn(httpResponse);
        when(httpResponse.getEntity()).thenReturn(httpEntity);
        when(httpEntity.getContentType()).thenReturn(contentType);
        byte[] data = {};
        InputStream stream = new ByteArrayInputStream(data);
        when(httpEntity.getContent()).thenReturn(stream);
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
    }

    private void preparePostRequestStubs() throws IOException {
        when(httpClient.execute(any(HttpPost.class))).thenReturn(httpResponse);
        when(httpResponse.getEntity()).thenReturn(httpEntity);
        when(httpEntity.getContentType()).thenReturn(contentType);
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
    }

    private String captureUri() throws IOException {
        ArgumentCaptor<HttpUriRequest> httpUriRequestArgumentCaptor = ArgumentCaptor.forClass(HttpUriRequest.class);
        verify(httpClient).execute(httpUriRequestArgumentCaptor.capture());
        HttpUriRequest httpUriRequest = httpUriRequestArgumentCaptor.getValue();
        URI uri = httpUriRequest.getURI();
        return uri.getScheme()+"://"+uri.getHost()+":"+uri.getPort()+uri.getPath();
    }

}
