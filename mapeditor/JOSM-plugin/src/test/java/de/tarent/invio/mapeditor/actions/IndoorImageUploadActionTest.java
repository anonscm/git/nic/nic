package de.tarent.invio.mapeditor.actions;

import de.tarent.invio.mapeditor.wrapper.JOSMWrapper;
import de.tarent.invio.mapeditor.wrapper.JOptionPaneWrapper;
import org.junit.Before;
import org.junit.Test;
import org.openstreetmap.josm.Main;
import org.openstreetmap.josm.data.Preferences;
import org.openstreetmap.josm.gui.MapView;
import org.openstreetmap.josm.gui.layer.Layer;
import org.openstreetmap.josm.gui.layer.OsmDataLayer;
import org.openstreetmap.josm.gui.preferences.ToolbarPreferences;
import org.openstreetmap.josm.tools.PlatformHook;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.net.URL;

import static org.mockito.Mockito.*;

public class IndoorImageUploadActionTest {

    private JOSMWrapper josm;

    private JOptionPaneWrapper jOptionPane;

    private MapView mapView;

    private ActionEvent event;

    private IndoorImageUploadAction imageUploadAction;

    @Before
    public void setUp() throws Exception {
        // Mock our wrapper classes so that we can do what we want with them.
        josm = mock(JOSMWrapper.class);
        jOptionPane = mock(JOptionPaneWrapper.class);
        event = mock(ActionEvent.class);
        mapView = mock(MapView.class);
        when(josm.getMapView()).thenReturn(mapView);

        // Call the real methods for the messages.
        when(jOptionPane.getErrorMessage()).thenCallRealMethod();
        when(jOptionPane.getWarningMessage()).thenCallRealMethod();
        when(jOptionPane.getInformationMessage()).thenCallRealMethod();

        // Mock the static stuff for the JOSM Main class in order to keep the beast quite :)
        Main.platform = mock(PlatformHook.class);
        Preferences myPref = mock(Preferences.class);
        when(myPref.get("nicserver.url")).thenReturn("http://nic-mapserver:8080");
        Main.pref = myPref;
        Main.toolbar = mock(ToolbarPreferences.class);
        Main.main = mock(Main.class);

        // This is our class under test
        imageUploadAction = new IndoorImageUploadAction(josm, jOptionPane);
    }

    @Test
    public void testThatWrongSelectedLayerThrowsAnException() {
        final OsmDataLayer osmDataLayer = mock(OsmDataLayer.class);
        when(mapView.getActiveLayer()).thenReturn(osmDataLayer);

        imageUploadAction.actionPerformed(event);

        //Verify that the correct error message is shown
        verify(jOptionPane, times(1)).showMessageDialog(
                any(Component.class),
                eq("Bitte aktivieren Sie erst die PicLayer-Ebene."),
                anyString(),
                anyInt());
    }

    @Test
    public void testThatUploadWillFailIfFileDoesNotExist(){
        final Layer picLayer = mock(Layer.class);
        when(picLayer.getToolTipText()).thenReturn("foo");
        when(mapView.getActiveLayer()).thenReturn(picLayer);

        imageUploadAction.actionPerformed(event);

        //Verify that the correct error message is shown
        verify(jOptionPane, times(1)).showMessageDialog(
                any(Component.class),
                eq("Die Bilddatei-Information konnte von der PicLayer-Ebende " +
                        "nicht geladen werden, \n" +
                        "oder die PicLayer-Ebene wurde nicht aktiviert."),
                anyString(),
                anyInt());
    }

    @Test
    public void testThatUploadWillProceedWhenFileDoesExist(){
        URL url = this.getClass().getResource("/invio.png");
        final Layer picLayer = mock(Layer.class);
        when(picLayer.getToolTipText()).thenReturn(url.getPath());
        when(mapView.getActiveLayer()).thenReturn(picLayer);
        final OsmDataLayer osmDataLayer = mock(OsmDataLayer.class);
        when(josm.getOsmLayer()).thenReturn(osmDataLayer);

        //Verify that the correct error message is show. This will be the case because we can't mock the Main.data which
        //contains the reference points.
        imageUploadAction.actionPerformed(event);
        verify(jOptionPane, times(1)).showMessageDialog(
                any(Component.class),
                eq("Bitte erstellen Sie erst die Referenzpunkte."),
                anyString(),
                anyInt());
    }

    @Test
    public void test_That_Notification_Is_Shown_If_File_Or_Points_Are_Null(){
        imageUploadAction.handleUpload(null, null);
        verify(jOptionPane, times(1)).showMessageDialog(
                any(Component.class),
                eq("Entweder die Bilddatei oder die Referenzpunkte (oder beides) sind nicht vorhanden."),
                anyString(),
                anyInt());
    }
}
