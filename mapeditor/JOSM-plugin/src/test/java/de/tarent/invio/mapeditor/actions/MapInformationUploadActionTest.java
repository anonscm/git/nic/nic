package de.tarent.invio.mapeditor.actions;

import de.tarent.invio.mapeditor.wrapper.JOSMWrapper;
import de.tarent.invio.mapeditor.wrapper.JOptionPaneWrapper;
import org.junit.Before;
import org.junit.Test;
import org.openstreetmap.josm.Main;
import org.openstreetmap.josm.data.Preferences;
import org.openstreetmap.josm.gui.MapFrame;
import org.openstreetmap.josm.gui.MapView;
import org.openstreetmap.josm.gui.layer.OsmDataLayer;
import org.openstreetmap.josm.gui.preferences.ToolbarPreferences;
import org.openstreetmap.josm.tools.PlatformHook;

import java.awt.Component;
import java.awt.event.ActionEvent;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.openstreetmap.josm.tools.I18n.tr;

/**
 * @author Désirée Amling <d.amling@tarent.de>
 */
public class MapInformationUploadActionTest {

    private JOSMWrapper josm;

    private JOptionPaneWrapper jOptionPane;

    private MapInformationUploadAction miua;

    @Before
    public void setup() {
        // Mock our wrapper classes so that we can do what we want with them.
        josm = mock(JOSMWrapper.class);
        jOptionPane = mock(JOptionPaneWrapper.class);

        // Call the real methods for the messages.
        when(jOptionPane.getErrorMessage()).thenCallRealMethod();
        when(jOptionPane.getWarningMessage()).thenCallRealMethod();
        when(jOptionPane.getInformationMessage()).thenCallRealMethod();

        // Mock every static method called in the JOSM classes.
        Main.platform = mock(PlatformHook.class);
        Preferences myPref = mock(Preferences.class);
        when(myPref.get("nicserver.url")).thenReturn("http://nic-mapserver:8080");
        Main.pref = myPref;
        Main.toolbar = mock(ToolbarPreferences.class);
        Main.main = mock(Main.class);

        // Create our MapInformationUploadAction.
        miua = new MapInformationUploadAction(josm, jOptionPane);
    }

    @Test
    public void testThatWhenNoChangesAreDetectedNoChangesToUploadDialogIsShown() {
        final ActionEvent event = mock(ActionEvent.class);
        final OsmDataLayer osmDataLayer = mock(OsmDataLayer.class);
        final MapFrame mapFrame = mock(MapFrame.class);
        final MapView mapView = mock(MapView.class);
        final Component component = mock(Component.class);
        Main main = mock(Main.class);
        when(josm.getMain()).thenReturn(main);
        when(josm.getMap()).thenReturn(mapFrame);
        when(josm.getMapView()).thenReturn(mapView);
        when(josm.getParent()).thenReturn(component);
        when(mapView.getEditLayer()).thenReturn(osmDataLayer);

        miua.actionPerformed(event);

        verify(jOptionPane, times(1)).showMessageDialog(
                josm.getParent(), tr("No changes to upload."), tr("Warning"), jOptionPane.getInformationMessage());
    }

    @Test
    public void testIfIsConnectedToMapServerReturnsFalseWhenThereIsNoClient() {
        miua.client=null;
        doNothing().when(jOptionPane).showMessageDialog(any(Component.class), anyObject());
        assertTrue(!miua.isConnectedToMapServer());
    }

}
