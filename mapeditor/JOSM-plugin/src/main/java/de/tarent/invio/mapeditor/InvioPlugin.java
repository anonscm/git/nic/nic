package de.tarent.invio.mapeditor;

import de.tarent.invio.mapeditor.actions.IndoorImageUploadAction;
import de.tarent.invio.mapeditor.actions.MapInformationUploadAction;
import org.openstreetmap.josm.Main;
import org.openstreetmap.josm.gui.MainMenu;
import org.openstreetmap.josm.plugins.Plugin;
import org.openstreetmap.josm.plugins.PluginInformation;


/**
 * A Invio plugin for the JOSM interface. This will add some menu options to upload images and the extra information
 * required for the map to the server.
 */
public class InvioPlugin extends Plugin {

    /**
     * Constructor.
     *
     * @param info the {@link PluginInformation}
     */
    public InvioPlugin(final PluginInformation info) {
        super(info);
        createImageUploadMenu();
        createMapInformationUploadMenu();
    }

    /**
     * Creates the menu button inside the 'File' menu, used for uploading map info.
      */
    private void createMapInformationUploadMenu() {
        MainMenu.addAfter(Main.main.menu.fileMenu, new MapInformationUploadAction(), false, Main.main.menu.upload);
    }

    /**
     * Creates the menu button inside the 'Imagery' menu, used for uploading images.
     */
    private void createImageUploadMenu() {
        MainMenu.add(Main.main.menu.imageryMenu, new IndoorImageUploadAction());
        Main.main.menu.imageryMenu.addSeparator();
    }
}
