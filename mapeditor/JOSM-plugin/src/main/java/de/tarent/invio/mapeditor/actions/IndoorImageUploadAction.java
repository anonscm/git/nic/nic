package de.tarent.invio.mapeditor.actions;

import de.tarent.invio.mapeditor.exceptions.InvioPluginException;
import de.tarent.invio.mapeditor.wrapper.JOSMWrapper;
import de.tarent.invio.mapeditor.wrapper.JOptionPaneWrapper;
import de.tarent.invio.mapserver.response.MapImageResponse;
import org.apache.commons.io.FilenameUtils;
import org.openstreetmap.josm.data.coor.LatLon;
import org.openstreetmap.josm.data.osm.DataSet;
import org.openstreetmap.josm.data.osm.Node;
import org.openstreetmap.josm.gui.layer.Layer;
import org.openstreetmap.josm.gui.layer.OsmDataLayer;

import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The IndoorImageUploadAction uploads a map-image to the map-server for tiling.
 */
public class IndoorImageUploadAction extends MapServerAction {

    private static final Logger LOGGER = Logger.getLogger(IndoorImageUploadAction.class.getName());
    private static final boolean DEBUG = false;

    private static final String REFERENCE_POINT_LEFT_UPPER = "leftUpper";
    private static final String REFERENCE_POINT_RIGHT_LOWER = "rightLower";

    private JOSMWrapper josm;

    /**
     * Default constructor.
     */
    public IndoorImageUploadAction() {
        this(new JOSMWrapper(), new JOptionPaneWrapper());
    }

    /**
     * Constructor to create a new MapInformationUploadAction.
     *
     * @param josm the {@link JOSMWrapper}
     * @param jOptionPane the {@link JOptionPaneWrapper}
     */
    public IndoorImageUploadAction(final JOSMWrapper josm, final JOptionPaneWrapper jOptionPane) {
        super("Upload indoor image now", "Upload indoor image of your location", jOptionPane);
        this.josm = josm;
    }

    /**
     * The action performed when the user wants to upload an image.
     *
     * @param event we don't use this event, it's just part of the interface
     */
    public void actionPerformed(final ActionEvent event) {
        if (isConnectedToMapServer()) {

            File imageFile = null;
            Map<String, LatLon> geoReferencePoints = null;
            try {
                imageFile = getImageFile();
                geoReferencePoints = getGeoReferencePointsMap();
            } catch (InvioPluginException e) {
                showErrorMessage(e.getMessage());
                LOGGER.log(Level.SEVERE, e.toString());
                return;
            }

            handleUpload(imageFile, geoReferencePoints);
        }
    }

    /**
     * Uploads the the given image file with the given reference points to the configured invio server
     *
     * @param imageFile indoor image
     * @param geoReferencePoints two points for the bounding box
     */
    void handleUpload(File imageFile, Map<String, LatLon> geoReferencePoints) {
        if (imageFile != null && geoReferencePoints != null) {
            try {
                handleImageUpload(imageFile, geoReferencePoints);
            } catch (IOException e) {
                showErrorMessage("Bilddatei konnte nicht hochgeladen werden. " +
                        "Dabei ist folgender Fehler aufgetreten: \n" +
                        e.toString());
                LOGGER.log(Level.SEVERE, e.toString());
            }
        } else {
            showErrorMessage("Entweder die Bilddatei oder die Referenzpunkte (oder beides) sind nicht vorhanden.");
            LOGGER.log(Level.SEVERE, "No image file or reference points (or both) are present. " +
                    "Image file: " + imageFile + " Reference points: " + geoReferencePoints);
        }
    }

    /**
     * TODO: This must be available for all classes!
     * Shows simple JOptionPane error message
     *
     * @param message error to show
     */
    private void showErrorMessage(String message) {
        jOptionPane.showMessageDialog(josm.getParent(), message, "Invio-Plugin Error", jOptionPane.getErrorMessage());
    }

    /**
     * Gets the image file for the active pic layer plugin
     *
     * @return the image file
     * @throws InvioPluginException if pic layer is not an active layer or image information couldn't be get
     */
    private File getImageFile() throws InvioPluginException {
        final Layer picLayer = josm.getMapView().getActiveLayer();
        //We don't need the osm layer but PicLayer
        if (picLayer instanceof OsmDataLayer) {
            throw new InvioPluginException("Bitte aktivieren Sie erst die PicLayer-Ebene.");
        }
        final File imageFile = new File(picLayer.getToolTipText());

        if (!imageFile.exists()) {
            throw new InvioPluginException("Die Bilddatei-Information konnte von der PicLayer-Ebende " +
                    "nicht geladen werden, \n" +
                    "oder die PicLayer-Ebene wurde nicht aktiviert.");
        }
        return imageFile;
    }

    /**
     * Gets the two reference points as an LatLon object in puts it into a map with REFERENCE_POINT_LEFT_UPPER or
     * REFERENCE_POINT_RIGHT_LOWER keys.
     *
     * @return map with to latlon geo reference points
     * @throws InvioPluginException if no or not exactly two points were found
     */
    private Map<String, LatLon> getGeoReferencePointsMap() throws InvioPluginException {
        final Map<String, LatLon> resultMap = new HashMap<String, LatLon>();

        final DataSet dataSet = josm.getDataSet();

        if (dataSet == null || dataSet.getNodes() == null) {
            throw new InvioPluginException("Bitte erstellen Sie erst die Referenzpunkte.");
        }

        final List<Node> nodes = new ArrayList<Node>(dataSet.getNodes());

        if (nodes.size() == 2) {
            fillResultMap(resultMap, nodes);
        } else {
            throw new InvioPluginException(
                    "Bitte platzieren Sie genau 2 Referenzpunkte oben links und unten rechts.");
        }

        return resultMap;
    }

    private void fillResultMap(Map<String, LatLon> resultMap, List<Node> nodes) {
        final Node node1 = nodes.get(0);
        final Node node2 = nodes.get(1);

        if (node1.getCoor().lat() > node2.getCoor().lat()) {
            resultMap.put(REFERENCE_POINT_LEFT_UPPER, node1.getCoor());
            resultMap.put(REFERENCE_POINT_RIGHT_LOWER, node2.getCoor());
        } else {
            resultMap.put(REFERENCE_POINT_LEFT_UPPER, node2.getCoor());
            resultMap.put(REFERENCE_POINT_RIGHT_LOWER, node1.getCoor());
        }
    }


    /**
     * Delegates the image upload to the more complex handleImageUpload with single coordinates
     *
     * @param imageFile                indoor image
     * @param getGeoReferencePointsMap reference map with two latlong reference points
     * @throws IOException in case of image file read problems
     */
    private void handleImageUpload(File imageFile, Map<String, LatLon> getGeoReferencePointsMap) throws IOException {
        final LatLon leftUpper = getGeoReferencePointsMap.get(REFERENCE_POINT_LEFT_UPPER);
        final LatLon rightLower = getGeoReferencePointsMap.get(REFERENCE_POINT_RIGHT_LOWER);


        final float left = (float) leftUpper.lon();
        final float upper = (float) leftUpper.lat();
        final float right = (float) rightLower.lon();
        final float lower = (float) rightLower.lat();

        handleImageUpload(imageFile, left, upper, right, lower);
    }


    /**
     * Handles the whole process of image uploading
     *
     * @param imageFile indoor image
     * @param left      upper left longitude
     * @param upper     upper left latitude
     * @param right     lower right longitude
     * @param lower     lower right latitude
     * @throws IOException
     */
    private void handleImageUpload(final File imageFile, float left, float upper,
                                   float right, float lower) throws IOException {
        final MapImageResponse response = uploadImage(imageFile, left, upper, right, lower);
        if (DEBUG) {
            jOptionPane.showMessageDialog(josm.getParent(), "Image path: " + imageFile +
                    " left upper: " + left + " " + upper + " right lower: " + right + " " + lower);
            return;
        }
        //TODO: Static {zoom}/{x}/{-y}.png MUST be delivered by the SERVER or configured as PROPERTY
        final String fullImageUrl = response.getImageURL() + "{zoom}/{x}/{-y}.png";
        jOptionPane.showMessageDialog(josm.getParent(), new JTextField(fullImageUrl));
    }

    /**
     * Uploads the image with the map server {@link #client}.
     *
     * @param imageFile the
     * @return the {@link MapImageResponse}
     * @throws IOException when the image upload failed
     */
    private MapImageResponse uploadImage(final File imageFile, float left, float upper,
                                         float right, float lower) throws IOException {
        //TODO: Null check for imageFile is already done before. Do it here to?
        final String mapName = FilenameUtils.removeExtension(imageFile.getName());
        final MapImageResponse response = client.uploadMapImage(mapName, imageFile.getAbsoluteFile(),
                left, upper, right, lower);
        return response;
    }
}