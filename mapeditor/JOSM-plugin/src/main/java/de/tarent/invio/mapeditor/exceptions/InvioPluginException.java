package de.tarent.invio.mapeditor.exceptions;

/**
 * Simple invio plugin exception
 */
public class InvioPluginException extends Exception {

    /**
     * Default constructor.
     */
    public InvioPluginException() {
        super();
    }

    /**
     * Constructor.
     *
     * @param message the message to be shown
     */
    public InvioPluginException(final String message) {
        super(message);
    }

    /**
     * Constructor.
     *
     * @param cause the cause of the exception
     */
    public InvioPluginException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     *
     * @param message the message to be shown
     * @param cause the cause of the exception
     */
    public InvioPluginException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
