package de.tarent.invio.mapeditor.actions;

import de.tarent.invio.mapeditor.wrapper.JOptionPaneWrapper;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.MapServerClientImpl;
import org.openstreetmap.josm.Main;
import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.tools.Shortcut;

import javax.swing.JTextField;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.openstreetmap.josm.tools.I18n.tr;

/**
 * Base {@link MapServerAction}.
 */
public abstract class MapServerAction extends JosmAction {

    /**
     * The {@link Logger} to log the information output.
     */
    private static final Logger LOGGER = Logger.getLogger(IndoorImageUploadAction.class.getName());

    /**
     * {@link MapServerClient} to be used by the map server actions.
     */
    protected MapServerClient client;

    protected JOptionPaneWrapper jOptionPane;


    /**
     * Constructor.
     *
     * This constructor should be used when no shortcuts are given for the action.
     *
     * @param menuText the text to display in the menu
     * @param mouseOverText the text to display on mouse over
     */
    public MapServerAction(final String menuText, final String mouseOverText) {
        super(tr(menuText), "invio", tr(mouseOverText),
                Shortcut.registerShortcut("", "", Shortcut.NONE, Shortcut.NONE), true);
        client = new MapServerClientImpl(Main.pref.get("nicserver.url"));
    }

    /**
     * Constructor.
     *
     * This constructor should be used when no shortcuts are given for the action.
     *
     * @param menuText the text to display in the menu
     * @param mouseOverText the text to display on mouse over
     * @param jOptionPane the wrapper user for the static JOptionPane
     */
    public MapServerAction(final String menuText, final String mouseOverText, final JOptionPaneWrapper jOptionPane) {
        this(menuText, mouseOverText);
        this.jOptionPane = jOptionPane;
    }

    /**
     * If the {@link #client} is null (not connected) then show an error message and return false. Else return true.
     *
     * @return whether or not the map server is connected
     */
    protected boolean isConnectedToMapServer() {
        if (client == null) {
            jOptionPane.showMessageDialog(Main.parent, new JTextField("Mapserver-configuration is invalid."));
            LOGGER.log(Level.WARNING, "Mapserver-configuration is invalid.");
            return false;
        }
        return true;
    }
}
