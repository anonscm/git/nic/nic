package de.tarent.invio.mapeditor.wrapper;

import javax.swing.JOptionPane;
import java.awt.Component;

/**
 * A wrapper class to hold all of the static calls to the {@link JOptionPane} class for easier testing.
 *
 * @author Désirée Amling <d.amling@tarent.de>
 */
public class JOptionPaneWrapper {

    /**
     * See {@link JOptionPane#showMessageDialog(Component, Object, String, int)}.
     *
     * @param parentComponent the parent {@link Component}
     * @param message the message
     * @param title the title
     * @param messageType the message type
     */
    public void showMessageDialog(final Component parentComponent, final Object message, final String title,
                                  final int messageType) {
        JOptionPane.showMessageDialog(parentComponent, message, title, messageType);
    }

    /**
     * See {@link JOptionPane#showMessageDialog(Component, Object)}.
     *
     * @param parentComponent the parent {@link Component}
     * @param message the message
     */
    public void showMessageDialog(final Component parentComponent, final Object message) {
        JOptionPane.showMessageDialog(parentComponent, message);
    }

    /**
     * See {@link JOptionPane#INFORMATION_MESSAGE}.
     *
     * @return the information message
     */
    public int getInformationMessage() {
        return JOptionPane.INFORMATION_MESSAGE;
    }

    /**
     * See {@link JOptionPane#ERROR_MESSAGE}.
     *
     * @return the error message
     */
    public int getErrorMessage() {
        return JOptionPane.ERROR_MESSAGE;
    }

    /**
     * See {@link JOptionPane#WARNING_MESSAGE}.
     *
     * @return the warning message
     */
    public int getWarningMessage() {
        return JOptionPane.WARNING_MESSAGE;
    }
}
