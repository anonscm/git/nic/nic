package de.tarent.invio.mapeditor.wrapper;

import org.openstreetmap.josm.Main;
import org.openstreetmap.josm.data.osm.DataSet;
import org.openstreetmap.josm.gui.MapFrame;
import org.openstreetmap.josm.gui.MapView;
import org.openstreetmap.josm.gui.layer.Layer;
import org.openstreetmap.josm.gui.layer.OsmDataLayer;

import java.awt.Component;
import java.util.List;

/**
 * A wrapper class to hold all of the static calls to the JOSM {@link Main} class for easier testing.
 *
 * @author Désirée Amling <d.amling@tarent.de>
 */
public class JOSMWrapper {

    /**
     * See {@link Main#map}.
     *
     * @return the {@link Main#map}
     */
    public MapFrame getMap() {
        return Main.map;
    }

    /**
     * Gets the {@link MapView} from the map.
     *
     * @return the {@link MapView}
     */
    public MapView getMapView() { return getMap().mapView; }

    /**
     * See {@link Main#parent}.
     *
     * @return the {@link Main#parent}
     */
    public Component getParent() {
        return Main.parent;
    }

    /**
     * See {@link Main#main}.
     *
     * @return the {@link Main#main}
     */
    public Main getMain() {
        return Main.main;
    }

    /**
     * Looks up for the OsmDataLayer and if present returns its
     * {@link org.openstreetmap.josm.gui.layer.OsmDataLayer#data}
     *
     * @return the found data of the layer or null if no layer were found
     */
    public DataSet getDataSet() {
        final OsmDataLayer dataLayer = getOsmLayer();
        if (dataLayer != null) {
            return dataLayer.data;
        } else {
            return null;
        }
    }

    /**
     * Returns the osm data layer
     *
     * @return osm layer or null if none found
     */
    public OsmDataLayer getOsmLayer(){
        OsmDataLayer osmLayer = null;
        final List<Layer> layers = getMapView().getAllLayersAsList();
        if (layers != null && layers.size() > 0) {
            for (Layer layer : layers) {
                if (layer instanceof OsmDataLayer) {
                    osmLayer = (OsmDataLayer) layer;
                    break;
                }
            }
        }
        return osmLayer;
    }

}