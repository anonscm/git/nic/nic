package de.tarent.invio.mapeditor.actions;

import de.tarent.invio.mapeditor.wrapper.JOSMWrapper;
import de.tarent.invio.mapeditor.wrapper.JOptionPaneWrapper;
import de.tarent.invio.mapserver.response.MapDataResponse;
import org.apache.commons.io.FilenameUtils;
import org.openstreetmap.josm.actions.upload.ValidateUploadHook;
import org.openstreetmap.josm.data.APIDataSet;
import org.openstreetmap.josm.data.conflict.ConflictCollection;
import org.openstreetmap.josm.gui.HelpAwareOptionPane;
import org.openstreetmap.josm.gui.help.HelpUtil;
import org.openstreetmap.josm.gui.layer.OsmDataLayer;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.openstreetmap.josm.tools.I18n.tr;


/**
 * The MapInformationUploadAction uploads an .osm file to the map-server.
 */
public class MapInformationUploadAction extends MapServerAction {

    private static final Logger LOGGER = Logger.getLogger(MapInformationUploadAction.class.getName());

    //
    private ValidateUploadHook josmValidateUploadHook;

    private JOSMWrapper josm;



    /**
     * Default constructor.
     */
    public MapInformationUploadAction() {
        this(new JOSMWrapper(), new JOptionPaneWrapper());
    }

    /**
     * Constructor to create a new MapInformationUploadAction.
     *
     * @param josm the {@link JOSMWrapper}
     * @param jOptionPane the {@link JOptionPaneWrapper}
     */
    public MapInformationUploadAction(final JOSMWrapper josm, final JOptionPaneWrapper jOptionPane) {
        super("Upload to invio...", "Upload map information to invio", jOptionPane);
        josmValidateUploadHook = new ValidateUploadHook();
        this.josm = josm;
    }

    /**
     * The action performed when the user wants to upload the map information.
     *
     * @param event we don't use this event, it's just part of the interface
     */
    public void actionPerformed(final ActionEvent event) {
        if (isConnectedToMapServer()) {
            if (josm.getMap() == null) {
                jOptionPane.showMessageDialog(josm.getParent(), tr("Nothing to upload. Get some data first."),
                        tr("Warning"), jOptionPane.getWarningMessage());
                return;
            }

            if(!checkAndAlertUploadPreconditions()) {
                return;
            }

            handleUpload();
        }
    }

    /**
     * Refreshes the enabled state of the layer, in case the OSMLayer was created or removed.
     *
     */
    @Override
    protected void updateEnabledState() {
        setEnabled(getEditLayer() != null);
    }

    /**
     * This method uploads the associated OsmDataLayer .osm file to the mapserver. The name of the .osm file
     * must match an already existing folder / project on the server.
     *
     * @param editLayer OsmDataLayer to get the associated .osm file
     * @throws IOException if associated file cannot be accessed
     */
    private void uploadData(final OsmDataLayer editLayer) throws IOException {
        final File layerFile = editLayer.getAssociatedFile();
        final String mapName = FilenameUtils.removeExtension(layerFile.getName());
        final String fileName = layerFile.getName();
        final MapDataResponse response = client.uploadMapData(mapName, fileName, layerFile);
        jOptionPane.showMessageDialog(josm.getParent(),
                response.getStatusMessage(), "Invio Server Response Message", jOptionPane.getInformationMessage());
    }

    private void handleUpload() {
        try {
            final OsmDataLayer editLayer = josm.getMapView().getEditLayer();
            uploadData(editLayer);
        } catch (IOException e) {
            jOptionPane.showMessageDialog(josm.getParent(), "Beim Hochladen ist der folgende Fehler aufgetreten: " +
                    "\n" + e.getMessage(), "Fehler", jOptionPane.getErrorMessage());
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }

    /**
     * This method checks the entire session for different preconditions before upload. It checks that the
     * Invio TMSLayer is present, that the .osm file was saved and the data has no conflicts or errors.
     *
     * @return true if all preconditions are valid
     */
    private boolean checkAndAlertUploadPreconditions() {
        final OsmDataLayer editLayer = josm.getMapView().getEditLayer();
        final APIDataSet apiData = new APIDataSet(josm.getMain().getCurrentDataSet());

        if (apiData.isEmpty()) {
            jOptionPane.showMessageDialog(josm.getParent(), tr("No changes to upload."), tr("Warning"),
                    jOptionPane.getInformationMessage());
            return false;
        }

        if (!checkAndAlertOSMLayerUploadPreconditions(editLayer, apiData)) {
            return false;
        }

        return true;
    }

    /**
     * Check the OsmDataLayer for preconditions. It checks for unsaved changes and for conflicts or errors, such as
     * untagged data etc. In case of false preconditions it will alert the user.
     *
     * @param editLayer OsmDataLayer to check
     * @param apiData current data set
     * @return true if the osm data is valid
     */
    private boolean checkAndAlertOSMLayerUploadPreconditions(final OsmDataLayer editLayer, final APIDataSet apiData) { //NOSONAR
        // Method length is fine... but maybe we can put the dialogs somewhere nicer and easier to change? It would
        // shorten the method enough to remove the //NOSONAR and this comment.

        final File layerFile = editLayer.getAssociatedFile();

        if(layerFile == null || editLayer.requiresSaveToFile()) {
            jOptionPane.showMessageDialog(josm.getParent(),
                    "Bitte speichern Sie erst Ihr Projekt, bevor Sie es hochladen.",
                    "Fehler", jOptionPane.getErrorMessage());
            return false;
        }

        final ConflictCollection conflicts = editLayer.getConflicts();
        if (apiData.participatesInConflict(conflicts)) {
            alertUnresolvedConflicts(editLayer);
            return false;
        }

        if(!josmValidateUploadHook.checkUpload(apiData)) {
            return false;
        }

        return true;
    }

    /**
     * Alert the user about the unresolved conflicts
     * TODO: what the purpose of this method????
     * @param layer participating in conflicts
     */
    private void alertUnresolvedConflicts(final OsmDataLayer layer) {
        HelpAwareOptionPane.showOptionDialog(
                josm.getParent(),
                tr("<html>The data to be uploaded participates in unresolved conflicts of layer ''{0}''.<br>"
                        + "You have to resolve them first.</html>", layer.getName()
                ),
                tr("Warning"),
                jOptionPane.getWarningMessage(),
                HelpUtil.ht("/Action/Upload#PrimitivesParticipateInConflicts")
        );
    }

}
