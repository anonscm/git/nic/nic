1. Copy the following files to your users JOSM directory "~/.josm":
- hallway.png
- invio.png
- inviopresets.xml
- inviostyles.xml

2. Reference the two config xml files in your JOSM main config file by adding the following nodes replacing {USERNAME}
   with your unix username:

a)
<maps key='taggingpreset.entries'>
    <map>
        <tag key='title' value='Invio Preset'/>
        <tag key='url' value='file:///home/{USERNAME}/.josm/inviopresets.xml'/>
    </map>
</maps>

b)
<maps key='mappaint.style.entries'>
    [...]
    <map>
        <tag key='title' value='Invio Indoor Style'/>
        <tag key='active' value='true'/>
        <tag key='ptoken' value='standard'/>
        <tag key='url' value='file:///home/{USERNAME}/.josm/inviostyles.xml'/>
    </map>
</maps>
<list key='mappaint.style.known-defaults'>
    [...]
    <entry value='file:///home/{USERNAME}/.josm/inviostyles.xml'/>
</list>

View 'preferences.xml' in this folder for example positioning of the required elements!

3. Restart JOSM

4. Once a layer is added in JOSM you now should be able to mark the Invio objects.